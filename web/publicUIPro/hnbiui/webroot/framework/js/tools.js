/*
 * Copyright (C) 2015 raisecom, Inc. and others. All rights reserved. (raisecom)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 初始化脚本文件装载工具
 *  modify：
 */ 
$Boot = {};

/**
 * 创建命名空间
 * @param {Object} name
 * @param {Object} object
 */
$Boot.createNamespace = function(name, object) {
    var splits = name.split(".");
    var parent = window;
    //document.window浏览器内置对象
    var part = splits[0];
    for (var i = 0, len = splits.length - 1; i < len; i++, part = splits[i]) {
        if (!parent[part]) {
            parent = parent[part] = {};
        } else {
            parent = parent[part];
        }
    }
    // 存放对象
    parent[part] = object;
    // 返回 last part name (例如：classname)
    return part;
}

$Boot.isDefined = function(o) {
    return typeof (o) != "undefined"
}
/**
 * 启动配置类
 */
$Boot.Config = function() {

    function isDefined(o) {
        return typeof (o) != "undefined"
    }

    //用户应用当前目录
    if (!isDefined(window.$userAppDir)) {
        window.$userAppDir = './'
    }
    //组件库目录
    if (!isDefined(window.$userFrameDir)) {
        window.$userFrameDir = '/common/'
    }
    //用户i18文件目录
    if (!isDefined(window.$userI18nDir)) {
        window.$userI18nDir = './'
    }

    //当前语言 默认为英语
    var language = "zh-CN";
    //var languageList = ['ar', 'ba', 'cr', 'cs', 'de', 'el', 'es', 'fi', 'fr', 'fr-FR', 'hu-HU', 'id', 'it', 'ja', 'nb-NO', 'nl', 'pl', 'pl-PL', 'pt', 'pt-BR', 'ro-RO', 'ru-RU', 'sk', 'sr', 'sr-Latn', 'sv-SE', 'en-US','uk-UA', 'zh-CN', 'zh-TW'];
    var languageList = ['en-US', 'zh-CN'];
	
	//从服务端取客户端接受语言类型
    var getAcceptLangFromServer = true;
	
	
	    /**
     * 创建XMLHttpRequest对象
     */
    function createXMLHttpRequest() {
        if (window.ActiveXObject) {
            return new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            return new XMLHttpRequest();
        } else {
            throw new Error("This Brower do not support XMLHTTP!!");
        }
    }

	
	    /**
     * 同步发送xml http  请求
     * @param {Object} url
     * @param {Object} data
     * @param {Object} method
     */
    function httpRequest(method, url, data) {
        var xmlhttp;
        xmlhttp = createXMLHttpRequest();
        var sendData = null;
        if (method == "get") {
            url = url + "?" + data;

        } else if (method == "post") {
            sendData = data;
        }
        xmlhttp.open(method, url, false);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xmlhttp.setRequestHeader("If-Modified-Since", "0");
        xmlhttp.send(sendData);
        if (xmlhttp.status == 200)
            return xmlhttp.responseText;
    }
	
	    /**
     * 同步发送xml http  请求（给外部调用）
     * @param {Object} url
     * @param {Object} data
     * @param {Object} method
     */
    this.httpRequestStatic = function(method, url, data) {
        var xmlhttp;
        xmlhttp = createXMLHttpRequest();
        var sendData = null;
        if (method == "get") {
            url = url + "?" + data;

        } else if (method == "post") {
            sendData = data;
        }
        xmlhttp.open(method, url, false);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xmlhttp.setRequestHeader("If-Modified-Since", "0");
        xmlhttp.send(sendData);
        if (xmlhttp.status == 200)
            return xmlhttp.responseText;
    }
	
	function inArray(array, obj) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == obj) {
                return true;
            }
        }
        return false;

    }
	
    /**
     * 取得浏览器语言信息
     */
    this.getLanguage = function() {        
		var rtnLanguage = localStorage.getItem("language-option");		
		if( rtnLanguage == "null" || rtnLanguage == null ){
			rtnLanguage = window.navigator.userLanguage||window.navigator.language;
		}
		if( rtnLanguage == '"zh-CN"' || rtnLanguage == "zh-CN" ){
			return "zh-CN";
		}else{
			return "en-US";
		}
		//return "en-US";
    }

	this.getUrlParam=function(name){
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
            var search =decodeURIComponent(location.search.substring(1)); //decodeURIComponent() 函数可对 encodeURIComponent() 函数编码的 URI 进行解码。
            var r =search.match(reg);  //匹配目标参数
            if (r != null) return unescape(r[2]); //unescape() 函数可对通过 escape() 编码的字符串进行解码。
            return null; //返回参数值
    }


}

//创建命名空间
$Boot.createNamespace("com.raisecom.ums.aos.framework.BootConfig", $Boot.Config);
//创建基础配置对象实例
$Boot.bootConfig = new com.raisecom.ums.aos.framework.BootConfig();

function getLanguage(){
   return $Boot.bootConfig.getLanguage();
}

function getStringWidth(text,fontSize)
{
    var span = document.getElementById("_ictframework_getwidth");
    if (span == null) {
        span = document.createElement("span");
        span.id = "_ictframework_getwidth";
        document.body.appendChild(span);
    }
    span.innerText = text;
    span.style.whiteSpace = "nowrap";
    $("#_ictframework_getwidth").attr('style','font-size:'+fontSize+'px;');
	var width = span.offsetWidth;
	$("#_ictframework_getwidth").attr('style','display:none');
    return width;
}

function getUrlParam(name){
    return $Boot.bootConfig.getUrlParam(name);
}

function httpRequest(method, url, data) {
    return $Boot.bootConfig.httpRequestStatic(method, url, data)
}

// 定义JQUERY AJAX 完成函数，判断返回状态，如果状态正常，但HEADER头里有session超时信息，则刷新重登录
// 如果状态为 401, 也刷新重登录
// 注意如果在$.ajax() 函数中定义了 complete，则覆盖了这里预定义complete内容，以$.ajax()函数中定义的为准，这里预定义的函数则失效，如果
// 要继续判断session超时，则需要在 $.ajax()函数中定义的complete函数中加入这里预定义内容。
if (jQuery) {
	$.ajaxSetup({
		complete:function(XMLHttpRequest,textStatus){
			 if (XMLHttpRequest.status == 401) {
				window.location.replace("login.html"); 
			 }
			 // if (XMLHttpRequest.status == 200) {
				// var sessionstatus=XMLHttpRequest.getResponseHeader("sessionstatus"); ////通过XMLHttpRequest取得响应头，sessionstatus，  
				 // if(sessionstatus=="timeout"){
					 // window.location.replace("/");
				 // }
			 // } else if (XMLHttpRequest.status == 401) {
				// window.location.replace("/");
			 // } 
		}
	});
}
