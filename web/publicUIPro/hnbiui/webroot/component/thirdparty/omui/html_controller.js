/**
 * <发送ajax获取表单属性级别并控制表单控件显示隐藏>
 * <功能详细描述>
 * @param url       ajax请求字段级别的controller路径
 * @param inputArr  表单控件集合
 * @param loginName 当前登录人ID
 * @returns
 * @see [类、类#方法、类#成员]
 */
var controller_form_input = function(url, inputArr, loginName)
{
	// 获取页面各个字段的级别
	$.ajax(
	{
		type : 'GET',
		url : url,
		dataType : 'json',
		success : function(msg)
		{
			// 表单控件控制函数
			control_form_input(msg[0], inputArr, parent.loginName);
		}
	});
}

/**
 * <根据参数控制界面表单控件显示隐藏>
 * <该函数内部使用，控制表单请调用controller_form_input函数>
 * @param json      控件表单级别的JSON串
 * @param arr       表单控件集合
 * @param loginName 当前登录人ID
 * @returns
 * @see [类、类#方法、类#成员]
 */
var control_form_input = function(json, arr, loginName)
{
	// 循环集合，根据字段级别控制界面显示
	for ( var i = 0; i < arr.length; i++)
	{
		// 表单属性名称、类型及显示级别
		// Type 0-输入框 1-下拉框 2-单选框 3-textarea
		// alias 输入框别名，适用于一个界面有多个form，级别KEY一样但是input name不一样的输入框
		var inputName = arr[i].name;
		var inputType = arr[i].type;
		var inputAlias = arr[i].alias;
		var inputLevel = 'A';
		if(null != eval("json." + inputName) && undefined != eval("json." + inputName))
		{
			inputLevel = eval("json." + inputName).toUpperCase();
		}
		
		// B级别都是只读属性
		if("B" == inputLevel)
		{
			if(1 == inputType)
			{
				if(!isEmpty(inputAlias) && !isEmpty($('input[name=' + inputAlias + ']')))
				{
					$('input[name=' + inputAlias + ']').omCombo('disable');
				}
				else if(!isEmpty($('input[name=' + inputName + ']')))
				{
					$('input[name=' + inputName + ']').omCombo('disable');
				}
			}
			else if(3 == inputType)
			{
				if(!isEmpty(inputAlias) && !isEmpty($('textarea[name=' + inputAlias + ']')))
				{
					$('textarea[name=' + inputAlias + ']').attr({readonly : 'readonly'});
				}
				else if(!isEmpty($('textarea[name=' + inputName + ']')))
				{
					$('textarea[name=' + inputName + ']').attr({readonly : 'readonly'});
				}
			}
			else
			{
				if(!isEmpty(inputAlias) && !isEmpty($('input[name=' + inputAlias + ']')))
				{
					$('input[name=' + inputAlias + ']').attr({readonly : 'readonly'});
				}
				else if(!isEmpty($('input[name=' + inputName + ']')))
				{
					$('input[name=' + inputName + ']').attr({readonly : 'readonly'});
				}
			}
		}
		else if("C" == inputLevel && "admin" == loginName)
		{
			if(1 == inputType)
			{
				if(!isEmpty(inputAlias) && !isEmpty($('input[name=' + inputAlias + ']')))
				{
					$('input[name=' + inputAlias + ']').parent().parent().parent().hide();
				}
				else if(!isEmpty($('input[name=' + inputName + ']')))
				{
					$('input[name=' + inputName + ']').parent().parent().parent().hide();
				}
			}
			else if(3 == inputType)
			{
				if(!isEmpty(inputAlias) && !isEmpty($('textarea[name=' + inputAlias + ']')))
				{
					$('textarea[name=' + inputAlias + ']').parent().parent().hide();
				}
				else if(!isEmpty($('textarea[name=' + inputName + ']')))
				{
					$('textarea[name=' + inputName + ']').parent().parent().hide();
				}
			}
			else
			{
				if(!isEmpty(inputAlias) && !isEmpty($('input[name=' + inputAlias + ']')))
				{
					$('input[name=' + inputAlias + ']').parent().parent().hide();
				}
				else if(!isEmpty($('input[name=' + inputName + ']')))
				{
					$('input[name=' + inputName + ']').parent().parent().hide();
				}
			}
		}
		else if("D" == inputLevel && "admin" == loginName)
		{
			if(1 == inputType)
			{
				if(!isEmpty(inputAlias) && !isEmpty($('input[name=' + inputAlias + ']')))
				{
					$('input[name=' + inputAlias + ']').omCombo('disable');
				}
				else if(!isEmpty($('input[name=' + inputName + ']')))
				{
					$('input[name=' + inputName + ']').omCombo('disable');
				}
			}
			else if(3 == inputType)
			{
				if(!isEmpty(inputAlias) && !isEmpty($('textarea[name=' + inputAlias + ']')))
				{
					$('textarea[name=' + inputAlias + ']').attr({readonly : 'readonly'});
				}
				else if(!isEmpty($('textarea[name=' + inputName + ']')))
				{
					$('textarea[name=' + inputName + ']').attr({readonly : 'readonly'});
				}
			}
			else
			{
				if(!isEmpty(inputAlias) && !isEmpty($('input[name=' + inputAlias + ']')))
				{
					$('input[name=' + inputAlias + ']').attr({readonly : 'readonly'});
				}
				else if(!isEmpty($('input[name=' + inputName + ']')))
				{
					$('input[name=' + inputName + ']').attr({readonly : 'readonly'});
				}
			}
		}
	}
}

/**
 * <控制表格字段的显示>
 * <功能详细描述>
 * @param url        ajax请求字段级别的controller路径
 * @param gril_id    表格ID
 * @param colModel   表格列模型，必须是完整的列模型
 * @param loginName  当前登录人
 * @returns
 * @see [类、类#方法、类#成员]
 */
var controller_table_filed = function(url, gril_id, colModel, loginName)
{
	// 获取页面各个字段的级别
	$.ajax(
	{
		type : 'POST',
		url : url,
		dataType : 'json',
		success : function(msg)
		{
			// 表单控件控制函数
			control_table_field(msg[0], gril_id, colModel, parent.loginName);
		}
	});
}

/**
 * <控制表格字段的显示>
 * <该函数内部使用，控制表格请调用controller_table_filed函数>
 * @param json       表格字段级别的JSON串
 * @param gril_id    表格ID
 * @param colModel   表格列模型，必须是完整的列模型
 * @param loginName  当前登录人
 * @returns
 * @see [类、类#方法、类#成员]
 */
var control_table_field = function(json, gril_id, colModel, loginName)
{
	// 用来存储过滤后的显示字段
	var newColModel = [];
	
	// 循环集合，根据字段级别控制界面显示
	for ( var i = 0; i < colModel.length; i++)
	{
		// 表单属性名称、类型及显示级别
		var inputName = colModel[i].name;
		
		// 表格字段级别
		var inputLevel = 'A';
		if(null != eval("json." + inputName) && undefined != eval("json." + inputName))
		{
			inputLevel = eval("json." + inputName).toUpperCase();
		}
		
		// 如果字段级别不是C或者登录人不是admin则显示
		if("C" != inputLevel || "admin" != loginName)
		{
			newColModel.push(colModel[i]);
		}
	}
	
	// 更新表格显示的字段
	$("#" + gril_id).omGrid({colModel : newColModel});
}