$(document).ready(function()
{
	// 设置界面显示i18n
	$('.login_head_text_div').text(i18n.prop('firstLoginHeadText'));
	$('#spanUserName').text(i18n.prop('spanUserName') + i18n.prop('colon'));
	$('#spanNewPassword').html(i18n.prop('newPassword') + i18n.prop('colon'));
	$('#spanRePassword').text(i18n.prop('rePassword') + i18n.prop('colon'));
	$('#confirm').html('<b>' + i18n.prop('confirm') + '</b>');
	$('#close').html('<b>' + i18n.prop('close') + '</b>');
	
	// 显示用户名
	$('#tdUsername').text(userName);
	
	// 验证规则(数字和字母)
	var rules =
	{   
		newPassword : 
		{
			required : true,
			lengthrange : [6, 12],
			checkPwdComplexity : true
		},
		rePassword : 
		{
            required : true,
            equalTo : '#newPassword'
        }
	};
	
	
	// 验证后的提示消息
	var messages =
	{
		newPassword : 
		{
			required : i18n.prop('newPwdRequired'),
			lengthrange : i18n.prop('newPwdRange')
		},
		rePassword : 
		{
           required : i18n.prop('affirmPwdRequired'),
            equalTo : i18n.prop('affirmPwdEqualTo')
        }
	};
	
	// 调用验证方法，创建验证对象
	var validator = new form_validator(rules, messages, 'errorMsg', 'firstLoginForm');
	
	// 根据密码复杂度显示lable提示内容
	var getPwdFormat = function()
	{
		if (pwdComplexity == "1")
		{
			$('#lbPwdFormat').text(i18n.prop('lbPwdFormatOne'));
		}
		if (pwdComplexity == "2")
		{
			$('#lbPwdFormat').text(i18n.prop('lbPwdFormatTwo'));
		}
		if (pwdComplexity == "3")
		{
			$('#lbPwdFormat').text(i18n.prop('lbPwdFormatThree'));
		}
		if (pwdComplexity == "4")
		{
			$('#lbPwdFormat').text(i18n.prop('lbPwdFormatFour'));
		}
	}
	
	// 查询密码策略
	var getPwdConfig = function()
	{
		$.ajax(
		{
			type : 'POST',
			url : path + '/safeConfig/getSafeConfig',
			dataType : 'json',
			success : function(msg)
			{
				if (msg.length > 0)
				{
					// 查询到的JSON对象
					var json = msg[0];
					pwdComplexity = json.pwdComplexity;
					pwdLength = json.pwdLength;
					
					getPwdFormat();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 提示数据加载失败
				alertError();
			}
		});
	}
	
	// 获取密码策略信息（复杂度和长度） 
	getPwdConfig();
	
	// 设置密码按钮
	$("#confirm").click(function()
	{
		submitDialog();
		this.blur();
		return false;
	});
	
	// 设置密码按钮
	$("#close").click(function()
	{
	    $('input[name=newPassword]').val('');
	    $('input[name=rePassword]').val('');
	    $('input[name=newPassword]').focus();
	    
	    // 清除红框样式
		$("input").removeClass("x-form-invalid");
	});
	
	// dialog中点提交按钮时将数据提交到后台并执行相应的add或modify操作
	// 0--添加，1--修改
	var submitDialog = function()
	{
		console.log(validator.form());
		if (validator.form())
		{
			$.omMessageBox.confirm(
			{
				title : i18n.prop('alert'),
				content : i18n.prop('modifyContent'),
				onClose : function(value)
				{
					if(value)
					{
						var submitData =
						{
							newPwd : $("#newPassword").val(),
							userName : userName
						};
						
						// 提交数据到后台
						$.post(path + "/index/modifyFirstPwd", submitData, function(msg)
						{
							if(msg == '0')
							{
								 $.omMessageBox.alert(
								 {
									 content:i18n.prop('pwdSuccess'),
									 onClose:function(value)
									 {
					                    // 跳转到登录页面	
										currpage = self.location.href;
										currpage = currpage.replace("/login","");
										window.location.href = currpage;
					                }
								 });
							}
							else
							{
								$.omMessageTip.show(
								{
									type : 'error',
									title : i18n.prop('alert'),
									content : i18n.prop('pwdFail'),
									timeout : 1500
								});
							}
							
							// 清除红框样式
							$("input").removeClass("x-form-invalid");
						}, "text");
					}
				}
			});
		}
	};
	
});
