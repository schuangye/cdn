@echo off
title zenap-portal+ Database setup

set RUNHOME=%~dp0
echo ### RUNHOME: %RUNHOME%

title UMC Database setup

echo ### Starting UMC Database setup
rem set JAVA_HOME=D:\JDK1.7\jdk\jdk\windows
set JAVA="%JAVA_HOME%/bin/java"

set class_path=%RUNHOME%;%RUNHOME%hnb-service.jar
echo ### class_path: %class_path%

echo.
echo.
%JAVA% -classpath %class_path% %jvm_opts% com.raisecom.itms.hnb.HnbApp db migrate %RUNHOME%conf/hnb.yml

pause