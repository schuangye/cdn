#user xfs xfs;
worker_processes 1;

error_log logs/error.log warn;
pid logs/nginx.pid;

events {
    worker_connections 1024;
}

http {
	include mime.types;
	
    upstream iui {
        server 127.0.0.1:8202;
    }

    upstream usmc {
        server 127.0.0.1:8204;
    }
	
	upstream bmc {
        server 127.0.0.1:8206;
    }
	
	
    # Basic Settings
    default_type  application/octet-stream;   
    sendfile       on;
    tcp_nopush     on;
    server_names_hash_bucket_size 128;  
    keepalive_timeout  120s;
	
	#the maximum allowed size of the client request body,current 10G
	client_max_body_size 10240m; 
    client_body_buffer_size 128k;
	
	#set the time wait for connect to proxy_pass target,avoid waiting too long
	proxy_connect_timeout 10s;
	proxy_read_timeout 120s;
	proxy_send_timeout 120s;
	proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $host;
    proxy_buffers 4 32k;
	
    #set the nginx_cache parameter
    #proxy_cache_path  temp/proxy_cache  levels=1:2   keys_zone=nginx_cache:10m inactive=1d max_size=100m;
	
    # Logging
    access_log off;

    # Lua settings
    lua_package_path "$prefix/../lualib/?.lua;waf/?.lua;;";
	lua_package_cpath "$prefix/../lualib/?.so;;";

    lua_shared_dict ceryx 10M;
    lua_code_cache on;
	lua_shared_dict limit 10m;
    init_by_lua_file  waf/init.lua; 
    access_by_lua_file waf/waf.lua;

	
    server {
        listen       18088;
        default_type text/html;
		
		location = / {
			rewrite ^ /iui/framework redirect;
		}

		location = /itms {
			rewrite ^ /iui/framework redirect;
		}

		location = /iui/itms {
			rewrite ^ /iui/framework redirect;
		}
		
		location = /iui/framework/login.html {
			access_by_lua_file luaext/iui_auth.lua;
			proxy_pass http://iui;
		}

		location = /api/uiframe/v1/login {
			rewrite ^ /api/usmc/v1/login break;
			proxy_pass http://usmc;
			header_filter_by_lua_file luaext/login.lua;
		}
		
		location = /api/usmcsf/v1/loginout {
		    rewrite ^ /api/usmc/v1/loginout break;
			access_by_lua_file luaext/logout.lua;
			proxy_pass http://usmc;	
		}		
		
		location = /api/hsif/v1/authentication {
			access_by_lua_file luaext/make_authed.lua;
		}		

		location /iui {
			access_by_lua_file luaext/iui_auth.lua;
			proxy_pass http://iui;
		}
		
	
		location /api/uiframe/v1 {
			rewrite ^/api/uiframe/v1/(.*) /api/usmc/v1/$1 break;
			access_by_lua_file luaext/iui_auth.lua;
			proxy_pass http://usmc;
		}
        
		location ~ ^/api/usmc(?:pm|fm|drill|sm|monitor)/v1 {
			access_by_lua_file luaext/auth.lua;
			proxy_pass http://usmc;
		}

		
		location /api/umcswagger/v1 {
			rewrite ^/api/umcswagger/v1/(.*) /api/$1 break;
			access_by_lua_file luaext/auth.lua;
			proxy_pass http://usmc;
		}
		
		location = /api/usmc/v1/forcespwd {
			proxy_pass http://usmc;
			#header_filter_by_lua_file luaext/login.lua;
		}
		
		location = /api/usmc/v1/generatecode {
			proxy_pass http://usmc;
			#header_filter_by_lua_file luaext/login.lua;
		}
				
		location /api/usmc/v1 {
			access_by_lua_file luaext/auth.lua;
			proxy_pass http://usmc;
		}
		
		
		
		location /api/v1/resource {
			access_by_lua_file luaext/auth.lua;
			proxy_pass http://bmc;
		}
    }	
}
