
--iui_auth.lua
local cache = ngx.shared.ceryx
local uri = ngx.var.uri
if uri == "/iui/framework/login.html" and ngx.var.cookie_sessioncode ~= nil then
	local resp = ngx.location.capture("/api/uiframe/v1/userheart?sessioncode="..ngx.var.cookie_sessioncode, {
		method = ngx.HTTP_GET
	})	
	if 200 == resp.status and "false" == resp.body then
		cache:delete(ngx.var.cookie_sessioncode)
		ngx.header["Set-Cookie"] = {"sessioncode="..ngx.var.cookie_sessioncode..";path=/; expires=31 Dec 2011 23:55:55 GMT",
		"username="..ngx.var.cookie_username..";path=/; expires=31 Dec 2011 23:55:55 GMT"}		
	end	
	local value, flags = cache:get(ngx.var.cookie_sessioncode)
	if not value then
		return
	else
		ngx.redirect("/iui/framework/main-page.html")
	end
end

local referer =  ngx.var.http_referer
local refererList = {
	"/iui/framework/login.html",
	"/iui/framework/css/login.css",
	"/iui/component/thirdparty/font%-awesome/css/font%-awesome.min.css",
	"/iui/framework/css/style%-custom.css",
	"/iui/framework/modifyPassword.html",
	"/iui/component/css/omui/default/om-default.css"
}	
local function referer_matches(t, r)
	for k,_ in pairs(t) do
		if string.match(r, t[k]) then
			return true
		end
	end
	return false
end	

if referer and referer_matches(refererList, referer) then
	return
end	

if ngx.var.cookie_sessioncode == nil and uri ~= "/iui/framework/login.html" then
	ngx.redirect("/iui/framework/login.html")
end


if ngx.var.cookie_sessioncode ~= nil then
	local succ, err, forcible = cache:replace(ngx.var.cookie_sessioncode, "place_holder", 0)

	if not succ then		
		if err == 'not found' then			
			ngx.redirect("/iui/framework/login.html")			
		end
	end
end