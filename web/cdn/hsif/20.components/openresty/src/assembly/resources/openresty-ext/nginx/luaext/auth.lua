
local cache = ngx.shared.ceryx
local resp = ngx.location.capture("/api/uiframe/v1/userheart?sessioncode="..ngx.var.cookie_sessioncode, {
    method = ngx.HTTP_GET
})
ngx.log(ngx.WARN, "body:"..resp.body..",status:"..resp.status)

if 200 == resp.status and "false" == resp.body then
    cache:delete(ngx.var.cookie_sessioncode)
	ngx.header["Set-Cookie"] = {"sessioncode="..ngx.var.cookie_sessioncode..";path=/; expires=31 Dec 2011 23:55:55 GMT",
	                            "username="..ngx.var.cookie_username..";path=/; expires=31 Dec 2011 23:55:55 GMT"}
    ngx.log(ngx.WARN, "==user logout!")
    return
end
	
local succ, err, forcible = cache:replace(ngx.var.cookie_sessioncode, "place_holder", 0)
if not succ then
	if err == 'not found' then
		ngx.log(ngx.WARN, "access record not found for "..ngx.var.cookie_sessioncode)
		ngx.exit(401)
	else
		ngx.log(ngx.WARN, err)
	end	
end