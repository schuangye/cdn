#!/bin/bash
DIRNAME=`dirname $0`
HOME=`cd $DIRNAME/nginx; pwd`
_NGINXCMD="$HOME/sbin/nginx"

echo =========== check whether libluajit  is resolved ========================================
LUAJIT_HOME=`cd $DIRNAME/luajit; pwd`
LUAJIT_FILENAME="$LUAJIT_HOME/lib/libluajit-5.1.so.2"
LN_TARGET_FILE='/lib/libluajit-5.1.so.2'
LN_TARGET_FILE64='/lib64/libluajit-5.1.so.2'
if [ -f $LN_TARGET_FILE ]; then 
      echo "libluajit file resolved in lib!" 
else 
       echo "libluajit file not resolved in lib! Now make links between files!" 
      echo @LN_CMD@ ln -s $LUAJIT_FILENAME $LN_TARGET_FILE
      ln -s $LUAJIT_FILENAME $LN_TARGET_FILE
fi
if [ -f $LN_TARGET_FILE64 ]; then 
      echo "libluajit file resolved in lib64!" 
else 
      echo "libluajit file not resolved in lib64! Now make links between files!" 
	  echo @LN_CMD@ ln -s $LUAJIT_FILENAME $LN_TARGET_FILE64
      ln -s $LUAJIT_FILENAME $LN_TARGET_FILE64
fi
echo ===============================================================================

echo =========== openresty config info  =============================================
echo HOME=$HOME
echo _NGINXCMD=$_NGINXCMD
echo ===============================================================================
cd $HOME; pwd

echo @WORK_DIR@ $HOME
echo @C_CMD@ $_NGINXCMD -p $HOME/
$_NGINXCMD -p $HOME/

