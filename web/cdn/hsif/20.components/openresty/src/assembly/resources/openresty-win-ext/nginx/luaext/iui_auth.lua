local content_length=tonumber(ngx.req.get_headers()['content-length'])
local method=ngx.req.get_method()
local ngxmatch=ngx.re.match
if whiteip() then
elseif blockip() then
elseif denycc() then
elseif ngx.var.http_Acunetix_Aspect then
    ngx.exit(444)
elseif ngx.var.http_X_Scan_Memo then
    ngx.exit(444)
elseif whiteurl() then
elseif ua() then
elseif url() then
elseif args() then
elseif cookie() then
elseif PostCheck then
    if method=="POST" then   
            local boundary = get_boundary()
	    if boundary then
	    local len = string.len
            local sock, err = ngx.req.socket()
    	    if not sock then
					return
            end
	    ngx.req.init_body(128 * 1024)
            sock:settimeout(0)
	    local content_length = nil
    	    content_length=tonumber(ngx.req.get_headers()['content-length'])
    	    local chunk_size = 4096
            if content_length < chunk_size then
					chunk_size = content_length
	    end
            local size = 0
	    while size < content_length do
		local data, err, partial = sock:receive(chunk_size)
		data = data or partial
		if not data then
			return
		end
		ngx.req.append_body(data)
        	if body(data) then
	   	        return true
    	    	end
		size = size + len(data)
		local m = ngxmatch(data,[[Content-Disposition: form-data;(.+)filename="(.+)\\.(.*)"]],'ijo')
        	if m then
            		fileExtCheck(m[3])
            		filetranslate = true
        	else
            		if ngxmatch(data,"Content-Disposition:",'isjo') then
                		filetranslate = false
            		end
            		if filetranslate==false then
            			if body(data) then
                    			return true
                		end
            		end
        	end
		local less = content_length - size
		if less < chunk_size then
			chunk_size = less
		end
	 end
	 ngx.req.finish_body()
    else
			ngx.req.read_body()
			local args = ngx.req.get_post_args()
			if not args then
				return
			end
			for key, val in pairs(args) do
				if type(val) == "table" then
					if type(val[1]) == "boolean" then
						return
					end
					data=table.concat(val, ", ")
				else
					data=val
				end
				if data and type(data) ~= "boolean" and body(data) then
                			body(key)
				end
			end
		end
    end
else
    return
end

--iui_auth.lua
local cache = ngx.shared.ceryx
local uri = ngx.var.uri
if uri == "/iui/framework/login.html" and ngx.var.cookie_sessioncode ~= nil then
	local resp = ngx.location.capture("/api/uiframe/v1/userheart?sessioncode="..ngx.var.cookie_sessioncode, {
		method = ngx.HTTP_GET
	})	
	if 200 == resp.status and "false" == resp.body then
		cache:delete(ngx.var.cookie_sessioncode)
		ngx.header["Set-Cookie"] = {"sessioncode="..ngx.var.cookie_sessioncode..";path=/; expires=31 Dec 2011 23:55:55 GMT",
		"username="..ngx.var.cookie_username..";path=/; expires=31 Dec 2011 23:55:55 GMT"}		
	end	
	local value, flags = cache:get(ngx.var.cookie_sessioncode)
	if not value then
		return
	else
		ngx.redirect("/iui/framework/main-page.html")
	end
end

local referer =  ngx.var.http_referer
local refererList = {
	"/iui/framework/login.html",
	"/iui/framework/css/login.css",
	"/iui/component/thirdparty/font%-awesome/css/font%-awesome.min.css",
	"/iui/framework/css/style%-custom.css",
	"/iui/framework/modifyPassword.html",
	"/iui/component/css/omui/default/om-default.css"
}	
local function referer_matches(t, r)
	for k,_ in pairs(t) do
		if string.match(r, t[k]) then
			return true
		end
	end
	return false
end	

if referer and referer_matches(refererList, referer) then
	return
end	

if ngx.var.cookie_sessioncode == nil and uri ~= "/iui/framework/login.html" then
	ngx.redirect("/iui/framework/login.html")
end


if ngx.var.cookie_sessioncode ~= nil then
	local succ, err, forcible = cache:replace(ngx.var.cookie_sessioncode, "place_holder", 0)

	if not succ then		
		if err == 'not found' then			
			ngx.redirect("/iui/framework/login.html")			
		end
	end
end