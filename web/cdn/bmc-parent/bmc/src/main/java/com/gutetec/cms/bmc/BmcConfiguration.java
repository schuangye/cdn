package com.gutetec.cms.bmc;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

/***
 * 业务管理模块微服务配置类
 * 
 * @author macui
 *
 */
public class BmcConfiguration extends Configuration {
	@NotEmpty
	private String template;

	@NotEmpty
	private String defaultName = "BMC";

	@NotNull
	private DataSourceFactory database = new DataSourceFactory();

	@JsonProperty
	public String getTemplate() {
		return template;
	}

	@JsonProperty
	public void setTemplate(String template) {
		this.template = template;
	}

	@JsonProperty
	public String getDefaultName() {
		return defaultName;
	}

	@JsonProperty
	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	@JsonProperty("database")
	public DataSourceFactory getDataSourceFactory() {
		return database;
	}

	@JsonProperty
	public DataSourceFactory getDatabase() {
		return database;
	}

	@JsonProperty
	public void setDatabase(DataSourceFactory database) {
		this.database = database;
	}
}
