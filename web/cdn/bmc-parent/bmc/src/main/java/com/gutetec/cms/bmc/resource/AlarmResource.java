package com.gutetec.cms.bmc.resource;

import java.util.HashMap;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.wrapper.AlarmWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 告警接口类
 * 
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " alarm Management " })
public class AlarmResource {

	@GET
	@Path("/activealarm")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query alarm by conditions")
	@Timed
	public Response queryAlarms(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		return AlarmWrapper.findActiveAlarmByPage(paramMap);
	}
	
	@GET
	@Path("/historyalarm")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query alarm by conditions")
	@Timed
	public Response queryHistoryAlarms(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		return AlarmWrapper.findByPage(paramMap);
	}
	
	@DELETE
	@Path("/historyalarm/{alarmId}")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "delete history alarm by conditions")
	@Timed
	public Response deleteAlarm(
			@ApiParam(value = "alarmId id", required = true) @PathParam("alarmId") String alarmId)
	{
		return AlarmWrapper.deleteAlarm(alarmId);
	}
}
