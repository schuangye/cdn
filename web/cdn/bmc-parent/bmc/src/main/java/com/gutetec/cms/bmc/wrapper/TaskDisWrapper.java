package com.gutetec.cms.bmc.wrapper;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.TaskDistributeInfo;
import com.gutetec.cms.bmc.db.dao.TaskDisDao;
import com.gutetec.cms.bmc.db.dbUtil.FmDBUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TaskDisWrapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(TaskDisWrapper.class);
	
	/**
	 * find by page
	 * 
	 * @param paramMap
	 * @return
	 */
	public static Response findByPage(HashMap<String, Object> paramMap) {
		TaskDisDao dao = (TaskDisDao) FmDBUtil.getDaoInstance("TaskDistributeInfo");
		List<TaskDistributeInfo> result = null;
		int totalRows = 0;
		try {
			result = dao.findByPage(paramMap);
			totalRows = dao.getDataNum();
		} catch (HibernateException e) {
			LOGGER.error("error is" + e.getMessage());
			return Response.serverError().build();
		}

		JSONObject object = new JSONObject();
		if (0 == totalRows) {
			object.put("total", totalRows);
			object.put("rows", "[]");
			return Response.ok(object).build();
		} else {
			object.put("total", totalRows);
			object.put("rows", result);
			return Response.ok(object).build();
		}
	}
	
	public static Response addChannle(String  providerid, String assetid) {
		JSONObject result = new JSONObject();
		try {
			TaskDisDao dao = (TaskDisDao) FmDBUtil.getDaoInstance("TaskDistributeInfo");
			TaskDistributeInfo file = new TaskDistributeInfo();
			file.setProviderid(providerid);
			file.setAssetid(assetid);
			dao.save(file);
		} catch (Exception e) {
			LOGGER.error("create task fail.", e);
			result.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		result.put("result", "0");
		return Response.status(Response.Status.CREATED).entity(result).build();
	}
	
	/**
	 * 全量分发
	 * @param body
	 * @return
	 */
	public static Response distributeAllTask(String  body) {
		JSONObject result = new JSONObject();
		try {
			TaskDisDao dao = (TaskDisDao) FmDBUtil.getDaoInstance("TaskDistributeInfo");
			TaskDistributeInfo file = new TaskDistributeInfo();
			
			JSONArray array = JSONArray.fromObject(body);
			int size = array.size();
			for(int i = 0; i < size; i++)
			{
				String[]  tasks = array.get(i).toString().split("-");
				String providerid = tasks[1];
				String assetid = tasks[2];
				int servicetype = Integer.parseInt(tasks[3]);
				file.setProviderid(providerid);
				file.setAssetid(assetid);
				file.setServicetype(servicetype);
				dao.save(file);
			}
			
		} catch (Exception e) {
			LOGGER.error("create task fail.", e);
			result.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		result.put("result", "0");
		return Response.status(Response.Status.CREATED).entity(result).build();
	}
	
	/**
	 * 增量分发
	 * @param body
	 * @return
	 */
	public static Response distributeBatchTask(String  body) {
		JSONObject result = new JSONObject();
		try {
			TaskDisDao dao = (TaskDisDao) FmDBUtil.getDaoInstance("TaskDistributeInfo");
			TaskDistributeInfo file = new TaskDistributeInfo();
			JSONObject obj = JSONObject.fromObject(body);
			JSONArray array = obj.getJSONArray("selectIds");
			String subinaddress = obj.getString("subinaddress");
			int size = array.size();
			for(int i = 0; i < size; i++)
			{
				String[]  tasks = array.get(i).toString().split("-");
				String providerid = tasks[1];
				String assetid = tasks[2];
				int servicetype = Integer.parseInt(tasks[3]);
				file.setProviderid(providerid);
				file.setAssetid(assetid);
				file.setServicetype(servicetype);
				file.setSubnodeaisaddr(subinaddress);
				dao.save(file);
			}
			
		} catch (Exception e) {
			LOGGER.error("create task fail.", e);
			result.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		result.put("result", "0");
		return Response.status(Response.Status.CREATED).entity(result).build();
	}
}
