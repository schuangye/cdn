package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 注入监控
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_citask")
public class CitaskInfo {

	/**
	 * 任务ID
	 */
	@Id
	@Column(name = "taskid", nullable=true,unique =true)
	private long taskid;
	
	/**
	 * 任务类型
	 * 0:注入 1: 取消 2：删除
	 */
	@Column(name = "cmd")
	private int cmd;
	
	/**
	 * 提供商ID
	 */
	@Column(name = "providerid")
	private String providerid;
	
	/**
	 * 媒资id
	 */
	@Column(name="assetid")
	private String assetid;
	
	/**
	 *业务类型(可以支持的协议)：
	 * 0:未知 1：文件类型 2：HLS协议，需要生成对应m3u8文件 
	 * 3：NGOD协议，生成对应ngod索引 4：HLS+NGOD协议
	 */
	@Column(name = "servicetype")
	private int servicetype;
	
	/**
	 *'注入下载文件url
	 */
	@Column(name = "url")
	private String url;
	
	/**
	 *'分配的节点ID
	 */
	@Column(name = "nodeid")
	private String nodeid;
	
	/**
	 * 文件大小
	 */
	@Column(name="filesize")
	private long filesize;
	
	/**
	 * 文件时长
	 */
	@Column(name="duration")
	private long duration;
	
	/**
	 * 文件码率,非视频文件为0
	 * 
	 */
	@Column(name="bitrate")
	private int bitrate;
	
	/**
	 * 注入状态
	 * 0:Waiting 等待注入 1：Transfer 内容正在注入 2：Complete完成
	 */
	@Column(name="status")
	private int status;
	
	/**
	 * 注入进度
	 */
	@Column(name="progress")
	private int progress;
	/**
	 * 任务创建时间，UTC秒时间戳
	 */
	@Column(name="createtime")
	private long createtime;
	
	/**
	 * 任务开始时间，UTC秒时间戳
	 */
	@Column(name="starttime")
	private long starttime;
	
	/**
	 * 任务结束时间，UTC秒时间戳
	 */
	@Column(name="endtime")
	private long endtime;
	
	/**
	 * 存储路径
	 */
	@Column(name="path")
	private String path;
	
	/**
	 * '注入结果
	 */
	@Column(name="errcode")
	private int errcode;
	
	/**
	 * 注入结果描述
	 */
	@Column(name="errdes")
	private String errdes;
	
  /**
	 * 上报状态url
	 */
	@Column(name="reporturl")
	private String reporturl;
	
  /**
	 * 内容对应的md5值
	 */
	@Column(name="md5")
	private String md5;
	
}
