package com.gutetec.cms.bmc.bean;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 分发监控
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_task_distribute")
public class TaskDistributeInfo {
	
	/**
	 * 任务ID
	 */
	@Id
	@Column(name="taskid",nullable=false, unique=true)
	private long taskid;
	
	/**
	 * 提供商ID
	 */
	@Column(name = "providerid")
	private String providerid;
	
	/**
	 * 媒资id
	 */
	@Column(name="assetid")
	private String assetid;
	
	/**
	 *业务类型(可以支持的协议)：
	 * 0:未知 1：文件类型 2：HLS协议，需要生成对应m3u8文件 
	 * 3：NGOD协议，生成对应ngod索引 4：HLS+NGOD协议
	 */
	@Column(name = "servicetype")
	private int servicetype;
	
	/**
	 * 文件下载url
	 */
	@Column(name="url")
	private String url;
	
	/**
	 * 子节点注入地址
	 */
	@Column(name="subnodeaisaddr")
	private  String subnodeaisaddr;
	
	/**
	 * 注入状态 
	 * 0:Waiting 等待注入 1：Transfer 内容正在注入 2：Complete完成
	 */
	@Column(name="status")
	private int status;
	
	/**
	 * 分发进度
	 */
	@Column(name="progress")
	private int progress;
	
	/**
	 * 开始时间
	 */
	@Column(name="createtime")
	private long createtime;
	
	/**
	 * 开始时间
	 */
	@Column(name="endtime")
	private long endtime;
	
	/**
	 * 执行结果
	 */
	@Column(name="errcode")
	private int errcode;
	
	/**
	 * 执行结果描述
	 */
	@Column(name="errdes")
	private String errdes;
}
