package com.gutetec.cms.bmc.resource;

import java.util.HashMap;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.bean.ChannleInfo;
import com.gutetec.cms.bmc.wrapper.ChannleWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 頻道資源接口类
 * 
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " Channle Management " })
public class ChannleResource {

	@POST
	@Path("/channles")
	@ApiOperation(value = "create channle.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response addChannle(@ApiParam(value = "chl", required = true) ChannleInfo chl) {
		return ChannleWrapper.addChannle(chl);
	}

	@GET
	@Path("/channles")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query channle by conditions")
	@Timed
	public Response queryChannles(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		return ChannleWrapper.findByPage(paramMap);
	}

	@DELETE
	@Path("/channles/{channle_id}")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	@ApiOperation(value = "delete channle by id")
	@Timed
	public Response deleteChannle(
			@ApiParam(value = "channle id", required = true) @PathParam("channle_id") String channleId) {
		return ChannleWrapper.delChannle(channleId);
	}

	@PUT
	@Path("/channles")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "update channle by id")
	@Timed
	public Response updateCps(@ApiParam(value = "chl", required = true) ChannleInfo chl) {
		return ChannleWrapper.updateChannle(chl);
	}
}
