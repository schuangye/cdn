package com.gutetec.cms.bmc.db.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.AlarmInfo;

public class AlarmDao extends FmDbDAO<AlarmInfo> {
	private static final Logger logger = LoggerFactory.getLogger(AlarmDao.class);

	private final SessionFactory sessionFactory;

	public AlarmDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	/**
     * {@inheritDoc}
     */
    protected Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }
    
    
    
    /**
     * 分页查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("unchecked")
	public List<AlarmInfo> findActiveAlarmByPage(HashMap<String, Object> paramMap)
    {
        List<AlarmInfo> result = null;
        try
        {
            beginTransaction();
            
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            hql.append("from AlarmInfo info WHERE info.stoptime is NULL or info.stoptime = 0"); 
            //hql.append(" order by info.firstAccessTime asc ");
            
            Query query = session.createQuery(hql.toString());
            query.setFirstResult(Integer.parseInt(paramMap.get("start").toString())); //从第0条开始      
            query.setMaxResults(Integer.parseInt(paramMap.get("limit").toString())); //取出num条      
            result = (List<AlarmInfo>)query.list();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 获取数据库条数
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public int getActiveAlarmNum()
    {
       int result =0;
    	try
        {
            beginTransaction();
            String hql = "select count(*) from AlarmInfo info where  info.stoptime is NULL or info.stoptime =0";  
            Query query = session.createQuery(hql.toString());
            result =  ((Number)query.uniqueResult()).intValue();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 分页查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("unchecked")
	public List<AlarmInfo> findHistoryAlarmByPage(HashMap<String, Object> paramMap)
    {
        List<AlarmInfo> result = null;
        try
        {
            beginTransaction();
            
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            hql.append("from AlarmInfo info WHERE info.stoptime is not NULL and info.stoptime > 0"); 
            //hql.append(" order by info.firstAccessTime asc ");
            
            Query query = session.createQuery(hql.toString());
            query.setFirstResult(Integer.parseInt(paramMap.get("start").toString())); //从第0条开始      
            query.setMaxResults(Integer.parseInt(paramMap.get("limit").toString())); //取出num条      
            result = (List<AlarmInfo>)query.list();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 获取数据库条数
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public int getHistoryAlarmNum()
    {
       int result =0;
    	try
        {
            beginTransaction();
            String hql = "select count(*) from AlarmInfo info where  info.stoptime is not NULL and info.stoptime > 0";  
            Query query = session.createQuery(hql.toString());
            result =  ((Number)query.uniqueResult()).intValue();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * delete
     * @param id
     * @return
     */
    public int deleteById(String id)
    {
        int result = -1;
        long ids = Long.parseLong(id);
        try
        {
            beginTransaction();
            //             session.createQuery("delete NatInfo info where info.id = :id")
            //                .setParameter("id", id)
            session.createQuery("delete from AlarmInfo info where info.id = :id")
                .setParameter("id", ids)
                .executeUpdate();
            
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        result = 0;
        return result;
    }
}
