package com.gutetec.cms.bmc.db.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.FileInfo;

public class FileInfoDao extends FmDbDAO<FileInfo> {
	private static final Logger logger = LoggerFactory.getLogger(FileInfoDao.class);

	private final SessionFactory sessionFactory;

	public FileInfoDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	/**
     * {@inheritDoc}
     */
    protected Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }
    
    /**
     * 存储一条新的数据
     * <功能详细描述>
     * @param chl
     * @return
     * @see [类、类#方法、类#成员]
     */
    public boolean save(FileInfo file)
    {
        try
        {
            beginTransaction();
            session.save(file);
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
            return false;
        }
        finally
        {
            closeTransaction();
        }
        return true;
    }
    
    /**
     * 分页查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("unchecked")
    public List<FileInfo> findByPage(HashMap<String, Object> paramMap)
    {
        List<FileInfo> result = null;
        try
        {
            beginTransaction();
            
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            hql.append("from FileInfo info WHERE 1=1"); 
            //hql.append(" order by info.firstAccessTime asc ");
            
            if(null != paramMap.get("providerid") && !"".equals(paramMap.get("providerid").toString()))
            {
                hql.append(" AND info.providerid like" + " '%" + paramMap.get("providerid").toString() + "%'");
            }
            
            if(null != paramMap.get("assetid") && !"".equals(paramMap.get("assetid").toString()))
            {
                hql.append(" AND info.assetid like" + " '%" + paramMap.get("assetid").toString() + "%'");
            }
            
            Query query = session.createQuery(hql.toString());
            query.setFirstResult(Integer.parseInt(paramMap.get("start").toString())); //从第0条开始      
            query.setMaxResults(Integer.parseInt(paramMap.get("limit").toString())); //取出num条      
            result = (List<FileInfo>)query.list();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 获取数据库条数
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public int getDataNum(HashMap<String, Object> paramMap)
    {
       int result =0;
    	try
        {
            beginTransaction();
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            hql.append("select count(*) from FileInfo info  WHERE 1=1"); 
            if(null != paramMap.get("providerid") && !"".equals(paramMap.get("providerid").toString()))
            {
                hql.append(" AND info.providerid like" + " '%" + paramMap.get("providerid").toString() + "%'");
            }
            
            if(null != paramMap.get("assetid") && !"".equals(paramMap.get("assetid").toString()))
            {
                hql.append(" AND info.assetid like" + " '%" + paramMap.get("assetid").toString() + "%'");
            }
            
            Query query = session.createQuery(hql.toString());
            result =  ((Number)query.uniqueResult()).intValue();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
}
