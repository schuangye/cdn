package com.gutetec.cms.bmc.db.dbUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gutetec.cms.bmc.bean.AlarmInfo;
import com.gutetec.cms.bmc.bean.ChannleInfo;
import com.gutetec.cms.bmc.bean.CitaskInfo;
import com.gutetec.cms.bmc.bean.ConfInfo;
import com.gutetec.cms.bmc.bean.FileDistributeInfo;
import com.gutetec.cms.bmc.bean.FileInfo;
import com.gutetec.cms.bmc.bean.NodeInfo;
import com.gutetec.cms.bmc.bean.SubNodeInfo;
import com.gutetec.cms.bmc.bean.SysMonitorInfo;
import com.gutetec.cms.bmc.bean.TaskDistributeInfo;


/**
 * <Function brief>
 * <Function details>
 * @author  Administrator
 * @version  [Version, 2016-2-17]
 * @see  [Classs/Method]
 * @since  [Product/Model Version]
 */
public class FmDbMapper
{
    private static final String fmDaoPackageUrl = "com.gutetec.cms.bmc.db.dao";
    
    private static Map<String, String> dbMap = new HashMap<String, String>();
    
    static
    {
        // bean实体类，dao实体类
        dbMap.put("CHANNLEINFO", "ChlDao");
        dbMap.put("NODEINFO", "NodeDao");
        dbMap.put("FILEDISTRIBUTEINFO", "FileDisDao");
        dbMap.put("FILEINFO", "FileInfoDao");
        dbMap.put("TASKDISTRIBUTEINFO", "TaskDisDao");
        dbMap.put("CITASKINFO", "CitaskDao");
        dbMap.put("CONFINFO", "ConfDao");
        dbMap.put("SYSMONITORINFO", "MonitorDao");
        dbMap.put("ALARMINFO", "AlarmDao");
        dbMap.put("SUBNODEINFO", "SubNodeDao");
    }
    
    /**
     * get all bean
     * @return
     */
    public static List<Class<?>> getFmEntityClasses()
    {
        List<Class<?>> fmdbEntitys = new ArrayList<Class<?>>();
        fmdbEntitys.add(ChannleInfo.class);
        fmdbEntitys.add(NodeInfo.class);
        fmdbEntitys.add(FileDistributeInfo.class);
        fmdbEntitys.add(FileInfo.class);
        fmdbEntitys.add(TaskDistributeInfo.class);
        fmdbEntitys.add(CitaskInfo.class);
        fmdbEntitys.add(ConfInfo.class);
        fmdbEntitys.add(SysMonitorInfo.class);
        fmdbEntitys.add(AlarmInfo.class);
        fmdbEntitys.add(SubNodeInfo.class);
        return fmdbEntitys;
    }
   
    /**
     * get dao name
     * @param tableName
     * @return
     */
    public static String getDaoName(String tableName)
    {
        return fmDaoPackageUrl + "." + dbMap.get(tableName.toUpperCase());
    }
}
