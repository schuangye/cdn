package com.gutetec.cms.bmc.wrapper;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.SysMonitorInfo;
import com.gutetec.cms.bmc.db.dao.MonitorDao;
import com.gutetec.cms.bmc.db.dbUtil.FmDBUtil;

import net.sf.json.JSONObject;

/**
 * Channle rest interface processing class
 * 
 * @author macui
 *
 */
public class MonitorWrapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(MonitorWrapper.class);

	/**
	 * find by page
	 * 
	 * @param paramMap
	 * @return
	 */
	public static Response findByPage(HashMap<String, Object> paramMap) {
		MonitorDao dao = (MonitorDao) FmDBUtil.getDaoInstance("SysMonitorInfo");
		List<SysMonitorInfo> result = null;
		int totalRows = 0;
		try {
			result = dao.findByPage(paramMap);
			totalRows = dao.getDataNum();
		} catch (HibernateException e) {
			return Response.serverError().build();
		}

		JSONObject object = new JSONObject();
		if (0 == totalRows) {
			object.put("total", totalRows);
			object.put("rows", "[]");
			return Response.ok(object).build();
		} else {
			object.put("total", totalRows);
			object.put("rows", result);
			return Response.ok(object).build();
		}
	}
	
	public static Response findMoudlesByPage(HashMap<String, Object> paramMap) {
		MonitorDao dao = (MonitorDao) FmDBUtil.getDaoInstance("SysMonitorInfo");
		List<SysMonitorInfo> result = null;
		int totalRows = 0;
		try {
			result = dao.findmoudlesByPage(paramMap);
			totalRows = dao.getMoudleDataNum(paramMap.get("serviceid").toString());
		} catch (HibernateException e) {
			return Response.serverError().build();
		}

		JSONObject object = new JSONObject();
		if (0 == totalRows) {
			object.put("total", totalRows);
			object.put("rows", "[]");
			return Response.ok(object).build();
		} else {
			object.put("total", totalRows);
			object.put("rows", result);
			return Response.ok(object).build();
		}
	}
	
	/**
	 * @param serviceId
	 * @return
	 */
	public static Response updateService(String serviceId, int status)
	{
		JSONObject rt = new JSONObject();
		try
		{
			MonitorDao dao = (MonitorDao) FmDBUtil.getDaoInstance("SysMonitorInfo");
			
			List<SysMonitorInfo> list = dao.findModulesById(serviceId);
			
			for (SysMonitorInfo sysMonitorInfo : list)
			{
				sysMonitorInfo.setControl(status);
				dao.update(sysMonitorInfo);
			}
		}
		catch (Exception e) {
			rt.put("result", "-1");
		}
		rt.put("result", "0");
		return Response.ok(rt).build();
	}
	

	/**
	 * update chl
	 * @param chl
	 * @return
	 */
	public static Response updateChannle(SysMonitorInfo chl) {
		JSONObject rt = new JSONObject();
		try {
			MonitorDao dao = (MonitorDao) FmDBUtil.getDaoInstance("SysMonitorInfo");
			dao.update(chl);
		} catch (Exception e) {
			LOGGER.error("update chl fail.", e);
			rt.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rt).build();
		}
		rt.put("result", "0");
		return Response.status(Response.Status.OK).entity(rt).build();
	}
}
