package com.gutetec.cms.bmc.resource;

import java.util.HashMap;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.bean.NodeInfo;
import com.gutetec.cms.bmc.wrapper.NodeWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 节点資源接口类
 * 
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " node Management " })
public class NodeResource {

	@POST
	@Path("/nodes")
	@ApiOperation(value = "create node.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response addNode(@ApiParam(value = "chl", required = true) NodeInfo node) {
		return NodeWrapper.addNode(node);
	}
	
	@PUT
	@Path("/nodes")
	@ApiOperation(value = "create node.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response UPDATENode(@ApiParam(value = "chl", required = true) NodeInfo node) {
		return NodeWrapper.addNode(node);
	}
	
	@GET
	@Path("/nodes")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query node by conditions")
	@Timed
	public Response queryChannles(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		return NodeWrapper.findByPage(paramMap);
	}
	
	
	@DELETE
	@Path("/nodes/{id}")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	@ApiOperation(value = "delete node by id")
	@Timed
	public Response deleteChannle(
			@ApiParam(value = "key id", required = true) @PathParam("id") String nodeId) {
		return NodeWrapper.delNode(nodeId);
	}
}
