package com.gutetec.cms.bmc.resource;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.wrapper.TaskDisWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *任务分发接口类
 * 
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " taskdistribute Management " })
public class TaskDisResource {

	@GET
	@Path("/taskdistribute")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query taskdistribute by conditions")
	@Timed
	public Response queryChannles(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		return TaskDisWrapper.findByPage(paramMap);
	}
	
	@POST
	@Path("/taskdistribute")
	@ApiOperation(value = "create filedistributeInfo.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response addChannle(@ApiParam(value = "providerid  assetid", required = true) String  body) {
		JSONObject obj = JSONObject.fromObject(body);
		String providerid = obj.getString("providerid");
		String assetid = obj.getString("assetid");
		return TaskDisWrapper.addChannle(providerid, assetid);
	}
	
	@POST
	@Path("/taskdistributeAll")
	@ApiOperation(value = "create filedistributeInfo.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response distributeAllInfo(@ApiParam(value = "providerid  assetid", required = true) String  body) {
		return TaskDisWrapper.distributeAllTask(body);
	}
	
	@POST
	@Path("/taskdistributeBatch")
	@ApiOperation(value = "create filedistributeInfo.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response distributeBatchInfo(@ApiParam(value = "providerid  assetid", required = true) String  body) {
		return TaskDisWrapper.distributeBatchTask(body);
	}
}
