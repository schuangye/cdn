package com.gutetec.cms.bmc.wrapper;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.SubNodeInfo;
import com.gutetec.cms.bmc.db.dao.SubNodeDao;
import com.gutetec.cms.bmc.db.dbUtil.FmDBUtil;
import com.gutetec.cms.bmc.util.DacUtil;

import net.sf.json.JSONObject;

/**
 * Channle rest interface processing class
 * 
 * @author macui
 *
 */
public class SubNodeWrapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(SubNodeWrapper.class);

	/**
	 * Create data acquisition task
	 * 
	 * @param taskBean
	 *            task detail
	 * @return status code 201(success) or 500(fail)
	 */
	public static Response addChannle(SubNodeInfo chl) {
		LOGGER.info("Receive create chl request.taskBean:" + DacUtil.convertBeanToJson(chl));
		JSONObject result = new JSONObject();
		try {
			SubNodeDao dao = (SubNodeDao) FmDBUtil.getDaoInstance("SubNodeInfo");
			dao.save(chl);
		} catch (Exception e) {
			LOGGER.error("create subnode fail.", e);
			result.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		result.put("result", "0");
		return Response.status(Response.Status.CREATED).entity(result).build();
	}

	/**
	 * find by page
	 * 
	 * @param paramMap
	 * @return
	 */
	public static Response findByPage(HashMap<String, Object> paramMap) {
		SubNodeDao dao = (SubNodeDao) FmDBUtil.getDaoInstance("SubNodeInfo");
		List<SubNodeInfo> result = null;
		int totalRows = 0;
		try {
			result = dao.findByPage(paramMap);
			totalRows = dao.getDataNum();
		} catch (HibernateException e) {
			return Response.serverError().build();
		}

		JSONObject object = new JSONObject();
		if (0 == totalRows) {
			object.put("total", totalRows);
			object.put("rows", "[]");
			return Response.ok(object).build();
		} else {
			object.put("total", totalRows);
			object.put("rows", result);
			return Response.ok(object).build();
		}
	}

	/**
	 * delete channle by id
	 * 
	 * @param id
	 * @return
	 */
	public static Response delChannle(String id) {
		JSONObject result = new JSONObject();
		try {
			SubNodeDao dao = (SubNodeDao) FmDBUtil.getDaoInstance("SubNodeInfo");
			dao.deleteById(id);
		} catch (Exception e) {
			LOGGER.error("del subnode fail.", e);
			result.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		result.put("result", "0");
		return Response.status(Response.Status.OK).entity(result).build();
	}

	/**
	 * update chl
	 * @param chl
	 * @return
	 */
	public static Response updateChannle(SubNodeInfo chl) {
		JSONObject result = new JSONObject();
		try {
			SubNodeDao dao = (SubNodeDao) FmDBUtil.getDaoInstance("SubNodeInfo");
			dao.update(chl);
		} catch (Exception e) {
			LOGGER.error("update subnode fail.", e);
			result.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(result).build();
		}
		result.put("result", "0");
		return Response.status(Response.Status.OK).entity(result).build();
	}
}
