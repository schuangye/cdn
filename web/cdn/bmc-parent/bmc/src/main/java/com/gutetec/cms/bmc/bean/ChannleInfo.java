package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 频道管理
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_channel")
public class ChannleInfo {

	/**
	 * 主键
	 */
	@Id
	@Column(name = "id", nullable = true, unique=true)
	private long id;

	/**
	 * 频道名
	 */
	@Column(name = "channelname",length=128)
	private String channelname;

	/**
	 * 提供商ID
	 */
	@Column(name = "providerid",length=64)
	private String providerid;
	
	/**
	 * 提供商ID
	 */
	@Column(name = "nodeid",length=64)
	private String nodeid;
	
	/**
	 * 媒资id
	 */
	@Column(name="assetid",length=64)
	private String assetid;
	
	/**
	 * 收流地址
	 */
	@Column(name="sourceurl",length=256)
	private String sourceurl;
	/**
	 * 源类型
	 */
	@Column(name="sourcetype",length=256)
	private String sourcetype;
	/**
	 * 传输码率
	 */
	@Column(name="biterate")
	private int biterate;
	
	/**
	 * 切片时长
	 */
	@Column(name="slicedur")
	private int slicedur;
	
	/**
	 * 录制时常
	 */
	@Column(name="recordedur")
	private int recordedur;
	
	/**
	 * 存储路径
	 */
	@Column(name="storepath",length=256)
	private String storepath;
	
	/**
	 * 是否启用  0 不起用  1启用
	 */
	@Column(name="isenable")
	private int  isenable;
}
