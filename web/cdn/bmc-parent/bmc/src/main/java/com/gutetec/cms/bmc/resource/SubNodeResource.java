package com.gutetec.cms.bmc.resource;

import java.util.HashMap;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.bean.SubNodeInfo;
import com.gutetec.cms.bmc.wrapper.SubNodeWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 頻道資源接口类
 * 
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " subnode Management " })
public class SubNodeResource {

	@POST
	@Path("/subnode")
	@ApiOperation(value = "create subnode.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response addChannle(@ApiParam(value = "chl", required = true) SubNodeInfo chl) {
		return SubNodeWrapper.addChannle(chl);
	}

	@GET
	@Path("/subnode")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query subnode by conditions")
	@Timed
	public Response queryChannles(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		return SubNodeWrapper.findByPage(paramMap);
	}

	@DELETE
	@Path("/subnode/{subnode_id}")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	@ApiOperation(value = "delete subnode by id")
	@Timed
	public Response deleteChannle(
			@ApiParam(value = "channle id", required = true) @PathParam("subnode_id") String channleId) {
		return SubNodeWrapper.delChannle(channleId);
	}

	@PUT
	@Path("/subnode")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "update subnode by id")
	@Timed
	public Response updateCps(@ApiParam(value = "subnode", required = true) SubNodeInfo chl) {
		return SubNodeWrapper.updateChannle(chl);
	}
}
