/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.12-log : Database - cdn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cdn` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cdn`;

/*Table structure for table `t_alarm` */

DROP TABLE IF EXISTS `t_alarm`;

CREATE TABLE `t_alarm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `module` char(64) DEFAULT NULL COMMENT '告警模块ID',
  `alarmid` int(20) DEFAULT NULL COMMENT '告警ID',
  `level` int(11) DEFAULT NULL COMMENT '告警级别 1:一般 2:提示 3:警告  4:严重 5:致命',
  `name` char(64) DEFAULT NULL COMMENT '告警名称',
  `starttime` bigint(20) DEFAULT NULL COMMENT '告警开始时间，UTC秒时间戳',
  `stoptime` bigint(20) DEFAULT NULL COMMENT '告警结束时间,UTC秒时间戳',
  `describe` varchar(256) DEFAULT NULL COMMENT '告警描述',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_channel` */

DROP TABLE IF EXISTS `t_channel`;

CREATE TABLE `t_channel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `channelname` char(128) DEFAULT NULL COMMENT '频道名',
  `providerid` char(64) DEFAULT NULL COMMENT '提供商ID',
  `assetid` char(64) DEFAULT NULL COMMENT '媒资id',
  `sourceurl` varchar(256) DEFAULT NULL COMMENT '收流地址',
  `biterate` int(11) DEFAULT NULL COMMENT '传输码率,可以为空',
  `slicedur` int(11) DEFAULT NULL COMMENT '切片时长，单位为s',
  `recordedur` int(11) DEFAULT NULL COMMENT '录制时长，单位为小时',
  `storepath` varchar(256) DEFAULT NULL COMMENT '存储路径',
  `isenable` int(11) DEFAULT NULL COMMENT '是否启动，0:不启用 1：启用',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_citask` */

DROP TABLE IF EXISTS `t_citask`;

CREATE TABLE `t_citask` (
  `taskid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `cmd` int(11) DEFAULT NULL COMMENT '0:注入 1: 取消 2：删除',
  `providerid` char(64) DEFAULT NULL COMMENT '提供商ID',
  `assetid` char(64) DEFAULT NULL COMMENT '媒资ID',
  `contenttype` int(11) DEFAULT NULL COMMENT '内容注入类似1:文本 2:图片 3:视频',
  `url` varchar(256) DEFAULT NULL COMMENT '注入下载文件url',
  `nodeid` char(32) DEFAULT NULL COMMENT '分配的节点ID',
  `status` int(11) DEFAULT NULL COMMENT '注入状态 0:Waiting 等待注入 1：Transfer 内容正在注入 2：Complete完成',
  `progress` int(11) DEFAULT NULL COMMENT '注入进度',
  `createtime` bigint(20) DEFAULT NULL COMMENT '任务创建时间',
  `starttime` bigint(20) DEFAULT NULL COMMENT '任务开始时间',
  `endtime` bigint(20) DEFAULT NULL COMMENT '任务结束时间',
  `path` char(128) DEFAULT NULL COMMENT '存储路径',
  `errcode` int(11) DEFAULT NULL COMMENT '注入结果',
  `errdes` varchar(256) DEFAULT NULL COMMENT '注入结果描述',
  PRIMARY KEY (`taskid`),
  KEY `taskid` (`taskid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_conf` */

DROP TABLE IF EXISTS `t_conf`;

CREATE TABLE `t_conf` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` char(32) DEFAULT NULL COMMENT '参数名',
  `type` int(11) DEFAULT NULL COMMENT '参数类型 0:string 1:int',
  `value` varchar(128) DEFAULT NULL COMMENT '参数值',
  `serviceid` char(64) DEFAULT NULL COMMENT '参数所属模块',
  `lasttime` bigint(20) DEFAULT NULL COMMENT '参数上次更新时间戳',
  `updatetime` bigint(20) DEFAULT NULL COMMENT '参数更新时间戳',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_file_distribute` */

DROP TABLE IF EXISTS `t_file_distribute`;

CREATE TABLE `t_file_distribute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `providerid` char(64) NOT NULL COMMENT '提供商ID',
  `assetid` char(64) NOT NULL COMMENT '媒资ID',
  `path` varchar(128) DEFAULT NULL COMMENT '存储路径',
  `nodeid` char(32) NOT NULL COMMENT '节点ID',
  `createtime` bigint(20) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_file_info` */

DROP TABLE IF EXISTS `t_file_info`;

CREATE TABLE `t_file_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `providerid` char(255) NOT NULL COMMENT '提供商ID',
  `assetid` char(255) NOT NULL COMMENT '媒资ID',
  `contenttype` int(11) DEFAULT NULL COMMENT '内容类型',
  `filename` char(255) NOT NULL COMMENT '文件名',
  `filesize` bigint(20) unsigned DEFAULT '0' COMMENT '文件大小',
  `duration` bigint(20) DEFAULT '0' COMMENT '文件时长',
  `bitrate` bigint(20) unsigned DEFAULT '0' COMMENT '文件码率',
  `updateTime` bigint(20) DEFAULT NULL COMMENT '更新时间',
  `valid` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_module` */

DROP TABLE IF EXISTS `t_module`;

CREATE TABLE `t_module` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `name` char(64) DEFAULT NULL COMMENT '真实模块的名字，例如：rti，ottrti',
  `anothername` char(128) DEFAULT NULL COMMENT '模块别名，例如：录制系统',
  `describe` varchar(256) DEFAULT NULL COMMENT '模块描述',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_node` */

DROP TABLE IF EXISTS `t_node`;

CREATE TABLE `t_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `nodeid` char(32) NOT NULL COMMENT '节点ID',
  `nodetype` int(11) DEFAULT NULL COMMENT '节点类型  0:CI注入节点  1:RTI节点',
  `path` varchar(256) DEFAULT NULL COMMENT '节点存储路径',
  `maxsize` bigint(20) DEFAULT NULL COMMENT '最大空间',
  `usesize` bigint(20) DEFAULT NULL COMMENT '使用空间',
  `reservesize` bigint(20) DEFAULT NULL COMMENT '保留空间',
  `publicstreamaddr` char(64) DEFAULT NULL COMMENT '公网推流地址',
  `privatestreamaddr` char(64) DEFAULT NULL COMMENT '内网推流地址',
  `bandwidth` bigint(20) DEFAULT NULL COMMENT '总带宽',
  `hearttime` bigint(20) DEFAULT NULL COMMENT '心跳时间戳',
  `isonline` int(11) DEFAULT NULL COMMENT '节点是否在线，只针对CI类型节点。0:离线 1:在线',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_service` */

DROP TABLE IF EXISTS `t_service`;

CREATE TABLE `t_service` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `serviceid` char(64) NOT NULL COMMENT '服务ID',
  `modulename` char(64) DEFAULT NULL COMMENT '模块名',
  `installaddr` char(128) DEFAULT NULL COMMENT '服务安装地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_sysmonitor` */

DROP TABLE IF EXISTS `t_sysmonitor`;

CREATE TABLE `t_sysmonitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `serviceid` char(64) DEFAULT NULL COMMENT '服务器ID,监控服务ID',
  `servicename` char(64) DEFAULT NULL COMMENT '服务名称',
  `modulename` char(64) DEFAULT NULL COMMENT '模块名称',
  `runstatus` int(11) DEFAULT NULL COMMENT '运行状态  0:停止 1:正在启动 2:运行',
  `firststarttime` bigint(20) DEFAULT NULL COMMENT '第一次启动UTC时间戳',
  `control` int(11) DEFAULT NULL COMMENT '服务控制 0:禁止 1:停止 2:启动',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_task_distribute` */

DROP TABLE IF EXISTS `t_task_distribute`;

CREATE TABLE `t_task_distribute` (
  `taskid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `providerid` char(64) DEFAULT NULL COMMENT '提供商ID',
  `assetid` char(64) DEFAULT NULL COMMENT '媒资ID',
  `contenttype` int(11) DEFAULT NULL COMMENT '内容注入类似1:文本 2:图片 3:视频',
  `url` varchar(256) DEFAULT NULL COMMENT '文件下载url',
  `subnodeaisaddr` char(64) DEFAULT NULL COMMENT '子节点注入地址',
  `status` int(11) DEFAULT NULL COMMENT '注入状态 0:Waiting 等待注入 1：Transfer 内容正在注入 2：Complete完成',
  `progress` int(11) DEFAULT NULL COMMENT '分发进度',
  `createtime` bigint(20) DEFAULT NULL COMMENT '开始时间',
  `endtime` bigint(20) DEFAULT NULL COMMENT '结束时间',
  `errcode` int(11) DEFAULT NULL COMMENT '执行结果',
  `errdes` varchar(256) DEFAULT NULL COMMENT '执行结果描述',
  PRIMARY KEY (`taskid`),
  KEY `taskid` (`taskid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userID` varchar(128) NOT NULL,
  `userName` varchar(128) DEFAULT NULL,
  `userPasswd` varchar(128) DEFAULT NULL,
  `userRole` int(8) DEFAULT NULL,
  `userPhone` varchar(32) DEFAULT NULL,
  `userMailbox` varchar(32) DEFAULT NULL,
  `userSafeState` int(8) DEFAULT NULL,
  `userSafeAddress` varchar(32) DEFAULT NULL,
  `userLoginCount` int(16) DEFAULT '0',
  `userLoginTime` date DEFAULT NULL,
  `userAccountTime` date DEFAULT NULL,
  `userExpiryDateCount` int(16) DEFAULT NULL,
  `userExpiryDate` date DEFAULT NULL,
  `userPassExpiryDateCount` int(16) DEFAULT NULL,
  `userPassExpiryDate` date DEFAULT NULL,
  `userSessionTime` int(16) DEFAULT NULL,
  `userSort` int(16) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------

INSERT INTO `user` VALUES ('73d13858-3487-448c-861a-206370aebf98', 'root', 'go6xmIW/l7ZZlm8m3aj81Q==', '0', null, null, null, null, '3', '2016-07-17', '2016-05-17', null, null, null, null, null, null);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
