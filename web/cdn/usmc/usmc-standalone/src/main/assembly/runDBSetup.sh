DIRNAME=`dirname $0`
RUNHOME=`cd $DIRNAME/; pwd`
echo @RUNHOME@ $RUNHOME

echo ### Starting USMC Database setup

#JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")
echo @JAVA_HOME@ $JAVA_HOME
JAVA="$JAVA_HOME/bin/java"
echo @JAVA@ $JAVA

class_path="$RUNHOME/:$RUNHOME/usmc.jar"
echo @class_path@ $class_path

"$JAVA" -classpath "$class_path" com.raisecom.itms.usmc.UsmcApp db migrate "$RUNHOME/conf/usmc.yml"
