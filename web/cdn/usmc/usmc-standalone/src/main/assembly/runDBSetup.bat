@echo off
title usmc Database setup

set RUNHOME=%~dp0
echo ### RUNHOME: %RUNHOME%

title USMC Database setup

echo ### Starting USMC Database setup
rem set JAVA_HOME=D:\JDK1.7\jdk\jdk\windows
set JAVA="%JAVA_HOME%/bin/java"

set class_path=%RUNHOME%;%RUNHOME%usmc.jar
echo ### class_path: %class_path%

echo.
echo.
%JAVA% -classpath %class_path% com.raisecom.itms.usmc.UsmcApp db migrate %RUNHOME%conf/usmc.yml

pause