/*
 * Filename:  UserService.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016-5-4
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc.wrapper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.core.Response;

import net.sf.json.JSONObject;

import org.hibernate.HibernateException;

import com.gutetec.cms.usmc.bean.UserInfo;
import com.gutetec.cms.usmc.db.dao.UserDao;
import com.gutetec.cms.usmc.db.util.UsmcDBUtil;
import com.gutetec.cms.usmc.util.UsmcUtil;

/**
 * <User business process>
 * <User business process>
 * @author  donghu
 * @version  [version, 2016-5-4]
 * @see  [class/function]
 * @since  [version]
 */
public class UserService
{
    private static UserService instance = new UserService();
    
    private static UserDao userDao = (UserDao)UsmcDBUtil.getDaoInstance("USER");
    
    public synchronized static UserService getInstance()
    {
        return instance;
    }
    
    /** 
    * @Title adduser 
    * @Description TODO(check user user Info ) 
    * @param userinfo
    * @return bloolean    
    */
    @SuppressWarnings("deprecation")
    public boolean addUser(UserInfo userinfo)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        // set user id
        String userId = UUID.randomUUID().toString();
        userinfo.setUserID(userId);
        userinfo.setUserLoginCount("0");
        // user register time
        Date currData = new Date(System.currentTimeMillis());
        userinfo.setUserAccountTime(formatter.format(currData));
        
        // set user expiry time,default 6 months
        userinfo.setUserExpiryDateCount(UsmcUtil.USMC_ACCOUNT_EXPIRY);
        Date expiryDate = (Date)currData.clone();
        // expiryDate.setDate(expiryDate.getDate() + UsmcUtil.getInt(UsmcUtil.USMC_ACCOUNT_EXPIRY));
        Calendar calendar = new GregorianCalendar(); 
        calendar.setTime(expiryDate);
        calendar.add(calendar.DATE,UsmcUtil.getInt(UsmcUtil.USMC_ACCOUNT_EXPIRY));
        expiryDate=calendar.getTime();
        userinfo.setUserExpiryDate(formatter.format(expiryDate));
        
        // set user password expiry time,default 3 months
        userinfo.setUserPassExpiryDateCount(UsmcUtil.USMC_PASS_EXPIRY);
        Date expiryPwd = (Date)currData.clone();
        // expiryPwd.setDate(expiryPwd.getDate() + UsmcUtil.getInt(UsmcUtil.USMC_PASS_EXPIRY));
        Calendar calendarPwd = new GregorianCalendar(); 
        calendarPwd.setTime(expiryPwd);
        calendarPwd.add(calendarPwd.DATE,UsmcUtil.getInt(UsmcUtil.USMC_PASS_EXPIRY));
        expiryPwd=calendarPwd.getTime();
        userinfo.setUserPassExpiryDate(formatter.format(expiryPwd));
        return userDao.save(userinfo);
    }
    
    /** 
    * @Title updateuser 
    * @Description TODO(check user user Info ) 
    * @param userinfo
    * @return bloolean    
    */
    @SuppressWarnings("deprecation")
    public boolean updateUser(UserInfo userinfo)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // user update time
        Date currData = new Date(System.currentTimeMillis());
        
        if (!UsmcUtil.isEmpty(userinfo.getUserExpiryDateCount()))
        {
            // update user expiry time,default 6 months
            Date expiryDate = (Date)currData.clone();
            //expiryDate.setDate(expiryDate.getDate() + UsmcUtil.getInt(userinfo.getUserExpiryDateCount()));
            Calendar calendar = new GregorianCalendar(); 
            calendar.setTime(expiryDate);
            calendar.add(calendar.DATE,UsmcUtil.getInt(userinfo.getUserExpiryDateCount()));
            expiryDate=calendar.getTime();
            userinfo.setUserExpiryDate(formatter.format(expiryDate));
        }
        
        if (!UsmcUtil.isEmpty(userinfo.getUserPassExpiryDateCount()))
        {
            // update user password expiry time,default 3 months
            Date expiryPwd = (Date)currData.clone();
            // expiryPwd.setDate(expiryPwd.getDate() + UsmcUtil.getInt(userinfo.getUserPassExpiryDateCount()));
            Calendar calendarPwd = new GregorianCalendar(); 
            calendarPwd.setTime(expiryPwd);
            calendarPwd.add(calendarPwd.DATE,UsmcUtil.getInt(userinfo.getUserPassExpiryDateCount()));
            expiryPwd=calendarPwd.getTime();
            userinfo.setUserPassExpiryDate(formatter.format(expiryPwd));
        }
        
        return userDao.modifyUserInfo(userinfo);
    }
    
    /** 
    * @Title get all user 
    * @Description TODO(check user user Info ) 
    * @param userinfo
    * @return bloolean    
    */
    public Response findUserByPage(int pageSize, int page)
    {
        List<UserInfo> result = null;
        int totalRows = 0;
        try
        {
            result = userDao.findByPage(pageSize, page);
            totalRows = userDao.getUserNum();
        }
        catch (HibernateException e)
        {
            return Response.serverError().build();
        }
        
        JSONObject object = new JSONObject();
        
        if (null == result || result.isEmpty() || totalRows == 0)
        {
            object.put("total", 0);
            object.put("rows", "[]");
        }
        else
        {
            object.put("total", totalRows);
            object.put("rows", result);
            
        }
        return Response.ok(object).build();
    }
    
    /** 
    * @Title get all user by name
    * @Description TODO(check user user Info ) 
    * @param userinfo
    * @return bloolean    
    */
    public Response findUserByName(int pageSize, int page, String name)
    {
        List<UserInfo> result = null;
        int totalRows = 0;
        try
        {
            result = userDao.findByName(pageSize, page, name);
            totalRows = userDao.getUserByNameNum(name);
        }
        catch (HibernateException e)
        {
            return Response.serverError().build();
        }
        
        JSONObject object = new JSONObject();
        
        if (null == result || result.isEmpty() || totalRows == 0)
        {
            object.put("total", 0);
            object.put("rows", "[]");
        }
        else
        {
            object.put("total", totalRows);
            object.put("rows", result);
            
        }
        return Response.ok(object).build();
    }
    
    /** 
    * @Title delete user 
    * @Description TODO(check user user Info ) 
    * @param userinfo
    * @return bloolean
    */
    public boolean deleteUser(UserInfo userinfo)
    {
        return userDao.delete(userinfo);
    }
    
    /** 
    * @Title is user exist 
    * @Description TODO(check user user Info ) 
    * @param userinfo
    * @return bloolean
    */
    public boolean isUserExist(String userinfo)
    {
        return userDao.isUserExist(userinfo);
    }
    
}
