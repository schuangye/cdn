/*
 * Filename:  UsmcAppConfig.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016-5-4
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * <config of usmc>
 * <Function details>
 * @author  donghu
 * @version  [Version, 2016-5-4]
 * @see  [Classs/Method]
 * @since  [Product/Model Version]
 */
public class UsmcAppConfig extends Configuration
{
    @NotEmpty
    private String template;
    
    @NotEmpty
    private String defaultName = "USMC";
    
    @NotEmpty
    private String rocServerAddr;
    
    @NotEmpty
    private String rocResourceAddr;
    
    @NotEmpty
    private String dacServerPort;
    
    @JsonProperty
    public String getTemplate()
    {
        return template;
    }
    
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();
    
    @JsonProperty
    public void setTemplate(String template)
    {
        this.template = template;
    }
    
    @JsonProperty
    public String getDefaultName()
    {
        return defaultName;
    }
    
    @JsonProperty
    public void setDefaultName(String name)
    {
        this.defaultName = name;
    }
    
    @JsonProperty
    public String getRocServerAddr()
    {
        return rocServerAddr;
    }
    
    @JsonProperty
    public void setRocServerAddr(String rocServerConf)
    {
        this.rocServerAddr = rocServerConf;
    }
    
    @JsonProperty
    public String getRocResourceAddr()
    {
        return rocResourceAddr;
    }
    
    @JsonProperty
    public void setRocResourceAddr(String rocResourceAddr)
    {
        this.rocResourceAddr = rocResourceAddr;
    }
    
    @JsonProperty
    public String getDacServerPort()
    {
        return dacServerPort;
    }
    
    @JsonProperty
    public void setDacServerPort(String dacServerPort)
    {
        this.dacServerPort = dacServerPort;
    }
    
    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory()
    {
        return database;
    }
    
    @JsonProperty
    public DataSourceFactory getDatabase()
    {
        return database;
    }
    
    @JsonProperty
    public void setDatabase(DataSourceFactory database)
    {
        this.database = database;
    }
}
