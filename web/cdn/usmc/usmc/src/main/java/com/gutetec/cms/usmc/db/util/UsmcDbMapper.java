/*
 * Filename:  UsmcDbMapper.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016-5-04
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc.db.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gutetec.cms.usmc.bean.UserInfo;

/**
 * <Get DB dao entity class>
 * <Function details>
 * @author  donghu
 * @version  [Version, 2016-5-4]
 * @see  [Classs/Method]
 * @since  [Product/Model Version]
 */
public class UsmcDbMapper
{
    private static final String fmDaoPackageUrl = "com.gutetec.cms.usmc.db.dao";
    
    private static Map<String, String> dbMap = new HashMap<String, String>();
    
    static
    {
        dbMap.put("SAFE", "SafeDao");
        dbMap.put("USER", "UserDao");
    }
    
    public static List<Class<?>> getSafeEntityClasses()
    {
        List<Class<?>> fmdbEntitys = new ArrayList<Class<?>>();
        fmdbEntitys.add(UserInfo.class);
        return fmdbEntitys;
    }
    
    public static String getDaoName(String tableName)
    {
        return fmDaoPackageUrl + "." + dbMap.get(tableName.toUpperCase());
    }
}
