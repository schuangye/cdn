/*
 * Filename:  UserResource.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016-5-4
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc.resource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.sf.json.JSONObject;

import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.usmc.bean.UserInfo;
import com.gutetec.cms.usmc.wrapper.UserService;

/**
 * <user business process>
 * <user business process>
 * @author  donghu
 * @version  [version, 2016-5-4]
 * @see  [class/function]
 * @since  [version]
 */
@Path("/usmc/v1")
@Api(tags = {"umc user"})
public class UserResource
{
    
    @POST
    @Path("/userexist")
    @ApiOperation(value = "is user exist", response = JSONObject.class)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject userExist(@ApiParam(value = "userName", required = true) @QueryParam("userName") String userName)
    {
        boolean result = UserService.getInstance().isUserExist(userName);
        JSONObject object = new JSONObject();
        if (result)
        {
            object.put("result", "0");
        }
        else
        {
            object.put("result", "1");
        }
        return object;
    }
    
    @POST
    @Path("/useradd")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "useradd ", response = JSONObject.class)
    @Timed
    public JSONObject adduser(
        @ApiParam(value = "user info", required = true) UserInfo userinfo,
        @Context HttpServletRequest request, @Context HttpServletResponse servletResponse)
    {
        boolean result = UserService.getInstance().addUser(userinfo);
        
        JSONObject object = new JSONObject();
        if (result)
        {
            object.put("result", "0");
        }
        else
        {
            object.put("result", "1");
        }
        return object;
        
    }
    
    @POST
    @Path("/userupdate")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "userupdate ", response = JSONObject.class)
    @Timed
    public JSONObject updateuser(
        @ApiParam(value = "user info", required = true) UserInfo userinfo,
        @Context HttpServletRequest request, @Context HttpServletResponse servletResponse)
    {
        boolean result = UserService.getInstance().updateUser(userinfo);
        
        JSONObject object = new JSONObject();
        if (result)
        {
            object.put("result", "0");
        }
        else
        {
            object.put("result", "1");
        }
        return object;
        
    }
    
    @GET
    @Path("/userget")
    @ApiOperation(value = "get user Info by page", response = Response.class)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAllUser(@ApiParam(value = "limit", required = true) @QueryParam("limit") int pageSize,
        @ApiParam(value = "start", required = true) @QueryParam("start") int page)
    {
        return UserService.getInstance().findUserByPage(pageSize, page);
    }
    
    @GET
    @Path("/userbyname")
    @ApiOperation(value = "get user Info by name", response = Response.class)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByName(@ApiParam(value = "limit", required = true) @QueryParam("limit") int pageSize,
        @ApiParam(value = "start", required = true) @QueryParam("start") int page,
        @ApiParam(value = "username", required = true) @QueryParam("username") String username)
    {
        return UserService.getInstance().findUserByName(pageSize, page, username);
    }
    
    @POST
    @Path("/userdel")
    @ApiOperation(value = "delete user", response = JSONObject.class)
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject deleteUser(UserInfo userinfo)
    {
        boolean result = UserService.getInstance().deleteUser(userinfo);
        JSONObject object = new JSONObject();
        if (result)
        {
            object.put("result", "0");
        }
        else
        {
            object.put("result", "1");
        }
        return object;
    }
    
}
