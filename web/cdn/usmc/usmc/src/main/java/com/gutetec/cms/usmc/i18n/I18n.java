package com.gutetec.cms.usmc.i18n;

import java.util.ResourceBundle;

public class I18n {
	private static ResourceBundle umcResources;
	static {

	    String basename = "usmc";
	    umcResources = ResourceBundle.getBundle(basename);
	}
	
	public static String getI18nText(String key)
	{
		return umcResources.getString(key);
	}
}
