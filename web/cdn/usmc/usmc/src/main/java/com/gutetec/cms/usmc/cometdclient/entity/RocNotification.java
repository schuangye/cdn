package com.gutetec.cms.usmc.cometdclient.entity;

import java.util.Properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class RocNotification
{
    private String operationType;
    
    private String resourceType;
    
    private Properties[] data;
    
    private String[] deleteIds;
}
