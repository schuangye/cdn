/*
 * Filename:  UsmcDBUtil.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016-5-4
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc.db.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.usmc.db.dao.UsmcDbDao;

/**
 * <Function brief>
 * <Function details>
 * @author  donghu
 * @version  [Version, 2016-5-04]
 * @see  [Classs/Method]
 * @since  [Product/Model Version]
 */
public class UsmcDBUtil
{
    private static final Logger logger = LoggerFactory.getLogger(UsmcDBUtil.class);
    
    private static SessionFactory sessionFactory = null;
    
    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    public static Date getDate(String dateStr)
        throws ParseException
    {
        return df.parse(dateStr);
    }
    
    public static String formatDate(Date date)
    {
        return df.format(date);
    }
    
    public static String formatArray(String[] sArray)
    {
        StringBuffer sb = new StringBuffer();
        for (String s : sArray)
        {
            sb.append(s).append(",");
        }
        
        return sb.deleteCharAt(sb.length() - 1).toString();
    }
    
    @SuppressWarnings("rawtypes")
    public static UsmcDbDao getDaoInstance(String tableName)
    {
        
        String daoName = UsmcDbMapper.getDaoName(tableName);
        UsmcDbDao usmcDbDao = null;
        
        try
        {
            usmcDbDao =
                (UsmcDbDao)Class.forName(daoName).getConstructor(SessionFactory.class)
                    .newInstance(sessionFactory);
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            logger.error("Get Dao Error, Please check tableName, tableName:" + tableName);
        }
        
        return usmcDbDao;
    }
    
    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }
    
    public static void setSessionFactory(SessionFactory sessionFactory)
    {
        UsmcDBUtil.sessionFactory = sessionFactory;
    }
}
