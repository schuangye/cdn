/*
 * Filename:  SafeDao.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016-5-4
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc.db.dao;

import java.sql.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.usmc.bean.UserInfo;

/**
 * <Database operation>
 * <Function details>
 * @author  donghu
 * @version  [Version, 2016-5-4]
 * @see  [Classs/Method]
 * @since  [Product/Model Version]
 */
public class SafeDao extends UsmcDbDao<UserInfo>
{
    private static final Logger logger = LoggerFactory.getLogger(SafeDao.class);
    
    private final SessionFactory sessionFactory;
    
    /**
     * 
     * <默认构造函数>
     */
    public SafeDao(SessionFactory sessionFactory)
    {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * {@inheritDoc}
     */
    protected Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }
    
    /**
     * get user info by uaername
     * <get user info by uaername>
     * @param id
     * @return
     * @see [class]
     */
    public UserInfo loginByName(String userName)
    {
        List<UserInfo> resultList = null;
        UserInfo result = new UserInfo();
        try
        {
            beginTransaction();
            resultList = list(session
                .createQuery(
                    "FROM UserInfo info WHERE userName =:userName")
                .setParameter("userName", userName));
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        
        if (resultList.size() != 0)
        {
            result = resultList.get(0);
            return result;
        }
        else
        {
            return null;
        }
    }
    
    /**
     * update user login info by uaername
     * <update user login info by uaername>
     * @param id
     * @return
     * @see [class]
     */
    public void updateUserloginInfo(String userName)
    {
        try
        {
            beginTransaction();
            Date currData = new Date(System.currentTimeMillis());
            
            Query query =
                session.createQuery("update UserInfo info set userLoginCount = userLoginCount + 1, userLoginTime =? WHERE userName =?");
            query.setDate(0, currData);
            query.setString(1, userName);
            query.executeUpdate();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
    }
    
    /**
     * update user login info by uaername
     * <update user login info by uaername>
     * @param id
     * @return
     * @see [class]
     */
    public boolean modifyPassword(String userName, String newPwd)
    {
        try
        {
            beginTransaction();
            Query query = session.createQuery("update UserInfo info set userPasswd =? WHERE userName =?");
            query.setString(0, newPwd);
            query.setString(1, userName);
            query.executeUpdate();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
            return false;
        }
        finally
        {
            closeTransaction();
        }
        return true;
    }
    
}
