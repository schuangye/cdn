$(document).ready(function()
{

});
//初始化设置界面语言
loadPropertiesMainMenu();

// 渲染保存按钮
$("#setPassword").omButton(
{
	label : i18n.prop('btnEdit'),
	icons : 
	{
		left : '../component/css/omui/default/images/application_edit.png'
	},
	width : 80,
	onClick : function(event)
	{
		modifyPasswdSubmit();
	}
});

// 获取地址栏参数
function getQueryString(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}

// 修改按钮
function modifyPasswdSubmit()
{
	if (validator.form())
	{
		var isForces = getQueryString("isForces");
		
		var userName = "";
		if(isForces)
		{
			userName = getQueryString("userName");
		}
		else
		{
			userName = window.parent.currUser;
		}
		
		// bug fix:0005868 5873 by macui 2016-09-13 begin
		if(ict_framework_func1($("input[name=oldpasswd]").val()) == ict_framework_func1($("input[name=newpasswd]").val()))
		{
			$.omMessageTip.show(
			{
				type : 'warning',
				title : i18n.prop('alert'),
				content : main_i18n.prop('com_raisecom_itms_usmc_passd_dif'),
				timeout : 1500
			});
			$('input[name=newpasswd]').focus();
			return;
		}
		
		if(ict_framework_func1($("input[name=newpasswd]").val()) != ict_framework_func1($("input[name=cfnewpasswd]").val()))
		{
			$.omMessageTip.show(
			{
				type : 'warning',
				title : i18n.prop('alert'),
				content : main_i18n.prop('com_raisecom_itms_usmc_passdif'),
				timeout : 1500
			});
			$('input[name=cfnewpasswd]').focus();
			return;
		}
		// bug fix:0005868  by macui 2016-09-13 end
		
		var submitData =
		{
			username : userName,
			oldpassword : ict_framework_func1($("input[name=oldpasswd]").val()),
			newpassword : ict_framework_func1($("input[name=cfnewpasswd]").val())
		};
		
		var urls = "";
		if(isForces)
		{
			urls = '/api/usmc/v1/forcespwd';
		}
		else
		{
			urls = '/api/usmc/v1/modifypwd';
		}
		
		$.ajax(
		{
		    type : 'POST',
		    url : urls,
		    data : JSON.stringify(submitData),
		    contentType : 'application/json',
		    success: function (msg)
		    {
		    	// 查询到的JSON对象
				if (msg.result == "0")
				{
					$.omMessageTip.show(
					{
						type : 'success',
						title : i18n.prop('alert'),
						content : i18n.prop('operationSuccess'),
						timeout : 1500
					});
					
					$("input").val(""),
					
					// 清除红框样式
					$("input").removeClass("x-form-invalid");
					
					setTimeout(function()
					{
						if(isForces)
						{
							window.location.href = '/iui/framework/login.html';
						}
					},1000);
				}
				else
				{
					alertFail();
				}
				
				// 主界面关闭弹出框
				if($("#update_pwd_dialog-modal").length > 0)
				{
					$("#update_pwd_dialog-modal").omDialog("close");
				}
		    }
		});
	}
}

// 过滤字段
var rules  = 
{
	oldpasswd :
	{
		required : true,
		lengthrange : [6, 64]
	},
	newpasswd :
	{
		required : true,
		lengthrange : [6, 64]
	},
	cfnewpasswd :
	{
		required : true,
		lengthrange : [6, 64]
	}
};

// 提示消息
var errMsgs = 
{
	oldpasswd :
	{
		required : main_i18n.prop('usmc_not_empty'),
		lengthrange : main_i18n.prop('usmc_passTooshort')
	},
	newpasswd :
	{
		required :  main_i18n.prop('usmc_not_empty'),
		lengthrange :  main_i18n.prop('usmc_passTooshort')
	},
	cfnewpasswd :
	{
		required :  main_i18n.prop('usmc_not_empty'),
		lengthrange :  main_i18n.prop('usmc_passTooshort')
	}
};

// 调用验证方法，创建验证对象
var validator = new form_validator(rules, errMsgs, 'errorMsg', 'passwordForm');

// 密码强度判断显示
    $('#newpasswd').keyup(function(){ 	
    var Mcolor,Wcolor,Scolor,Color_Html; 
    var m=0; 
    var Modes=0;
    var strongType=0; 
  
    /* 获取输入密码  */
    var pwd = $(this).val();
  
    /* 计算输入密码包含几种类型 */
    for(i=0; i<pwd.length; i++){ 
        var charType=0; 
        var t=pwd.charCodeAt(i); 
        if(t>=48 && t <=57){charType=1;} 
        else if(t>=65 && t <=90){charType=2;} 
        else if(t>=97 && t <=122){charType=4;} 
        else{charType=4;} 
        Modes |= charType; 
    }
   
    for(i=0;i<4;i++){ 
    if(Modes & 1){m++;} 
      Modes>>>=1; 
    } 
  
    /* 密码只有一种输入类型  */
    if ( 1 == m ) 
    {  
        strongType = 1; 
    }
    /* 密码只有两种输入类型  */
    if ( 2 == m )
    {
        if ( pwd.length >= 6 )
        {
            strongType = 2;    
        }
        else
        {
            strongType = 1;
        }    
    }
    /* 密码只有三种输入类型  */
    if ( 3 == m )
    {
        if ( pwd.length > 9 )
        {
            strongType = 3;    
        }
        else if ( pwd.length >= 6 && pwd.length <= 9)
        {
            strongType = 2;    
        }    
        else
        {
            strongType = 1;
        }    
    }
  
    /* 密码长度小于5  */
    if(pwd.length<=5){strongType=1;}
    /* 密码长度为0  */ 
    if(pwd.length<=0){strongType=0;} 
  
    switch(strongType){ 
    case 1 : 
        Wcolor="pwd pwd_Weak_c"; 
        Mcolor="pwd pwd_c"; 
        Scolor="pwd pwd_c pwd_c_r"; 
        Color_Html="弱"; 
    break;
     
    case 2 : 
        Wcolor="pwd pwd_Medium_c"; 
        Mcolor="pwd pwd_Medium_c"; 
        Scolor="pwd pwd_c pwd_c_r"; 
        Color_Html="中"; 
    break; 
    
    case 3 : 
        Wcolor="pwd pwd_Strong_c"; 
        Mcolor="pwd pwd_Strong_c"; 
        Scolor="pwd pwd_Strong_c pwd_Strong_c_r"; 
        Color_Html="强"; 
    break; 
    
    default : 
        Wcolor="pwd pwd_c"; 
        Mcolor="pwd pwd_c pwd_f"; 
        Scolor="pwd pwd_c pwd_c_r"; 
        Color_Html="无"; 
    break; 
  } 
  document.getElementById('pwd_Weak').className=Wcolor; 
  document.getElementById('pwd_Medium').className=Mcolor; 
  document.getElementById('pwd_Strong').className=Scolor; 
  document.getElementById('pwd_Medium').innerHTML=Color_Html; 
});
