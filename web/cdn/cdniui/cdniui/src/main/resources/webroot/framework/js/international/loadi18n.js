var lang = getLanguage();
//lang = 'en-US';
var i18n = null;
var main_i18n = null;

jQuery.i18n.properties({
    language:lang,
    name:'lang-i18n',// properties file name
    path:'../component/thirdparty/omui/i18n/',// properties file path
    mode:'map',
    callback: function() {
    	i18n = $.i18n;
    }
});

//加载top页面国际化
var loadPropertiesMainMenu = function(){
	jQuery.i18n.properties({
		language:lang,
		name:'web-framework-main-i18n',
		path:'i18n/', // 资源文件路径
		mode:'map', // 用 Map 的方式使用资源文件中的值
		callback: function() {// 加载成功后设置显示内容
			main_i18n = $.i18n;	
			
			var i18nItems = $("[name_i18n=rtms_framework_ui_i18n]");
			for(var i=0;i<i18nItems.length;i++){
			    var $item = $(i18nItems.eq(i));
			    var itemId = $item.attr('id');
				var itemValue = $.i18n.prop(itemId);
				//从老的js文本文件中读取可能包含"和;字样
				if(itemValue.indexOf(';')>0){
					itemValue = itemValue.replace(';', '');
				}
				if(/[\'\"]/.test(itemValue)){
					itemValue = itemValue.replace(/\"/g,'');
					itemValue = itemValue.replace(/\'/g,'');
				}
				if(typeof($item.attr("title"))!="undefined"){
					$item.attr("title", itemValue);
				}else if(typeof($item.attr("placeholder"))!="undefined"){
					$item.attr("placeholder", itemValue);
				}else{
					$item.text(itemValue);
				}
			}		
		}
	});
};
