$(document).ready(function()
{	
	// 国际化赋值
	$("#dhcpConfig").html(i18n.prop('acs_config'));
	$("#tdStunAddr").html(i18n.prop('syscfg_tdStunAddr') + i18n.prop('colon'));
	$("#tdStunPort").html(i18n.prop('syscfg_tdStunPort') + i18n.prop('colon'));
	$("#tdrtmsUserName").html(i18n.prop('syscfg_tdrtmsUserName') + i18n.prop('colon'));
	$("#tdRtmsPwd").html(i18n.prop('syscfg_tdRtmsPwd') + i18n.prop('colon'));
	$("#tdInterval").html(i18n.prop('syscfg_tdInterval') + i18n.prop('colon'));
	$("#tdFileServerAddr").html(i18n.prop('syscfg_FileServerAddr') + i18n.prop('colon'));
	$("#lblStunAddr").html(i18n.prop('syscfg_lblStunAddr'));
	$("#lblInterval").html(i18n.prop('syscfg_lblInterval'));
	$("#lblFileServerAddr").html(i18n.prop('syscfg_lblFileServerAddr'));

	
	// 渲染保存按钮
	$("#setStunServer").omButton(
	{
		label : i18n.prop('btnEdit'),
		icons : 
		{
			left : '../images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 检查session是否过期
			var flag = true;
			if(flag)
			{
				// 如果校验通过则提交数据到后台
				if (checkFormInput())
				{
					$.omMessageBox.confirm(
					{
						title : i18n.prop('alert'),
						content : i18n.prop('applyUpdate'),
						onClose : function(value)
						{
							// 如果确认设置并校验通过则提交数据到后台
							if (value)
							{
								// 提交的数据对象
								var submitValue = 
								{
									id : 1,
									stunIpAddr : $('input[name=stunAddr]').val(),
									stunPort : parseInt($('input[name=stunPort]').val()),
									userName : $('input[name=rtmsUserName]').val(),
									userPassWord : $('input[name=rtmsPwd]').val(),
									tr069HB : parseInt($('input[name=interval]').val()),
									fileServerIpAddr : $('input[name=fileServerAddr]').val()
								};
								
								// 提交数据到后台 
								$.ajax(
								{
									type : 'POST',
									url : '/api/sm/v1/SysConfigInfo/',
									contentType: "application/json",
									dataType : 'json',
									data : JSON.stringify(submitValue),
									success : function(msg)
									{
										if (msg.result == "0")
										{
											var data  =
											{
													stunServerIP : $('input[name=stunAddr]').val(),
													stunServerPort : parseInt($('input[name=stunPort]').val())
											};
											// 提交数据到后台 
											$.ajax(
											{
												type : 'POST',
												url : '/api/stun/v1/configStun/',
												contentType: "application/json",
												dataType : 'json',
												data : JSON.stringify(data),
												success : function(msg)
												{
													if (msg.result == "true")
													{
														alertSuccess();
													}
													else
													{
														alertFail();
													}
												}
											});
											// alertSuccess();
										}
										else
										{
											alertFail();
										}
									}
								});
							}
						}
					});
				}
				return false;
			}
			
			// 按钮失去焦点
			$(this).blur();
		}
	});

	// 渲染保存按钮
	$("#refStunServer").omButton(
	{
		label : i18n.prop('btRefresh'),
		icons : 
		{
			left : '../images/page_refresh.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 检查session是否过期
			var flag = true;
			if(flag)
			{
				// 禁用刷新按钮
				$("#refDhcpServer").attr("disabled","disabled");
				
				// 执行单击函数
				getDhcpServerInfo();
				
				// 按钮失去焦点
				this.blur();
				
				return false;
			}
			
			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	// 查询当前服务器IP信息函数
	var getDhcpServerInfo = function()
	{
		// 根据服务器类型查询对应的IP设置信息
		$.ajax(
		{
			type : 'GET',
			url : '/api/sm/v1/SysConfigInfo/',
			dataType: 'json',
		    contentType: "application/json",
			success : function(msg)
			{
				// 赋值
				$('input[name=stunAddr]').val(msg.ipAddr);
				$('input[name=stunPort]').val(msg.port);
				$('input[name=rtmsUserName]').val(msg.userName);
				$('input[name=rtmsPwd]').val(msg.passWord);
				$('input[name=interval]').val(msg.Tr069HB);
				$('input[name=fileServerAddr]').val(msg.FileServerIpAddr);
				
				// 启用刷新按钮
				$("#refDhcpServer").removeAttr("disabled","disabled");
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 提示数据加载失败
				alertError();
				
				// 启用刷新按钮
				$("#refDhcpServer").removeAttr("disabled","disabled");
			}
		});
	};
	
	// 查询当前服务器IP配置信息
	getDhcpServerInfo();
	
	// 过滤字段
	var rules =
	{
		stunAddr :
		{
			required : true,
			isIp : true
		},
		stunPort : 
		{
			required : true,
			range : [1, 65535]
		},
		rtmsUserName : 
		{
			required : true,
			lengthrange : [1, 30]
		},
		rtmsPwd : 
		{
			required : true,
			lengthrange : [1, 30]
		},
		interval : 
		{
			required : true,
			digits	:  true
		},
		fileServerAddr : 
		{
			required : true,
			digits	:  true
		}
	};
	
	// 提示消息
	var messages =
	{
		stunAddr : 
		{
			required : i18n.prop('inputRequired'),
			isIp : i18n.prop('inputIsIp')
		},	
		stunPort : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('inputRangeLength50')
		},
		rtmsUserName : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('inputRangeLength30')
		},
		rtmsPwd : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('inputRangeLength30')
		},
		fileServerAddr : 
		{
			required : i18n.prop('inputRequired'),
			digits : i18n.prop('isNumMust')
		}
	};
	
	// 调用验证方法，创建验证对象
	var validator = new form_validator(rules, messages, 'errorMsg', 'stunForm');

	// 校验页面输入框内容
	var checkFormInput = function()
	{
		return $('#stunForm').valid();
	};
});
