var Omui={};
Omui.TABLE_ROWS = 15;
Omui.TABLE_HEIGHT = 450;

/**
* 字符串格式化
*/
function add0(m)
{	
	return m<10?'0'+m:m; 
} 
/**
 * <时间戳转换为日期时间> 
 * @param obj
 * @returns
 * @see [类、类#方法、类#成员]
 */
function formatUTC(shijianchuo) 
{ 
	if(0 == shijianchuo)
	{
		return "";
	}
	//shijianchuo是整数，否则要parseInt转换 
	var time = new Date(shijianchuo); 
	var y = time.getFullYear(); 
	var m = time.getMonth()+1; 
	var d = time.getDate(); 
	var h = time.getHours(); 
	var mm = time.getMinutes(); 
	var s = time.getSeconds(); 
	return y+'-'+add0(m)+'-'+add0(d)+' '+add0(h)+':'+add0(mm)+':'+add0(s); 
}

/**
 * <判断对象是否为空或未定义> 
 * <如果为空或未定义返回TRUE，否则返回FALSE>
 * @param obj
 * @returns
 * @see [类、类#方法、类#成员]
 */
var isEmpty = function(obj)
{
	if(null == obj)
	{
		return true;
	}
	else if(undefined == obj)
	{
		return true;
	}
	else if("" == obj)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * <错误消息提示> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertError = function()
{
	$.omMessageBox.alert(
	{
        type : 'error',
        title : i18n.prop('alert'),
        content : i18n.prop('loadError')
    });
}

/**
 * <数据操作成功提示消息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertSuccess = function()
{
	$.omMessageTip.show(
	{
		type : 'success',
		title : i18n.prop('alert'),
		content : i18n.prop('operationSuccess'),
		timeout : 1500
	});
}

/**
 * <数据操作失败提示消息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertFail = function()
{
	$.omMessageTip.show(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('operationFailed'),
		timeout : 1500
	});
}

/**
 * <未选中提示> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertUnCheckWarn = function()
{
	$.omMessageBox.alert(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('unCheckWarn')
	});
}

/**
 * <文件上传成功提示消息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertUploadSuccess = function()
{
	$.omMessageTip.show(
	{
		type : 'success',
		title : i18n.prop('alert'),
		content : i18n.prop('uploadSuccess'),
		timeout : 1500
	});
}

/**
 * <文件上传失败提示消息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alerUploadFailed = function()
{
	$.omMessageTip.show(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('uploadFailed'),
		timeout : 1500
	});
}

/**
 * <文件上传失败提示消息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertUploadIsExist = function()
{
	$.omMessageTip.show(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('alertUploadIsExist'),
		timeout : 1500
	});
}

/**
 * <SESSION超时提示>
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertSessionTimeOut = function()
{
	$.omMessageBox.alert(
	{
		type : 'alert',
		title : i18n.prop('alert'),
		content : i18n.prop('sessionError'),
		onClose : function(v)
		{
			top.window.location = path + "/";
		}
	});
}

/**
 * <选中多于一条记录提示> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertSelectMoreThanOneWarn = function()
{
	$.omMessageBox.alert(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('selectMoreThanOneWarn')
	});
}

/**
 * <备份文件不存在提示消息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var reductionFileNotExist = function()
{
	$.omMessageTip.show(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('reductionFileNotExist'),
		timeout : 1500
	});
}

/**
 * <基站离线，查询参数配置信息失败提示信息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertHnbOffline = function()
{
	$.omMessageTip.show(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('queryHnbParamsInfoFailed'),
		timeout : 1500
	});
}

/**
 * <基站忙提示信息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertHnbBusy = function()
{
	$.omMessageTip.show(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('hnbBusy'),
		timeout : 1500
	});
}

/**
 * <日志文件不存在提示信息> 
 * <功能详细描述>
 * @returns
 * @see [类、类#方法、类#成员]
 */
var alertLobFileNotExist = function()
{
	$.omMessageTip.show(
	{
		type : 'warning',
		title : i18n.prop('alert'),
		content : i18n.prop('logFileNotExist'),
		timeout : 1500
	});
}

Date.prototype.format = function(format) 
{  
    /* 
     * eg:format="yyyy-MM-dd hh:mm:ss"; 
     */  
    var o = {  
        "M+" : this.getMonth() + 1, // month
        "d+" : this.getDate(), // day
        "h+" : this.getHours(), // hour
        "m+" : this.getMinutes(), // minute
        "s+" : this.getSeconds(), // second
        "q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
        "S" : this.getMilliseconds()  
        // millisecond
    };  
  
    if (/(y+)/.test(format)) {  
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4  
                        - RegExp.$1.length));  
    }  
  
    for (var k in o) {  
        if (new RegExp("(" + k + ")").test(format)) {  
            format = format.replace(RegExp.$1, RegExp.$1.length == 1  
                            ? o[k]  
                            : ("00" + o[k]).substr(("" + o[k]).length));  
        }  
    }  
    return format;  
};

// 获取字符串长度，其中数字、字符占一个字节，汉字占两个字节
function getStrLength(str) 
{
	var length = 0;
	for(var i = 0; i < str.length; i++) 
	{
		var c = str.charCodeAt(i);
		if((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f))
		{
			length++;
		} 
		else 
		{
			length += 2;
		}
	}
	return length;
}

/**
 * <创建设置界面的设置按钮> 
 * <功能详细描述>
 * @param btnID 按钮ID
 * @param checkFormFun 表单验证函数
 * @param submitFormFun 设置提交函数
 * @returns
 * @see [类、类#方法、类#成员]
 */
var createSetBtn = function(btnID, checkFormFun, submitFormFun)
{
	// 渲染设置按钮
	$("#" + btnID).omButton(
	{
		label : i18n.prop('btnEdit'),
		icons :
		{
			left : path + '/images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 如果校验通过则提交数据到后台
			if (checkFormFun())
			{
				$.omMessageBox.confirm(
				{
					title : i18n.prop('alert'),
					content : i18n.prop('applyUpdate'),
					onClose : function(value)
					{
						// 如果确认设置并校验通过则提交数据到后台
						if (value)
						{
							submitFormFun();
						}
					}
				});
			}
			
			// 按钮失去焦点
			this.blur();
		}
	});
}


/**
 * <构造设置界面的刷新按钮> 
 * <功能详细描述>
 * @param refID     刷新按钮ID
 * @param clickFun  刷新按钮单击时调用函数的函数名称
 * @returns
 * @see [类、类#方法、类#成员]
 */
var createRefBtn = function(refID, clickFun)
{
	// 渲染刷新按钮
	$('#' + refID).omButton(
	{
		label : i18n.prop('btRefresh'),
		icons :
		{
			left : path + '/images/page_refresh.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 禁用刷新按钮
			$('#' + refID).omButton('disable');
			
			// 执行单击函数
			clickFun();
			
			// 启用刷新按钮
			$('#' + refID).omButton('enable');
			
			// 按钮失去焦点
			this.blur();
		}
	});
}

/**
 * <时间格式化> 
 * <功能详细描述>
 * @param flag  年月日的连接符号
 * @param date  时间对象
 * @returns
 * @see [类、类#方法、类#成员]
 */
var dataFormat = function(flag,date)
{
	var year = date.year + 1900;   //获取系统的年；
	var monthTmp = date.month + 1; //获取系统月份，由于月份是从0开始计算，所以要加1
	var month =  (monthTmp + "").length == 1?"0" + monthTmp:monthTmp;   
	var day = (date.date + "").length == 1?"0" + date.date:date.date; // 获取系统日，
	var hour = (date.hours + "").length == 1?"0" + date.hours:date.hours; //获取系统时，
	var min = (date.minutes + "").length == 1?"0" + date.minutes:date.minutes; //分
	var sec = (date.seconds + "").length == 1?"0" + date.seconds:date.seconds; //秒
	
	return year + flag + month + flag + day + " " + hour + ":" + min + ":" + sec;
};