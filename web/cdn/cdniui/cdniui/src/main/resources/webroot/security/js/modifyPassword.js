$(document).ready(function()
{
	// 初始化设置界面语言
	loadLanguage();
	
	$("#tdModifyPwd").html(i18n.prop('usmc_password_mdf'));
	
	// 获取地址栏参数
	function getQueryString(name)
	{
	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null)return  unescape(r[2]); return null;
	}
	
	// 渲染保存按钮
	$("#setDhcpServer").omButton(
	{
		label : i18n.prop('btnEdit'),
		icons : 
		{
			left : '../../component/css/omui/default/images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			modifyPasswdSubmit();
		}
	});

	// 渲染查询按钮
	/*$("#com_raisecom_itms_mdypasswd_btn").omButton(
	{
		label : usmc_i18n.prop('com_raisecom_itms_mdypasswd_btn'),
		icons :
		{
			left : '../../component/css/omui/default/images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			modifyPasswdSubmit();
		}
	});*/
		
	// 修改按钮
	function modifyPasswdSubmit()
	{
		if (validator.form())
		{
			var isForces = getQueryString("isForces");
			
			var userName = "";
			if(isForces)
			{
				userName = getQueryString("userName");
			}
			else
			{
				userName = window.parent.currUser;
			}
			
			// bug fix:0005868 5873 by macui 2016-09-13 begin
			if(ict_framework_func1($("input[name=oldpasswd]").val()) == ict_framework_func1($("input[name=newpasswd]").val()))
			{
				$.omMessageTip.show(
				{
					type : 'warning',
					title : i18n.prop('alert'),
					content : i18n.prop('com_raisecom_itms_usmc_passd_dif'),
					timeout : 1500
				});
				$('input[name=newpasswd]').focus();
				return;
			}
			
			if(ict_framework_func1($("input[name=newpasswd]").val()) != ict_framework_func1($("input[name=cfnewpasswd]").val()))
			{
				$.omMessageTip.show(
				{
					type : 'warning',
					title : i18n.prop('alert'),
					content : i18n.prop('com_raisecom_itms_usmc_passdif'),
					timeout : 1500
				});
				$('input[name=cfnewpasswd]').focus();
				return;
			}
			// bug fix:0005868  by macui 2016-09-13 end
			
			var submitData =
			{
				username : userName,
				oldpassword : ict_framework_func1($("input[name=oldpasswd]").val()),
				newpassword : ict_framework_func1($("input[name=cfnewpasswd]").val())
			};
			
			var urls = "";
			if(isForces)
			{
				urls = '/api/usmc/v1/forcespwd';
			}
			else
			{
				urls = '/api/usmc/v1/modifypwd';
			}
			
			$.ajax(
			{
			    type : 'POST',
			    url : urls,
			    data : JSON.stringify(submitData),
			    contentType : 'application/json',
			    success: function (msg)
			    {
			    	// 查询到的JSON对象
					if (msg.result == "0")
					{
						$.omMessageTip.show(
						{
							type : 'success',
							title : i18n.prop('alert'),
							content : i18n.prop('operationSuccess'),
							timeout : 1500
						});
						
						$("input").val(""),
						
						// 清除红框样式
						$("input").removeClass("x-form-invalid");
						
						setTimeout(function()
						{
							if(isForces)
							{
								window.location.href = '/iui/framework/login.html';
							}
						},1000);
					}
					else
					{
						alertFail();
					}
			    }
			});
		}
	}
	
	
	// 过滤字段
	var rules  = 
	{
		oldpasswd :
		{
			required : true,
			lengthrange : [6, 64]
		},
		newpasswd :
		{
			required : true,
			lengthrange : [6, 64]
		},
		cfnewpasswd :
		{
			required : true,
			lengthrange : [6, 64]
		}
	};
	
	// 提示消息
	var errMsgs = 
	{
		oldpasswd :
		{
			required : usmc_i18n.prop('usmc_not_empty'),
			lengthrange : usmc_i18n.prop('usmc_passTooshort')
		},
		newpasswd :
		{
			required :  usmc_i18n.prop('usmc_not_empty'),
			lengthrange :  usmc_i18n.prop('usmc_passTooshort')
		},
		cfnewpasswd :
		{
			required :  usmc_i18n.prop('usmc_not_empty'),
			lengthrange :  usmc_i18n.prop('usmc_passTooshort')
		}
	};
	
	// 调用验证方法，创建验证对象
	var validator = new form_validator(rules, errMsgs, 'errorMsg', 'passwordForm');
	
});

