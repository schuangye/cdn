$(document).ready(function(){
	// 设置图片按钮
	$("#sidebarImg").css("background-image:","url('./img/sidebar-toggler-light.jpg')");
	
	//对一级导航菜单
	$('.main_nav').click(function(){
		//获取当前对象的下一个ul对象
		var mthis = $(this);
		var mul = $(this).next('ul');
		//判断当前菜单下有无子菜单
		if($(mul).length == 0){
			//收起其它全部一级菜单
			//$('.main_nav').next('ul').attr('class','ul_nav_hide');
			//首先先把二级菜单全部样式复原，如果是点击当前一级菜单下的
			$('.jt_show').attr('class','jt_hide');
            //$('.sub_nav').css('backgroundColor','#D1E9FE');			
			//把所有的一级菜单的背景、字体改为原来的颜色
			//$('.main_nav').css('backgroundColor','#D1E9FE');				
			$('.main_nav').css('color','#000');				
			//把当前的同等级一级菜单背景色改为白色，并该字体颜色为黑色，把箭头加载过来
			$(mthis).css('backgroundColor','#1178EE');
			$(mthis).css('color','#000');
		}else if($(mul).length == 1){			
			//判断当前的一级菜单是否展开，并作出相反操作
			if($(mul).attr('class') == 'ul_nav_show'){
				mul.attr('class','ul_nav_hide');
				$("p",mthis).css('border-bottom','');
			}else if($(mul).attr('class') == 'ul_nav_hide'){
				//$('.main_nav').next('ul').attr('class','ul_nav_hide');
				mul.attr('class','ul_nav_show');
				$("p",mthis).css('border-bottom','1px solid #C5BFBF');
			}
		}				
	});
	
	//对二级导航菜单
	$('.sub_nav').click(function(){
		//获取当前对象的一级菜单a标签对象
		var parent_prev = $(this).parent().parent().prev('a');
		var img = $(this).next('img');
		
		//首先先把二级菜单全部样式复原，如果是点击当前一级菜单下的
		$('.jt_show').attr('class','jt_hide');				
		//把所有的一级菜单的背景、字体改为原来的颜色
		//$('.main_nav').css('backgroundColor','#D1E9FE');	
		$('.main_nav').css('backgroundColor','');	
		$('.main_nav').css('color','#000');
		//$('.sub_nav').css('backgroundColor','#D1E9FE');
		$('.sub_nav').css('backgroundColor','');
		//把当前的同等级一级菜单背景色改为白色，并该字体颜色为黑色，把箭头加载过来
		$(parent_prev).css('backgroundColor','#9AC0CD');
		$(parent_prev).css('color','#000');
		$(img).attr('class','jt_show');
		$(this).css('backgroundColor','#9AC0CD');
	});
	
	// default set first menu click
	$("#first_menu").parent().parent().prev('a').trigger("click");
	$("#first_menu").trigger("click");
});