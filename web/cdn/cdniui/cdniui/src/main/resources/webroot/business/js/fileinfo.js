$(document).ready(function()
{
	parent.setIframeHeight("fileinfo", 650);
	var isAdd = false;
	// 国际化
	//$('#cod_assetid').text(cdn_i18n.prop('cdn_nodeid') + i18n.prop('colon'));
	$('#bmc_assetid').text('媒资ID');
	$('#bmc_providerid').text('提供商ID');
//
	$('#cdn_submit').text(cdn_i18n.prop('cdn_submit'));
	$('#cdn_cancel').text(cdn_i18n.prop('cdn_cancel'));
	
	// 条件查询的面板
	var panel = $("#codition_div").omPanel(
	{
        iconCls : "panel_search",
        header : true,
        title : '查询',
        collapsed : false,
        collapsible : true
    });
	
	
	// 把查询的表单加入到面板当中
	$("#condition_query").append($("form[name='codition_form']"));
	
	// 修改图标
	$(".panel_search").removeClass("om-icon");
	$(".panel_search").removeClass("om-panel-icon");
	
	// 渲染查询按钮
	$("#cod_btn_search").omButton(
	{
		label : '查询',
		icons : 
		{
			left : '../../component/css/omui/default/images/search.png'
		},
		width : 80,
		onClick : function(event)
		{
			var providerid = encodeURIComponent(encodeURIComponent($
											.trim($('#cod_providerid').val())));
			var assetid = encodeURIComponent(encodeURIComponent($
											.trim($('#cod_assetid').val())));
			$('#cdn_nodeGrid').omGrid("setData", '/api/v1/resource/files?providerid=' + providerid + '&assetid=' + assetid);
			//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	// 回车事件
	// 点击“Enter”键，触发查询事件
	$(this).keydown(function(e)
	{
		if (e.which == "13")
		{
			if ((document.getElementsByName('cod_providerid')[0] == document.activeElement)
				|| (document.getElementsByName('cod_assetid')[0] == document.activeElement))
		     {
				 var providerid = encodeURIComponent(encodeURIComponent($
												.trim($('#cod_providerid').val())));
				var assetid = encodeURIComponent(encodeURIComponent($
												.trim($('#cod_assetid').val())));
				$('#cdn_nodeGrid').omGrid("setData", '/api/v1/resource/files?providerid=' + providerid + '&assetid=' + assetid);
				// 按钮失去焦点
				$(this).blur();
		     }
		}
	});
	
	// 渲染清空按钮
	$("#cod_btn_clear").omButton(
	{
		label : cdn_i18n.prop('cdn_reset'),
		icons : 
		{
			left : '../../component/css/omui/default/images/clear.png'
		},
		width : 80,
		onClick : function(event)
		{
			$('input[name=cod_providerid]').val('');
			$('input[name=cod_assetid]').val('');
			$('input[name=cod_providerid]').focus();

			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	// 初始化buttonbar
	$('#buttonbar').omButtonbar(
	{
		//width : 500,
		btns :
		[
//			
//				{
//					label : '分发',
//					id : "btn_dis",
//					icons :
//					{
//						left : '../../component/css/omui/default/images/edit_hover.png'
//					},
//					onClick : function()
//					{
//						updateInfo();
//					}
//				},
				{
					label : '全量分发',
					id : "btn_disAll",
					icons :
					{
						left : '../../component/css/omui/default/images/application_edit.png'
					},
					onClick : function()
					{
						disAllInfo();
					}
				},
				{
					label : '增量分发',
					id : "btn_disBatch",
					icons :
					{
						left : '../../component/css/omui/default/images/edit_add.png'
					},
					onClick : function()
					{
						disBatch();
					}
				}
		]
	});
	
	// 文件分发
	var updateInfo = function()
	{   
		var selections = $('#cdn_nodeGrid').omGrid('getSelections',true);
		if(selections.length==0)
		{
			alertUnCheckWarn();
		}
		else
		{
			var request = 
			{   
				providerid :  selections[0].providerid,
				assetid :  selections[0].assetid
			};
			
			$.ajax(
			{
				type : 'POST',
				url : '/api/v1/resource/taskdistribute' ,
				dataType :  'json',
				data : JSON.stringify(request),
				contentType : 'application/json; charset=UTF-8',
				success : function(msg)
				{
					// 提示消息
					if (msg.result == "0")
					{
						// 更新表格
						//$('#cdn_nodeGrid').omGrid('reload');
						// 删除成功提示
						alertSuccess();
					}
					else
					{
						// 删除失败提示
						alertFail();
					}
				}
			});
		}
	   	
	};
	
	// 选择框
	var selectIds = [];	
	
	/**
	 * 全量分发
	 */
	var disAllInfo = function()
	{   
		var selections = $('#cdn_nodeGrid').omGrid('getSelections',true);
		if(selections.length==0)
		{
			alertUnCheckWarn();
		}
		else
		{
			$.ajax(
			{
				type : 'POST',
				url : '/api/v1/resource/taskdistributeAll' ,
				dataType :  'json',
				data : JSON.stringify(selectIds),
				contentType : 'application/json; charset=UTF-8',
				success : function(msg)
				{
					// 提示消息
					if (msg.result == "0")
					{
						// 更新表格
						//$('#cdn_nodeGrid').omGrid('reload');
						
						// 删除成功后清空selectIds中的数据
						selectIds = [];
						
						// 删除成功提示
						alertSuccess();
					}
					else
					{
						// 删除失败提示
						alertFail();
					}
				}
			});
		}
	   	
	};
	
	// 增量分发
	var disBatch = function()
	{   
		showDialog('子节点信息',selectIds);
	};
	
	// 定义修改或添加的弹出框
	var dialog = $("#add_dialog-form").omDialog(
	{
		width : '70%',
		autoOpen : false,
		modal : true,
		resizable : true,
		onClose : function(event)
		{
			// 清除红框样式
			$("input").removeClass("x-form-invalid");
		}
	});
	
	// 显示修改或增加的弹出框
	var showDialog = function(title, rowData)
	{
		dialog.omDialog("option", "title", title);
		dialog.omDialog("open");
		
		// 创建表格
		$("#cdn_channleoGrid").omGrid(
		{
			dataSource : "/api/v1/resource/subnode",
			limit : 5,
			height : 300,
			singleSelect : true,
			showIndex : true,
			colModel :
			[
				{
					header : cdn_i18n.prop("cdn_subareacode"),
					name : 'subareacode',
					width : 150,
					align : 'left'
				},
				{
					header : cdn_i18n.prop("cdn_subinaddress"),
					name : 'subinaddress',
					width : 200,
					align : 'left'
				},
				
				{
					header : cdn_i18n.prop("cdn_submanageurl"),
					name : 'submanageurl',
					width : 200,
					align : 'left',
					renderer : function(colValue, rowData, rowIndex)
					{
						var data = $("#cdn_channleoGrid").omGrid("getData").rows[rowIndex];
						return '<span><a href="' + colValue	+ '"><u><b style="color:blue;">' +colValue + '</b></u></a></span>';
					}
				},
				
				{
					header : cdn_i18n.prop("cdn_remarks"),
					name : 'remarks',
					width : 460,
					align : 'left'
				}
			],
			onRefresh : function(data, testStatus, XMLHttpRequest, event)
			{
				// 隐藏勾选框
				$(".checkboxCol").hide();
			}
		});
	};
	
	// 弹出框取消按钮
	$('#cdn_cancel').click(function()
	{
		$("#add_dialog-form").omDialog("close");
		
		// 清除红框样式
		$("input").removeClass("x-form-invalid");
		return false;
	});
	
	// 弹出框确定按钮：添加or修改
	$('#cdn_submit').click(function()
	{
		dataSubmitDialog();
		return false;
	});
	
	
	// 弹出框确定按钮的具体操作
	var dataSubmitDialog = function()
	{
		if (true)
		{
			var selections = $('#cdn_channleoGrid').omGrid('getSelections', true);
			var ss = selectIds + '-' + selections[0].subinaddress;
			if (true)
			{
				submitData = 
				{
					selectIds : selectIds,
					subinaddress : selections[0].subinaddress
				};
				
				$.ajax(
				{
					type : 'POST',
					url : '/api/v1/resource/taskdistributeBatch',
					dataType : 'json',
					data : JSON.stringify(submitData),
					contentType : 'application/json',
					success : function(msg)
					{
						// 查询到的JSON对象
						if (msg.result == "0")
						{
							// 关闭dialog
							$("#add_dialog-form").omDialog("close");
							
							selectIds = [];
							
							alertSuccess();
						}
						else
						{
							alertFail();
						}
					}
				});
			}
			
		}
	};
	
	// 创建表格
	$("#cdn_nodeGrid").omGrid(
	{
		limit : Omui.TABLE_ROWS,
		height : Omui.TABLE_HEIGHT,
		singleSelect : false,
		showIndex : true,
		width : 'auto',
		dataSource : "/api/v1/resource/files",
		colModel :
		[
			
			{
				header : cdn_i18n.prop("cdn_providerid"),
				name : 'providerid',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_assetid"),
				name : 'assetid',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_citask_servicetype"),
				name : 'servicetype',
				width : 80,
				align : 'left',
				renderer : function(v, rowData , rowIndex)
				{
                    if("0" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_file");
                    }
                    else if("1" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_txt");
                    }
                    else if("2" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_image");
                    }
                    else if("3" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_hls");
                    }
                    else if("4" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_mp4");
                    }
                    else if("5" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_ngod");
                    }
                    else if("6" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_ngod_hls");
                    }
				}
			},
					
			{
				header : cdn_i18n.prop("cdn_file_filename"),
				name : 'filename',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_file_filesize"),
				name : 'filesize',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_file_duration"),
				name : 'duration',
				width : 100,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_file_bitrate"),
				name : 'bitrate',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_updateTime"),
				name : 'updateTime',
				width : 110,
				align : 'left',
				renderer : function(v, rowData, rowIndex)
				{
					return formatUTC(v*1000);
				}
			},
			
			{
				header : cdn_i18n.prop("cdn_file_md5"),
				name : 'md5',
				width : 100,
				align : 'left'
			}		
		],
		onRefresh : function(data, testStatus, XMLHttpRequest, event)
		{
			// 隐藏勾选框
			$(".checkboxCol").hide();
		},
		onRowSelect : function(index, data)
		{
			var ids = data.id + "-" + data.providerid + "-" + data.assetid + "-" + data.contenttype;
			if ($.inArray(ids, selectIds) == -1)
			{
				selectIds.push(ids);
			}
		},
		onRowDeselect : function(index, data)
		{
			var ids = data.id + "-" + data.providerid + "-" + data.assetid + "-" + data.contenttype;
			if ("" != ids)
			{
				var i = $.inArray(ids, selectIds);
				selectIds.splice(i, 1);
			}
		}
	});
});
