$(document).ready(function()
{
	parent.setIframeHeight("subnode", 550);
	var isAdd = false;
	// 国际化
	$('#cdn_subareacode').text(cdn_i18n.prop('cdn_subareacode') + i18n.prop('colon'));
	$('#cdn_subinaddress').text(cdn_i18n.prop('cdn_subinaddress') + i18n.prop('colon'));
	$('#cdn_subvodaddress').text(cdn_i18n.prop('cdn_subvodaddress') + i18n.prop('colon'));
	$('#cdn_submanageurl').text(cdn_i18n.prop('cdn_submanageurl') + i18n.prop('colon'));
	$('#cdn_remarks').text(cdn_i18n.prop('cdn_remarks') + i18n.prop('colon'));
	$('#cdn_submit').text(cdn_i18n.prop('cdn_submit'));
	$('#cdn_cancel').text(cdn_i18n.prop('cdn_cancel'));
	
	// 条件查询的面板
	var panel = $("#codition_div").omPanel(
	{
        iconCls : "panel_search",
        header : true,
        title : cdn_i18n.prop('cdn_search'),
        collapsed : false,
        collapsible : true
    });
	
	// 把查询的表单加入到面板当中
	$("#condition_query").append($("form[name='codition_form']"));
	
	// 修改图标
	$(".panel_search").removeClass("om-icon");
	$(".panel_search").removeClass("om-panel-icon");
	
	// 渲染查询按钮
	$("#cod_btn_search").omButton(
	{
		label : cdn_i18n.prop('usmc_user_search'),
		icons : 
		{
			left : '../../component/css/omui/default/images/search.png'
		},
		width : 80,
		onClick : function(event)
		{
			//var username = encodeURIComponent($.trim($('input[name="cod_abyEnbName"]').val()));
			//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
			// 按钮失去焦点
			//$(this).blur();
		}
	});
	
	// 回车事件
	// 点击“Enter”键，触发查询事件
	$(this).keydown(function(e)
	{
		if (e.which == "13")
		{
			if(document.getElementsByName('cod_abyEnbName')[0] == document.activeElement)
			{
				//var username = encodeURIComponent(encodeURIComponent($.trim($('input[name="cod_abyEnbName"]').val())));
				//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
				
				return true;
			}
		}
	});
	
	// 渲染清空按钮
	$("#cod_btn_clear").omButton(
	{
		label : cdn_i18n.prop('cdn_reset'),
		icons : 
		{
			left : '../../component/css/omui/default/images/clear.png'
		},
		width : 80,
		onClick : function(event)
		{
			//$('input[name=cod_abyEnbName]').val('');
			//$('input[name=cod_abyEnbName]').focus();

			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	
	// 初始化botton按钮
	$('#buttonbar').omButtonbar(
	{
		btns :
		[
			{
				label : cdn_i18n.prop('cdn_delete'),
				id : "cdn_delete",
				icons :
				{
					left : '../../component/css/omui/default/images/del_hover.png'
				},
				onClick : function()
				{
					
					deleteChannle();
				}
			},
			
			{
				separtor : true
			},
			
			{
				label : cdn_i18n.prop('cdn_modify'),
				id : "cdn_modify",
				icons :
				{
					left : '../../component/css/omui/default/images/edit_hover.png'
				},
				onClick : function()
				{
					modifyChannle();
				}
			},
			
			{
				separtor : true
			},
			
			{
				label : cdn_i18n.prop('cdn_add'),
				id : "cdn_add",
				icons :
				{
					left : '../../component/css/omui/default/images/add_hover.png'
				},
				onClick : function()
				{
					addChannle();
				}
			}
		]
	});
	
	// 创建表格
	$("#cdn_channleoGrid").omGrid(
	{
		dataSource : "/api/v1/resource/subnode",
		limit : Omui.TABLE_ROWS,
		height : Omui.TABLE_HEIGHT,
		singleSelect : true,
		showIndex : true,
		colModel :
		[
			{
				header : cdn_i18n.prop("cdn_subareacode"),
				name : 'subareacode',
				width : 150,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_subinaddress"),
				name : 'subinaddress',
				width : 200,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_subvodaddress"),
				name : 'subvodaddress',
				width : 200,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_submanageurl"),
				name : 'submanageurl',
				width : 250,
				align : 'left',
				renderer : function(colValue, rowData, rowIndex)
				{
					var data = $("#cdn_channleoGrid").omGrid("getData").rows[rowIndex];
					return '<span><a href="' + colValue	+ '"><u><b style="color:blue;">' +colValue + '</b></u></a></span>';
				}
			},
			
			{
				header : cdn_i18n.prop("cdn_remarks"),
				name : 'remarks',
				width : 460,
				align : 'left'
			}
		],
		onRefresh : function(data, testStatus, XMLHttpRequest, event)
		{
			// 隐藏勾选框
			$(".checkboxCol").hide();
		}
	});
	
	
	// 新增按钮
	var addChannle = function()
	{
		isAdd = true;
		showDialog(cdn_i18n.prop('cdn_add'));
	};
	
	// 修改信息
	var modifyChannle = function()
	{
		isAdd = false;
		var selections = $('#cdn_channleoGrid').omGrid('getSelections', true);
		
		if (selections.length == 0)
		{
			alertUnCheckWarn();
			return false;
		}
		
		showDialog(cdn_i18n.prop('cdn_modify'), selections[0]);
	};
	
	// 删除信息
	var deleteChannle = function()
	{
		var selections = $('#cdn_channleoGrid').omGrid('getSelections', true);
		if (selections.length == 0)
		{
			alertUnCheckWarn();
		}
		else
		{
			$.omMessageBox.confirm(
			{
				title : i18n.prop("alert"),
				content : i18n.prop("deleteContent"),
				onClose : function(value)
				{
					if (value)
					{
						// 提交删除请求
						$.ajax(
						{
							type : 'DELETE',
							url : '/api/v1/resource/subnode/' + selections[0].id,
							dataType : 'json',
//							data : JSON.stringify(request),
							contentType : 'application/json',
							success : function(msg)
							{
								// 提示消息
								if (msg.result == "0")
								{
									// 更新表格
									$('#cdn_channleoGrid').omGrid('reload');
									
									// 删除成功提示
									alertSuccess();
								}
								else
								{
									// 删除失败提示
									alertFail();
								}
							}
						});
					}
				}
			});
		}
	};
	
	
	// 定义修改或添加的弹出框
	var dialog = $("#add_dialog-form").omDialog(
	{
		width : '50%',
		autoOpen : false,
		modal : true,
		resizable : false,
		onClose : function(event)
		{
			// 清除红框样式
			$("input").removeClass("x-form-invalid");
		}
	});
	
	// 显示修改或增加的弹出框
	var showDialog = function(title, rowData)
	{
		//validator.resetForm();
		
		// 设置表格提示信息
		var trs = $('table tr', dialog);
		
		// input disable
		// $("#id").removeAttr("disabled");
//		$("#userbingip").attr("disabled", "disabled");
//		$("#useruserexpiry").attr("disabled", "disabled");
//		$("#userpassexpiry").attr("disabled", "disabled");
//		$("#usersessionexpiry").attr("disabled", "disabled");
		
		rowData = rowData || {};
		
		$('input[name=subareacode]', dialog).val(rowData.subareacode);
		$('input[name=subinaddress]', dialog).val(rowData.subinaddress);
		$('input[name=subvodaddress]', dialog).val(rowData.subvodaddress);
		$('input[name=submanageurl]', dialog).val(rowData.submanageurl);
		$('textarea[name=remarks]', dialog).val(rowData.remarks);
		
		// channelname can not modify here
		if (isAdd)
		{
			$('input[name=subareacode]', dialog).removeAttr("disabled");
			//$('input[name=subinaddress]', dialog).removeAttr("disabled");
			//$('input[name=subvodaddress]', dialog).removeAttr("disabled");
		}
		else
		{
			$('input[name=subareacode]', dialog).attr("disabled", "disabled");
			//$('input[name=subinaddress]', dialog).attr("disabled", "disabled");
			//$('input[name=subvodaddress]', dialog).attr("disabled", "disabled");
		}
		
		dialog.omDialog("option", "title", title);
		dialog.omDialog("open");
	};
	
	// 弹出框取消按钮
	$('#cdn_cancel').click(function()
	{
		$("#add_dialog-form").omDialog("close");
		
		// 清除红框样式
		$("input").removeClass("x-form-invalid");
		return false;
	});
	
	// 弹出框确定按钮：添加or修改
	$('#cdn_submit').click(function()
	{
		dataSubmitDialog();
		return false;
	});
	
	// 弹出框确定按钮的具体操作
	var dataSubmitDialog = function()
	{
		if (validator.form())
		//if (true)
		{
			var selections = $('#cdn_channleoGrid').omGrid('getSelections', true);
			var submitData = null;
			
			var urls = "/api/v1/resource/subnode";
			
			if (isAdd)
			{
				submitData = 
				{
					subareacode : $('input[name=subareacode]', dialog).val(),
					subinaddress : $('input[name=subinaddress]', dialog).val(),
					subvodaddress : $('input[name=subvodaddress]', dialog).val(),
					submanageurl : $('input[name=submanageurl]', dialog).val(),
					remarks : $('textarea[name=remarks]', dialog).val()
				};
				
				$.ajax(
				{
					type : 'POST',
					url : urls,
					dataType : 'json',
					data : JSON.stringify(submitData),
					contentType : 'application/json',
					success : function(msg)
					{
						// 查询到的JSON对象
						if (msg.result == "0")
						{
							alertSuccess();
							// 关闭dialog
							$("#add_dialog-form").omDialog("close");
							
							// 清除红框样式
							$("input").removeClass("x-form-invalid");
							
							// 更新表格
							$('#cdn_channleoGrid').omGrid('reload');
						}
						else
						{
							alertFail();
						}
					}
				});
			}
			else
			{
				submitData = 
				{
					id : selections[0].id,
					subareacode : $('input[name=subareacode]', dialog).val(),
					subinaddress : $('input[name=subinaddress]', dialog).val(),
					subvodaddress : $('input[name=subvodaddress]', dialog).val(),
					submanageurl : $('input[name=submanageurl]', dialog).val(),
					remarks : $('textarea[name=remarks]', dialog).val()
				};
				
				$.ajax(
				{
					type : 'PUT',
					url :urls,
					dataType : 'json',
					data : JSON.stringify(submitData),
					contentType : 'application/json',
					success : function(msg)
					{
						// 查询到的JSON对象
						if (msg.result == "0")
						{
							alertSuccess();
							// 关闭dialog
							$("#add_dialog-form").omDialog("close");
							
							// 清除红框样式
							$("input").removeClass("x-form-invalid");
							
							// 更新表格
							$('#cdn_channleoGrid').omGrid('reload');
						}
						else
						{
							alertFail();
						}
					}
				});
			}
		}
	};
	
	
	// 过滤字段
	var rules =
	{
		subareacode :
		{
			required : true
		},
		subinaddress :
		{
			required : true
		},
		submanageurl :
		{
			required : true
		}
	};
	
	// 提示消息
	var errMsgs =
	{
		subareacode :
		{
			required : cdn_i18n.prop('cdn_parameter_not_empty')
		},
		subinaddress :
		{
			required : cdn_i18n.prop('cdn_parameter_not_empty')
		},
		submanageurl :
		{
			required : cdn_i18n.prop('cdn_parameter_not_empty')
		}
	};
	
	// 调用验证方法，创建验证对象
	var validator = new form_validator(rules, errMsgs, 'errorMsg', 'userInfoForm');
     
});
