$(document).ready(function()
{
	parent.setIframeHeight("taskdis", 550);
	var isAdd = false;
	// 国际化
//	$('#cdn_nodeid').text(cdn_i18n.prop('cdn_nodeid') + i18n.prop('colon'));
//
//	$('#cdn_submit').text(cdn_i18n.prop('cdn_submit'));
//	$('#cdn_cancel').text(cdn_i18n.prop('cdn_cancel'));
	
	// 条件查询的面板
	var panel = $("#codition_div").omPanel(
	{
        iconCls : "panel_search",
        header : true,
        title : cdn_i18n.prop('cdn_search'),
        collapsed : false,
        collapsible : true
    });
	
	
	// 把查询的表单加入到面板当中
	$("#condition_query").append($("form[name='codition_form']"));
	
	// 修改图标
	$(".panel_search").removeClass("om-icon");
	$(".panel_search").removeClass("om-panel-icon");
	
	// 渲染查询按钮
	$("#cod_btn_search").omButton(
	{
		label : cdn_i18n.prop('usmc_user_search'),
		icons : 
		{
			left : '../../component/css/omui/default/images/search.png'
		},
		width : 80,
		onClick : function(event)
		{
			//var username = encodeURIComponent($.trim($('input[name="cod_abyEnbName"]').val()));
			//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
			// 按钮失去焦点
			//$(this).blur();
		}
	});
	
	// 回车事件
	// 点击“Enter”键，触发查询事件
	$(this).keydown(function(e)
	{
		if (e.which == "13")
		{
			if(document.getElementsByName('cod_abyEnbName')[0] == document.activeElement)
			{
				//var username = encodeURIComponent(encodeURIComponent($.trim($('input[name="cod_abyEnbName"]').val())));
				//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
				
				return true;
			}
		}
	});
	
	// 渲染清空按钮
	$("#cod_btn_clear").omButton(
	{
		label : cdn_i18n.prop('cdn_reset'),
		icons : 
		{
			left : '../../component/css/omui/default/images/clear.png'
		},
		width : 80,
		onClick : function(event)
		{
			//$('input[name=cod_abyEnbName]').val('');
			//$('input[name=cod_abyEnbName]').focus();

			// 按钮失去焦点
			$(this).blur();
		}
	});
	

	// 创建表格
	$("#cdn_nodeGrid").omGrid(
	{
		dataSource : "/api/v1/resource/taskdistribute",
		limit : Omui.TABLE_ROWS,
		height : Omui.TABLE_HEIGHT,
		singleSelect : true,
		showIndex : true,
		colModel :
		[
			{
				header : cdn_i18n.prop("cdn_citask_taskid"),
				name : 'taskid',
				width : 50,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_providerid"),
				name : 'providerid',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_assetid"),
				name : 'assetid',
				width : 100,
				align : 'left'
			},
			
		  {
				header : cdn_i18n.prop("cdn_citask_servicetype"),
				name : 'servicetype',
				width : 80,
				align : 'left',
				renderer : function(v, rowData , rowIndex)
				{
					          if("0" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_file");
                    }
                    else if("1" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_txt");
                    }
                    else if("2" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_image");
                    }
                    else if("3" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_hls");
                    }
                    else if("4" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_mp4");
                    }
                    else if("5" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_ngod");
                    }
                    else if("6" == v)
                    {
                        return cdn_i18n.prop("cdn_citask_ngod_hls");
                    }
				}
			},
					
			{
				header : cdn_i18n.prop("cdn_citask_url"),
				name : 'url',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_taskdis_subnodeaisaddr"),
				name : 'subnodeaisaddr',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_citask_status"),
				name : 'status',
				width : 60,
				align : 'left',
				renderer : function(v, rowData , rowIndex)
				{
					if("0" == v)
					{
						return cdn_i18n.prop("cdn_citask_wait");
					}
					else if("1" == v)
					{
						return cdn_i18n.prop("cdn_citask_transfer");
					}
					else if("2" == v)
					{
						return cdn_i18n.prop("cdn_citask_complete");
					}
				}
			},
			
			{
				header : cdn_i18n.prop("cdn_citask_progress"),
				name : 'progress',
				width : 60,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_citask_createtime"),
				name : 'createtime',
				width : 120,
				align : 'left',
				renderer : function(v, rowData, rowIndex)
				{
					return formatUTC(v*1000);
				}
			},
			{
				header : cdn_i18n.prop("cdn_citask_endtime"),
				name : 'endtime',
				width : 120,
				align : 'left',
				renderer : function(v, rowData, rowIndex)
				{
					return formatUTC(v*1000);
				}
			},
			
			{
				header : cdn_i18n.prop("cdn_citask_errcode"),
				name : 'errcode',
				width : 100,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_citask_errdes"),
				name : 'errdes',
				width : 100,
				align : 'left'
			}
//			,
//			{
//				header : cdn_i18n.prop("cdn_citask_errcode"),
//				name : 'errcode',
//				width : 100,
//				align : 'left'
//			}			
		],
		onRefresh : function(data, testStatus, XMLHttpRequest, event)
		{
			// 隐藏勾选框
			$(".checkboxCol").hide();
		}
	});
});
