$(document).ready(function()
{
	// 备份规则主键ID
	var backupRuleId = "";
	
	// 设置界面的i18n
	$("#tdRuleDay").html(i18n.prop('backupRule'));
	$("#tdRule").html(i18n.prop('backupRuleDay'));
	$("#lblRuleDay").html(i18n.prop('lblRuleDay'));
	$("#lblRuleHour").html(i18n.prop('lblRuleHour'));
	$("#saveTime").html(i18n.prop('saveTime'));
	$("#tdSaveTime").html(i18n.prop('tdSaveTime'));
	$("#lblDatabaseSaveDay").html(i18n.prop('lblDatabaseSaveDay'));
	
	// 查询备份规则
	var getRuleData = function()
	{
		$.ajax(
		{
			type : 'GET',
			url : '/api/sm/v1/DbBackupInfo',
			dataType : 'json',
			success : function(msg)
			{
				if (msg != null)
				{
					// 查询到的JSON对象
					//var json = msg[0];
					$('input[name=ruleDay]').val(msg.cycleTime);
					$('input[name=ruleHour]').val(msg.backupTime);
					$('input[name=databaseSaveDay]').val(msg.retainTime);
					backupRuleId = msg.ruleId;
				}
				else
				{
					$('input[name=ruleId]').val('');
					$('input[name=ruleDay]').val('');
					$('input[name=ruleHour]').val('');
					$('input[name=databaseSaveDay]').val('');
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 提示数据加载失败
				alertError();
			}
		});
	};
	
	
	// 初始化 
	getRuleData();
	
	// 验证规则
	var backupRules =
	{
		ruleDay :
		{
			required : true,
			isNum : true,
			range : [1, 30]
		},
		ruleHour :
		{
			required : true,
			isNum : true,
			range : [1, 24]
		},
		databaseSaveDay :
		{
			required : true,
			isNum : true,
			range : [7, 365]
		}
	};
	
	// 验证后的提示消息
	var backupMessages =
	{
		ruleDay :
		{
			required : i18n.prop('inputRequired'),
			isNum : i18n.prop('inputIsDigits'),
			range : i18n.prop('ruleDayRange')
		},
		ruleHour :
		{
			required : i18n.prop('inputRequired'),
			isNum : i18n.prop('inputIsDigits'),
			range : i18n.prop('ruleHourRange')
		},
		databaseSaveDay :
		{
			required : i18n.prop('inputRequired'),
			isNum : i18n.prop('inputIsDigits')  ,
			range : i18n.prop('inputRange7365')
		}
	};
	
	// 调用验证方法，创建验证对象
	var backupValidator = new form_validator(backupRules, backupMessages, 'errorMsg', 'backupRuleForm');
	
	// 校验页面输入框内容
	var checkBackupFormInput = function()
	{
		return backupValidator.form();
	};
	
	// 备份规则设置
	$("#setSaveTime").omButton(
	{
		label : i18n.prop('btnEdit'),
		icons :
		{
			left : '../images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 如果校验通过则提交数据到后台
			if (checkBackupFormInput())
			{
				$.omMessageBox.confirm(
				{
					title : i18n.prop('alert'),
					content : i18n.prop('applyUpdate'),
					onClose : function(value)
					{
						// 如果确认设置并校验通过则提交数据到后台
						if (value)
						{
							// 按天选择
							var cycleTime = $('input[name=ruleDay]').val();
							var backupTime = $('input[name=ruleHour]').val();
							var retainTime = $('input[name=databaseSaveDay]').val();
							
							// 提交的数据对象
							var submitValue =
							{
								id : 1,
								cycleTime : cycleTime,
								backupTime : backupTime,
								retainTime : retainTime
							};
							
							// 提交数据到后台
							$.ajax(
							{
								type : 'PUT',
								url : '/api/sm/v1/DbBackupInfo/{id}',
								contentType: "application/json",
								dataType : 'json',
								data : JSON.stringify(submitValue),
								success : function(msg)
								{
									if ("0" == msg.result)
									{
										alertSuccess();
									}
									else
									{
										alertFail();
									}
								}
							});
						}
					}
				});
			}
		}
	});
	
		
	// 刷新
	$("#refSaveTime").omButton(
	{
		label : i18n.prop('btRefresh'),
		icons :
		{
			left :  '../images/page_refresh.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 禁用刷新按钮
			$('#refSaveTime').omButton('disable');
			
			// 执行单击函数
			getRuleData();
			
			// 启用刷新按钮
			$('#refSaveTime').omButton('enable');
			
			// 按钮失去焦点
			this.blur();
		}
	});
});
