$(document).ready(function()
{
	parent.setIframeHeight("historyalarm", 550);
	var isAdd = false;
	// 国际化
//	$('#cdn_nodeid').text(cdn_i18n.prop('cdn_nodeid') + i18n.prop('colon'));
//
//	$('#cdn_submit').text(cdn_i18n.prop('cdn_submit'));
//	$('#cdn_cancel').text(cdn_i18n.prop('cdn_cancel'));
	
	// 条件查询的面板
	var panel = $("#codition_div").omPanel(
	{
        iconCls : "panel_search",
        header : true,
        title : cdn_i18n.prop('cdn_search'),
        collapsed : false,
        collapsible : true
    });
	
	
	// 把查询的表单加入到面板当中
	$("#condition_query").append($("form[name='codition_form']"));
	
	// 修改图标
	$(".panel_search").removeClass("om-icon");
	$(".panel_search").removeClass("om-panel-icon");
	
	// 渲染查询按钮
	$("#cod_btn_search").omButton(
	{
		label : cdn_i18n.prop('usmc_user_search'),
		icons : 
		{
			left : '../../component/css/omui/default/images/search.png'
		},
		width : 80,
		onClick : function(event)
		{
			//var username = encodeURIComponent($.trim($('input[name="cod_abyEnbName"]').val()));
			//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
			// 按钮失去焦点
			//$(this).blur();
		}
	});
	
	// 回车事件
	// 点击“Enter”键，触发查询事件
	$(this).keydown(function(e)
	{
		if (e.which == "13")
		{
			if(document.getElementsByName('cod_abyEnbName')[0] == document.activeElement)
			{
				//var username = encodeURIComponent(encodeURIComponent($.trim($('input[name="cod_abyEnbName"]').val())));
				//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
				
				return true;
			}
		}
	});
	
	// 渲染清空按钮
	$("#cod_btn_clear").omButton(
	{
		label : cdn_i18n.prop('cdn_reset'),
		icons : 
		{
			left : '../../component/css/omui/default/images/clear.png'
		},
		width : 80,
		onClick : function(event)
		{
			//$('input[name=cod_abyEnbName]').val('');
			//$('input[name=cod_abyEnbName]').focus();

			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	// 初始化botton按钮
	$('#buttonbar').omButtonbar(
	{
		btns :
		[
			{
				label : cdn_i18n.prop('cdn_delete'),
				id : "cdn_delete",
				icons :
				{
					left : '../../component/css/omui/default/images/del_hover.png'
				},
				onClick : function()
				{
					
					deleteChannle();
				}
			}
		]
	});
	
	// 删除信息
	var deleteChannle = function()
	{
		var selections = $('#cdn_nodeGrid').omGrid('getSelections', true);
		if (selections.length == 0)
		{
			alertUnCheckWarn();
		}
		else
		{
			$.omMessageBox.confirm(
			{
				title : i18n.prop("alert"),
				content : i18n.prop("deleteContent"),
				onClose : function(value)
				{
					if (value)
					{
						// 提交删除请求
						$.ajax(
						{
							type : 'DELETE',
							url : '/api/v1/resource/historyalarm/' + selections[0].id,
							dataType : 'json',
//							data : JSON.stringify(request),
							contentType : 'application/json',
							success : function(msg)
							{
								// 提示消息
								if (msg.result == "0")
								{
									// 更新表格
									$('#cdn_nodeGrid').omGrid('reload');
									
									// 删除成功提示
									alertSuccess();
								}
								else
								{
									// 删除失败提示
									alertFail();
								}
							}
						});
					}
				}
			});
		}
	};
	

	// 创建表格
	$("#cdn_nodeGrid").omGrid(
	{
		dataSource : "/api/v1/resource/historyalarm",
		limit : Omui.TABLE_ROWS,
		height : Omui.TABLE_HEIGHT,
		singleSelect : true,
		showIndex : true,
		colModel :
		[
			{
				header : cdn_i18n.prop("cdn_alarm_moduleid"),
				name : 'moduleid',
				width : 80,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_alarm_serviceid"),
				name : 'serviceid',
				width : 80,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_alarm_alarmid"),
				name : 'alarmid',
				width : 100,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_alarm_level"),
				name : 'alarmlevel',
				width : 100,
				align : 'left',
				renderer : function(v, rowData , rowIndex)
				{
					if("1" == v)
					{
						return cdn_i18n.prop("cdn_alarm_general");
					}
					else if("2" == v)
					{
						//return cdn_i18n.prop("cdn_alarm_info");
						return '<span style="color:green">' + cdn_i18n.prop("cdn_alarm_info") +' </span>';
					}
					else if("3" == v)
					{
						//return cdn_i18n.prop("cdn_alarm_warn");
						return '<span style="color:olive">' + cdn_i18n.prop("cdn_alarm_warn") +' </span>';
					}
					else if("4" == v)
					{
//						return cdn_i18n.prop("cdn_alarm_serious");
						return '<span style="color:sienna">' + cdn_i18n.prop("cdn_alarm_serious") +' </span>';
					}
					else if("5" == v)
					{
						return '<span style="color:red">' + cdn_i18n.prop("cdn_alarm_fatal") +' </span>';
					}
				}
			}
		   ,
					
			{
				header : cdn_i18n.prop("cdn_alarm_name"),
				name : 'alarmname',
				width : 120,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_alarm_starttime"),
				name : 'starttime',
				width : 150,
				align : 'left',
				renderer : function(v, rowData, rowIndex)
				{
					return formatUTC(v*1000);
				}
			},
			
			{
				header : cdn_i18n.prop("cdn_alarm_stoptime"),
				name : 'stoptime',
				width : 150,
				align : 'left',
				renderer : function(v, rowData, rowIndex)
				{
					return formatUTC(v*1000);
				}
			},
			{
				header : cdn_i18n.prop("cdn_alarm_describe"),
				name : 'alarmdes',
				width : 200,
				align : 'left'
			}
		],
		onRefresh : function(data, testStatus, XMLHttpRequest, event)
		{
			// 隐藏勾选框
			$(".checkboxCol").hide();
		}
	});
});
