$(document).ready(function()
{
	// 初始化页面
	var init = function()
	{
		$("#infoQueryGrid").omGrid(
		{
			limit : Omui.TABLE_ROWS,
			showIndex : true,
			height : Omui.TABLE_HEIGHT,
			autoFit : true,
			dataSource : '/api/sm/v1/SystemMonitorInfoResource/findAll',
			colModel :
			[
				{
					header : i18n.prop('memoryRate'),
					name : 'memoryUseRatio',
					width : 200,
					align : 'center'
				},
				{
					header : i18n.prop('diskRate'),
					name : 'diskUseRatio',
					width : 200,
					align : 'center'
				},
				{
					header : i18n.prop('cpuRate'),
					name : 'cpuUseRatio',
					width : 200,
					align : 'center'
				},
				{
					header : i18n.prop('monitorTime'),
					name : 'time',
					width : 200,
					align : 'center'
				}
			]
		});
	};
	
	// 初始化页面
	init();
	timers();
});

/**
 * 获取一条当前系统监控信息
 * @returns
 */
var getOneActiveMonitorInfo = function()
{
	// 随机数， 防止页面缓存
	var ss = Math.random();
	
	// 组装访问参数
	var data = 'id=' + ss;
	
	// 获取当前一条系统监控数据信息
	$.ajax(
	{
		type : 'GET',
		url : '/api/sm/v1/SystemMonitorInfoResource/findRecent',
		data : data,
		dataType : 'json',
		success : function(msg)
		{
			if(msg != null)
			{
				// CPU使用监控数据显示
				$("#cpuRate").html(msg.cpuUseRatio);
				$("#cpuRate").css("color", "#0000FFFF");
				changeDivBgColor("cpuRate", msg.cpuUseRatio1);
				
				// 内存使用监控数据显示
				$("#memoryRate").html(msg.memoryUseRatio);
				$("#memoryRate").css("color", "#0000FFFF");
				changeDivBgColor("memoryRate", msg.memoryUseRatio1);
				
				// 硬盘使用监控数据显示
				$("#diskRate").html(msg.diskUseRatio);
				$("#diskRate").css("color", "#0000FFFF");
				changeDivBgColor("diskRate", msg.diskUseRatio1);
			}
		}
	});
};

/**
 * 改变div层背景色的方法
 * @param keys
 * @param values
 */
function changeDivBgColor(keys, values)
{
	if (values <= 5)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 10)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 15)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 20)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 25)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 30)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 35)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 35)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 40)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 45)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 50)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 55)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 60)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#E8EEF7"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 65)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#BBFFCC"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 70)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#BBFFCC"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 75)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#BBFFCC"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 80)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#BBFFCC"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 85)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#BBFFCC"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 90)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#BBFFCC"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#E8EEF7"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else if (values <= 95)
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#BBFFCC"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#E8EEF7"
		});
	}
	else
	{
		$("#" + keys + "1").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "2").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "3").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "4").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "5").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "6").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "7").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "8").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "9").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "10").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "11").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "12").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "13").css(
		{
			"background-color" : "#BBFFCC"
		});
		
		$("#" + keys + "14").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "15").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "16").css(
		{
			"background-color" : "#BBFFCC"
		});
		$("#" + keys + "17").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "18").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "19").css(
		{
			"background-color" : "#FFA4A4"
		});
		$("#" + keys + "20").css(
		{
			"background-color" : "#FFA4A4"
		});
	}
}

/**
 * 系统监控数据定时器
 */
function timers()
{
	// 更新表格
	$("#infoQueryGrid").omGrid('reload');
	
	// 获取一条最新的系统监控数据
	getOneActiveMonitorInfo();
	
	// 设置定时器时间，并调用方法
	setTimeout(timers, 10000);
}
