uiframe
===============

uiframe

界面框架：
界面框架将各个模块及应用集成到一个统一的界面中，提供统一的操作菜单展现方式及调用入口，同时提供对各模块视图的生命周期控制等，支持模块间的导航、切换；提供了皮肤定制、国际化、登录、注销等功能；支持图标和文字的定制；自带了一些常用的组件包括：bootstrap、jquery、bootbox、font-awesome、freewall、select2、uniform 、data-table。