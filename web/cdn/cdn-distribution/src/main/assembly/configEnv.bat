@echo off
:: BatchGotAdmin 
:------------------------------------- 
REM --> Check for permissions 
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system" 

REM --> If error flag set, we do not have admin. 
if '%errorlevel%' NEQ '0' ( 
echo Requesting administrative privileges... 
goto UACPrompt 
) else ( goto gotAdmin ) 

:UACPrompt 
echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs" 
echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs" 

"%temp%\getadmin.vbs" 
exit /B 

:gotAdmin 
if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" ) 
pushd "%CD%" 
CD /D "%~dp0" 
echo **********Set Environment*********
set rootpath=%CD%
set class_path=.;
set java_path=%rootpath%\installer\jdk\jdk1.7.0_79
set mysql_path=%rootpath%\installer\mysql\mysql-RC
setx JAVA_HOME %java_path% /M
setx CLASSPATH %class_path% /M
set b=%rootpath%\installer\jdk\jdk
set c=%rootpath%\installer\mysql\mysql
set a=%PATH%

echo %a% | findstr "%b%"
if %errorlevel% equ 0 (
    echo "load success"
) else (
    goto sqlenv
)

:sqlenv
echo %a% | findstr "%c%
if %errorlevel% equ 0 (
    echo "load success"
) else (
    setx PATH "%%JAVA_HOME%%\bin;%%JAVA_HOME%%\jre\bin;%mysql_path%\bin;%PATH%" /M
)