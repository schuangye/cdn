#!/bin/bash

DIRNAME=`dirname $0`
RUNHOME=`cd $DIRNAME/; pwd`
echo @RUNHOME@ $RUNHOME
source /etc/profile
while true
do
    echo " WatchRuning Entering... "
    
    echo "\n\n### Starting USMC..."
    port=8204
    TCPListeningnum=`netstat -an | grep ":$port " | awk '$1 == "tcp" && $NF == "LISTEN" {print $0}' | wc -l`
    start=/var/rtms/usmc/run.sh
    stop=/var/rtms/usmc/stop.sh

    if [ $TCPListeningnum = 1 ]
    then
    {
       echo "The port is listening"
       echo "实例$Name 端口$port 正在监听。"
    }
    else
    {
        echo "The port is not listening"
        # $stop &
        $start &
    }
    fi

    echo "\n\n### Starting ITMSIUI..."
    port=8202
    TCPListeningnum=`netstat -an | grep ":$port " | awk '$1 == "tcp" && $NF == "LISTEN" {print $0}' | wc -l`
    start=/var/rtms/itmsiui/run.sh
    stop=/var/rtms/itmsiui/stop.sh

    if [ $TCPListeningnum = 1 ]
    then
    {
       echo "The port is listening"
       echo "实例$Name 端口$port 正在监听。"
    }
    else
    {
        echo "The port is not listening"
        # $stop &
        $start &
    }
    fi

    echo "\n\n### Starting OpenResty..."
    nginxID=`ps -ef |grep nginx | grep master | awk '{print $2}'`
    start=/var/rtms/startup.sh
    stop=/var/rtms/shutdown.sh

    if [  -z $nginxID ]
    then
    {
        echo " nginx is stopping "
        # $stop &
        $start &
    }
    else
    {
        echo " nginx is running... "
    }
    fi

    echo "\n\n### Starting Sm..."
    port=8208
    TCPListeningnum=`netstat -an | grep ":$port " | awk '$1 == "tcp" && $NF == "LISTEN" {print $0}' | wc -l`
    start=/var/rtms/sm/run.sh
    stop=/var/rtms/sm/stop.sh

    if [ $TCPListeningnum = 1 ]
    then
    {
       echo "The port is listening"
       echo "实例$Name 端口$port 正在监听。"
    }
    else
    {
        echo "The port is not listening"
        # $stop &
        $start &
    }
    fi

    echo "\n\n### Starting Rc..."
    port=8212
    TCPListeningnum=`netstat -an | grep ":$port " | awk '$1 == "tcp" && $NF == "LISTEN" {print $0}' | wc -l`
    start=/var/rtms/rc/run.sh
    stop=/var/rtms/rc/stop.sh

    if [ $TCPListeningnum = 1 ]
    then
    {
       echo "The port is listening"
       echo "实例$Name 端口$port 正在监听。"
    }
    else
    {
        echo "The port is not listening"
        #$stop &
        $start &
    }
    fi

    # echo "\n\n### Starting Wm..."
    port=8213
    TCPListeningnum=`netstat -an | grep ":$port " | awk '$1 == "tcp" && $NF == "LISTEN" {print $0}' | wc -l`
    start=/var/rtms/wm/run.sh
    stop=/var/rtms/wm/stop.sh

    if [ $TCPListeningnum = 1 ]
    then
    {
       echo "The port is listening"
       echo "实例$Name 端口$port 正在监听。"
    }
    else
    {
        echo "The port is not listening"
        #$stop &
        $start &
    }
    fi

    echo "\n\n### Starting Httpfs..."
    port=8214
    TCPListeningnum=`netstat -an | grep ":$port " | awk '$1 == "tcp" && $NF == "LISTEN" {print $0}' | wc -l`
    start=/var/rtms/httpfs/run.sh
    stop=/var/rtms/httpfs/stop.sh

    if [ $TCPListeningnum = 1 ]
    then
    {
       echo "The port is listening"
       echo "实例$Name 端口$port 正在监听。"
    }
    else
    {
        echo "The port is not listening"
        #$stop &
        $start &
    }
    fi

    echo "\n\n### Starting Tr069..."
    port=8210
    TCPListeningnum=`netstat -an | grep ":$port " | awk '$1 == "tcp" && $NF == "LISTEN" {print $0}' | wc -l`
    start=/var/rtms/tr069/run.sh
    stop=/var/rtms/tr069/stop.sh

    if [ $TCPListeningnum = 1 ]
    then
    {
       echo "The port is listening"
       echo "实例$Name 端口$port 正在监听。"
    }
    else
    {
        echo "The port is not listening"
        #$stop &
        $start &
    }
    fi
    
    echo "\n\n### Starting Stun..."
    port=8215
    TCPListeningnum=`netstat -an | grep ":$port " | awk '$1 == "tcp" && $NF == "LISTEN" {print $0}' | wc -l`
    start=/var/rtms/stun/run.sh
    stop=/var/rtms/stun/stop.sh

    if [ $TCPListeningnum = 1 ]
    then
    {
       echo "The port is listening"
       echo "实例$Name 端口$port 正在监听。"
    }
    else
    {
        echo "The port is not listening"
        #$stop &
        $start &
    }
    fi    
    sleep 30
done
exit 0