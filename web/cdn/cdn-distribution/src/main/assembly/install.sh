#!/bin/bash
RUNHOME=`pwd`
RTMS_DIR=/var/rtms
echo @RUNHOME@ $RUNHOME

read -p "Please confirm whether install JDK (y|n)" choice_jdk
case $choice_jdk in
	n|N)
	    echo "\n\n### jump over install JDK..."
		cd $RUNHOME
        cd ./installer/jdk
		cp -rf ${RTMS_DIR}_backup/installer/jdk/jdk-linux-1.7.0.79 ./
	;;
    *)
	    echo "\n\n### Starting install JDK..."
        cd $RUNHOME
        cd ./installer/jdk
        source ./install_jdk.sh
    ;;
esac


exit 0