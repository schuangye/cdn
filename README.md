# cdn

#### 介绍
此CDN是一个融合CDN，支持广电标准(NGOD)，以及互联网OTT标准，主要特性如下：

1. 支持NGOD标准
2. 支持HLS协议
3. 支持防盗链
4. 支持实时录制，回看
5. 支持在线转码，统一内容格式
6. 内容节点实时监控
7. 智能预分发

#### 软件架构
 **系统架构** 
   ![输入图片说明](image_1image.png)

 **部署图** 
![输入图片说明](image_2image.png)

 **核心技术** 

![输入图片说明](image_8image.png)

#### 安装教程

1.  后台编译
    1)进入代码cdn\build目录执行. build_linux.sh -re -debug[release]
    2)在cdn\version目录就会生成对应的版本包
2.  web管理系统编译
    1)进入web\目录执行build.bat

#### 使用说明
管理系统截图：
![输入图片说明](image_3image.png)
![输入图片说明](image_4image.png)
![输入图片说明](image_5image.png)
![输入图片说明](image_6image.png)
![输入图片说明](image_7image.png)

#### 技术支持
  ![输入图片说明](wchatimage.png)