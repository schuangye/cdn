#!/bin/sh
##############定义打印#####################
ECHO_RED()
{
    echo -e "\033[41;37m$1 \033[0m"
}
ECHO_YELLOW()
{
    echo -e "\033[43;30m$1 \033[0m"
}
ECHO_BLUE()
{
    echo -e "\033[44;37m$1 \033[0m"
}
##############启动服务#####################
cd bin
chmod +x ./*

#启动release版本
sh start.sh mainservice
ret=$?
if [ $ret -ne 0 ];then
  #文件存在
	if [ $ret -ne 2 ];then
  	ECHO_RED "start mainservice failed.ret:"$ret
  	exit 1
  fi
else
  exit 0;
fi

#启动debug版本
sh start.sh mainserviced
if [ $? -ne 0 ];then
  ECHO_RED "start mainserviced failed"
  exit 1
fi

