#ifndef LIB_CLS_DLL_H
#define LIB_CLS_DLL_H

#ifdef  WIN32
	#ifdef LIBCLS_STATIC
		#define LIBCLS_EXPORT
	#else
		#ifdef LIBCLS_EXPORTS
			#define LIBCLS_EXPORT __declspec(dllexport)
		#else
			#define LIBCLS_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBCLS_EXPORT
#endif


#define  LIBCLS_API  extern "C" LIBCLS_EXPORT

#endif

