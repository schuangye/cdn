#ifndef LIB_CLS_MACROS_H
#define LIB_CLS_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libclsmacros.h
*  Description:   cls模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
//cls缓存类型
#define CLS_CACHE_TYPE                            "ClsCacheType"  

//直播相关参数
#define CLS_SHIFT_TIME_DURATION                   "ClsShiftTimeDuration" 
#define CLS_SHIFT_TIME_DELAYNUM                   "ClsShiftTimeDelayNum"   
#define CLS_SHIFT_TIME_RESERVE_NUM                "ClsShiftTimeReserveNum"
#define CLS_SQL_STATEMENT_FILE                    "ClsSqlStatementFile"
#define CLS_INCSYN_INTERVAL                       "ClsIncSynInterval"
#define CLS_ALLSYN_INTERVAL                       "ClsAllSynInterval"
#define CLS_SYN_NODE_INTERVAL                     "ClsSynNodeInterval"
#define CLS_USE_STREAMADDR_TYPE                   "ClsUseStreamAddrType"

#define CLS_TOKENKEY                              "ClsTokenKey"
#define CLS_IS_CHECK_TOKEN                        "ClsIsCheckToken"
#define CLS_TOKEN_ALGORITHMIC                     "ClsTokenAlgorithmic"
/*******************************************************************************
*                              定义常量
*******************************************************************************/
#define CLS_CACHE_TYPE_DB             0
#define CLS_CACHE_TYPE_MEN            1
#define CLS_CACHE_TYPE_REDIS          2

//使用内外网地址
#define CLS_USE_PRIVATEADDR_TYPE           0
#define CLS_USE_PUBLICADDR_TYPE            1

//直播类型
#define TYPE_LIVE                0
#define TYPE_SHIFT_TIME          1
#define TYPE_LOOK_BACK           2

//是否开启token校验
#define CHECKTOKEN_DISABLE       0
#define CHECKTOKEN_ENABLE        1

//token算法
#define  TOKENALGORITHMIC_URL              0      
#define  TOKENALGORITHMIC_URL_TRYPLAY      1
/*******************************************************************************
*                              时移m3u8宏
*******************************************************************************/
#define SUB_M3U8_HEAD     "#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-TARGETDURATION:%d\n#EXT-X-MEDIA-SEQUENCE:%lld\n"
#define M3U8_FILE_SPLICE  "#EXTINF:%d,\n%s%s_%s/%lld.ts\n"
#define M3U8_END_LIST     "#EXT-X-ENDLIST\n"
/*******************************************************************************
*                              ngod
*******************************************************************************/
#define Locate_Response      "<LocateResponse>\r\n"\
                             "	<TransferPort>%s</TransferPort>\r\n"\
	                         "	<TransferID>%s</TransferID>\r\n"\
	                         "	<TransferTimeout>500</TransferTimeout>\r\n"\
	                         "	<AvailableRange>0-%lld</AvailableRange>\r\n"\
	                         "</LocateResponse>"


#endif


