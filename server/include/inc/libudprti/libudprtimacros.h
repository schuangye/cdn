#ifndef LIB_HLS_RTI_MACROS_H
#define LIB_HLS_RTI_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libudprtimacros.h
*  Description:   udprti模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/

/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define UDP_RTI_LOCAL_IP               "UDPRtiLocalIP"
#define UDP_RTI_REVBUFF_SIZE           "UDPRtiRevBuffSize"

/*******************************************************************************
*                              名宏定义
*******************************************************************************/
#define DEFAULT_REVBUFF_SIZE                            192512


#endif
