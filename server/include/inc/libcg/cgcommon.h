#ifndef CG_COMMON_H
#define CG_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cgcommon.h
*  Description:  cg常见类型定义
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/

//上报类型
typedef enum ReportType
{
	REPORT_ADD          = 0,		//添加文件

	REPORT_UPDATE       = 1,	    //修改文件

	REPORT_DELETE       = 2			//删除文件
}ReportType;

//上报文件结构
typedef struct ReportFileItem
{
	std::string      m_FileName;
	std::string      m_FilePath;
	UInt64           m_FileSize;
	Int32            m_IsOpen;
	ReportType       m_ReportType;
	ReportFileItem()
	{
		m_FilePath = "";
		m_FileName = "";
		m_FileSize = 0;
		m_IsOpen = 0;
		m_ReportType = REPORT_ADD;
	}

	ReportFileItem(const ReportFileItem &other)
	{
		m_FileName   = other.m_FileName;
		m_FilePath   = other.m_FilePath;
		m_FileSize   = other.m_FileSize;
		m_IsOpen     = other.m_IsOpen;
		m_ReportType = other.m_ReportType;
	}

	ReportFileItem &operator =(const ReportFileItem &other)
	{
		m_FileName   = other.m_FileName;
		m_FilePath   = other.m_FilePath;
		m_FileSize   = other.m_FileSize;
		m_IsOpen     = other.m_IsOpen;
		m_ReportType = other.m_ReportType;

		return *this;
	}
}ReportFileItem;




#endif


