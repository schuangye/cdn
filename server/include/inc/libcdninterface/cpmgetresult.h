#ifndef CPM_GET_RESULT_H
#define CPM_GET_RESULT_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cpmgetresult.h
*  Description:  查询文件注入状态
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/libcdninterface_dll.h"
#include "libhttpinterface/httpjsonmessage.h" 
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/cdnifcommon.h"
#include "libcore/cstring.h"

#include <list>

//请求消息
class LIBCDNINTERFACE_EXPORT CCPMGetResultReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCPMGetResultReq)

	//定义providerID
    HTTP_JSON_CHAR_DEFINE(providerID, 64)

	//定义assetID
	HTTP_JSON_CHAR_DEFINE(assetID, 64)

	//定义contentType
	HTTP_JSON_INT32_DEFINE(contentType)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//响应消息
class LIBCDNINTERFACE_EXPORT CCPMGetResultRsp : public CHttpJsonMessageRsp
{
	HTTP_JSON_CLASS_DEFINE(CCPMGetResultRsp)

	//文件列表
	HTTP_JSON_STRUCT_DEFINE(CPMIngestResult,  Result)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};


#endif


