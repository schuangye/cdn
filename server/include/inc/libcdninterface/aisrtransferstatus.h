#ifndef AIS_RTRANSFER_CONTENT_H
#define AIS_RTRANSFER_CONTENT_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    aisrtransferstatus.h
*  Description:  上报注入结果
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/libcdninterface_dll.h"
#include "libcdninterface/aisdeletecontent.h"
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/cdnifcommon.h"
#include <string.h>


class LIBCDNINTERFACE_EXPORT CAISRTransferStatusReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CAISRTransferStatusReq)

	//定义result
	HTTP_JSON_STRUCT_DEFINE(CIGetStatusResult, result)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//使用默认返回
typedef CHttpJsonMessageRsp   CAISRTransferStatusRsp;


#endif



