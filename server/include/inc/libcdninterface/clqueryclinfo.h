#ifndef CL_QUERY_CL_INFO_H
#define CL_QUERY_CL_INFO_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clqueryclinfo.h
*  Description:  查询cl信息
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/libcdninterface_dll.h"
#include "libhttpinterface/httpjsonmessage.h" 
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/cdnifcommon.h"


class LIBCDNINTERFACE_EXPORT CCLQueryCLInfoReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCLQueryCLInfoReq)

	//定义节点
	HTTP_JSON_CHAR_DEFINE(nodeID,  64)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//响应消息
class LIBCDNINTERFACE_EXPORT CCLQueryCLInfoRsp : public CHttpJsonMessageRsp
{
	HTTP_JSON_CLASS_DEFINE(CCLQueryCLInfoRsp)

	//文件列表
	HTTP_JSON_STRUCT_DEFINE(CLInfoResult,  result)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

#endif


