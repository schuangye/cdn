#ifndef CLS_REGISTER_H
#define CLS_REGISTER_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsregister.h
*  Description:  cls注册接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpinterface/httpjsonmessage.h" 
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/libcdninterface_dll.h"
#include <string.h>

class LIBCDNINTERFACE_EXPORT CCLSRegisterReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCLSRegisterReq)

	//节点名称
	HTTP_JSON_CHAR_DEFINE(NodeName, 64)

	//私网推流ip
	HTTP_JSON_CHAR_DEFINE(PrivateStreamAddress, 64)

	//公网推流ip
	HTTP_JSON_CHAR_DEFINE(PublicStreamAddress,  64)

	//总带宽
	HTTP_JSON_INT64_DEFINE(TotalBand)

	//空闲带宽
	HTTP_JSON_INT64_DEFINE(FreeBand)

	//区域码
	HTTP_JSON_CHAR_DEFINE(AreaCode, 64)

	//优先级
	HTTP_JSON_INT32_DEFINE(Priority)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//使用默认返回
typedef CHttpJsonMessageRsp   CCLSRegisterRsp;

#endif



