#ifndef CG_ADD_FILE_H
#define CG_ADD_FILE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cgaddfile.h
*  Description:  cg添加文件上报接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpinterface/httpjsonmessage.h" 
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/cdnifcommon.h"
#include "libcdninterface/libcdninterface_dll.h"



class LIBCDNINTERFACE_EXPORT CCLSAddFileReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCLSAddFileReq)
    
	//节点名称
	HTTP_JSON_CHAR_DEFINE(NodeName, 64)

	//文件列表
	HTTP_JSON_LIST_DEFINE(AddFileInf,  FileList)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//使用默认返回
typedef CHttpJsonMessageRsp   CCLSAddFileRsp;

//升级文件接口
typedef CCLSAddFileReq          CCLSUpdateFileReq;
typedef CHttpJsonMessageRsp     CCLSUpdateFileRsp;

#endif


