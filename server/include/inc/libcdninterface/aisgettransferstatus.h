#ifndef AIS_GET_TRANSFER_STATUS_H
#define AIS_GET_TRANSFER_STATUS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    aisgettransferstatus.h
*  Description:  查询内容注入状态
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/libcdninterface_dll.h"
#include "libcdninterface/aisdeletecontent.h"
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/cdnifcommon.h"
#include <string.h>

//定义请求
typedef CAISDeleteContentReq   CAISGetTransferStatusReq;

//定义响应接口
class LIBCDNINTERFACE_EXPORT CAISGetTransferStatusRsp : public CHttpJsonMessageRsp
{
	HTTP_JSON_CLASS_DEFINE(CAISGetTransferStatusRsp)
		
	//定义result
    HTTP_JSON_STRUCT_DEFINE(CIGetStatusResult, result)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

#endif

