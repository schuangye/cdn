#ifndef CDN_HTTP_TASK_BASE_H
#define CDN_HTTP_TASK_BASE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cdnhttptaskbase.h
*  Description:  cdn基础httptask
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpinterface/httpjsonmessage.h" 
#include "libhttpserver/httptaskbase.h"
#include "libcdnutil/libcdnutil_dll.h"

class LIBCDNUTIL_EXPORT CCDNHttpTaskBase : public CHttpTaskBase
{
public:

	CCDNHttpTaskBase();

	virtual ~CCDNHttpTaskBase();

protected:

	//填充http头
	virtual void FillRspHttpHead(CHTTPResponse *rsp, const char *server = NULL);

	//设置包体
	virtual void FillRspBody(CHTTPResponse *rsp, CHttpJsonMessageRsp *jsonrsp);
};


#endif

