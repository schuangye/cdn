#ifndef CDN_ALARM_DEFINE_H
#define CDN_ALARM_DEFINE_H


/******************************************************************************/
/********************************CDN_ALARM_BASE********************************/
/******************************************************************************/
#define CDN_ALARM_BASE                                          1000
#define CDN_ALARM_STREAM_INTERRUPT                              CDN_ALARM_BASE+1
#define CDN_ALARM_NO_CG_NODE                                    CDN_ALARM_BASE+2

#endif
