#ifndef INGEST_IF_REGISTER_MACROS_H
#define INGEST_IF_REGISTER_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingestifregistermacros.h
*  Description:   注入任务宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/citaskinterfacemgr.h"

//注册注入任务处理实现类接口
#define INGEST_TASK_REGISTER(contentType, ingestimp)\
		class CreateIngestTaskRegister##ingestimp\
		{\
		public:\
			CreateIngestTaskRegister##ingestimp()\
			{\
				CCITaskInterfaceMgr::Intstance()->RegisterCITask(contentType, #ingestimp);\
			}\
		};\
		CreateIngestTaskRegister##ingestimp  g_CreateIngestTaskRegister##ingestimp;

//注册注入任务处理接口以及动态创建类
#define INGEST_TASK_REGISTER_DYNCREATE(contentType, ingestimp, baseclass)\
		CONF_IMPLEMENT_DYNCREATE(ingestimp, baseclass)\
		INGEST_TASK_REGISTER(contentType, ingestimp)

#endif



