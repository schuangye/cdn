#ifndef REDIS_NODE_DATAOBJECT_H
#define REDIS_NODE_DATAOBJECT_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    redisnodedataobject.h
*  Description:  节点管理数据对象
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libredis/redisdataobject.h"
#include "libcdnutil/libcdnutil_dll.h"
#include "libcdnutil/cdncommon.h"


class LIBCDNUTIL_EXPORT CRedisNodeDataObject : public CRedisDataObject
{
	CONF_DECLARE_DYNCREATE(CRedisNodeDataObject)
public:

	CRedisNodeDataObject();

	virtual ~CRedisNodeDataObject();

	//将结构数据编码成连续数据，buff返回编码后数据指针，返回buff数据长度
	virtual Int32 Encode(char *&buff);

	//将buff数据转码成结构数据,返回成功失败,0:成功，非零失败
	virtual Int32 Decode(const char *buff, Int32 len);

	//添加节点列表
	void AddCGNode(CGNodeItem nodevec);

	//获取节点信息
	CGNodeItem GetCGNode();

private:

	CGNodeItem      m_NodeItem;
};

#endif



