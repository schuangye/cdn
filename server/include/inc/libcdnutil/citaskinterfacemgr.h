#ifndef CI_TASK_INTERFACE_MGR_H
#define CI_TASK_INTERFACE_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    citaskinterfacemgr.h
*  Description:  注入任务接口管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/citaskinterface.h"
#include "libcdnutil/libcdnutil_dll.h"
#include "libutil/singleton.h"

#include <map>
#include <list>
#include <string>

typedef std::map<Int32, std::string >                  IntStringMap;
typedef std::list<CCITaskInterface*>                   CITaskIFList;
typedef std::map<Int32, CITaskIFList >                 IntIngestIFMap;

class LIBCDNUTIL_EXPORT CCITaskInterfaceMgr
{
	DECLARATION_SINGLETON(CCITaskInterfaceMgr)
public:

	//注册接口任务
	Int32 RegisterCITask(Int32 contentType,const char *taskname);

	//创建注入任务
	CCITaskInterface* CreateCITask(Int32 contentType);

	//释放注入任务
	void ReleaseCITask(Int32 contentType, CCITaskInterface *task);

protected:

	//获取缓存的接口
	CCITaskInterface* GetCacheCITask(Int32 contentType);

	//通过内容类型创建注入类型缓存接口
	CCITaskInterface* CreateCITaskFromConType(Int32 contentType);

private:

	OSMutex                m_MutexConTask;
	IntStringMap           m_ConTypeTaskNameMap;

	OSMutex                m_Mutex;
	IntIngestIFMap         m_IntIngestTaskMap;
};

#endif



