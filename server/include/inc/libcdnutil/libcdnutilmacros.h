#ifndef LIB_CDN_UTIL_MACROS_H
#define LIB_CDN_UTIL_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:   libcdnutilmacros.h
*  Description:   cdnutil模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
/*******************************************************************************
*                              定义常量
*******************************************************************************/
//注入消息命令
#define CMD_TRANSFER                    0      //注入
#define CMD_CANCEL                      1      //取消
#define CMD_DELETE                      2      //删除

#define TASK_IDLE                       0      //空闲
#define TASK_BUSY                       1      //忙

#define CANCEL_TASK                     1      //取消
/*******************************************************************************
*                              redis键值
*******************************************************************************/
#define  FILE_DISTRIBUTE_SET       "FileDistributeSet:"


/*******************************************************************************
*                              HLS协议定义
*******************************************************************************/
//主m3u8
#define EXT_M3U                         "#EXTM3U"
#define EXT_X_STREAM_INF                "#EXT-X-STREAM-INF"
#define PROGRAM_ID                      "PROGRAM-ID="
#define BANDWIDTH                       "BANDWIDTH="

//子m3u8
#define EXT_X_VERSION                   "#EXT-X-VERSION:"
#define EXT_X_ALLOW_CACHE               "#EXT-X-ALLOW-CACHE:"
#define EXT_X_TARGETDURATION            "#EXT-X-TARGETDURATION:"
#define EXT_X_MEDIA_SEQUENCE            "#EXT-X-MEDIA-SEQUENCE:"
#define EXTINF                          "#EXTINF:"
#define EXT_X_ENDLIST                   "#EXT-X-ENDLIST"
/*******************************************************************************
*                              常量定义
*******************************************************************************/

//定义临时下载m3u8空间大小
#define TMP_DOWN_M3U8_BUFF_SIZE         2*1024*1024
#define TMP_DOWN_DATA_BUFF_SIZE         15*1024*1024

//hls处理缓存大小
#define  CAHCE_WRITE_SIZE               4096*188

//http下载超时时间
#define HTTP_DOWNLOAD_TIMEOUT            30000

#endif



