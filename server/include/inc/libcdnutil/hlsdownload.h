#ifndef HLS_DOWNLOAD_H
#define HLS_DOWNLOAD_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsdownload.h
*  Description:  hls客户端下载
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcdnutil/cdncommon.h"
#include "libcdnutil/libcdnutil_dll.h"
#include "libcdnutil/libcdnutilmacros.h"
#include <list>
#include <string>


class LIBCDNUTIL_EXPORT CHLSDownload
{
public:

    CHLSDownload();

    virtual ~CHLSDownload();

	/*初始化hls下载，
	 url：m3u8地址
	 tmpdatabuffsize：临时下载数据buff大小
	 tmpm3u8buffsize:下载m3u8 buff大小, 
	 onececount：一次循环下载数据文件个数
	*/
    virtual Int32 Init(const char *url, Int32 tmpdatabuffsize, Int32 tmpm3u8buffsize, 
		               Int32 onececount = 0, Int32 timeout = 30000);

    //开始下载,index表示子m3u8在主m3u8位置
    SInt32  StartDownLoad(SInt32 index = 0);

    //判断主url m3u8类型
    M3u8Type  GetMainUrlM3u8Type();

	//获取码率数
	SInt32 GetBitRateCount();

protected:

	//刷新m3u8
    virtual Int32 RefreshSubM3U8();

	//下载数据
	virtual Int32 DownLoadData();

	//保存下载数据
    virtual Int32 SaveMediaData(const char *buff, SInt32 len);

	//加载m3u8
	virtual Int32 LoadM3U8(const char *url);

	//解析m3u8
    virtual Int32 ParaseM3u8(const char *m3u8);

	//解析主m3u8
	virtual Int32 ParaseMainM3U8(const char *m3u8);

	//解析子m3u8
	virtual Int32 ParaseSubM3U8(const char *m3u8, SInt32 index = 0);

	//检查m3u8类型
	M3u8Type CheckM3u8Type(const char *m3u8);

	//添加分片
	virtual void AddSplitList(HLSSlipInfoList srcsplitlist, HLSSlipInfoList &distsplitlist);

	//判读分片连接是否存在
	Bool IsExistSplitUrl(const HLSSlipInfoList splitlist, const std::string url);

protected:

	MainMediaStreamVector            m_MainMediaStreamVector;

	SInt32                           m_CurIndex; //当前下载索引 

    M3u8Type                         m_M3u8Type;

    std::string                      m_Url;

	std::string                      m_DomainUrl;

	char                             *m_TmpM3U8Buff;  //下载m3u8临时buff
	Int32                            m_M3U8BuffSize;   

	char                             *m_TmpDataBuff;  //下载数据临时buff
	Int32                            m_DataBuffSize;  //下载数据buff大小

	Int32                            m_OneceCount;  //一次下载的数量

	Int32                            m_DownLoadTimeOut;  //下载超时时间

	HLSSlipInfoList                  m_FinishSlipList; //完成的分片列表
};

#endif
