#ifndef LIB_HLS_RTI_MACROS_H
#define LIB_HLS_RTI_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libhlsrtimacros.h
*  Description:   hlsrti模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/

/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define HLS_RTI_DATA_BUFF_SIZE                "HlsRtiDataBuffSize"
#define HLS_RTI_M3U8_BUFF_SIZE                "HlsRtim3u8BuffSize"
#define HLS_RTI_REFRESH_TIME                  "HlsRtiRefreshTime"
#define HLS_ONECE_DOWNLOAD_NUM                "HlsOneceDownLoadNum"
#define HLS_SUB_M3U8_INDEX                    "HlsSubM3U8Index"
#define HLS_DOWNLOAD_TIMEOUT                  "HlsDownLoadTimeOut"

#endif
