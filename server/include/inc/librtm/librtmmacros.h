#ifndef LIB_RTM_MACROS_H
#define LIB_RTM_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    librtmmacros.h
*  Description:   ci模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/libcdnutilmacros.h"

#define LIB_RTM_ID        "librtm"
/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define RTM_NODE_ID                                 "RTMNodeID"
#define RTM_SHELL_CMD                               "RTMShellCmd"
#define RTM_CLEAR_EXPIRE_FILE_TIME                  "RTMClearExpireFileTime"
#define RTM_PRIVATE_STREAM_ADDR                     "RTMPrivateStreamAddr"
#define RTM_PUBLIC_STREAM_ADDR                      "RTMPublicStreamAddr"
#define RTM_NETWORK_CARD_CONF_FILE                  "RTMNetWorkCard"
#define RTM_SQL_STATEMENT_FILE                      "RTMSqlStatementFile"

#endif



