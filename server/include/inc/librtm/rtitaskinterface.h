#ifndef RTI_TASK_INTERFACE_H
#define RTI_TASK_INTERFACE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rtitaskinterface.h
*  Description: 
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/RuntimeObj.h"
#include "libutil/OSTypes.h"
#include "librtm/librtm_dll.h"
#include "librtm/rtmcommon.h"

class LIBRTM_EXPORT CRTITaskInterface : public RuntimeObject
{
	CONF_DECLARE_DYNCREATE(CRTITaskInterface)
public:

	CRTITaskInterface();

	virtual ~CRTITaskInterface();

	//直播任务初始化,返回非0表示失败
	virtual SInt32 Init(const LiveChannelItem &liveitem);

	//开启直播任务,返回非0表示失败
	virtual SInt32 StartRecordTask();

	//停止直播任务,返回非0表示失败
	virtual SInt32 StopRecordTask();

	//检查直播任务的状态,在录制返回TRUE, 否则返回FALSE
	virtual Bool CheckRecordTaskStatus();

	//检查是否需要重启,在录制返回TRUE, 否则返回FALSE
	virtual Bool IsReStartRecordTask();

	//清理多余文件
	virtual void ClearExpireFile();

	//设置流中断
	virtual void SetStreamInterruput();

	//设置流恢复
	virtual void SetStreamResume();

	//获取流中断 TRUE:表示中断  FALSE:表示未中断
	virtual Bool GetStreamInterruput();

protected:
	LiveChannelItem            m_LiveChannel;

	std::string		           m_StoreFullPath;      //总存储路径

	SInt32                     m_AllSliceCount;      //总分片数

	Bool                       m_IsStreamInterruput;  //是否流中断
};

#endif