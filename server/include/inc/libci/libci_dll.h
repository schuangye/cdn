#ifndef LIB_CI_DLL_H
#define LIB_CI_DLL_H

#ifdef  WIN32
	#ifdef LIBCI_STATIC
		#define LIBCI_EXPORT
	#else
		#ifdef LIBCI_EXPORTS
			#define LIBCI_EXPORT __declspec(dllexport)
		#else
			#define LIBCI_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBCI_EXPORT
#endif


#define  LIBCI_API  extern "C" LIBCI_EXPORT

#endif

