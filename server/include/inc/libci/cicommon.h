#ifndef CL_COMMON_H
#define CL_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clcommon.h
*  Description:  cl公共结构
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcore/cstring.h"
#include "libcdnutil/cdncommon.h"
#include "libci/libcimacros.h"
#include <map>
#include <list>
#include <string>

typedef  struct  CINode_Stru
{
	std::string     m_NodeID;
	std::string     m_PrivateStreamAddr;
	std::string     m_PublicStreamAddr;
	std::string     m_Path;
	SInt64          m_AllDiskSize;
	SInt64          m_UseDiskSize;
	SInt64          m_ReserDiskSize;
	SInt64          m_TotalBandWidth;
	SInt64          m_FreeBandWidth;

	CINode_Stru()
	{
		m_NodeID = "";
		m_PrivateStreamAddr  = "";
		m_PublicStreamAddr   = "";
		m_Path      = "";
		m_AllDiskSize = 0;
		m_UseDiskSize = 0;
		m_ReserDiskSize   = 0;
		m_TotalBandWidth = 0;
		m_FreeBandWidth  = 0;
	}

	CINode_Stru(const CINode_Stru &other)
	{
		m_NodeID      = other.m_NodeID;
		m_PrivateStreamAddr  = other.m_PrivateStreamAddr;
		m_PublicStreamAddr   = other.m_PublicStreamAddr;
		m_Path        = other.m_Path;
		m_AllDiskSize = other.m_AllDiskSize;
		m_UseDiskSize = other.m_UseDiskSize;
		m_ReserDiskSize   = other.m_ReserDiskSize;
		m_TotalBandWidth = other.m_TotalBandWidth;
		m_FreeBandWidth   = other.m_FreeBandWidth;
	}

	CINode_Stru &operator= (const CINode_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_NodeID      = other.m_NodeID;
		m_PrivateStreamAddr  = other.m_PrivateStreamAddr;
		m_PublicStreamAddr   = other.m_PublicStreamAddr;
		m_Path        = other.m_Path;
		m_AllDiskSize = other.m_AllDiskSize;
		m_UseDiskSize = other.m_UseDiskSize;
		m_ReserDiskSize   = other.m_ReserDiskSize;
		m_TotalBandWidth = other.m_TotalBandWidth;
		m_FreeBandWidth   = other.m_FreeBandWidth;

		return *this;
	}
}CINode;

typedef struct CIDiskPath_Stru
{
	std::string    m_PathName;
	SInt64	       m_ReserveSize;

	CIDiskPath_Stru()
	{
		m_PathName   = "";
		m_ReserveSize = 0;
	}

	CIDiskPath_Stru(const CIDiskPath_Stru &other)
	{
		m_PathName      = other.m_PathName;
		m_ReserveSize   = other.m_ReserveSize;
	}

	CIDiskPath_Stru &operator= (const CIDiskPath_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_PathName      = other.m_PathName;
		m_ReserveSize   = other.m_ReserveSize;

		return *this;
	}
}CIDiskPath;
typedef std::list<CIDiskPath >     CIDiskPathList;

//上报文件结构
typedef struct FileItem_Stru
{
	std::string      m_FileName;
	std::string      m_FilePath;
	UInt64           m_FileSize;
	Int32            m_IsOpen;

	FileItem_Stru()
	{
		m_FilePath = "";
		m_FileName = "";
		m_FileSize = 0;
		m_IsOpen = 0;
	}

	FileItem_Stru(const FileItem_Stru &other)
	{
		m_FileName   = other.m_FileName;
		m_FilePath   = other.m_FilePath;
		m_FileSize   = other.m_FileSize;
		m_IsOpen     = other.m_IsOpen;
	}

	FileItem_Stru &operator =(const FileItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}
		m_FileName   = other.m_FileName;
		m_FilePath   = other.m_FilePath;
		m_FileSize   = other.m_FileSize;
		m_IsOpen     = other.m_IsOpen;

		return *this;
	}
}FileItem;
typedef std::list<FileItem>                FileItemList;
typedef std::map<std::string, FileItem >   FileItemMap;

//文件分布
typedef struct FileDistribute_Stru
{
	std::string     m_ProviderID;
	std::string     m_AssetID;
	std::string     m_FileName;
	std::string     m_Path;
	std::string     m_NodeID;

	FileDistribute_Stru()
	{
		m_ProviderID = "";
		m_AssetID = "";
		m_FileName = "";
		m_NodeID = "";
		m_Path = "";
	}

	FileDistribute_Stru(const FileDistribute_Stru &other)
	{
		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_FileName = other.m_FileName;
		m_Path = other.m_Path;
		m_NodeID = other.m_NodeID;
	}

	FileDistribute_Stru &operator=(const FileDistribute_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}
		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_FileName = other.m_FileName;
		m_Path = other.m_Path;
		m_NodeID = other.m_NodeID;

		return *this;
	}
}FileDistribute;
typedef std::list<FileDistribute>   FileDistributeList;

typedef std::map<std::string, Int64 >             StringIntMap;
typedef std::pair<std::string, Int64 >            StringIntPair;
typedef std::map<std::string, StringIntMap >      PathFileMap;

#endif


