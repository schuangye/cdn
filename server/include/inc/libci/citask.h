#ifndef CI_TASK_H
#define CI_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    citask.h
*  Description:  通用注入任务处理，只注入文件不做任务文件处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/citaskifregistermacros.h"
#include "libcdnutil/citaskinterface.h"
#include "libci/cicommon.h"
#include "libci/libci_dll.h"

class CCITask : public CCITaskInterface
{
	CONF_DECLARE_DYNCREATE(CCITask)
public:

	CCITask();

	virtual ~CCITask();

	//处理注入消息,不同的注入类型对应不同的注入接口
	virtual Int32 StartTask(void *taskitem);

	//删除内容接口
	virtual Int32 DeleteContent(void *taskitem);

protected:

	//获取文件
	std::string  GetFileName(const IngestTaskItem *task); 

	//下载源文件
	virtual Int32 DownLoadFile(IngestTaskItem *task);
};

#endif

