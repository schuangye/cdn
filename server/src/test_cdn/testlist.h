#include <list>
#include <vector>
#include "libhttpjsonif/libhttpjsonif_dll.h"


//注入数据结构
typedef struct IngestResult_struct
{
	char         m_FileName[64]; //定义文件名
	int        m_Result;        //注入状态

	IngestResult_struct()
	{
		memset(m_FileName, 0, sizeof(m_FileName));
		m_Result = 0;
	}

	IngestResult_struct(const IngestResult_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		m_Result = other.m_Result;
	}

	IngestResult_struct &operator =(const IngestResult_struct &other)
	{
		if (this == &other)
		{
			return *this;
		}

		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		m_Result = other.m_Result;

		return *this;
	}
}IngestResult;

class LIBHTTPJSONIF_EXPORT TestList
{
public: 
	TestList();
	~TestList() {}

	std::list<IngestResult> GetList();

	std::vector<IngestResult> Getvector();

private:

	std::list<IngestResult>   m_list;
	std::vector<IngestResult>  m_Vector;

};