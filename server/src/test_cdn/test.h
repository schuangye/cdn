#include "libcdnutil/cdnhttptaskbase.h"
#include "librtm/rtitaskinterface.h"

class CTestTask : public CCDNHttpTaskBase
{
public:

	CTestTask();

	~CTestTask();

protected:

	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);
};

class CTestCRTITaskInterface : public CRTITaskInterface
{
	CONF_DECLARE_DYNCREATE(CTestCRTITaskInterface)
public:

	CTestCRTITaskInterface();

	virtual ~CTestCRTITaskInterface();

	//开启直播任务
	virtual SInt32 StartRecordTask();

	//停止直播任务
	virtual SInt32	StopRecordTask();
};



