/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cachespacemgr.cpp
*  Description:  get file or path attribute info.
*  Others:
*  Version: 1.0       Author:scy 907818       Date: 2014-04-30
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy    2014-04-30       1.0         Original

*******************************************************************************/
#include "libconf/ParamManager.h"
#include "libconf/logmodule.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libcg/libcgmacros.h"
#include "disk/cachespacemgr.h"

IMPLEMENT_SINGLETON(CCacheSpaceMgr)
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCacheSpaceMgr::CCacheSpaceMgr()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCacheSpaceMgr::~CCacheSpaceMgr()
{
	this->StopTask();
}
/*******************************************************************************
*  Function   : Initialize()
*  Description: 初始化文件管理
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCacheSpaceMgr::Initialize()
{
	//磁盘保留空间百分比
	m_CacheSpaceScale = CParamManager::Intstance()->GetIntParam(CG_CACHE_SPACE_SCALE, 10);

	//获取缓存目录
	const char *tmpCacheDir = CParamManager::Intstance()->GetCharParam(CG_CACHE_DIR, "./cache/");
	std::vector<std::string > line;
	STR::ReadLinesExt(tmpCacheDir, line, ",");

	m_Mutex.Lock();
	std::vector<std::string >::iterator it = line.begin();
	for (; it != line.end(); it++)
	{
		CacheSpace cache;
		Int32 ret = RefreshCacheSpace(it->c_str(), cache);
		if (ret == 0)
		{
			m_CacheList.push_back(cache);
		}
		else
		{
			ERROR_LOG("InitDiskSpace fail.%s.ret:%d", it->c_str(), ret)
		}
	}

	//cache为空返回FALSE
	if (m_CacheList.empty())
	{
		m_Mutex.Unlock();
		return FALSE;
	}
	m_Mutex.Unlock();

	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("Run task fail.")
		return FALSE;
	}
	
	return TRUE;
}

/*******************************************************************************
*  Function   : Run()
*  Description: 执行磁盘管理任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCacheSpaceMgr::Run()
{
	while(TRUE)
	{
		if (IsNormalQuit())
		{
			return TRUE;
		}

		//清除空间
		ClearCacheSpace();

		OS::Sleep(5000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : GetCachePath
*  Description: 获取存储路径
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
std::string CCacheSpaceMgr::GetCachePath()
{
	m_Mutex.Lock();
	if (m_CacheList.empty())
	{
		m_Mutex.Unlock();
		return "";
	}

	CacheSpace  tmpcache;
	std::list<CacheSpace >::iterator it = m_CacheList.begin();
	tmpcache = *it;
	it++;
	for (; it != m_CacheList.end(); it++)
	{
		if (tmpcache.m_CacheFreeSpace < it->m_CacheFreeSpace)
		{
			tmpcache = *it;
		}
	}
	m_Mutex.Unlock();

	return tmpcache.m_CachePath;
}
/*******************************************************************************
*  Function   : ClearDiskSpace
*  Description: 清理磁盘
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCacheSpaceMgr::ClearCacheSpace()
{
	std::list<CacheSpace >::iterator it = m_CacheList.begin();
	for (; it != m_CacheList.end(); it++)
	{
		ClearCache(*it);
	}

	return;
}
/*******************************************************************************
*  Function   : ClearCache()
*  Description: 执行删除操作
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCacheSpaceMgr::ClearCache(CacheSpace &cache)
{
	//获取磁盘空间信息
	if (!IsCleanFile(cache))
	{
		return 0;
	}

	std::list<FileTypeAttr> filelist;
	std::list<DirTypeAttr> dirlist;

	//获取路径下的文件列表
	Int32 ret = DiskInfo::GetPathAll(cache.m_CachePath.c_str(), filelist, dirlist);
	if (ret != 0)
	{
		ERROR_LOG("check disk %s fail. ret:%d", cache.m_CachePath.c_str(), ret)
		return -1;
	}

	if (filelist.empty())
	{
		return 0;
	}

	//遍历删除文件
	while(filelist.empty())
	{
		FileTypeAttr  tmpfileattr = filelist.front();

		//清除文件
		filelist.pop_front();

		std::string tmpfile = cache.m_CachePath + std::string(tmpfileattr.filename);
		OS::DelFile(tmpfile.c_str());

		//检查是否需要再清理文件
		if (!IsCleanFile(cache))
		{
			return 0;
		}
	}

	return 0;
}
/*******************************************************************************
*  Function   : RefreshCacheSpace
*  Description: 初始化磁盘信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCacheSpaceMgr::RefreshCacheSpace(const char *diskpath, CacheSpace &cache)
{
	//获取磁盘空间信息
	SpaceInfo pathinfo;
	if (DiskInfo::GetDiskSpaceInfo(cache.m_CachePath.c_str(), pathinfo) != TRUE)
	{
		INFO_LOG("GetDiskSpaceInfo() filed. this path:%s", cache.m_CachePath.c_str())
		return -1;
	}

	cache.m_CachePath = std::string(diskpath);
	cache.m_CacheTotalSpace   = (pathinfo.totalsize/(UInt64)1024);
	cache.m_CacheReserveSpace = (cache.m_CacheTotalSpace * m_CacheSpaceScale)/100;
	cache.m_CacheFreeSpace    = (pathinfo.canusesize/(UInt64)1024);

    return 0;
}
/*******************************************************************************
*  Function   : IsCleanFile
*  Description: 检查是否需要清除文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCacheSpaceMgr::IsCleanFile(CacheSpace &cache)
{
	//刷新cache空间
	if (RefreshCacheSpace(cache.m_CachePath.c_str(), cache))
	{
		return FALSE;
	}

	if (cache.m_CacheReserveSpace < cache.m_CacheFreeSpace)
	{
		return FALSE;
	}

	return TRUE;
}

