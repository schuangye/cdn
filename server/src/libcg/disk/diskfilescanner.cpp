/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    diskcscanner.cpp
*  Description:  磁盘文件扫描
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libconf/ParamManager.h"
#include "libconf/logmodule.h"
#include "libutil/stringtool.h"
#include "libcg/libcgmacros.h"
#include "diskfilescanner.h"
#include <sys/stat.h>
#ifndef WIN32
	#include <sys/inotify.h>
	#include <sys/select.h>
#endif

IMPLEMENT_SINGLETON(CDiskFileScanner)
/*******************************************************************************
*  Function   : CDiskFileScanner
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CDiskFileScanner::CDiskFileScanner()
{
	m_LoadPathInterval = 0;
	m_LastLoadPathTime = 0;
	m_LastAllScanTime = 0;
}
/*******************************************************************************
*  Function   : ~CDiskFileScanner
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CDiskFileScanner::~CDiskFileScanner()
{
	//停止线程
	this->StopTask();

	//清除监听事件
	m_ScanMutex.Lock();
	std::map<std::string, Int32 >::iterator it2 = m_MapNotify.begin();
	for (; it2 != m_MapNotify.end(); it2++)
	{
#ifndef WIN32
		close(it2->second);
#endif
	}
	m_MapNotify.clear();
	m_ScanMutex.Unlock();
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CDiskFileScanner::Initialize()
{
	//获取加载扫描路径时间间隔
	m_LoadPathInterval = CParamManager::Intstance()->GetIntParam(LOAD_SCAN_PATH_INTERVAL, 30)*1000;

	//采用间隔扫描
	m_ScanMode = CParamManager::Intstance()->GetIntParam(SCAN_MODE, SCAN_MODE_TIMING);

	//全量扫描时间间隔
	m_AllScanInterval = CParamManager::Intstance()->GetIntParam(ALL_SCAN_INTERVAL, DEFAULT_ALL_SCAN_INTERVAL)*1000;

	//加载配置文件
	const char *scanpath = CParamManager::Intstance()->GetCharParam(CG_SCAN_PATH, "./");
	std::vector<std::string > line;
	STR::ReadLinesExt(scanpath, line, ",");

	std::vector<std::string >::iterator it = line.begin();

	m_ScanMutex.Lock();
	for (; it != line.end(); it++)
	{
		m_ScanPathTimeMap[*it] = 0;
	}
	m_ScanMutex.Unlock();


	return RunTask();
}
/*******************************************************************************
*  Function   : SetSynAll
*  Description: 设置全量同步
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::SetScanAll()
{
	m_PathFileTimeMap.clear();
	StringIntMap::iterator it = m_ScanPathTimeMap.begin();
	for (; it != m_ScanPathTimeMap.end(); it++)
	{
		it->second = 0;
	}

	//清除监听事件
	std::map<std::string, Int32 >::iterator it2 = m_MapNotify.begin();
	for (; it2 != m_MapNotify.end(); it2++)
	{
#ifndef WIN32
		close(it2->second);
#endif
	}
	m_MapNotify.clear();

	return;
}
/*******************************************************************************
*  Function   : Run
*  Description: OSTask的执行函数, 执行具体的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CDiskFileScanner::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//加载配置文件
		LoadScanPathFromCfg();

		//扫描文件
		ScanFileFromDisk();

		//检查改变
		CheckChangeNotify();

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : CheckChangeNotify
*  Description: 检查文件改变通知
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::CheckChangeNotify()
{
#ifndef WIN32
	std::map<std::string, Int32 >::iterator it = m_MapNotify.begin();
	for (;it != m_MapNotify.end(); it++)
	{
		int fd = it->second;
		struct inotify_event* ie = NULL;
		Int32 iCount	= 0;
		Int32 iLen	= 0;
		char inotifyBuf[1024] = {0};
		fd_set rfds;
		struct timeval tv = {0, 200000};
		FD_ZERO(&rfds);
		FD_SET(fd, &rfds);
		iCount = select(fd+1, &rfds, NULL, NULL, &tv);
		if (iCount == 0) 
		{
			continue;
		}

		if (iCount < 0)
		{
			if (errno == EINTR)
			{
				continue;
			}
			else
			{
				break;
			}
		}

		iLen = read(fd, inotifyBuf, 1024);
		Int32 iIdx = 0;
		while (iIdx < iLen)
		{
			ie = (struct inotify_event *)(inotifyBuf+iIdx);
			printf("inotifys: idx: %d - wd: %d - mask: %X - fileName:%s\n", iIdx, ie->wd, ie->mask, ie->name);
			iIdx = iIdx + sizeof(inotify_event) + ie->len;

			std::string fullName = it->first + ie->name;

			ReportFileItem  fileitem;
			fileitem.m_FileName = ie->name;
			fileitem.m_FilePath = it->first;	

			UInt64 lasttime = 0;
			fileitem.m_FileSize = OS::GetFileSize(fullName.c_str(), &lasttime);			

			if(ie->mask == 0x200 || ie->mask == 0x40)//200 删除， 40 移除
			{
				fileitem.m_ReportType = REPORT_DELETE;

				AddFileToCache(fileitem, lasttime);
				CFileReportMgr::Intstance()->AddReportFileItem(fileitem);
			}
			else if(ie->mask == 0x100 || ie->mask == 0x80) //100创建文件，80移入
			{
				fileitem.m_ReportType = REPORT_ADD;
				fileitem.m_IsOpen = 1;

				//加入缓存
				AddFileToCache(fileitem, lasttime);

				//发送数据
				CFileReportMgr::Intstance()->AddReportFileItem(fileitem);
			}
			else if (ie->mask == 0x08)
			{
				fileitem.m_ReportType = REPORT_UPDATE;
				fileitem.m_IsOpen = 1;

				//加入缓存
				AddFileToCache(fileitem, lasttime);

				//发送数据
				CFileReportMgr::Intstance()->AddReportFileItem(fileitem);
			}
		}
	}
#endif

	return;
}
/*******************************************************************************
*  Function   : ScanFileFromDisk
*  Description: 同步扫描路径
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::ScanFileFromDisk()
{
	m_ScanMutex.Lock();
#ifdef WIN32
	DirModScan();
#else
	DirNotifyScan();
#endif	

	m_ScanMutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : DirModScan
*  Description: 按照目录修改时间扫描
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::DirModScan()
{
	Int64 current = OS::Milliseconds();

	//判断是否全量更新
	if ((SCAN_MODE_TIMING == m_ScanMode) && (current - m_LastAllScanTime > m_AllScanInterval))
	{
		SetScanAll();
		m_LastAllScanTime = current;
	}
	
	//扫描路径
	std::map<std::string, Int64>::iterator it = m_ScanPathTimeMap.begin();
	for (; it != m_ScanPathTimeMap.end(); it++)
	{
		std::string tmpdir = it->first;
		
		//判断目录是否存在变化
		Int64 modtime = OS::ModTimeOfFile(tmpdir.c_str())*1000;
		if (it->second != modtime)
		{
			OS::DirScanner(*this, tmpdir);
			it->second = modtime;
		}
	}

	return;
}
/*******************************************************************************
*  Function   : DirNotifyScan
*  Description: 目录改变通知方式扫描
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::DirNotifyScan()
{
	Int64 current = OS::Milliseconds();

	//判断是否全量更新
	if ((SCAN_MODE_TIMING == m_ScanMode) && (current - m_LastAllScanTime > m_AllScanInterval))
	{
		SetScanAll();
		m_LastAllScanTime = current;
	}

	//扫描路径
	std::map<std::string, Int64>::iterator it = m_ScanPathTimeMap.begin();
	for (; it != m_ScanPathTimeMap.end(); it++)
	{
		std::string tmpdir = it->first;
		std::map<std::string, Int32 >::iterator it3 = m_MapNotify.find(tmpdir);
		if (it3 == m_MapNotify.end())
		{
			//添加目录监听
			InitChangeNotify(it->first);

			//扫描文件
			OS::DirScanner(*this, tmpdir);

			continue;
		}
	}
}
/*******************************************************************************
*  Function   : InitChangeNotify
*  Description: 添加监听时间
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::InitChangeNotify(const std::string &strDir)
{
	//扫描路径
	std::map<std::string, Int32 >::iterator it = m_MapNotify.find(strDir);
	if (it == m_MapNotify.end())
	{
#ifndef WIN32
		Int32 fd = inotify_init();
		if (fd > 0)
		{
			int watchfd = inotify_add_watch(fd, strDir.c_str(), IN_DELETE|IN_MOVED_FROM|
				                            IN_MOVED_TO|IN_CREATE|IN_CLOSE_WRITE);
			if(watchfd  == -1)
			{
				ERROR_LOG("inotify_add_watch  failed. %s errno:%d", strDir.c_str(), errno);
			}
			else
			{
				INFO_LOG("inotify_add_watch success. %s fd:%d watchfd:%d", strDir.c_str(), fd, watchfd);
				m_MapNotify[strDir] = fd;
			}
		}
		else
		{
			ERROR_LOG("inotify_add_watch  failed. fd < 0 .strDir:%s errno:",strDir, errno);
		}		
#endif
	}

	return;
}
/*******************************************************************************
*  Function   : CloseChangeNotify
*  Description: 关闭文件改变通知
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::CloseChangeNotify(const std::string &strDir)
{
	//停止监听
	std::map<std::string, Int32 >::iterator it = m_MapNotify.find(strDir);
	if (it != m_MapNotify.end())
	{
#ifndef WIN32
		close(it->second);
#endif
		m_MapNotify.erase(it);
	}
}
/*******************************************************************************
*  Function   : LoadScanPathFromCfg
*  Description: 加载配置文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::LoadScanPathFromCfg()
{
	if (OS::Milliseconds() - m_LastLoadPathTime < m_LoadPathInterval)
	{
		return;
	}
	
	//加载配置文件
	const char *scanpath = CParamManager::Intstance()->GetCharParam(CG_SCAN_PATH, "./");
	std::vector<std::string > line;
	STR::ReadLinesExt(scanpath, line, ",");

	StringIntMap  tmpmap;
	m_ScanMutex.Lock();
	std::vector<std::string >::iterator it = line.begin();
	for (; it != line.end(); it++)
	{
		StringIntMap::iterator tmpit = m_ScanPathTimeMap.find(*it);
		if (tmpit != m_ScanPathTimeMap.end())
		{
			tmpmap[tmpit->first] = tmpit->second;
			m_ScanPathTimeMap.erase(tmpit);
		}
		else
		{
			tmpmap[*it] = 0;
		}
	}
	
	//清除删除的目录
	StringIntMap::iterator it2 = m_ScanPathTimeMap.begin();
	for (; it2 != m_ScanPathTimeMap.end(); it2++)
	{
		PathFileMap::iterator it3 = m_PathFileTimeMap.find(it2->first);
		if(it3 != m_PathFileTimeMap.end())
		{
			m_PathFileTimeMap.erase(it3);
		}
	}

	//复制新目录
	m_ScanPathTimeMap.clear();
	StringIntMap::iterator it4 = tmpmap.begin();
	for(; it4 != tmpmap.end(); it4++)
	{
		m_ScanPathTimeMap[it4->first] = it4->second;
	}

	m_ScanMutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : HandleFile
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::HandleFile(const std::string &rootpath, const std::string &subpath, 
								  const std::string &filename)
{	
	std::string fullPath = rootpath + filename;
	UInt64 lastTime = 0;
	Int64 fileSize = OS::GetFileSize(fullPath.c_str(), &lastTime);

    //目录或者文件太小
	if(fileSize <= 0)
	{
		return;
	}

	//确认文件是否需要上报
	StringIntMap::iterator itfile;
	PathFileMap::iterator it = m_PathFileTimeMap.find(rootpath);
	if (it != m_PathFileTimeMap.end())
	{
		itfile = it->second.find(filename);
		if (itfile != it->second.end())
		{
			//修改时间没有变化
			if (itfile->second == lastTime)
			{
				return;
			}
		}
	}

	ReportFileItem  fileitem;
	fileitem.m_FileName = filename;
	fileitem.m_FilePath = WindowsPathToLinuxPath(rootpath);
	fileitem.m_FileSize = fileSize;

	//确认文件是否关闭
	if(OS::Milliseconds() - lastTime*1000 > 10*1000)
	{
		fileitem.m_IsOpen = 0;
	}
	else
	{
		fileitem.m_IsOpen = 1;
	}

	//确认文件是否存在
	if (it == m_PathFileTimeMap.end())
	{
		fileitem.m_ReportType = REPORT_ADD;
		m_PathFileTimeMap[rootpath].insert(StringIntPair(filename, lastTime));
	}
	else
	{
		if (itfile != it->second.end())   //修改文件
		{
			fileitem.m_ReportType = REPORT_UPDATE;
			itfile->second = (Int64)lastTime;
		}
		else
		{
			fileitem.m_ReportType = REPORT_ADD;
			it->second.insert(StringIntPair(filename, lastTime));
		}
	}

	//上报文件
	CFileReportMgr::Intstance()->AddReportFileItem(fileitem);

	INFO_LOG("Source scanner HandleFile: ");
}
/*******************************************************************************
*  Function   : AddFileToCache
*  Description: 添加数据到缓存
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDiskFileScanner::AddFileToCache(const ReportFileItem &reportfile, UInt64 lasttime)
{
	//移除缓存
	PathFileMap::iterator itpath = m_PathFileTimeMap.find(reportfile.m_FilePath);
	if (itpath != m_PathFileTimeMap.end())
	{
		StringIntMap::iterator tmpit = itpath->second.find(reportfile.m_FileName);
		if (tmpit != itpath->second.end())
		{
			itpath->second.erase(tmpit);
		}

		itpath->second.insert(StringIntPair(reportfile.m_FileName, lasttime));
	}
	else
	{
		m_PathFileTimeMap[reportfile.m_FilePath].insert(StringIntPair(reportfile.m_FileName, lasttime));
	}

	return;
}
/*******************************************************************************
*  Function   : WindowsPathToLinuxPath
*  Description: windows文件路径转换
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
std::string CDiskFileScanner::WindowsPathToLinuxPath(const std::string &winpath)
{
	if (winpath.empty())
	{
		return "";
	}

	//判断是否为windows路径
	if (winpath.find(":") == std::string::npos && winpath.find("\\") == std::string::npos)
	{
		return winpath;
	}

	std::string tmppath = winpath;
	tmppath = tmppath.replace(1, 3, "/");
	Int32 pos = tmppath.find("\\");
	while(pos != std::string::npos)
	{
		tmppath = tmppath.replace(pos, 2, "/");
		pos = tmppath.find("\\");
	}

	return "/" + tmppath;
}

