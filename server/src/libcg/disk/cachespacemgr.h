#ifndef CACHE_SPACE_MGR_H
#define CACHE_SPACE_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cachespacemgr.h
*  Description:  get file or path attribute info.
*  Others:
*  Version: 1.0       Author:Davsion 908217       Date: 2014-04-30
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           Davsion    2014-04-30       1.0         Original

*******************************************************************************/
#include "libconf/OSThread.h"
#include "libutil/diskinfo.h"
#include "libutil/singleton.h"

class CCacheSpaceMgr : public OSTask
{
	DECLARATION_SINGLETON(CCacheSpaceMgr)
public:

	//初始化
	virtual Bool Initialize();

	//执行磁盘管理线程
	virtual Bool Run();

	//获取存储路径
	std::string GetCachePath();

protected:

	typedef struct CacheSpaceStruc
	{
		std::string       m_CachePath;
		Int64             m_CacheTotalSpace;      //cache总空间，单位为MB
		Int64             m_CacheFreeSpace;       //空闲cache大小
		Int64             m_CacheReserveSpace;    //cache剩余空间，单位为MB
		CacheSpaceStruc()
		{
			m_CachePath         = "";
			m_CacheTotalSpace   = 0;
			m_CacheFreeSpace    = 0;
			m_CacheReserveSpace = 0;

		}

		CacheSpaceStruc(const CacheSpaceStruc &other)
		{
			m_CachePath         = other.m_CachePath;
			m_CacheTotalSpace   = other.m_CacheTotalSpace;
			m_CacheFreeSpace    = other.m_CacheFreeSpace;
			m_CacheReserveSpace = other.m_CacheReserveSpace;
		}

		CacheSpaceStruc &operator =(const CacheSpaceStruc &other)
		{
			m_CachePath         = other.m_CachePath;
			m_CacheTotalSpace   = other.m_CacheTotalSpace;
			m_CacheFreeSpace    = other.m_CacheFreeSpace;
			m_CacheReserveSpace = other.m_CacheReserveSpace;

			return *this;
		}
	}CacheSpace;

	//清理磁盘
	void ClearCacheSpace();

	//一次清除操作
	Int32 ClearCache(CacheSpace &cache);

	//刷新cache信息
	Int32 RefreshCacheSpace(const char *diskpath, CacheSpace &cache);

	//检查是否需要清除文件
	Bool IsCleanFile(CacheSpace &cache);

private:
	OSMutex                      m_Mutex;
	std::list<CacheSpace >       m_CacheList;

	Int32                        m_CacheSpaceScale;
};

#endif //_DISK_MGR_H_

