/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libclsmodule.cpp
*  Description:  libcls模块初始化类
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libmodulemgr/modulemac.h"
#include "disk/diskfilescanner.h"
#include "disk/filereportmgr.h"
#include "disk/cachespacemgr.h"
#include "libcg/libcg_dll.h"
#include "httpproxy/httpproxymgr.h"
#include "cgtimetask.h"

REGISTER_MODULE(libcg, LIBCG_EXPORT)
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化libcg模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libcg::Initialize(const char *argv)
{		
	//启动定时任务
	Bool ret = CCGTimeTask::Intstance()->Initialize();
	if (!ret)
	{
		printf("Init CFileReportMgr fail.\n");
		return -1;
	}

	//初始化文件上报
	ret = CFileReportMgr::Intstance()->Initialize();
	if (!ret)
	{
		printf("Init CFileReportMgr fail.\n");
		return -2;
	}

	//初始化磁盘扫描
	ret = CDiskFileScanner::Intstance()->Initialize();
	if (!ret)
	{
		printf("Init CDiskFileScanner fail.");
		return -3;
	}

	//初始化cache扫描
	if (!CCacheSpaceMgr::Intstance()->Initialize())
	{
		printf("Init CCacheSpaceMgr fail.");
		return -4;
	}

	//初始化代理
	CHttpProxyMgr::Intstance()->Init();
	if (!ret)
	{
		printf("Init CHttpProxyMgr fail.");
		return -5;
	}

	return 0;
}
/*******************************************************************************
*  Function   : Release
*  Description: 退出libcg模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libcg::Release()
{
	CDiskFileScanner::Intstance()->Destroy();
	CFileReportMgr::Intstance()->Destroy();
	CCGTimeTask::Intstance()->Destroy();
	CCacheSpaceMgr::Intstance()->Destroy();
	CHttpProxyMgr::Intstance()->Destroy();
	return 0;
}





