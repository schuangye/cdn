#ifndef LIB_UDP_RTI_STATIC_CONF_H
#define LIB_UDP_RTI_STATIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsrtistaicconf.h
*  Description:  rti静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include <string>

class CLibUDPRTIStaticConf : public OSTask
{
	DECLARATION_SINGLETON(CLibUDPRTIStaticConf)
public:

	//初始化线程
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//绑定本地IP地址
	static std::string						  m_LocalIP;

	//接收缓存大小
	static SInt32                             m_RevBuffSize;

protected:

	//同步参数
	void SynStaticParam();

};
#endif



