/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: udpsource.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : 直播频道管理
       
*******************************************************************************/
#ifndef WIN32
#include <sys/poll.h>
#endif
#include "libcore/OSTool.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "task/udpsource.h"
/*******************************************************************************
 Fuction name:CUDPSource
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CUDPSource::CUDPSource()
{
	m_UDPSocket    = NULL;
	m_HLSVideo     = NULL;
	m_RevBuff      = NULL;
	m_RevBuffSize  = DEFAULT_REVBUFF_SIZE;
}
/*******************************************************************************
 Fuction name:CUDPSource
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CUDPSource::~CUDPSource()
{
	if (m_UDPSocket != NULL)
	{
		m_UDPSocket->LeaveMulticast(m_UrlAddr.GetIP(), m_LocaleIP.GetIP());
		delete m_UDPSocket;
		m_UDPSocket = NULL;
	}

	m_HLSVideo = NULL;
}
/*******************************************************************************
 Fuction name:Init
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CUDPSource::Init(const char *url, const char *localip, CHLSVideo *hlsvideo, 
	                   Int32 buffsize)
{
	if (url == NULL || localip == NULL || hlsvideo == NULL)
	{
		return -1;
	}

	//
	std::string tmpurl = STR::ToUpperStr(url);
	if (tmpurl.find("UDP://") == std::string::npos)
	{
		return -2;
	}

	//获取udp地址
	std::string udpaddr = STR::GetKeyValue(tmpurl.c_str(), "UDP://", "/");
	m_UrlAddr.SetAddr(udpaddr.c_str());

	m_LocaleIP.SetAddr(localip);
	m_UDPSocket = new UDPSocket();
	if (m_UDPSocket->Open() != OS_ok)
	{
		return -3;
	}

	m_UDPSocket->ReuseAddr();
	m_UDPSocket->SetSocketRcvBufSize(1316*4);
	OS_Error ret = m_UDPSocket->SetMulticastInterface(m_LocaleIP.GetIP());
	if (m_UrlAddr.IsMultiCastAddr())
	{
		m_UDPSocket->SetTtl(10);

		//const int loopback = 0; //禁止回馈
		//LibConf::SetSockopt(m_UDPSocket->GetSocketFD(),IPPROTO_IP,IP_MULTICAST_LOOP,(char*)&loopback,sizeof(loopback));

		ret = m_UDPSocket->JoinMulticast(m_UrlAddr.GetIP(), m_LocaleIP.GetIP());
		if (ret != OS_ok)
		{
			return -4;
		}
	}

	//绑定网卡
	ret = m_UDPSocket->Bind(m_UrlAddr.GetIP(), m_UrlAddr.GetPort());
	if (ret != OS_ok)
	{
		return -5;
	}

	//分配缓存
	if (m_RevBuff == NULL)
	{
		//buff大小不能小于188
		if (buffsize > 188)
		{
			m_RevBuffSize = buffsize;
		}
		
		m_RevBuff = (UChar*)malloc(m_RevBuffSize);
		if (m_RevBuff == NULL)
		{
			return -6;
		}
	}

	m_HLSVideo = hlsvideo;

	return 0;
}
/*******************************************************************************
 Fuction name:Receive
 Description :接收数据
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CUDPSource::ReceiveDate()
{
	Int32 result = 0;
#ifndef WIN32

	struct pollfd pfds;

	pfds.fd = m_UDPSocket->GetSocketFD();
	if (pfds.fd < 1)
	{
		LOG_ERROR("pfds.fd is error");
		return 0;
	}
	pfds.events	= POLLIN;
	pfds.revents	= 0;

	//result = ::poll(&(m_UDPSocket->GetSocketFD()), 1, 100);

	result = ::poll(&pfds, 1, 100);
	if (result == 0)
	{
		return 0;
	}
	else if (result == -1)
	{
		if (errno == EINTR)
		{
			return 0;
		}

		LOG_ERROR("poll() failed. errno:"<< errno<<",strerror is "<<strerror(errno));
	}
#endif
	
	UInt32 outRecvLen = 0;
	result = m_UDPSocket->RecvFrom(m_RevBuff, m_RevBuffSize, &outRecvLen);
	if (outRecvLen > 0)
	{
		m_HLSVideo->ProcessData(m_RevBuff, outRecvLen);
	}
	
	return 0;
}














