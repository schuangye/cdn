#ifndef UDP_LIVE_RECORD_H
#define UDP_LIVE_RECORD_H
/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: udpliverecord.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : udp录制管理
       
*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcore/OSThread.h"
#include "task/udpsource.h"

class CUDPLiveRecord : public OSTask
{
public:

	CUDPLiveRecord();

	virtual ~CUDPLiveRecord();

	//初始化
	Int32 Init(const char *url, const char *localip, const char *storpath, Int32 splitdur =10);

	//线程调用函数
	virtual Bool Run();

	//更新接收数据
	Int32 RefreshData();

private:

	CUDPSource           *m_UdpSource;

	CHLSVideo           *m_HLSVideo;
};

#endif












