/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    librtimodule.cpp
*  Description:  librti模块初始化类
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libmodulemgr/modulemac.h"
#include "libdb/sqlstatementmgr.h"
#include "libudprtistaticconf.h"
#include "libudprti/libudprti_dll.h"

REGISTER_MODULE(libudprti, LIBUDPRTI_EXPORT)
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化libci模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libudprti::Initialize(const char *argv)
{	
	//初始化配置参数
	if (CLibUDPRTIStaticConf::Intstance()->Initialize() != TRUE)
	{
		printf("Init CCIStaticConf fail.\n");
		return -1;
	}

	return 0;
}
/*******************************************************************************
*  Function   : Release
*  Description: 退出librti模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libudprti::Release()
{
	CLibUDPRTIStaticConf::Intstance()->Destroy();

	return 0;
}





