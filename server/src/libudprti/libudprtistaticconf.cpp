/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsstaicparammgr.h
*  Description:  cls静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libudprti/libudprtimacros.h"
#include "libudprtistaticconf.h"

//绑定本地IP地址
std::string	CLibUDPRTIStaticConf::m_LocalIP = "0.0.0.0";

//接收缓存大小
SInt32 CLibUDPRTIStaticConf::m_RevBuffSize = DEFAULT_REVBUFF_SIZE;

IMPLEMENT_SINGLETON(CLibUDPRTIStaticConf)
/*******************************************************************************
*  Function   : CLibUDPRTIStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibUDPRTIStaticConf::CLibUDPRTIStaticConf()
{
}
/*******************************************************************************
*  Function   : CLibUDPRTIStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibUDPRTIStaticConf::~CLibUDPRTIStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibUDPRTIStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibUDPRTIStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibUDPRTIStaticConf::SynStaticParam()
{
	//绑定本地IP地址
	CLibUDPRTIStaticConf::m_LocalIP = GET_PARAM_CHAR(UDP_RTI_LOCAL_IP, "0.0.0.0");

	//接收缓存大小
	CLibUDPRTIStaticConf::m_RevBuffSize = GET_PARAM_INT(UDP_RTI_REVBUFF_SIZE, DEFAULT_REVBUFF_SIZE);

	return;
}









