/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    aistransfercontent.h
*  Description:  内容注入接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/aistransfercontent.h"
/*******************************************************************************
*  Function   : CAISTransferContentReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISTransferContentReq::CAISTransferContentReq()
{
	ResetproviderID();
	ResetassetID();
	SetserviceType(0);
	ResetsourceURL();
	ResetreportURL();
}
/*******************************************************************************
*  Function   : ~CAISTransferContentReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISTransferContentReq::~CAISTransferContentReq()
{
	ResetproviderID();
	ResetassetID();
	SetserviceType(0);
	ResetsourceURL();
	ResetreportURL();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISTransferContentReq::EncodeJson(cJSON *json)
{
	//添加providerID
	cJSON_AddStringToObject(json, "providerID", GetproviderID());
	cJSON_AddStringToObject(json, "assetID", GetassetID());
	cJSON_AddNumberToObject(json, "serviceType", GetserviceType());
	cJSON_AddStringToObject(json, "sourceURL", GetsourceURL());
	cJSON_AddStringToObject(json, "reportURL", GetreportURL());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISTransferContentReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	HTTP_CJSON_GET_STRING_FUN(providerID, json, "providerID");
	HTTP_CJSON_GET_STRING_FUN(assetID, json, "assetID");
	HTTP_CJSON_GET_INT32_FUN(serviceType, json, "serviceType");
	HTTP_CJSON_GET_STRING_FUN(sourceURL, json, "sourceURL");
	HTTP_CJSON_GET_STRING_FUN(reportURL, json, "reportURL");

	return 0;
}












