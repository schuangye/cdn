/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    aisrtransferstatus.h
*  Description:  上报注入结果
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/aisrtransferstatus.h"
/*******************************************************************************
*  Function   : CAISRTransferStatusReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISRTransferStatusReq::CAISRTransferStatusReq()
{
}
/*******************************************************************************
*  Function   : ~CAISRTransferStatusReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISRTransferStatusReq::~CAISRTransferStatusReq()
{
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISRTransferStatusReq::EncodeJson(cJSON *json)
{
	//构建json数组
	cJSON *jsonobject = cJSON_CreateObject();
	if (NULL == jsonobject)
	{
		return -2;
	}

	CIGetStatusResult result = Getresult();

	cJSON_AddStringToObject(jsonobject, "providerID", result.m_ProviderID);
	cJSON_AddStringToObject(jsonobject, "assetID", result.m_AssetID);
	cJSON_AddNumberToObject(jsonobject, "serviceType", result.m_ServiceType);
	cJSON_AddStringToObject(jsonobject, "createTime", result.m_CreateTime);
	cJSON_AddNumberToObject(jsonobject, "status", result.m_Status);
	cJSON_AddNumberToObject(jsonobject, "progress", result.m_Progress);
	cJSON_AddStringToObject(jsonobject, "startTime", result.m_StartTime);
	cJSON_AddStringToObject(jsonobject, "endTime", result.m_EndTime);
	cJSON_AddNumberToObject(jsonobject, "duration", result.m_Duration);
	cJSON_AddNumberToObject(jsonobject, "avgBitRate", result.m_AvgBitRate);
	cJSON_AddNumberToObject(jsonobject, "contentSize", result.m_ContentSize);
	cJSON_AddStringToObject(jsonobject, "md5Checksum", result.m_Md5CheckSum);

	cJSON_AddItemToObject(json, "result", jsonobject);

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISRTransferStatusReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	cJSON *jsonresult = cJSON_GetObjectItem(json,"result");
	if (jsonresult == NULL)
	{
		return -2;
	}

	CIGetStatusResult result;
	char *tmpproviderid = result.m_ProviderID;
	HTTP_CJSON_GET_STRING(tmpproviderid, jsonresult, "providerID");

	char *tmpassetID = result.m_AssetID;
	HTTP_CJSON_GET_STRING(tmpassetID, jsonresult, "assetID");

	Int32 tmpserviceType = 0;
	HTTP_CJSON_GET_INT32(tmpserviceType, jsonresult, "serviceType");
	result.m_ServiceType = tmpserviceType;

	char *tmpcreateTime = result.m_CreateTime;
	HTTP_CJSON_GET_STRING(tmpcreateTime, jsonresult, "createTime");

	Int32 tmpstatus = 0;
	HTTP_CJSON_GET_INT32(tmpstatus, jsonresult, "status");
	result.m_Status = tmpstatus;

	Int32 tmpprogress = 0;
	HTTP_CJSON_GET_INT32(tmpprogress, jsonresult, "progress");
	result.m_Progress = tmpprogress;

	char *tmpstartTime = result.m_StartTime;
	HTTP_CJSON_GET_STRING(tmpstartTime, jsonresult, "startTime");

	char *tmpendTime = result.m_EndTime;
	HTTP_CJSON_GET_STRING(tmpendTime, jsonresult, "endTime");

	Int32 tmpduration = 0;
	HTTP_CJSON_GET_INT32(tmpduration, jsonresult, "duration");
	result.m_Duration = tmpduration;

	Int32 tmpavgBitRate = 0;
	HTTP_CJSON_GET_INT32(tmpavgBitRate, jsonresult, "avgBitRate");
	result.m_AvgBitRate = tmpavgBitRate;

	Int32 tmpcontentSize = 0;
	HTTP_CJSON_GET_INT32(tmpcontentSize, jsonresult, "contentSize");
	result.m_ContentSize = tmpcontentSize;

	char *tmpmd5Checksum = result.m_Md5CheckSum;
	HTTP_CJSON_GET_STRING(tmpmd5Checksum, jsonresult, "md5Checksum");

	Setresult(result);

	return 0;
}











