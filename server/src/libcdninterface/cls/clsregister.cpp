/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsregister.cpp
*  Description:  cls注册接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/clsregister.h"
/*******************************************************************************
*  Function   : CCLSRegisterReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLSRegisterReq::CCLSRegisterReq()
{
	ResetNodeName();
	ResetPrivateStreamAddress();
	ResetPublicStreamAddress();
	ResetAreaCode();

	SetTotalBand(0);
	SetFreeBand(0);
	SetPriority(0);
}
/*******************************************************************************
*  Function   : CCLSRegisterReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLSRegisterReq::~CCLSRegisterReq()
{
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLSRegisterReq::EncodeJson(cJSON *json)
{
	//添加节点名
	cJSON_AddStringToObject(json, "NodeName", GetNodeName());
	cJSON_AddStringToObject(json, "PrivateStreamAddress", GetPrivateStreamAddress());	
	cJSON_AddStringToObject(json, "PublicStreamAddress", GetPublicStreamAddress());	
	cJSON_AddNumberToObject(json, "TotalBand", GetTotalBand());
	cJSON_AddNumberToObject(json, "FreeBand", GetFreeBand());
	cJSON_AddStringToObject(json, "AreaCode", GetAreaCode());	
	cJSON_AddNumberToObject(json, "Priority", GetPriority());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLSRegisterReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	HTTP_CJSON_GET_STRING_FUN(NodeName, json, "NodeName");
	HTTP_CJSON_GET_STRING_FUN(PrivateStreamAddress, json, "PrivateStreamAddress");
	HTTP_CJSON_GET_STRING_FUN(PublicStreamAddress, json, "PublicStreamAddress");
	HTTP_CJSON_GET_INT64_FUN(TotalBand, json, "TotalBand");
	HTTP_CJSON_GET_INT64_FUN(FreeBand, json, "FreeBand");
	HTTP_CJSON_GET_STRING_FUN(AreaCode, json, "AreaCode");
	HTTP_CJSON_GET_INT32_FUN(Priority, json, "Priority");

	return 0;
}

