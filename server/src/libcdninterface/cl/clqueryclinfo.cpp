/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clqueryclinfo.cpp
*  Description:  查询cl信息
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/clqueryclinfo.h"
/*******************************************************************************
*  Function   : CCLQueryCLInfoReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLQueryCLInfoReq::CCLQueryCLInfoReq()
{
	ResetnodeID();
}
/*******************************************************************************
*  Function   : ~CCLQueryCLInfoReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLQueryCLInfoReq::~CCLQueryCLInfoReq()
{
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLQueryCLInfoReq::EncodeJson(cJSON *json)
{
	//添加节点名
	cJSON_AddStringToObject(json, "nodeID", GetnodeID());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLQueryCLInfoReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	//查询节点id
	HTTP_CJSON_GET_STRING_FUN(nodeID, json, "nodeID");

	return 0;
}
/******************************************************************************/
/*                  CCLQueryCLInfoRsp                                         */
/******************************************************************************/
/*******************************************************************************
*  Function   : CCLQueryCLInfoRsp
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLQueryCLInfoRsp::CCLQueryCLInfoRsp()
{
}
/*******************************************************************************
*  Function   : ~CCLQueryCLInfoRsp
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLQueryCLInfoRsp::~CCLQueryCLInfoRsp()
{
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLQueryCLInfoRsp::EncodeJson(cJSON *json)
{
	//构建json数组
	cJSON *jsonobject = cJSON_CreateObject();
	if (NULL == jsonobject)
	{
		return -2;
	}

	CLInfoResult result = Getresult();

	cJSON_AddStringToObject(jsonobject, "nodeID", result.m_NodeID);
	cJSON_AddNumberToObject(jsonobject, "storageSpace", result.m_StorageSpace);
	cJSON_AddNumberToObject(jsonobject, "freeStorageSpace", result.m_FreeStorageSpace);

	cJSON_AddItemToObject(json, "result", jsonobject);

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLQueryCLInfoRsp::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	cJSON *jsonresult = cJSON_GetObjectItem(json,"result");
	if (jsonresult == NULL)
	{
		return -2;
	}

	CLInfoResult result;
	char *tmpNodeID = result.m_NodeID;
	HTTP_CJSON_GET_STRING(tmpNodeID, jsonresult, "nodeID");

	Int64 tmpStorageSpace = 0;
	HTTP_CJSON_GET_INT64(tmpStorageSpace, jsonresult, "storageSpace");
	result.m_StorageSpace = tmpStorageSpace;

	Int64 tmpFreeStorageSpace = 0;
	HTTP_CJSON_GET_INT64(tmpFreeStorageSpace, jsonresult, "freeStorageSpace");
	result.m_FreeStorageSpace = tmpFreeStorageSpace;

	Setresult(result);

	return 0;
}














