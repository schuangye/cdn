/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cldeletefile.cpp
*  Description:  删除文件
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/cldeletefile.h"
/*******************************************************************************
*  Function   : CCLDeleteFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLDeleteFileReq::CCLDeleteFileReq()
{
	SetIsFuzzyDel(0);
	ResetFileList();
}
/*******************************************************************************
*  Function   : ~CCLDeleteFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLDeleteFileReq::~CCLDeleteFileReq()
{
	SetIsFuzzyDel(0);
	ResetFileList();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLDeleteFileReq::EncodeJson(cJSON *json)
{
	//添加节点名
	cJSON_AddNumberToObject(json, "IsFuzzyDel", GetIsFuzzyDel());

	//构建json数组
	cJSON *jsonarray = cJSON_CreateArray();
	if (NULL == jsonarray)
	{
		return -2;
	}
	cJSON_AddItemToObject(json, "FileList", jsonarray);

	std::list<LibCore::CString>  *tmpfilelist = GetFileList();
	std::list<LibCore::CString >::iterator it = tmpfilelist->begin();
	for(; it != tmpfilelist->end(); it++)
	{
		cJSON *tmparry = cJSON_CreateObject();
		if (NULL == tmparry)
		{
			return -3;
		}

		cJSON_AddStringToObject(tmparry, "FileName", it->c_str());
		cJSON_AddItemToArray(jsonarray, tmparry);
	}

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLDeleteFileReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	//获取模糊查询
	HTTP_CJSON_GET_INT32_FUN(IsFuzzyDel, json, "IsFuzzyDel");

	cJSON *jsonArry = cJSON_GetObjectItem(json,"FileList");
	if (jsonArry == NULL)
	{
		return -2;
	}

	cJSON *jsonlist=jsonArry->child;
	while(jsonlist!=NULL)
	{
		char tmpfilename[64] = {0};
		HTTP_CJSON_GET_STRING(tmpfilename, jsonlist, "FileName");
		SetFileList(LibCore::CString(tmpfilename));

		jsonlist = jsonlist->next;
	}

	jsonArry = NULL;
	jsonlist     = NULL;

	return 0;
}




