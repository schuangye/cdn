/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clingestfile.h
*  Description:  文件注入接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/clingestfile.h"
/*******************************************************************************
*  Function   : CCLIngestFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLIngestFileReq::CCLIngestFileReq()
{
	ResetFileList();
}
/*******************************************************************************
*  Function   : ~CCLIngestFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLIngestFileReq::~CCLIngestFileReq()
{
	ResetFileList();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLIngestFileReq::EncodeJson(cJSON *json)
{
	//构建json数组
	cJSON *jsonarray = cJSON_CreateArray();
	if (NULL == jsonarray)
	{
		return -2;
	}

	cJSON_AddItemToObject(json, "FileList", jsonarray);

	//构造json列表
	std::list<IngestFileInf>  *tmpfilelist = GetFileList();
	std::list<IngestFileInf >::iterator it = tmpfilelist->begin();
	for(; it != tmpfilelist->end(); it++)
	{
		cJSON *tmparry = cJSON_CreateObject();
		if (NULL == tmparry)
		{
			return -3;
		}

		cJSON_AddStringToObject(tmparry, "FileName", it->m_FileName);
		cJSON_AddStringToObject(tmparry, "SourceUrl", it->m_SourceUrl);

		cJSON_AddItemToArray(jsonarray, tmparry);
	}

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLIngestFileReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	cJSON *jsonArry = cJSON_GetObjectItem(json,"FileList");
	if (jsonArry == NULL)
	{
		return -2;
	}

	cJSON *jsonlist=jsonArry->child;
	while(jsonlist!=NULL)
	{
		IngestFileInf fileinfo;
		char *tmpfilename= fileinfo.m_FileName;
		HTTP_CJSON_GET_STRING(tmpfilename, jsonlist, "FileName");

		char *tmpfilepath = fileinfo.m_SourceUrl;
		HTTP_CJSON_GET_STRING(tmpfilepath, jsonlist, "SourceUrl");

		SetFileList(fileinfo);

		jsonlist = jsonlist->next;
	}

	jsonArry = NULL;
	jsonlist     = NULL;

	return 0;
}




