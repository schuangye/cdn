/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cpmgetingeststatus.cpp
*  Description:  查询文件注入状态
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/cpmgetresult.h"
/*******************************************************************************
*  Function   : CCPMGetResultReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCPMGetResultReq::CCPMGetResultReq()
{
	ResetproviderID();
	SetcontentType(0);
	ResetassetID();
}
/*******************************************************************************
*  Function   : ~CCPMGetIngestResultReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCPMGetResultReq::~CCPMGetResultReq()
{
	ResetproviderID();
	SetcontentType(0);
	ResetassetID();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCPMGetResultReq::EncodeJson(cJSON *json)
{
	//添加节点名
	cJSON_AddStringToObject(json, "providerID", GetproviderID());
	cJSON_AddStringToObject(json, "assetID", GetassetID());
    cJSON_AddNumberToObject(json, "contentType", GetcontentType());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCPMGetResultReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	//获取模糊查询
	HTTP_CJSON_GET_STRING_FUN(providerID, json, "providerID");
	HTTP_CJSON_GET_STRING_FUN(assetID, json, "assetID");
	HTTP_CJSON_GET_INT32_FUN(contentType, json, "contentType")

	return 0;
}
/******************************************************************************/
/*                  CCPMGetResultRsp                                    */
/******************************************************************************/
/*******************************************************************************
*  Function   : CCPMGetResultRsp
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCPMGetResultRsp::CCPMGetResultRsp()
{
}
/*******************************************************************************
*  Function   : ~CCLGetIngestResultRsp
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCPMGetResultRsp::~CCPMGetResultRsp()
{
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCPMGetResultRsp::EncodeJson(cJSON *json)
{
	//构建json对象
	cJSON *jsonobject = cJSON_CreateObject();
	if (NULL == jsonobject)
	{
		return -2;
	}

	CPMIngestResult tmpresult = GetResult();
	cJSON_AddStringToObject(jsonobject, "providerID", tmpresult.m_ProviderID);
	cJSON_AddStringToObject(jsonobject, "assetID", tmpresult.m_AssetID);
	cJSON_AddNumberToObject(jsonobject, "contentType", tmpresult.m_ContentType);
	cJSON_AddNumberToObject(jsonobject, "status", tmpresult.m_Status);

	cJSON_AddItemToObject(json, "result", jsonobject);

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCPMGetResultRsp::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}


	cJSON *jsonresult = cJSON_GetObjectItem(json,"result");
	if (jsonresult == NULL)
	{
		return -2;
	}

	CPMIngestResult result;
	char *tmpproviderid = result.m_ProviderID;
	HTTP_CJSON_GET_STRING(tmpproviderid, jsonresult, "providerID");

	char *tmpassetID = result.m_AssetID;
	HTTP_CJSON_GET_STRING(tmpassetID, jsonresult, "assetID");

	Int32 tmpcontentType = 0;
	HTTP_CJSON_GET_INT32(tmpcontentType, jsonresult, "contentType");
	result.m_ContentType = tmpcontentType;

	Int32 tmpstatus = 0;
	HTTP_CJSON_GET_INT32(tmpstatus, jsonresult, "status");
	result.m_Status = tmpstatus;

	SetResult(result);

	return 0;
}




