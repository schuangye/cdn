/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    redisfileinfdataobject.cpp
*  Description:  文件信息存储结构
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/redisfileinfdataobject.h"

CONF_IMPLEMENT_DYNCREATE(CRedisFileInfDataObject, CRedisDataObject)
/*******************************************************************************
*  Function   : CRedisFileInfDataObject
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CRedisFileInfDataObject::CRedisFileInfDataObject()
{
}
/*******************************************************************************
*  Function   : ~CRedisFileInfDataObject
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CRedisFileInfDataObject::~CRedisFileInfDataObject()
{
}
/*******************************************************************************
*  Function   : Encode
*  Description: 将结构数据编码成连续数据，buff返回编码后数据指针，返回buff数据长度
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisFileInfDataObject::Encode(char *&buff)
{
	if (strlen(m_FileInf.m_FileName) <= 0)
	{
		return -1;
	}

	//分配空间
	Int32 filesize = sizeof(RedisFileInfo);
	buff = this->EncodeMalloc(filesize);
	if (buff == NULL)
	{
		return -2;
	}

	memcpy(buff , &m_FileInf, filesize);

	return filesize;
}
/*******************************************************************************
*  Function   : Decode
*  Description: 将buff数据转码成结构数据,返回成功失败,0:成功，非零失败
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisFileInfDataObject::Decode(const char *buff, Int32 len)
{
	if (buff == NULL || len <= 0)
	{
		return -1;
	}

	Int32 filesize = sizeof(RedisFileInfo);
	if (len != filesize)
	{
		return -2;
	}

	memcpy(&m_FileInf, buff, len);

	return 0;
}
/*******************************************************************************
*  Function   : AddFileInf
*  Description: 添加文件信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CRedisFileInfDataObject::AddFileInfo(RedisFileInfo  fileinf)
{
	m_FileInf = fileinf;
	return;
}
/*******************************************************************************
*  Function   : GetFileInf
*  Description: 获取文件信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
RedisFileInfo CRedisFileInfDataObject::GetFileInfo()
{
	return m_FileInf;
}




