/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingesttaskinterfacemgr.h
*  Description:  注入任务接口管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/RuntimeObj.h"
#include "libcdnutil/citaskinterfacemgr.h"

IMPLEMENT_SINGLETON(CCITaskInterfaceMgr)
/*******************************************************************************
*  Function   : CCITaskInterfaceMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCITaskInterfaceMgr::CCITaskInterfaceMgr()
{
}
/*******************************************************************************
*  Function   : ~CCITaskInterfaceMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCITaskInterfaceMgr::~CCITaskInterfaceMgr()
{
}
/*******************************************************************************
*  Function   : RegisterIngestTask
*  Description: 注册接口任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITaskInterfaceMgr::RegisterCITask(Int32 contentType,const char *taskname)
{
	if (taskname == NULL)
	{
		return -1;
	}

	m_MutexConTask.Lock();
	m_ConTypeTaskNameMap[contentType] = std::string(taskname);
	m_MutexConTask.Unlock();

	return 0;
}
/*******************************************************************************
*  Function   : CreateIngestTask
*  Description: 创建注入任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCITaskInterface* CCITaskInterfaceMgr::CreateCITask(Int32 contentType)
{
	CCITaskInterface *task = GetCacheCITask(contentType);
	if (task == NULL)
	{
		task = CreateCITaskFromConType(contentType);
	}

	return task;
}
/*******************************************************************************
*  Function   : ReleaseIngestTask
*  Description: 释放注入任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCITaskInterfaceMgr::ReleaseCITask(Int32 contentType, CCITaskInterface *task)
{
	if (task == NULL)
	{
		return;
	}

	m_Mutex.Lock();
	IntIngestIFMap::iterator it = m_IntIngestTaskMap.find(contentType);
	if (it != m_IntIngestTaskMap.end())
	{
		it->second.push_back(task);
	}
	else
	{
		m_IntIngestTaskMap[contentType].push_back(task);
	}
	m_Mutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : GetCacheIngestTask
*  Description: 获取缓存的接口
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCITaskInterface* CCITaskInterfaceMgr::GetCacheCITask(Int32 contentType)
{
	CCITaskInterface *task = NULL;
	m_Mutex.Lock();
	IntIngestIFMap::iterator it = m_IntIngestTaskMap.find(contentType);
	if (it != m_IntIngestTaskMap.end())
	{
		if (!(it->second.empty()))
		{
			task = it->second.front();
			it->second.pop_front();
		}
	}
	m_Mutex.Unlock();

	return task;
}
/*******************************************************************************
*  Function   : CreateCITaskFromConType
*  Description: 通过内容类型创建注入类型缓存接口
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCITaskInterface* CCITaskInterfaceMgr::CreateCITaskFromConType(Int32 contentType)
{
	CCITaskInterface *task = NULL;

	m_MutexConTask.Lock();
	IntStringMap::iterator it = m_ConTypeTaskNameMap.find(contentType);
	if (it != m_ConTypeTaskNameMap.end())
	{
		task = (CCITaskInterface*)CreateRuntimeObjectFromClassName(it->second.c_str());
	}
	m_MutexConTask.Unlock();

	return task;
}








