/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsdownload.cpp
*  Description:  hls客户端下载
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libcore/httpdownload.h"
#include "libcdnutil/hlsdownload.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libcdnutil/libcdnutilmacros.h"
#include <stdio.h>
#include <iostream>
/*******************************************************************************
*  Function   : CHLSDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHLSDownload::CHLSDownload()
{
	m_TmpM3U8Buff = NULL;
	m_M3U8BuffSize = 0;
	m_TmpDataBuff = NULL;
	m_DataBuffSize = 0;
    m_M3u8Type = TYPE_UNKNOWN;
	m_CurIndex = 0;

	m_Url = "";
	m_DomainUrl = "";

	m_OneceCount = 3;
	m_DownLoadTimeOut = 0;
}
/*******************************************************************************
*  Function   : CHLSDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHLSDownload::~CHLSDownload()
{
	if (m_TmpM3U8Buff != NULL)
	{
		free(m_TmpM3U8Buff);
		m_TmpM3U8Buff = NULL;
	}

	if (m_TmpDataBuff != NULL)
	{
		free(m_TmpDataBuff);
		m_TmpDataBuff = NULL;
	}

	m_MainMediaStreamVector.clear();
	m_FinishSlipList.clear();
}
/*******************************************************************************
*  Function   : CHLSDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::Init(const char *url, Int32 tmpdatabuffsize, Int32 tmpm3u8buffsize,
	                     Int32 onececount, Int32 timeout)
{
	if (url == NULL)
	{
		return -1;
	}

	m_M3U8BuffSize = tmpm3u8buffsize;
	if (m_M3U8BuffSize < 0)
	{
		m_M3U8BuffSize = TMP_DOWN_M3U8_BUFF_SIZE;
	}

	m_DataBuffSize = tmpdatabuffsize;
	if (m_DataBuffSize < 0)
	{
		m_DataBuffSize = TMP_DOWN_DATA_BUFF_SIZE;
	}

	//分配空间
	m_TmpM3U8Buff = (char*)malloc(m_M3U8BuffSize);
	if (m_TmpM3U8Buff == NULL)
	{
		LOG_ERROR("malloc fail. "<<m_M3U8BuffSize)
		return -2;
	}

	m_TmpDataBuff = (char*)malloc(m_DataBuffSize);
	if (m_TmpDataBuff == NULL)
	{
		LOG_ERROR("malloc fail. "<<m_DataBuffSize)
		return -3;
	}

	//保存url
	m_Url   = std::string(url);
	m_DownLoadTimeOut = timeout;

	//加载m3u8
	SInt32 ret = LoadM3U8(url);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("load m3u8 fail. ret:"<<ret)
		return -4;
	}

	m_OneceCount = onececount;
	if (m_OneceCount < 0)
	{
		m_OneceCount = 0;
	}

    return 0;
}
/*******************************************************************************
*  Function   : StartDownLoad
*  Description: 开始下载,index表示子m3u8在主m3u8位置
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::StartDownLoad(SInt32 index)
{
	if (m_MainMediaStreamVector.empty())
	{
		return -1;
	}

	//计算当前下载文件索引位置
	if (index > m_MainMediaStreamVector.size())
	{
		m_CurIndex = m_MainMediaStreamVector.size()-1;
	}
	else
	{
		m_CurIndex = index;
	}

	//判断索引是否大于0
	if (m_CurIndex < 0)
	{
		m_CurIndex = 0;
	}

	//更新m3u8
	Int32 ret = RefreshSubM3U8();
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("refresh sub m3u8 fail. ret:"<<ret)
		return -2;
	}

	//下载数据
	ret = DownLoadData();
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("down load data fail. ret:"<<ret)
		return -3;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetMainUrlM3u8Type
*  Description: 判断主url m3u8类型
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
M3u8Type  CHLSDownload::GetMainUrlM3u8Type()
{
	return m_M3u8Type;
}
/*******************************************************************************
*  Function   : GetBitRateCount
*  Description: 获取码率数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::GetBitRateCount()
{
	return m_MainMediaStreamVector.size();
}
/*******************************************************************************
*  Function   : RefreshSubM3U8
*  Description: 刷新m3u8
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::RefreshSubM3U8()
{
	if (m_MainMediaStreamVector[m_CurIndex].m_SubM3u8.m_IsEndList)
	{
		return CDN_ERR_SUCCESS;
	}

	//下载文件
	CHttpDownLoad  httpdownload;
	memset(m_TmpM3U8Buff, 0, TMP_DOWN_M3U8_BUFF_SIZE);

	std::string url = m_MainMediaStreamVector[m_CurIndex].m_StreamUrl;
	Int32 ret = httpdownload.DownLoadData(url.c_str(), m_TmpM3U8Buff, TMP_DOWN_M3U8_BUFF_SIZE, m_DownLoadTimeOut);
	if (ret < 0)
	{
		LOG_ERROR("down load m3u8 fail. ret:"<<ret)
		return -1;
	}

	//解析子m3u8
	ret = ParaseSubM3U8(m_TmpM3U8Buff, m_CurIndex);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("parase sub m3u8 fail. ret:"<<ret)
		return -2;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DownLoadData
*  Description: 下载数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::DownLoadData()
{
	if (m_MainMediaStreamVector[m_CurIndex].m_SubM3u8.m_HLSSlipList.empty())
	{
		return CDN_ERR_SUCCESS;
	}

	//下载切片内容
	if (m_TmpDataBuff == NULL)
	{
		LOG_ERROR("m_TmpDataBuff is null")
		return -1;
	}

	//保存数据
	Int32 count = 0;
	HLSSlipInfoList::iterator it = m_MainMediaStreamVector[m_CurIndex].m_SubM3u8.m_HLSSlipList.begin();
	for (; it != m_MainMediaStreamVector[m_CurIndex].m_SubM3u8.m_HLSSlipList.end();)
	{
		std::string tmpurl = it->m_SplipUrl;

		//添加到完成队列中,保持队列有100
		if (m_FinishSlipList.size() > 100)
		{
			m_FinishSlipList.pop_front();
		}
		m_FinishSlipList.push_back(*it);

		//释放已经下载的分片
		it = m_MainMediaStreamVector[m_CurIndex].m_SubM3u8.m_HLSSlipList.erase(it);


		CHttpDownLoad  httpdownload;
		Int32 ret = httpdownload.DownLoadData(tmpurl.c_str(), m_TmpDataBuff, m_DataBuffSize, HTTP_DOWNLOAD_TIMEOUT);
		if (ret < 0)
		{
			LOG_ERROR("down load data fail. ret:"<<ret)
			continue;
		}

		//保存数据
		ret = SaveMediaData(m_TmpDataBuff, ret);
		if (ret != CDN_ERR_SUCCESS)
		{
			LOG_ERROR("save medis data fail. ret:"<<ret)
			continue;
		}

		count++;
		if (m_OneceCount > 0 && count >= m_OneceCount)
		{
			break;
		}
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : SaveMediaDate
*  Description: 保存下载数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::SaveMediaData(const char *buff, SInt32 len)
{
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CHLSDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::LoadM3U8(const char *url)
{
	if (url == NULL)
	{
		return -1;
	}

	//判断是否分配空间成功
	if (m_TmpM3U8Buff == NULL)
	{
		LOG_ERROR("m_TmpM3U8Buff malloc fail")
		return -2;
	}

	//下载文件
	CHttpDownLoad  httpdownload;
	memset(m_TmpM3U8Buff, 0, TMP_DOWN_M3U8_BUFF_SIZE);
	Int32 ret = httpdownload.DownLoadData(url, m_TmpM3U8Buff, m_M3U8BuffSize, m_DownLoadTimeOut);
    if (ret < 0)
    {
		LOG_ERROR("down load m3u8 fail. ret:"<<ret)
		return -3;
    }

	//获取m3u8类型
	if (CheckM3u8Type(m_TmpM3U8Buff) ==  TYPE_UNKNOWN)
	{
		LOG_ERROR("chech m3u8 type fail."<<m_TmpM3U8Buff)
		return -4;
	}

	//获取域名
	m_DomainUrl = STR::ParaMainNetPathFromUrl(m_Url);

	//解析m3u8
	ret = ParaseM3u8(m_TmpM3U8Buff);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("parase m3u8 fail.ret:"<<ret<<".\n"<< m_TmpM3U8Buff)
		ret = -5;
	}

    return ret;
}
/*******************************************************************************
*  Function   : ParaseM3u8
*  Description: 解析m3u8
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::ParaseM3u8(const char *m3u8)
{
	if(m3u8 == NULL)
	{
		return -1;
	}

	Int32 ret = CDN_ERR_SUCCESS;
	if (m_M3u8Type == MAIN_M3U8)
	{
		ret = ParaseMainM3U8(m3u8);
	}
	else if (m_M3u8Type == SUB_M3U8)
	{
		ret = ParaseSubM3U8(m3u8);
	}
	else
	{
		LOG_ERROR("M3U8 is illegal."<<m3u8)
		ret = -3;
	}

	return ret;
}
/*******************************************************************************
*  Function   : ParaseM3u8
*  Description: 解析主m3u8
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::ParaseMainM3U8(const char *m3u8)
{
	if (m3u8 == NULL)
	{
		return -1;
	}

	Int32         programID;
	Int64         bandWidth;
	std::list<std::string> lines;
	STR::ReadLines(m3u8,lines);
	std::list<std::string>::iterator it = lines.begin();
	for (; it != lines.end(); it++)
	{
		if (it->find(EXT_X_STREAM_INF) != std::string::npos)
		{
			programID = STR::Str2int(STR::SubStr(it->c_str(),  PROGRAM_ID, ",").c_str());
			bandWidth = STR::Str2int64(STR::SubStr(it->c_str(),  BANDWIDTH, ",").c_str());
		}
		else if (it->at(0) != '#')
		{
			MediaStreamM3u8Info  medisastream;
			if (it->find("http://") != std::string::npos)
			{
				medisastream.m_StreamUrl = *it;
			}
			else
			{
				medisastream.m_StreamUrl = m_DomainUrl + *it;
			}

			medisastream.m_StreamUrl = STR::LRTrimString(medisastream.m_StreamUrl.c_str());
			medisastream.m_DomainUrl = STR::ParaMainNetPathFromUrl(medisastream.m_StreamUrl);
			medisastream.m_ProgramID = programID;
			medisastream.m_BandWidth = bandWidth;

			m_MainMediaStreamVector.push_back(medisastream);

			programID = 0;
			bandWidth = 0;
		}
		else if (it->find(EXT_M3U) != std::string::npos)
		{
			continue;
		}
		else
		{
			LOG_ERROR("m3u8 content is error."<<*it)
			return -2;
		}
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : ParaseM3u8
*  Description: 解析子m3u8
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHLSDownload::ParaseSubM3U8(const char *m3u8, SInt32 index)
{
	if (m3u8 == NULL || index > m_MainMediaStreamVector.size() || index < 0)
	{
		return -1;
	}

	//获取历史的url地址
	if (m_MainMediaStreamVector.empty())
	{
		MediaStreamM3u8Info  medisastream;
		medisastream.m_StreamUrl = m_Url;
		medisastream.m_DomainUrl = m_DomainUrl;
		m_MainMediaStreamVector.push_back(medisastream);
	}

	SubM3u8Info subM3u8 = m_MainMediaStreamVector[index].m_SubM3u8;
	SInt64  tmpMediaSeq = subM3u8.m_MediaSequence;

	HLSSlipInfoList tmpsplitlist;
	HLSSlipInfo   tmpsplit;
	std::list<std::string> lines;
	STR::ReadLines(m3u8,lines);
	std::list<std::string>::iterator it = lines.begin();
	for (; it != lines.end(); it++)
	{
		if (it->empty())
		{
			continue;
		}
		else if (it->find(EXTINF) != std::string::npos)
		{
			tmpsplit.m_Duration = STR::Str2int(STR::GetKeyValue(it->c_str(), EXTINF, ","));
		}
		else if (it->at(0) != '#')
		{
			std::string tmpurl = *it;
			if (it->find("http://") == std::string::npos)
			{
				tmpurl = m_MainMediaStreamVector[index].m_DomainUrl + tmpurl; 
			}

			tmpsplit.m_SplipUrl = STR::LRTrimString(tmpurl.c_str());
			tmpsplit.m_DomainUrl = m_MainMediaStreamVector[index].m_DomainUrl;
			tmpsplitlist.push_back(tmpsplit);
		}
		else if (it->find(EXT_M3U) != std::string::npos)
		{
			continue;
		}
		else if (it->find(EXT_X_VERSION) != std::string::npos)
		{
			continue;
		}
		else if (it->find(EXT_X_ALLOW_CACHE) != std::string::npos)
		{
			continue;
		}
		else if (it->find(EXT_X_TARGETDURATION) != std::string::npos)
		{
			subM3u8.m_TargetDuation = STR::Str2int(STR::GetKeyValue(it->c_str(), EXT_X_TARGETDURATION, ""));
		}
		else if (it->find(EXT_X_MEDIA_SEQUENCE) != std::string::npos)
		{
			tmpMediaSeq = STR::Str2int64(STR::GetKeyValue(it->c_str(), EXT_X_MEDIA_SEQUENCE, ""));
		}
		else if (it->find(EXT_X_ENDLIST) != std::string::npos)
		{
			subM3u8.m_IsEndList = TRUE;
		}
		else
		{
			LOG_DEBUG("m3u8 content is error."<<*it)
		}
	}

	//判断是否有下载片
	SInt64 count = tmpMediaSeq - subM3u8.m_MediaSequence;

	//小于0表示序号反转
	if (count < 0)
	{
		LOG_INFO("live m3u8 seq small."<<tmpMediaSeq<<"-"<<subM3u8.m_MediaSequence)

		//序列号比之前的小，保存所有切片
		AddSplitList(tmpsplitlist, subM3u8.m_HLSSlipList);
		subM3u8.m_MediaSequence = tmpMediaSeq;
		m_MainMediaStreamVector[index].m_SubM3u8 = subM3u8;

		return CDN_ERR_SUCCESS;
	}

	//大于0进行数据处理
	if (count > 0)
	{
		//过滤重复的分片
		while(tmpsplitlist.size() > count && !(subM3u8.m_IsEndList))
		{
			tmpsplitlist.pop_front();
		}

		if (!tmpsplitlist.empty() && index < m_MainMediaStreamVector.size())
		{
			AddSplitList(tmpsplitlist, subM3u8.m_HLSSlipList);
			subM3u8.m_MediaSequence = tmpMediaSeq;
			m_MainMediaStreamVector[index].m_SubM3u8 = subM3u8;
		}
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CHLSDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
M3u8Type CHLSDownload::CheckM3u8Type(const char *m3u8)
{
	if (m3u8 == NULL)
	{
		return TYPE_UNKNOWN;
	}

	std::string tmpstr = std::string(m3u8);
	if (tmpstr.find("EXT-X-STREAM-INF:") != std::string::npos)
	{
		m_M3u8Type = MAIN_M3U8;
	}
	else
	{
		m_M3u8Type = SUB_M3U8;
	}

	return m_M3u8Type;
}
/*******************************************************************************
*  Function   : IsExistSplitUrl
*  Description: 添加分片
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CHLSDownload::AddSplitList(HLSSlipInfoList srcsplitlist, HLSSlipInfoList &distsplitlist)
{
	HLSSlipInfoList::iterator it = srcsplitlist.begin();
	for (; it != srcsplitlist.end(); it++)
	{
		if (IsExistSplitUrl(distsplitlist, it->m_SplipUrl) || IsExistSplitUrl(m_FinishSlipList, it->m_SplipUrl))
		{
			continue;
		}

		distsplitlist.push_back(*it);
	}

	return;
}
/*******************************************************************************
*  Function   : IsExistSplitUrl
*  Description: 判读分片连接是否存在
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CHLSDownload::IsExistSplitUrl(const HLSSlipInfoList splitlist, const std::string url)
{
	if (splitlist.empty() || url.empty())
	{
		return FALSE;
	}

	HLSSlipInfoList::const_iterator it = splitlist.begin();
	for (; it != splitlist.end(); it++)
	{
		if (it->m_SplipUrl == url)
		{
			return TRUE;
		}
	}

	return FALSE;
}






