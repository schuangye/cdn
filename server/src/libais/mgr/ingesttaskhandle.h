#ifndef INGEST_TASK_HANDLE_H
#define INGEST_TASK_HANDLE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingesttaskhandle.h
*  Description:  注入内容管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "libais/aiscommon.h"

class CIngestTaskHandle : public OSTask
{
	DECLARATION_SINGLETON(CIngestTaskHandle)
public:

	//初始化注入内容管理器
	Int32 Init();

	//线程调用函数
	virtual Bool Run();

	//添加注入任务
	Int32 AddIngestTask(const IngestTaskItem &ingesttask);

	//查询注入状态
	Int32 GetIngestTaskStatus(const ContentKey &conkey, IngestTaskItem &ingesttask);

	//删除注入文件
	Int32 DeleteContent(const ContentKey &conkey);
};

#endif



