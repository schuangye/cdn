#ifndef DISTRIBUTE_TASK_H
#define DISTRIBUTE_TASK_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:distributetask.h
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 分发任务
*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libhttpinterface/httpclient.h"
#include "libais/aiscommon.h"

class CDistributeTask
{
public:

	CDistributeTask();

	virtual ~CDistributeTask();

	//初始化分发任务
	virtual SInt32 Init(const DistributeTaskItem &disttask);

	//开始分发
	virtual SInt32 StartDistribute();

	//检查分发状态是否完成
	virtual Bool CheckDistributeStatus();

	//取消分发
	virtual SInt32 CancelDistribute();

	//删除分发文件
	virtual SInt32 DeleteDistributeFile();

private:

	DistributeTaskItem      m_DistrTaskItem;    //分发任务项
	CHttpClient             m_HttpClient;
};

#endif





