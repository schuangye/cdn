#ifndef AIS_DB_PERATE_MGR_H
#define AIS_DB_PERATE_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    aisdbperatemgr.h
*  Description:  数据库操作
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libdb/dboperate.h"
#include "libais/libaismacros.h"
#include "libais/aiscommon.h"

class CAISDBOperatemgr : public CDBOperate
{
	DECLARATION_SINGLETON(CAISDBOperatemgr);
public:

	//插入注入任务
	Int32 InsertTask(const IngestTaskItem &taskitem, const std::string &nodeid);

	//查询注入任务状态
	Int32 QueryTaskStatus(const ContentKey &req, IngestTaskItem &rsp);

	//获取任务数
	Int32 GetTaskCount(std::string providerid, std::string assetID, Int32 cmd = 0);

	//删除文件
	Int32 DeleteContent(const ContentKey &req, const std::string &nodeid);

	//获取分配节点,按照剩余磁盘空间大小获取节点数
	Int32  GetCINode(std::list<std::string> &nodelist, Int32 nodenum);

	//获取所有节点
	Int32 GetAllCINode(std::list<std::string> &nodelist);

	//清除过期完成任务
	void CleanExpiredFinishTask(SInt32  expiredtime);

	//清除过期没有开始的任务
	void CleanExpiredUnFinishTask(SInt32  expiredtime);

	//清除已经分发完成的任务
	void CleanCompleteDistributeTask(SInt32  expiredtime);

	//获取已经完成的任务
	Int32 GetCompleteTask(DistTaskItemList &distributeTasklist);

	//获取子节点信息
	Int32 GetSubNodeList(SubNodeItemList  &subnodelist);

	//将分发任务插入到任务分发表
	Int32 InsertDataToTaskDistribute(const DistributeTaskItem &distributeTaskItem);

	//更新任务分发表中的进度及状态值
	Int32 UpdatePorcessTaskDistribute(const DistributeTaskItem &distributeTaskItem);

	//分发任务在分发表中是否存在
	Int32 GetDistributeCount(const DistributeTaskItem &distributeTaskItem);

	//从任务分发表中获取subnodeaisaddr为空的数据
	Int32 GetNoSubNodeAddrDistributeData(DistTaskItemList &distributeTasklist);

	//从任务分发表中删除subnodeaisaddr为空的记录
	Int32 DeleteNoSubNodeAddrDistributeTask(const DistributeTaskItem &distributeTaskItem);

	//删除分发任务
	Int32 DeleteDistributeTask(const DistributeTaskItem &distributeTaskItem);

	//加载需要分发的任务
	Int32 GetDistributeTask(DistTaskItemList &distributeTasklist, Int32 numbers, Int32 status = DISTRIBUTE_WAITING);

	//时间超时，将在线节点更新为不在线
	Int32 UpdateNoOnline(SInt32 timeout);

	//在超时时间范围内，将不在线节点更新为在线
	Int32 UpdateOnline(SInt32 timeout);

	//查询在线的所有节点
	Int32 QueryOnlineNode(NodeList &nodelist);

	//查询媒资分布在哪些节点
	Int32 QueryFileDistribute(const std::string &pid, const std::string &aid, 
		                      FileDistributeList &filedistributelist);

	Int32 QueryNodeAddr(const std::string &pid, const std::string &aid, 
		                std::string &nodeaddr, std::string &path);
};



#endif


