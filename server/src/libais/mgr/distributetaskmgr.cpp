/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:distributetaskmgr.h
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 分发任务管理器
*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libais/libaismacros.h"
#include "mgr/distributetaskmgr.h"
#include "mgr/aisdboperatemgr.h"
#include "libaisstaticconf.h"


IMPLEMENT_SINGLETON(CDistributeTaskMgr)
/*******************************************************************************
*  Function   :CDistributeTaskMgr
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDistributeTaskMgr::CDistributeTaskMgr()
{
	m_LastTime = 0;
	m_LastAsynSubNodeTime = 0;
}
/*******************************************************************************
*  Function   :~CDistributeTaskMgr
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDistributeTaskMgr::~CDistributeTaskMgr()
{
	//停止线程
	this->StopTask();

	//清除分发任务
	CDistributeTaskList::iterator it = m_DistributeTaskList.begin();
	for (; it != m_DistributeTaskList.end();it++)
	{
		if (*it != NULL)
		{
			delete (*it);
		}
	}
	m_DistributeTaskList.clear();

	//清除空闲任务
	it = m_FreeTaskList.begin();
	for (; it != m_FreeTaskList.end(); it++)
	{
		if (*it != NULL)
		{
			delete (*it);
		}
	}
	m_FreeTaskList.clear();
}
/*******************************************************************************
*  Function   :Init
*  Description:初始化
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32  CDistributeTaskMgr::Init()
{
	//程序重启，加载需要获取状态的分发任务
	RecoveryDistributeTask();

	if (!this->RunTask())
	{
		ERROR_LOG("CIngestTaskHandle run task fail")
		return -3;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   :DeleteSubNodeFile
*  Description:删除子节点文件
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeTaskMgr::DeleteSubNodeFile(DistributeTaskItem &taskitem)
{
	if (taskitem.m_ProviderID.empty() || taskitem.m_AssetID.empty())
	{
		return;
	}

	m_DelMutex.Lock();
	SubNodeItemList::iterator it =  m_SubNodeList.begin();
	for(; it != m_SubNodeList.end(); it++)
	{
		taskitem.m_SubNodeAisAddr = it->m_SubInAddress;
		m_DelFileList.push_back(taskitem);
	}
	m_DelMutex.Unlock();

	return ;
}
/*******************************************************************************
*  Function   :Run
*  Description:线程调用函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CDistributeTaskMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//加载子节点
		LoadSubNode();

		//加载分发任务
		LoadDistributeTask();

		//刷新分发状态
		RefreshDistributeStatus();

		//删除子节点文件
		DoDeleteSubNodeFile();

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : LoadSubNode
*  Description: 加载子节点
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeTaskMgr::LoadSubNode()
{
	if (OS::Milliseconds() - m_LastAsynSubNodeTime < 120000)
	{
		return ;
	}


	//获取子节点信息
	SubNodeItemList  subnodelist;
	Int32 ret = CAISDBOperatemgr::Intstance()->GetSubNodeList(subnodelist);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("GetSubNodeList fail. ret:"<<ret);
		return;
	}
	else
	{
		if (!subnodelist.empty())
		{
			m_SubNodeList.clear();
			m_SubNodeList.insert(m_SubNodeList.end(), subnodelist.begin(), subnodelist.end());
		}
	}

	m_LastAsynSubNodeTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   :LoadDistributeTask
*  Description:加载分发任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeTaskMgr::LoadDistributeTask()
{
	if (OS::Milliseconds() - m_LastTime < 60000 || CLibAISStaticConf::m_AisDistributeTaskNum <= m_DistributeTaskList.size())
	{
		return ;
	}

	//限制同时进行拉流的个数
	SInt32 count = CLibAISStaticConf::m_AisDistributeTaskNum - m_DistributeTaskList.size();
	DistTaskItemList distributeTaskItemlist;
	SInt32 ret = CAISDBOperatemgr::Intstance()->GetDistributeTask(distributeTaskItemlist, count);
	if (ret != CDN_ERR_SUCCESS)
	{
		return ;
	}

	//进行任务分发
	std::list<DistributeTaskItem>::iterator it = distributeTaskItemlist.begin();
	for (;it != distributeTaskItemlist.end(); it++)
	{
		CDistributeTask *pCDistributeTask = GetFreeDistributeTask();
		if (pCDistributeTask == NULL)
		{
			pCDistributeTask = new CDistributeTask();
		}

		if (pCDistributeTask != NULL )
		{
			ret = pCDistributeTask->Init(*it);
			if (ret == CDN_ERR_SUCCESS)
			{
				ret = pCDistributeTask->StartDistribute();
				if (ret == CDN_ERR_SUCCESS)
				{
					m_DistributeTaskList.push_back(pCDistributeTask);
				}
			}

			if (ret != CDN_ERR_SUCCESS)
			{
				m_FreeTaskList.push_back(pCDistributeTask);
			}	
		}
	}

	m_LastTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   :RefreshDistributeStatus
*  Description:刷新分发状态
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeTaskMgr::RefreshDistributeStatus()
{
	CDistributeTaskList::iterator it = m_DistributeTaskList.begin();
	for (; it != m_DistributeTaskList.end(); )
	{
		Bool ret = (*it)->CheckDistributeStatus();
		if (ret)
		{
			m_FreeTaskList.push_back(*it);
			it = m_DistributeTaskList.erase(it);
		}
		else
		{
			it++;
		}
	}
	
	return;
}
/*******************************************************************************
*  Function   :DoDeleteSubNodeFile
*  Description:分发删除的任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeTaskMgr::DoDeleteSubNodeFile()
{
	CDistributeTask *pCDistributeTask = new CDistributeTask();
	if (pCDistributeTask == NULL)
	{
		LOG_ERROR("pCDistributeTask is NULL");
		return ;
	}

	DistTaskItemList disttasklist;
	m_DelMutex.Lock();
	disttasklist.insert(disttasklist.end(), m_DelFileList.begin(), m_DelFileList.end());
	m_DelFileList.clear();
	m_DelMutex.Unlock();

	DistTaskItemList::iterator it = disttasklist.begin();
	for (; it != disttasklist.end();it++)
	{
		pCDistributeTask->Init(*it);
		pCDistributeTask->DeleteDistributeFile();
	}

	delete pCDistributeTask;
	pCDistributeTask = NULL;

	return ;
}
/*******************************************************************************
*  Function   :RecoveryDistributeTask
*  Description:程序重启，加载需要获取状态的分发任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeTaskMgr::RecoveryDistributeTask()
{
	//取出所有需要查询状态的分发任务
	DistTaskItemList distributeTaskItemlist;
	int ret = CAISDBOperatemgr::Intstance()->GetDistributeTask(distributeTaskItemlist, 100, DISTRIBUTE_TRANSFER);
	if (ret != CDN_ERR_SUCCESS)
	{
		return ;
	}

	DistTaskItemList::iterator it = distributeTaskItemlist.begin();
	for (;it != distributeTaskItemlist.end(); it++)
	{
		CDistributeTask *pCDistributeTask = GetFreeDistributeTask();
		if (pCDistributeTask == NULL)
		{
			pCDistributeTask = new CDistributeTask;
		}

		if (pCDistributeTask != NULL)
		{
			ret = pCDistributeTask->Init(*it);
			if (ret == CDN_ERR_SUCCESS)
			{
				m_DistributeTaskList.push_back(pCDistributeTask);
			}
			else
			{
				m_FreeTaskList.push_back(pCDistributeTask);
			}
		}
	}

	return;
}
/*******************************************************************************
*  Function   :GetFreeDistributeTask
*  Description:获取空闲分发任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDistributeTask* CDistributeTaskMgr::GetFreeDistributeTask()
{
	CDistributeTask *task = NULL;
	if (!m_FreeTaskList.empty())
	{
		task = m_FreeTaskList.front();
		m_FreeTaskList.pop_front();
	}

	return task;
}





