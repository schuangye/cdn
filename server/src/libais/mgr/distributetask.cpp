/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:distributetask.h
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 分发任务
*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdninterface/aisgettransferstatus.h"
#include "libcdninterface/aistransfercontent.h"
#include "libcdninterface/aisdeletecontent.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/aisdboperatemgr.h"
#include "distributetask.h"
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDistributeTask::CDistributeTask()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDistributeTask::~CDistributeTask()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 初始化分发任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CDistributeTask::Init(const DistributeTaskItem &disttask)
{
	m_DistrTaskItem = disttask;

	//初始化http客户端
	SInt32 ret = m_HttpClient.SetHttpServerAddr(disttask.m_SubNodeAisAddr.c_str());
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("set http server fail. ret:"<<ret)
		return -1;
	}

	return 0;
}
/*******************************************************************************
*  Function   : StartDistribute
*  Description: 开始分发
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CDistributeTask::StartDistribute()
{
	//获取节点的推流地址和路径
	std::string sStreamaddr = "";
	std::string sPath = "";
	SInt32 result = CAISDBOperatemgr::Intstance()->QueryNodeAddr(m_DistrTaskItem.m_ProviderID, m_DistrTaskItem.m_AssetID, 
								      sStreamaddr, sPath) ;
	if (result != 0)
	{
		LOG_ERROR("Content not exist;");

		m_DistrTaskItem.m_Url = "";
		m_DistrTaskItem.m_Status = 2;
		m_DistrTaskItem.m_Progress = 100;
		m_DistrTaskItem.m_EndTime = OS::Milliseconds()/1000;
		m_DistrTaskItem.m_ErrCode = CDN_ERR_NO_CONTENT;
		m_DistrTaskItem.m_ErrDes = "content not find";
		CAISDBOperatemgr::Intstance()->UpdatePorcessTaskDistribute(m_DistrTaskItem);
		
		return -1;
	}

	//组装推流地址
	std::string streamaddrurl = "http://" + sStreamaddr + sPath + m_DistrTaskItem.m_ProviderID + "_" + m_DistrTaskItem.m_AssetID;
	
	//组装body部分
    CAISTransferContentReq  req;
	CAISTransferContentRsp  rsp;

	req.SetRequstType(HTTP_POST_TYPE);
	req.SetCmdCode("JTransferContent");
	req.SetproviderID(m_DistrTaskItem.m_ProviderID.c_str());
	req.SetassetID(m_DistrTaskItem.m_AssetID.c_str());
	req.SetserviceType(m_DistrTaskItem.m_ServiceType);
	req.SetsourceURL(streamaddrurl.c_str());

	result = m_HttpClient.SendRequest(&req, &rsp);
	if (result != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("send Distribute request fail.ret:"<<result);
		m_DistrTaskItem.m_Url = streamaddrurl;
		m_DistrTaskItem.m_Status = 2;
		m_DistrTaskItem.m_Progress = 100;
		m_DistrTaskItem.m_EndTime = OS::Milliseconds()/1000;
		m_DistrTaskItem.m_ErrCode = CDN_ERR_REQUEST_FAIL ;
		m_DistrTaskItem.m_ErrDes = "request fail";
		CAISDBOperatemgr::Intstance()->UpdatePorcessTaskDistribute(m_DistrTaskItem);

		return -1;
	}
	else
	{
		m_DistrTaskItem.m_ErrCode = rsp.GetErrCode();
		if (m_DistrTaskItem.m_ErrCode == 0)
		{
			m_DistrTaskItem.m_Url = streamaddrurl;
			m_DistrTaskItem.m_ErrDes = "";
			m_DistrTaskItem.m_Status = 1;
			CAISDBOperatemgr::Intstance()->UpdatePorcessTaskDistribute(m_DistrTaskItem);
			
			return 0;
		}
		else
		{
			m_DistrTaskItem.m_Url = streamaddrurl;
			m_DistrTaskItem.m_Status = 2;
			m_DistrTaskItem.m_Progress = 100;
			m_DistrTaskItem.m_EndTime = OS::Milliseconds()/1000;
			m_DistrTaskItem.m_ErrDes = rsp.GetErrDes();
			m_DistrTaskItem.m_ErrCode = rsp.GetErrCode();
			CAISDBOperatemgr::Intstance()->UpdatePorcessTaskDistribute(m_DistrTaskItem);

			return -1;
		}
	}

	return 0;
}
/*******************************************************************************
*  Function   : QueryDistributeStatus
*  Description: 查询分发状态
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CDistributeTask::CheckDistributeStatus()
{
	//检查任务是否存在
	if (CAISDBOperatemgr::Intstance()->GetDistributeCount(m_DistrTaskItem) == 0)
	{
		return TRUE;
	}

	CAISGetTransferStatusReq  req;
	CAISGetTransferStatusRsp  rsp;

	req.SetRequstType(HTTP_POST_TYPE);
	req.SetCmdCode("JGetTransferStatus");
	req.SetproviderID(m_DistrTaskItem.m_ProviderID.c_str());
	req.SetassetID(m_DistrTaskItem.m_AssetID.c_str());
	req.SetserviceType(m_DistrTaskItem.m_ServiceType);

	SInt32 ret = m_HttpClient.SendRequest(&req, &rsp);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_INFO("send query distribute request fail.ret:"<<ret);

		m_DistrTaskItem.m_Status = 2;
		m_DistrTaskItem.m_Progress = 100;
		m_DistrTaskItem.m_EndTime = OS::Milliseconds()/1000;
		m_DistrTaskItem.m_ErrCode = CDN_ERR_REQUEST_FAIL ;
		m_DistrTaskItem.m_ErrDes = "request fail";

		CAISDBOperatemgr::Intstance()->UpdatePorcessTaskDistribute(m_DistrTaskItem);

		return TRUE;
	}

	m_DistrTaskItem.m_ErrCode = rsp.GetErrCode();
	m_DistrTaskItem.m_ErrDes = std::string(rsp.GetErrDes());
    m_DistrTaskItem.m_Status = rsp.Getresult().m_Status;
	m_DistrTaskItem.m_Progress = rsp.Getresult().m_Progress;

	Bool isfinish = FALSE;

	//分发成功的任务
	if (m_DistrTaskItem.m_ErrCode == CDN_ERR_SUCCESS)
	{
		//分发成功，已经完成
		if (m_DistrTaskItem.m_Status == TRANF_STATUS_COMPLETE && m_DistrTaskItem.m_Progress == 100) 
		{
			m_DistrTaskItem.m_EndTime = OS::Milliseconds()/1000;
			isfinish = TRUE;
		}
		else
		{
			m_DistrTaskItem.m_ErrDes = "";
		}
	}
	else
	{
		m_DistrTaskItem.m_Status = TRANF_STATUS_COMPLETE;
		m_DistrTaskItem.m_Progress = 100;
		m_DistrTaskItem.m_EndTime = OS::Milliseconds()/1000;
		isfinish = TRUE;
	}

	//更新分发任务状态
	Int32 result = CAISDBOperatemgr::Intstance()->UpdatePorcessTaskDistribute(m_DistrTaskItem);
	if (result != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("update process fail.ret:"<<ret)
		return FALSE;
	} 

	return isfinish;
}
/*******************************************************************************
*  Function   : CancelDistribute
*  Description: 取消分发
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CDistributeTask::CancelDistribute()
{
	return 0;
}
/*******************************************************************************
*  Function   : DeleteDistributeFile
*  Description: 预分发删除任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CDistributeTask::DeleteDistributeFile()
{
	CAISDeleteContentReq req;
	CAISDeleteContentRsp rsp;

	req.SetRequstType(HTTP_POST_TYPE);
	req.SetCmdCode("JDeleteContent");
	req.SetproviderID(m_DistrTaskItem.m_ProviderID.c_str());
	req.SetassetID(m_DistrTaskItem.m_AssetID.c_str());
	req.SetserviceType(m_DistrTaskItem.m_ServiceType);

	SInt32 ret = m_HttpClient.SendRequest(&req, &rsp);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_INFO("send delete distribute request fail.ret:"<<ret);
		return CDN_ERR_REQUEST_FAIL;
	}

	//删除分发任务表
	Int32 result = CAISDBOperatemgr::Intstance()->DeleteDistributeTask(m_DistrTaskItem);
	if (result != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("delete distribute task fail.ret:"<<ret)
		return SYS_ERR_OPER_DB_FAIL;
	} 

	return CDN_ERR_SUCCESS;
}


