/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingesttaskhandle.cpp
*  Description:  注入内容管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/aisdboperatemgr.h"
#include "mgr/ingesttaskhandle.h"
#include "libaisstaticconf.h"

IMPLEMENT_SINGLETON(CIngestTaskHandle)
/*******************************************************************************
*  Function   : CIngestTaskHandle
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CIngestTaskHandle::CIngestTaskHandle()
{
}
/*******************************************************************************
*  Function   : ~CIngestTaskHandle
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CIngestTaskHandle::~CIngestTaskHandle()
{
	//停止线程
	this->StopTask();
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化注入内容管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CIngestTaskHandle::Init()
{
	if (!this->RunTask())
	{
		ERROR_LOG("CIngestTaskHandle run task fail")
		return -3;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CIngestTaskHandle::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : AddIngestTask
*  Description: 添加注入任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CIngestTaskHandle::AddIngestTask(const IngestTaskItem &ingesttask)
{
	Int32 count = CAISDBOperatemgr::Intstance()->GetTaskCount(ingesttask.m_ContentKey.m_ProviderID, 
		                                                      ingesttask.m_ContentKey.m_AssetID, 
											                  CMD_TRANSFER);
	if ( count < 0)
	{
		return CDN_ERR_SERVER_FAIL;
	}

	if (count > 0)
	{
		return CDN_ERR_FILE_EXIST;
	}

	std::list<std::string> nodelist;
	if (CAISDBOperatemgr::Intstance()->GetCINode(nodelist, CLibAISStaticConf::m_BackupNum) != CDN_ERR_SUCCESS)
	{
		return CDN_ERR_SERVER_FAIL;
	}
	
	std::list<std::string>::iterator it = nodelist.begin();
	for (; it != nodelist.end(); it++)
	{
		if (CAISDBOperatemgr::Intstance()->InsertTask(ingesttask, *it) != 0)
		{
			return CDN_ERR_SERVER_FAIL;
		}
	}
	
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetIngestTaskStatus
*  Description: 查询注入状态
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CIngestTaskHandle::GetIngestTaskStatus(const ContentKey &conkey, IngestTaskItem &ingesttask)
{
	Int32 ret = CAISDBOperatemgr::Intstance()->QueryTaskStatus(conkey, ingesttask);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("QueryTaskStatus fail. ret:"<<ret)
		ret = -1;
	}

	return ret;
}
/*******************************************************************************
*  Function   : DeleteContent
*  Description: 删除内容
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CIngestTaskHandle::DeleteContent(const ContentKey &conkey)
{
	std::list<std::string> nodelist;
	if (CAISDBOperatemgr::Intstance()->GetAllCINode(nodelist) != CDN_ERR_SUCCESS)
	{
		return CDN_ERR_SERVER_FAIL;
	}

	Int32 ret = 0;
	std::list<std::string>::iterator it = nodelist.begin();
	for (; it != nodelist.end(); it++)
	{
		ret = CAISDBOperatemgr::Intstance()->DeleteContent(conkey, *it);
		if (ret != 0)
		{
			LOG_ERROR("delete file fail. ret:"<<ret);
		}
	}

	return ret;
}










