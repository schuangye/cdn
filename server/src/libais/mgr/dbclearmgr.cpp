/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    dbclearmgr.h
*  Description:  数据清理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/aisdboperatemgr.h"
#include "dbclearmgr.h"
#include "libaisstaticconf.h"

IMPLEMENT_SINGLETON(CDBClearMgr)
/*******************************************************************************
*  Function   : CDBClearMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CDBClearMgr::CDBClearMgr()
{
}
/*******************************************************************************
*  Function   : CDBClearMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CDBClearMgr::~CDBClearMgr()
{
	//停止线程
	this->StopTask();
}
/*******************************************************************************
*  Function   : CIngestTaskHandle
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CDBClearMgr::Init()
{
	if (!this->RunTask())
	{
		ERROR_LOG("CDBClearMgr run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CIngestTaskHandle
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CDBClearMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//清除完成任务
		CleanFinishiTask();

		//更新节点状态
		RefeshNodeStatus();
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : CIngestTaskHandle
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDBClearMgr::CleanFinishiTask()
{
	CAISDBOperatemgr::Intstance()->CleanExpiredFinishTask(CLibAISStaticConf::m_AisFinishCleanTime * 60 );
	CAISDBOperatemgr::Intstance()->CleanExpiredUnFinishTask(CLibAISStaticConf::m_AisAfterCleanTime * 60 * 60);
	CAISDBOperatemgr::Intstance()->CleanCompleteDistributeTask(CLibAISStaticConf::m_DistributeCleanTime * 60 *60);

	return;
}
/*******************************************************************************
*  Function   : RefeshNodeStatus
*  Description: 更新节点状态
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CDBClearMgr::RefeshNodeStatus()
{
	//节点超时将在线节点更新为不在线
	CAISDBOperatemgr::Intstance()->UpdateNoOnline(CLibAISStaticConf::m_NodeTimeOut);


	//节点恢复，将不在线节点更新为在
	CAISDBOperatemgr::Intstance()->UpdateOnline(CLibAISStaticConf::m_NodeTimeOut);

}
