#ifndef LIB_AIS_STATIC_CONF_H
#define LIB_AIS_STATIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libaisstaicconf.h
*  Description:  ci静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include <string>
#include <list>

class CLibAISStaticConf : public OSTask
{
	DECLARATION_SINGLETON(CLibAISStaticConf)
public:

	//初始化注入内容管理器
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//数据文件
	static std::string                    m_SqlFileName;

	//节点超时时间
	static  SInt32                        m_NodeTimeOut;

	//文件备份数
	static  SInt32                       m_BackupNum;

	//是否开启预分发
	static  SInt32                       m_DistributeIsEnable;

	//同时分发任务数
	static SInt32                        m_AisDistributeTaskNum;

	//注入任务长时间没有启动，经过多长时间后清理
	static SInt32			             m_AisAfterCleanTime;

	//注入任务完成后，经过多长时间后进行清理
	static SInt32			             m_AisFinishCleanTime;

	//任务分发完成后，经过多长时间后进行清理
	static SInt32                        m_DistributeCleanTime;

	//

protected:

	//同步参数
	void SynStaticParam();

};
#endif



