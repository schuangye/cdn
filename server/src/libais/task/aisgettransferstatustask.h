#ifndef AIS_GET_TRANSFER_STATUS_TASK_H
#define AIS_GET_TRANSFER_STATUS_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cigettransferstatustask.h
*  Description:  查询内容状态处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/aisgettransferstatus.h"
#include "libcdnutil/cdnhttptaskbase.h"
#include "libais/aiscommon.h"

class CAISGetTransferStatusTask : public CCDNHttpTaskBase
{
public:

	CAISGetTransferStatusTask();

	virtual ~CAISGetTransferStatusTask();

protected:

	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);

	//填充结构
	void FillContentKey(CAISGetTransferStatusReq *req, ContentKey &conkey);

	//填充返回结果
	void FillStatusResult(const IngestTaskItem &ingesttask, CAISGetTransferStatusRsp *rsp);
};


#endif


