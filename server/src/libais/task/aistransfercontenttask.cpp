/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    citransfercontenttask.cpp
*  Description:  注入内容处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpserver/httpcreatetaskmgr.h"
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libutil/stringtool.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/aistransfercontenttask.h"
#include "libais/libaismacros.h"
#include "mgr/ingesttaskhandle.h"

HTTP_TASK_REGISTER_URL(CMD_AIS_TRANSFER_CONTENT, CAISTransferContentTask)
/*******************************************************************************
*  Function   : CAISTransferContentTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISTransferContentTask::CAISTransferContentTask()
{
}
/*******************************************************************************
*  Function   : ~CAISTransferContentTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISTransferContentTask::~CAISTransferContentTask()
{
}
/*******************************************************************************
*  Function   : DoTask
*  Description: 应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISTransferContentTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp)
{
	if (req == NULL || rsp == NULL)
	{
		return -1;
	}

	//填充消息头
	FillRspHttpHead(rsp, AIS_HTTP_SERVER);

	CAISTransferContentReq reqjson;
	CAISTransferContentRsp rspjson;

	rspjson.SetErrCode(CDN_ERR_SUCCESS);
	rspjson.SetErrDes("Success");

	Int32 ret = reqjson.Decode(req->GetBody());
	std::string sProviderID = STR::LRTrimString(reqjson.GetproviderID());
	std::string sAssetID = STR::LRTrimString(reqjson.GetassetID());
	Int32 serviceType = reqjson.GetserviceType();
	std::string ssourceurl = std::string(reqjson.GetsourceURL());
	std::string sreportURL = std::string(reqjson.GetreportURL());

	if (ret != CDN_ERR_SUCCESS || sProviderID.empty() || sAssetID.empty() || ssourceurl.empty() ||
		serviceType < SERVICE_TYPE_FILE)
	{
		rspjson.SetErrCode(CDN_ERR_PARAME_ERROR);
		rspjson.SetErrDes(GET_ERROR_DES(CDN_ERR_PARAME_ERROR, ""));
	}
	else
	{
		//填充数据
		IngestTaskItem ingesttask;
		FillIngestTask(&reqjson, ingesttask);

		//添加注入任务
		ret = CIngestTaskHandle::Intstance()->AddIngestTask(ingesttask);
		if (ret != 0)
		{
			rspjson.SetErrCode(ret);
			rspjson.SetErrDes(GET_ERROR_DES(ret, ""));
		}
	}

	//填充包体
	FillRspBody(rsp, &rspjson);

	return 0;
}
/*******************************************************************************
*  Function   : FillIngestTask
*  Description: 填充数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CAISTransferContentTask::FillIngestTask(CAISTransferContentReq *req, IngestTaskItem &ingesttask)
{
	ingesttask.m_ContentKey.m_ProviderID   = STR::LRTrimString(req->GetproviderID());
	ingesttask.m_ContentKey.m_AssetID      = STR::LRTrimString(req->GetassetID());

	ingesttask.m_ContentKey.m_ServiceType  = (ServiceType)req->GetserviceType();
	ingesttask.m_SourceURL                 = std::string(req->GetsourceURL());
	ingesttask.m_Status                    = TRANF_STATUS_WAITING;
	ingesttask.m_CreateTime                = OS::Milliseconds();
	ingesttask.m_ReportURL                 = req->GetreportURL();

	return;
}











