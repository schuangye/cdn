/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cigettransferstatustask.cpp
*  Description:  查询内容状态处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpserver/httpcreatetaskmgr.h"
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libutil/stringtool.h"
#include "libais/libaismacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/aisgettransferstatustask.h"
#include "mgr/ingesttaskhandle.h"

HTTP_TASK_REGISTER_URL(CMD_AIS_GET_TRANSFER_STATUS, CAISGetTransferStatusTask)
/*******************************************************************************
*  Function   : CAISGetTransferStatusTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISGetTransferStatusTask::CAISGetTransferStatusTask()
{

}
/*******************************************************************************
*  Function   : ~CAISGetTransferStatusTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISGetTransferStatusTask::~CAISGetTransferStatusTask()
{

}
/*******************************************************************************
*  Function   : DoTask
*  Description: 应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISGetTransferStatusTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp)
{
	if (req == NULL || rsp == NULL)
	{
		return -1;
	}

	//填充消息头
	FillRspHttpHead(rsp, AIS_HTTP_SERVER);

	CAISGetTransferStatusReq reqjson;
	CAISGetTransferStatusRsp rspjson;

	rspjson.SetErrCode(CDN_ERR_SUCCESS);
	rspjson.SetErrDes("Success");

	Int32 ret = reqjson.Decode(req->GetBody());
	std::string sProviderID = STR::LRTrimString(reqjson.GetproviderID());
	std::string sAssetID = STR::LRTrimString(reqjson.GetassetID());
	Int32 serviceType = reqjson.GetserviceType();
	if (ret != 0 || sProviderID.empty() || sAssetID.empty() || serviceType < SERVICE_TYPE_FILE)
	{
		rspjson.SetErrCode(CDN_ERR_PARAME_ERROR);
		rspjson.SetErrDes(GET_ERROR_DES(CDN_ERR_PARAME_ERROR, ""));
	}
	else
	{
		//填充结构
		ContentKey conkey;
		FillContentKey(&reqjson, conkey);

		//查询注入文件状态
		IngestTaskItem ingesttask;
		ret = CIngestTaskHandle::Intstance()->GetIngestTaskStatus(conkey, ingesttask);
		if (ret != 0)
		{
			rspjson.SetErrCode(ret);
			rspjson.SetErrDes(GET_ERROR_DES(ret, ""));
		}
		else
		{
			if (ingesttask.m_ErrCode != 0)
			{
				rspjson.SetErrCode(ingesttask.m_ErrCode);
				rspjson.SetErrDes(GET_ERROR_DES(ingesttask.m_ErrCode, ""));
			}
			FillStatusResult(ingesttask, &rspjson);
		}
	}

	//填充包体
	FillRspBody(rsp, &rspjson);

	return 0;
}
/*******************************************************************************
*  Function   : FillContentKey
*  Description: 填充结构
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CAISGetTransferStatusTask::FillContentKey(CAISGetTransferStatusReq *req, ContentKey &conkey)
{
	conkey.m_ProviderID   = STR::LRTrimString(req->GetproviderID());
	conkey.m_AssetID      = STR::LRTrimString(req->GetassetID());
	conkey.m_ServiceType  = (ServiceType)req->GetserviceType();

	return;
}
/*******************************************************************************
*  Function   : FillStatusResult
*  Description: 填充返回结果
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CAISGetTransferStatusTask::FillStatusResult(const IngestTaskItem &ingesttask, 
												  CAISGetTransferStatusRsp *rsp)
{
	CIGetStatusResult result;
	memcpy(result.m_ProviderID, ingesttask.m_ContentKey.m_ProviderID.c_str(), ingesttask.m_ContentKey.m_ProviderID.length());
	memcpy(result.m_AssetID, ingesttask.m_ContentKey.m_AssetID.c_str(), ingesttask.m_ContentKey.m_AssetID.length());
	result.m_ServiceType = ingesttask.m_ContentKey.m_ServiceType;
	result.m_Status = ingesttask.m_Status;
	result.m_Progress = ingesttask.m_Progress;

	std::string tmpcreatetime = OS::Localtime(ingesttask.m_CreateTime*1000);
	memcpy(result.m_CreateTime,  tmpcreatetime.c_str(), tmpcreatetime.length());

	if (ingesttask.m_StartTime > 0)
	{
		std::string tmpstarttime = OS::Localtime(ingesttask.m_StartTime*1000);
		memcpy(result.m_StartTime,  tmpstarttime.c_str(), tmpstarttime.length());
	}

	if (ingesttask.m_EndTime > 0)
	{
		std::string tmpendtime = OS::Localtime(ingesttask.m_EndTime*1000);
		memcpy(result.m_EndTime,  tmpendtime.c_str(), tmpendtime.length());
	}

	result.m_ContentSize = ingesttask.m_FileSize;
	result.m_Duration    = ingesttask.m_Duration;
	result.m_AvgBitRate  = ingesttask.m_BiteRate;
	if (ingesttask.m_FileMD5.find("NULL") == std::string::npos && 
		ingesttask.m_FileMD5.find("null") == std::string::npos)
	{
		memcpy(result.m_Md5CheckSum,  ingesttask.m_FileMD5.c_str(), ingesttask.m_FileMD5.length());
	}

	rsp->Setresult(result);

	return;
}





