/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cideletecontenttask.h
*  Description:  删除内容处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpserver/httpcreatetaskmgr.h"
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libutil/stringtool.h"
#include "libais/libaismacros.h"
#include "libais/aiscommon.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/aisdeletecontenttask.h"
#include "mgr/ingesttaskhandle.h"
#include "mgr/distributetaskmgr.h"

HTTP_TASK_REGISTER_URL(CMD_AIS_DELETE_CONTENT, CAISDeleteContentTask)
/*******************************************************************************
*  Function   : CAISDeleteContentTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISDeleteContentTask::CAISDeleteContentTask()
{
}
/*******************************************************************************
*  Function   : ~CAISDeleteContentTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISDeleteContentTask::~CAISDeleteContentTask()
{
}
/*******************************************************************************
*  Function   : DoTask
*  Description: 应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDeleteContentTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp)
{
	if (req == NULL || rsp == NULL)
	{
		return -1;
	}

	//填充消息头
	FillRspHttpHead(rsp, AIS_HTTP_SERVER);

	CAISDeleteContentReq reqjson;
	CAISDeleteContentRsp rspjson;

	rspjson.SetErrCode(CDN_ERR_SUCCESS);
	rspjson.SetErrDes("Success");

	Int32 ret = reqjson.Decode(req->GetBody());
	std::string sProviderID = STR::LRTrimString(reqjson.GetproviderID());
	std::string sAssetID = STR::LRTrimString(reqjson.GetassetID());
	Int32 serviceType = reqjson.GetserviceType();
	if (ret != 0 || sProviderID.empty() || sAssetID.empty() || serviceType < SERVICE_TYPE_FILE)
	{
		rspjson.SetErrCode(CDN_ERR_PARAME_ERROR);
		rspjson.SetErrDes(GET_ERROR_DES(CDN_ERR_PARAME_ERROR, ""));
	}
	else
	{
		//填充结构
		ContentKey conkey;
		FillContentKey(&reqjson, conkey);

		//加载预分发删除任务
		DistributeTaskItem taskitem;
		taskitem.m_ProviderID = conkey.m_ProviderID;
		taskitem.m_AssetID = conkey.m_AssetID;
		taskitem.m_ServiceType = conkey.m_ServiceType;

		CDistributeTaskMgr::Intstance()->DeleteSubNodeFile(taskitem);

		ret = CIngestTaskHandle::Intstance()->DeleteContent(conkey);
		if (ret != 0)
		{
			rspjson.SetErrCode(ret);
			rspjson.SetErrDes(GET_ERROR_DES(ret, ""));
		}
	}

	//填充包体
	FillRspBody(rsp, &rspjson);
	return 0;
}
/*******************************************************************************
*  Function   : FillContentKe
*  Description: 填充结构
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CAISDeleteContentTask::FillContentKey(CAISDeleteContentReq *req, ContentKey &conkey)
{
	conkey.m_ProviderID   = STR::LRTrimString(req->GetproviderID());
	conkey.m_AssetID      = STR::LRTrimString(req->GetassetID());
	conkey.m_ServiceType  = (ServiceType)req->GetserviceType();

	return;
}




