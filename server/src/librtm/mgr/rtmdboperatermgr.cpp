/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rtmdbopreater.h
*  Description:  数据库处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libdb/dbconnectmgr.h"
#include "libdb/dbexception.h"
#include "libdb/dbresultset.h"
#include "libdb/sqlstatementmgr.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/rtmdbopreatermgr.h"
#include "librtmstaticconf.h"

IMPLEMENT_SINGLETON(CRTMDBOperatermgr)
/*******************************************************************************
 Fuction name:CRTMDBOperatermgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CRTMDBOperatermgr::CRTMDBOperatermgr()
{
}
/*******************************************************************************
 Fuction name:CRTMDBOperatermgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CRTMDBOperatermgr::~CRTMDBOperatermgr()
{
}
/*******************************************************************************
 Fuction name:GetChannelINFO
 Description :获取频道信息
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Int32 CRTMDBOperatermgr::GetAvtiveLiveChannel(LiveChannelList &channellist)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_CHANNEL_INFO_SQL", 
		                                       CLibRTMStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_CHANNEL_INFO_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, CLibRTMStaticConf::m_NodeID.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			LiveChannelItem  taskinfo;
			taskinfo.m_ChannelID = pRecord->GetColSInt64(0);
			taskinfo.m_ChannelName =  pRecord->GetColString(1).c_str();
			taskinfo.m_NodeID = pRecord->GetColString(2).c_str();
			taskinfo.m_ProviderID = pRecord->GetColString(3).c_str();
			taskinfo.m_AssetID = pRecord->GetColString(4).c_str();
			taskinfo.m_SourceUrl = pRecord->GetColString(5).c_str();
			taskinfo.m_SourceType = pRecord->GetColString(6).c_str();
			taskinfo.m_BiteRate = pRecord->GetColSInt32(7);
			taskinfo.m_SliceDur = pRecord->GetColSInt32(8);
			taskinfo.m_RecordeDur = pRecord->GetColSInt32(9);
			taskinfo.m_StorePath = pRecord->GetColString(10).c_str();
			taskinfo.m_IsEnable = pRecord->GetColSInt32(11);

			channellist.push_back(taskinfo);
			pRecord = resultset.NextRow();
		}

	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:InsertDataToFileDistribute
 Description :插入数据到文件分布表中
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Int32 CRTMDBOperatermgr::InsertToFileDistribute(const std::string &pid, const std::string &aid, 
	                                         const std::string &filename, const char *path)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_FILE_DISTRIBUTE_COUNT_SQL", 
		                                       CLibRTMStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_FILE_DISTRIBUTE_COUNT_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, pid.c_str(), aid.c_str(), path, CLibRTMStaticConf::m_NodeID.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

			//检查连接
			CheckDBConnect();
			return SYS_ERR_OPER_DB_FAIL;
		}
		else
		{
			countnum = 0;
		}
	}

	if (countnum == 0)
	{
		sqlstatement = GET_SQL_STRING("INSERT_FILE_DISTRIBUTE_SQL", CLibRTMStaticConf::m_SqlFileName);
		if (sqlstatement == NULL)
		{
			LOG_ERROR("sql not find. INSERT_FILE_DISTRIBUTE_SQL")
			return SYS_ERR_SQL_NO_EXIST;
		}

		memset(sql, 0, sizeof(sql));
		sprintf(sql, sqlstatement, pid.c_str(), aid.c_str(), filename.c_str(), path, 
			    CLibRTMStaticConf::m_NodeID.c_str(), OS::Milliseconds()/1000ll);
		try
		{
			m_DBConnect->Execute(sql);
		}
		catch (CDBException &e)
		{
			LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

			//检查连接
			CheckDBConnect();
			return SYS_ERR_OPER_DB_FAIL;
		}
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:DeleteDataFromFileDistribute
 Description :从文件分布表中删除对应数据
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Int32 CRTMDBOperatermgr::DeleteFromFileDistribute(const std::string &pid, const std::string &aid, 
	                                           const char *path)
{
	const char *sqlstatement = GET_SQL_STRING("DELETE_FROM_DISTRIBUTE_SQL", CLibRTMStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_FROM_DISTRIBUTE_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, pid.c_str(), aid.c_str(), path,  CLibRTMStaticConf::m_NodeID.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
 Fuction name:UpdateBandWidthInfo
 Description :更新带宽信息
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Int32 CRTMDBOperatermgr::UpdateBandWidthInfo(const SInt64 &totalbandwidth, const SInt64 &freebandwidth)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_NODE_COUNT_SQL", CLibRTMStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_NODE_COUNT_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, CLibRTMStaticConf::m_NodeID.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			return SYS_ERR_OPER_DB_FAIL;
		}
		else
		{
			countnum = 0;
		}
	}

	memset(sql, 0, sizeof(sql));
	if (countnum > 0)
	{
		sqlstatement = GET_SQL_STRING("UPDATE_NODE_SQL", CLibRTMStaticConf::m_SqlFileName);
		if (sqlstatement == NULL)
		{
			LOG_ERROR("sql not find. UPDATE_NODE_SQL")
			return SYS_ERR_SQL_NO_EXIST;
		}
		sprintf(sql, sqlstatement, CLibRTMStaticConf::m_PrivateStreamAddr.c_str(), 
			    CLibRTMStaticConf::m_PublicStreamAddr.c_str(), totalbandwidth, freebandwidth,
				OS::Milliseconds()/1000ll, CLibRTMStaticConf::m_NodeID.c_str());
	}
	else
	{
		sqlstatement = GET_SQL_STRING("INSERT_NODE_SQL", CLibRTMStaticConf::m_SqlFileName);
		if (sqlstatement == NULL)
		{
			LOG_ERROR("sql not find. INSERT_NODE_SQL")
			return SYS_ERR_SQL_NO_EXIST;
		}
		sprintf(sql, sqlstatement, CLibRTMStaticConf::m_NodeID.c_str(), "", 0ll, 0ll, 0ll, 
			    CLibRTMStaticConf::m_PrivateStreamAddr.c_str(), CLibRTMStaticConf::m_PublicStreamAddr.c_str(),
				totalbandwidth, freebandwidth, OS::Milliseconds()/1000ll , 1);		
	}

	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		return SYS_ERR_OPER_DB_FAIL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:GetChannelCount
 Description :查询频道是否存在
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Int32 CRTMDBOperatermgr::GetChannelCount(const SInt64 &channelid)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_CHANNEL_COUNT_SQL", 
		                                       CLibRTMStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_CHANNEL_COUNT_SQL")
		return -1;
	}

	SInt32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, channelid, CLibRTMStaticConf::m_NodeID.c_str());

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			countnum = -2;
		}
		else
		{
			countnum = 0;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return countnum;
}
/*******************************************************************************
 Fuction name:GetAllChannelINFO
 Description :获取所有的频道信息
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.15
*******************************************************************************/
Int32 CRTMDBOperatermgr::GetAllLiveChannel(LiveChannelList &channellist)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_ALL_CHANNEL_INFO_SQL", 
		                                       CLibRTMStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_ALL_CHANNEL_INFO_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, CLibRTMStaticConf::m_NodeID.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			LiveChannelItem  taskinfo;
			taskinfo.m_ChannelID = pRecord->GetColSInt64(0);
			taskinfo.m_ChannelName =  pRecord->GetColString(1).c_str();
			taskinfo.m_NodeID = pRecord->GetColString(2).c_str();
			taskinfo.m_ProviderID = pRecord->GetColString(3).c_str();
			taskinfo.m_AssetID = pRecord->GetColString(4).c_str();
			taskinfo.m_SourceUrl = pRecord->GetColString(5).c_str();
			taskinfo.m_SourceType = pRecord->GetColString(6).c_str();
			taskinfo.m_BiteRate = pRecord->GetColSInt32(7);
			taskinfo.m_SliceDur = pRecord->GetColSInt32(8);
			taskinfo.m_RecordeDur = pRecord->GetColSInt32(9);
			taskinfo.m_StorePath = pRecord->GetColString(10).c_str();
			taskinfo.m_IsEnable = pRecord->GetColSInt32(11);

			channellist.push_back(taskinfo);
			pRecord = resultset.NextRow();
		}

	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}