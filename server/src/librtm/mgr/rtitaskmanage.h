#ifndef RTI_TASK_MANAGER_H
#define RTI_TASK_MANAGER_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rtitaskmanager.h.h
*  Description:  直播任务管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "librtm/rtitaskinterface.h"
#include <map>

typedef std::map<SInt64, CRTITaskInterface* >     LiveRecordTaskMap;

class CRTITaskManage : public OSTask
{
	DECLARATION_SINGLETON(CRTITaskManage)
public:

	//初始化任务
	SInt32 Init();

	//线程调用函数
	virtual Bool Run();

protected:

	//开始频道录制任务
	void StartLiveRecordTask();

	//检查频道录制任务状态
	void CheckLiveRecordTask();

	//加载频道信息
	void LoadLiveChannel();

	//清除停止的频道
	void ClearStopLiveChannel();

	//清除过期数据
	void ClearExpireFile();

private:
	LiveRecordTaskMap                          m_LiveRecordTaskMap;
	LiveChannelMap                             m_LiveChannelMap;

	SInt64                                     m_LastLoadLiveTime;
	SInt64                                     m_LastCheckLiveTime;//检查直播状态时间 
	SInt64                                     m_LastClearLiveTime;
	SInt64                                     m_LastClearExpireFileTime;//清除过期文件 
};

#endif
