#ifndef RTI_HEART_BEAT_MGR_H
#define RTI_HEART_BEAT_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rtiheartbeatmgr.h
*  Description:  录制心跳管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"

class CRTIHeartBeatMgr : public OSTask
{
	DECLARATION_SINGLETON(CRTIHeartBeatMgr)
public:

	//初始化任务
	SInt32 Init();

	//线程调用函数
	virtual Bool Run();

protected:

	//更新带宽信息到DB中
	void UpdataBandWidthToDB();

private:

	SInt64                  m_TotalBandWidth;
	SInt64                  m_FreeBandWidth;

	SInt64                  m_LastTime;
	
};
#endif
