/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rtiheartbeatmgr.cpp
*  Description:  录制心跳管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libcore/netbandwidthmgr.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/rtiheartbeatmgr.h"
#include "mgr/rtmdbopreatermgr.h"
#include "librtmstaticconf.h"

IMPLEMENT_SINGLETON(CRTIHeartBeatMgr)
/*******************************************************************************
 Fuction name:CRTIHeartBeatMgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CRTIHeartBeatMgr::CRTIHeartBeatMgr()
{
	m_LastTime   = 0;
}
/*******************************************************************************
 Fuction name:~CRTIHeartBeatMgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
CRTIHeartBeatMgr::~CRTIHeartBeatMgr()
{
	//退出线程
	this->StopTask();

	LOG_DEBUG("CRTIHeartBeatMgr exit");
}
/*******************************************************************************
 Fuction name:Init
 Description :初始化任务
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
SInt32 CRTIHeartBeatMgr::Init()
{
	//网口初始化
	CNetBandWidthMgr::Intstance()->AddNetWorkCardConf(CLibRTMStaticConf::m_NetworkCardFile.c_str());

	if (!this->RunTask())
	{
		ERROR_LOG("CRTIHeartBeatMgr run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:Run
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Bool CRTIHeartBeatMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//更新带宽
		UpdataBandWidthToDB();	

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
 Fuction name:UpdataBandWidthToDB
 Description :更新带宽信息到DB中
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
void CRTIHeartBeatMgr::UpdataBandWidthToDB()
{
	if (OS::Milliseconds()- m_LastTime < 10000)
	{
		return;
	}

	//获取总带宽
	SInt64 totalBandWidth = CNetBandWidthMgr::Intstance()->GetTotalBandWidth(CLibRTMStaticConf::m_NetworkCardFile.c_str());

	//获取空闲带宽
	SInt64 freeBandWidth = CNetBandWidthMgr::Intstance()->GetFreeBandWidth(CLibRTMStaticConf::m_NetworkCardFile.c_str());

	//更新数据库
	SInt32 ret = CRTMDBOperatermgr::Intstance()->UpdateBandWidthInfo(totalBandWidth, freeBandWidth);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("update bandWidth fail.ret:"<<ret)
	}

	m_LastTime = OS::Milliseconds();

	return;
}


