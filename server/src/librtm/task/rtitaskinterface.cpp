/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:     rtitaskinterface.h
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : 
purpose  : 
*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libutil/dodiskapi.h"
#include "libalarm/alarmcommon.h"
#include "libalarm/alarmdefine.h"
#include "libcdnutil/cdnalarmdefine.h"
#include "libcore/ParamManager.h"
#include "libcore/notifyinterface.h"
#include "libcdnutil/cdnerrorcode.h"
#include "librtm/rtitaskinterface.h"
#include "librtmstaticconf.h"

CONF_IMPLEMENT_DYNCREATE(CRTITaskInterface, RuntimeObject)
/*******************************************************************************
*  Function   : CRTITaskInterface
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CRTITaskInterface::CRTITaskInterface()
{
	m_StoreFullPath      = "";
	m_AllSliceCount      = 0;
	m_IsStreamInterruput = FALSE;
}
/*******************************************************************************
*  Function   : ~CRTITaskInterface()
*  Description: 析构函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CRTITaskInterface::~CRTITaskInterface()
{
	StopRecordTask();
}
/*******************************************************************************
*  Function   : Init
*  Description: 直播任务初始化
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CRTITaskInterface::Init(const LiveChannelItem &liveitem)
{
	if (liveitem.m_AssetID.empty() || liveitem.m_ProviderID.empty() || liveitem.m_SliceDur <= 0)
	{
		return -1;
	}

	//构造路径
	std::string storepath = OS::AddEndSymbolToPath(liveitem.m_StorePath);

	//目录是否存在
	char fulldirpath[256] = {0};
	sprintf(fulldirpath,"%s%s_%s/", storepath.c_str(), liveitem.m_ProviderID.c_str(), liveitem.m_AssetID.c_str());
	if (!OS::IsDirExist(fulldirpath))
	{
		OS_Error ret = OS::MakeDir(fulldirpath);
		if (ret != OS_ok)
		{
			LOG_ERROR("make dier fail.ret:"<<ret);
			return -2;
		}
	}

	m_StoreFullPath = fulldirpath;
	m_LiveChannel = liveitem;
	m_AllSliceCount = (m_LiveChannel.m_RecordeDur*60*60)/m_LiveChannel.m_SliceDur;

	return CDN_ERR_SUCCESS;	
}
/*******************************************************************************
*  Function   : StartTask
*  Description: 开启直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CRTITaskInterface::StartRecordTask()
{
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : StopTask
*  Description: 停止直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32	CRTITaskInterface::StopRecordTask()
{
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CheckTaskStatus
*  Description: 检查直播录制任务的状态
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CRTITaskInterface::CheckRecordTaskStatus()
{
	//检查直播源是否正常
	SInt64 now = OS::Seconds();
	SInt64 startfile = (now/m_LiveChannel.m_SliceDur)-3;

	//构造文件名
	std::string dirpath = OS::AddEndSymbolToPath(m_LiveChannel.m_StorePath);
	char fullfilename[512] = {0};
	sprintf(fullfilename, "%s%s_%s/%lld.ts", dirpath.c_str(), m_LiveChannel.m_ProviderID.c_str(), 
		    m_LiveChannel.m_AssetID.c_str(), startfile);

	//通过录制最新文件判断是否断流
	if (!OS::IsFileExist(fullfilename))
	{
		LOG_INFO("fullfilename"<< fullfilename<< " not exit");

		//设置流中断
		SetStreamInterruput();

		return FALSE;
	}

	//设置流恢复
	SetStreamResume();

	return TRUE;
}
/*******************************************************************************
*  Function   : IsReStartRecordTask
*  Description: 检查是否需要重启,在录制返回TRUE, 否则返回FALSE
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CRTITaskInterface::IsReStartRecordTask()
{
	return FALSE;
}
/*******************************************************************************
*  Function   : DeleteExpireFile
*  Description: 清理多余文件
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CRTITaskInterface::ClearExpireFile()
{
	std::list<std::string > delfile;
	CDoDiskAPI::DelFileFromReserveFileNum(m_StoreFullPath.c_str(), m_AllSliceCount, delfile);

	std::list<std::string >::iterator it = delfile.begin();
	for (; it != delfile.end(); it++)
	{
		LOG_DEBUG("delete expire file. "<<it->c_str())
	}

	return;
}
/*******************************************************************************
*  Function   : SetStreamInterruput
*  Description: 设置流中断
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CRTITaskInterface::SetStreamInterruput()
{
	//如果流已经中断，就不需要再发送
	if (GetStreamInterruput())
	{
		return;
	}

	m_IsStreamInterruput = TRUE;

	//发送告警开始
	AlarmNotifyParam parm;
	parm.m_AlarmID = CDN_ALARM_STREAM_INTERRUPT;
	parm.m_AlarmStatus = ALARM_START_FLAG;
	memcpy(parm.m_ModuleID, LIB_RTM_ID, strlen(LIB_RTM_ID));

	const char *serviceid = SERVICE_ID;
	memcpy(parm.m_ServiceID, serviceid, strlen(serviceid));
	parm.m_Time = OS::Milliseconds()/1000;
	memcpy(parm.m_Describe, m_LiveChannel.m_SourceUrl.c_str(), m_LiveChannel.m_SourceUrl.length());

	ASYN_BROADCAST_NOTIFY(ALARM_NOTIFY_ID, &parm, sizeof(AlarmNotifyParam));

	return;
}
/*******************************************************************************
*  Function   : SetStreamInterruput
*  Description: 设置流恢复
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CRTITaskInterface::SetStreamResume()
{
	if (!m_IsStreamInterruput)
	{
		return;
	}

	m_IsStreamInterruput = FALSE;

	//发送告警结束
	AlarmNotifyParam parm;
	parm.m_AlarmID = CDN_ALARM_STREAM_INTERRUPT;
	parm.m_AlarmStatus = ALARM_STOP_FLAG;
	memcpy(parm.m_ModuleID, LIB_RTM_ID, strlen(LIB_RTM_ID));

	const char *serviceid = SERVICE_ID;
	memcpy(parm.m_ServiceID, serviceid, strlen(serviceid));
	parm.m_Time = OS::Milliseconds()/1000;

	ASYN_BROADCAST_NOTIFY(ALARM_NOTIFY_ID, &parm, sizeof(AlarmNotifyParam));

	return;
}
/*******************************************************************************
*  Function   : GetStreamInterruput
*  Description: 获取流中断
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CRTITaskInterface::GetStreamInterruput()
{
	return m_IsStreamInterruput;
}



