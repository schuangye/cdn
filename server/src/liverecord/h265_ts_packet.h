#ifndef H265_TS_PACKET_H
#define H265_TS_PACKET_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    mpeg2_h264_ts_packet.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#define _TS_PACKET_LENGTH_ 188
#include <stdio.h>
#include <string.h>
#include "gmdec.h"
#include "bs.h"
#include "mpeg2_h264_ts_packet.h"

typedef unsigned char uint8_t;
typedef unsigned int uint32_t;
////////////H265////////////////
typedef enum h265_nal_unit_type_t
{
	H265_NAL_UNIT_TRAIL_N     = 0,
	H265_NAL_UNIT_TRAIL_R     = 1,

	H265_NAL_UNIT_TSA_N       = 2,
	H265_NAL_UNIT_TSA_R       = 3,

	H265_NAL_UNIT_STSA_N      = 4,
	H265_NAL_UNIT_STSA_R      = 5,

	H265_NAL_UNIT_RADL_N      = 6,
	H265_NAL_UNIT_RADL_R      = 7,

	H265_NAL_UNIT_RASL_N      = 8,
	H265_NAL_UNIT_RASL_R      = 9,

	H265_NAL_UNIT_RSV_VCL_N10 = 10,
	H265_NAL_UNIT_RSV_VCL_R11 = 11,
	H265_NAL_UNIT_RSV_VCL_N12 = 12,
	H265_NAL_UNIT_RSV_VCL_R13 = 13,
	H265_NAL_UNIT_RSV_VCL_N14 = 14,
	H265_NAL_UNIT_RSV_VCL_R15 = 15,
	H265_NAL_UNIT_BLA_W_LP    = 16,
	H265_NAL_UNIT_BLA_W_RADL  = 17,
	H265_NAL_UNIT_BLA_N_LP    = 18,
	H265_NAL_UNIT_IDR_W_RADL  = 19,
	H265_NAL_UNIT_IDR_N_LP    = 20,
	H265_NAL_UNIT_CRA_NUT     = 21,
	H265_NAL_UNIT_RSV_IRAP_VCL22 = 22,
	H265_NAL_UNIT_RSV_IRAP_VCL23 = 23,
	H265_NAL_UNIT_RSV_VCL24 = 24,
	H265_NAL_UNIT_RSV_VCL25 = 25,
	H265_NAL_UNIT_RSV_VCL26 = 26,
	H265_NAL_UNIT_RSV_VCL27 = 27,
	H265_NAL_UNIT_RSV_VCL28 = 28,
	H265_NAL_UNIT_RSV_VCL29 = 29,
	H265_NAL_UNIT_RSV_VCL30 = 30,
	H265_NAL_UNIT_RSV_VCL31 = 31,

	H265_NAL_UNIT_VPS        = 32,
	H265_NAL_UNIT_SPS        = 33,
	H265_NAL_UNIT_PPS        = 34,
	H265_NAL_UNIT_AUD        = 35,
	H265_NAL_UNIT_EOS_NUT    = 36,
	H265_NAL_UNIT_EOB_NUT    = 37,
	H265_NAL_UNIT_FD_NUT     = 38,
	H265_NAL_UNIT_SEI_PREFIX = 39,
	H265_NAL_UNIT_SEI_SUFFIX = 40,

	H265_NAL_UNIT_RESERVED_NVCL41 =	41, 
	H265_NAL_UNIT_RESERVED_NVCL42 =	42,
	H265_NAL_UNIT_RESERVED_NVCL43 =	43,
	H265_NAL_UNIT_RESERVED_NVCL44 =	44,
	H265_NAL_UNIT_RESERVED_NVCL45 =	45,
	H265_NAL_UNIT_RESERVED_NVCL46 =	46,
	H265_NAL_UNIT_RESERVED_NVCL47 =	47,
	H265_NAL_UNIT_UNSPECIFIED_48  =	48,
	H265_NAL_UNIT_UNSPECIFIED_49  =	49,
	H265_NAL_UNIT_UNSPECIFIED_50  =	50,
	H265_NAL_UNIT_UNSPECIFIED_51  =	51,
	H265_NAL_UNIT_UNSPECIFIED_52  =	52,
	H265_NAL_UNIT_UNSPECIFIED_53  =	53,
	H265_NAL_UNIT_UNSPECIFIED_54  =	54,
	H265_NAL_UNIT_UNSPECIFIED_55  =	55,
	H265_NAL_UNIT_UNSPECIFIED_56  =	56,
	H265_NAL_UNIT_UNSPECIFIED_57  =	57,
	H265_NAL_UNIT_UNSPECIFIED_58  =	58,
	H265_NAL_UNIT_UNSPECIFIED_59  =	59,
	H265_NAL_UNIT_UNSPECIFIED_60  =	60,
	H265_NAL_UNIT_UNSPECIFIED_61  =	61,
	H265_NAL_UNIT_UNSPECIFIED_62  =	62,
	H265_NAL_UNIT_UNSPECIFIED_63  =	63,
	H265_NAL_UNIT_INVALID
}h265_nal_unit_type;


enum h265_slice_type_t
{
	H265_SLICE_TYPE_B = 0,
	H265_SLICE_TYPE_P = 1,
	H265_SLICE_TYPE_I = 2,
};

#define SIZE_OF_NAL_UNIT_HDR    2
#define BIT7            0x80
#define BIT6            0x40
#define BIT5            0x20
#define BIT4            0x10
#define BIT3            0x08
#define BIT2            0x04
#define BIT1            0x02
#define BIT0            0x01

bool find_start_code(uint8_t *addr, uint8_t  zeros)
{
	int i = 0;
	for (; i < zeros; i++)
	{
		if (addr[i]) return false;
	}

	return addr[i] == 0x01 ? true : false;
}

//获取h.265 nal
uint8_t read_nal(uint8_t *start_addr, uint8_t *nal_unit_header, uint32_t *nal_len,
	                    uint32_t eslen)
{
	uint8_t offset = 0;
	uint8_t pop = 0;
	uint8_t *p = start_addr;

	if (find_start_code(p, 2))      // short prefix
	{
		offset = 3;
	}
	else if (find_start_code(p, 3)) // long prefix
	{
		offset = 4;
	}

	//printf("offset=%d\n", offset);
	p += offset;
	pop += offset;

	while (!find_start_code(p, 2) && !find_start_code(p, 3) && pop<=eslen)
	{
		p++;
		pop++;
	}

	nal_unit_header[0] = start_addr[offset];
	nal_unit_header[1] = start_addr[offset + 1];
	*nal_len  = (uint32_t) (p - start_addr);

	return offset;
}

int Get_H265_FrameType(unsigned char *lpdata, unsigned char ucvideotype, int &log2_max_frame_num_minus4)
{

	//unsigned char *lp_pes_addr = lpdata;
	unsigned char *lp_pes_addr = TS_Get_PES_Addr(lpdata); //获取pes头位置
	if (lp_pes_addr == NULL)
	{
		return 0;
	}
	lp_pes_addr += 0x0a;

	h265_nal_unit_type nal_unit_type;
	uint8_t nal_unit_header[SIZE_OF_NAL_UNIT_HDR];
	uint32_t nal_len;

	//获取h.265 nal
	read_nal(lp_pes_addr, nal_unit_header, &nal_len, 188 -(lp_pes_addr-lpdata));
	nal_unit_type = (h265_nal_unit_type)(nal_unit_header[0] & 0x3F);
	switch (nal_unit_type)
	{
		case H265_NAL_UNIT_VPS:
		case H265_NAL_UNIT_SPS:
		case H265_NAL_UNIT_PPS:
		case H265_NAL_UNIT_BLA_W_LP: 
		case H265_NAL_UNIT_BLA_W_RADL:
		case H265_NAL_UNIT_BLA_N_LP:
		case H265_NAL_UNIT_IDR_W_RADL:
		case H265_NAL_UNIT_IDR_N_LP:
		case H265_NAL_UNIT_CRA_NUT:
		{		
				return 1;
		}
		case H265_NAL_UNIT_TRAIL_N:
		case H265_NAL_UNIT_TRAIL_R:
		case H265_NAL_UNIT_TSA_N:
		case H265_NAL_UNIT_TSA_R:
		case H265_NAL_UNIT_STSA_N:
		case H265_NAL_UNIT_STSA_R:
		case H265_NAL_UNIT_RADL_N:
		case H265_NAL_UNIT_RADL_R:
		case H265_NAL_UNIT_RASL_N:
		case H265_NAL_UNIT_RASL_R:
		{
			return 2;
		}
		default:
		{
			return 3;
		}
	}

	return 0;
}
#endif




