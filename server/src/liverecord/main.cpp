/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    main.cpp
*  Description:  测试工程
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <list>
#ifndef WIN32
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/resource.h>
#endif
#include "libutil/OS.h"
#include "libutil/stringtool.h"
#include "livechannel.h"

void Test(const char *url, const char *localip, const char *storpath, Int32 splitdur);
/*******************************************************************************
 Fuction name:Help
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
void Help()
{
	printf("---------------------------------\n");
	printf("-h show help information        -\n");
	printf("-i  input source url            -\n");
	printf("-l locale ip                    -\n");
	printf("-d split dur                    -\n");
	printf("-p store path                   -\n");
	printf("---------------------------------\n");
}

bool g_IsRuning = true;
/*******************************************************************************
 Fuction name:Help
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
void ctrlCHandler(int n)
{
	g_IsRuning = false;
}
/*******************************************************************************
 Fuction name:Help
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
void SetCtrlCHandler()
{
#ifdef WIN32
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)ctrlCHandler,  TRUE); 
#else
	signal(SIGINT, ctrlCHandler);
	signal(SIGTERM, ctrlCHandler);
	signal(SIGABRT, ctrlCHandler);
	signal(SIGUSR1, ctrlCHandler);
	signal(SIGUSR2, ctrlCHandler);
	signal(SIGPIPE, SIG_IGN);

	struct rlimit rl;
	getrlimit(RLIMIT_NOFILE, &rl);
	rl.rlim_cur = FD_SETSIZE;
	rl.rlim_max = FD_SETSIZE;
	setrlimit(RLIMIT_NOFILE, &rl);

#endif
}
int main (int argc, const char * argv[])
{
	const char *sourceurl = NULL;
	const char *localeip = NULL;
	int splitdur = 10;
	const char *storepath = NULL;
	const char *filename = NULL;
	for (int i = 0; i < argc; i++)
	{
		if (STR::StrCmpiCase(argv[i], "-h") == 0)
		{
			Help();
			return 0;
		}
		else if (STR::StrCmpiCase(argv[i], "-i") == 0)
		{
			i++;
			sourceurl = argv[i];
		}
		else if (STR::StrCmpiCase(argv[i], "-l") == 0)
		{
			i++;
			localeip = argv[i];
		}
		else if (STR::StrCmpiCase(argv[i], "-d") == 0)
		{
			i++;
			splitdur = atoi(argv[i]);
		}
		else if (STR::StrCmpiCase(argv[i], "-p") == 0)
		{
			i++;
			storepath = argv[i];
		}
	}

	if (strstr(sourceurl, "UDP://") == NULL || strstr(sourceurl, "udp://") == NULL)
	{
		Test(sourceurl, localeip, storepath, splitdur);
		return 0;
	}
	
	//初始化sockt
	OS::Initialize();

	CLiveChannel livechannel;
	int ret = livechannel.Init(sourceurl, localeip, storepath, splitdur);
	if (ret != 0)
	{
		printf("live channel init fail.ret:%d\n", ret);
		return -1;
	}

	//开始录制
	ret = livechannel.StartRecord();
	if (ret != 0)
	{
		printf("start record fail.ret:%d\n", ret);
		return -2;
	}

	while(g_IsRuning)
	{
		OS::Sleep(1000);
	}

	livechannel.StopRecord();

	return 0;
}

void Test(const char *url, const char *localip, const char *storpath, Int32 splitdur)
{
	if (url == NULL)
	{
		printf("file name is null\n");
		return;
	}

	FILE *file = fopen(url, "rb");
	if (file == NULL)
	{
		printf("open file fail. %s\n", url);
		return;
	}
	
	SInt64 count = 0;
	CHLSRecord    hlsrecord;
	int ret = hlsrecord.Init(storpath, splitdur, WRITE_SIZE);
	UChar *buff = (UChar*)malloc(188);
	int len = fread(buff, 1, 188, file);
	while(len > 0)
	{
		hlsrecord.ProcessData(buff, len);
		count += len;
		printf("======%lld\n", count);
		len = fread(buff, 1, 188, file);
	}

	fclose(file);
}



