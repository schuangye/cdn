#ifndef __PAT_PMT_H__
#define __PAT_PMT_H__
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    pat_pmt.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include <stdint.h>


typedef struct 
{
uint16_t program_num ;
uint16_t pmt_pid;
}program_list ;


typedef struct 
{
	uint8_t  total;
	program_list program[20] ;  //the max stream in one ts
}pat_list ;


pat_list patparse(uint8_t *buf) ;

typedef struct {

	uint8_t  stream_type ;
	uint16_t es_pid ;

}es_list ;

typedef struct {
	uint16_t program_num ;
	uint8_t  version_number;
	uint16_t pcr_pid ;
	uint8_t  total_es ;
	es_list alles[5] ; // video audio teltext,subtitle
}pmt_list;

pmt_list pmtparse(uint8_t *buf,uint16_t pmt_pid) ;
void setpmt_version_number(uint8_t *buf,unsigned char version_number);

#endif

