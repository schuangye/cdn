#ifndef TS_PACKET_H_
#define TS_PACKET_H_
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    mpeg2_h264_ts_packet.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#define _TS_PACKET_LENGTH_ 188
#include <stdio.h>
#include <string.h>
#include "gmdec.h"
#include "h265_ts_packet.h"

inline int TS_Get_FrameType(unsigned char *lpdata,unsigned char ucvideotype,int &log2_max_frame_num_minus4)
{
	if (ucvideotype == 0x02 || ucvideotype == 0x1B)//mpeg2 ����h.264
	{
		return Get_Mpeg_H264_FrameType(lpdata, ucvideotype, log2_max_frame_num_minus4);
	}

	if (ucvideotype == 0x24)//h.265
	{
		return Get_H265_FrameType(lpdata, ucvideotype, log2_max_frame_num_minus4);
	}
	
	return 0;
}


#endif
