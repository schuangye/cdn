#ifndef UDP_SOURCE_H
#define UDP_SOURCE_H
/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: udpsource.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : 直播频道管理
       
*******************************************************************************/
#include "libutil/UDPSocket.h"
#include "hlsrecord.h"

class CUDPSource
{
public:

	CUDPSource();

	virtual ~CUDPSource();

	Int32 Init(const char *url, const char *localip, CHLSRecord *hlsrecord);

	//接收数据
	Int32 Receive();

private:

	CSockAddr               m_UrlAddr;
	CSockAddr               m_LocaleIP;

	UDPSocket               *m_UDPSocket;

	CHLSRecord              *m_HLSRecord;
};




#endif



