#ifndef _DEFINE_MPEG2_H264_TS_PACKET_H_
#define _DEFINE_MPEG2_H264_TS_PACKET_H_
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    mpeg2_h264_ts_packet.h
*  Description:  
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#define _TS_PACKET_LENGTH_ 188
#include <stdio.h>
#include <string.h>
#include "gmdec.h"

////////////H264////////////////
#define NALU_TYPE_SLICE    1
#define NALU_TYPE_DPA      2
#define NALU_TYPE_DPB      3
#define NALU_TYPE_DPC      4
#define NALU_TYPE_IDR      5
#define NALU_TYPE_SEI      6
#define NALU_TYPE_SPS      7
#define NALU_TYPE_PPS      8
#define NALU_TYPE_AUD      9
#define NALU_TYPE_EOSEQ    10
#define NALU_TYPE_EOSTREAM 11
#define NALU_TYPE_FILL     12

typedef enum 
{
	P_SLICE = 0,
	B_SLICE,
	I_SLICE,
	SP_SLICE,
	SI_SLICE
} SliceType;
////////////H264////////////////

inline bool TS_Get_transport_error_indicator(unsigned char *lpdata)
{
	return (*(lpdata+1))&0x80;
}
inline bool TS_Get_payload_unit_start_indicator(unsigned char *lpdata)
{
	return (*(lpdata+1))&0x40;
}
inline void TS_Clear_payload_unit_start_indicator(unsigned char *lpdata)
{
	*(lpdata+1) = (*(lpdata+1)) & 0xBF;
}
inline bool TS_Get_transport_priority(unsigned char *lpdata)
{
	return (*(lpdata+1))&0x20;
}

inline short TS_Get_Pid(unsigned char *lpdata)
{
	return (((*(lpdata +1))&0x1f)<<8)|(*(lpdata +2));
}
inline void TS_Set_Pid(unsigned char *lpdata,short pid)
{
	*(lpdata+1) = (pid>>8)|((*(lpdata+1))&0xe0);
	*(lpdata+2) = pid&0xff;
}
inline unsigned char TS_Get_transport_scrambling_control(unsigned char *lpdata)
{
	return (*(lpdata+3))>>6;
}

inline unsigned char TS_Get_adaptation_field_control(unsigned char *lpdata)
{
	return (*(lpdata+3)&0x30)>>4;
}
inline void TS_Set_adaptation_field_control(unsigned char *lpdata,unsigned char afc)
{
	afc &= 0x03;
	*(lpdata+3) = (((*(lpdata+3))&0xcf)|(afc<<4));
}
inline bool TS_Get_Pcr_flag(unsigned char *lpdata)
{
	if(((*(lpdata +3) & 0x30)>>4)&0x02)
	{
		if(*(lpdata+4) == 0)
			return false;
		if(((*(lpdata+5))>>4)&0x01)
			return true;
		return false;
	}
	return false;
}
inline long long TS_Get_Pcr(unsigned char *lpdata)
{
	if(((*(lpdata + 3) &0x30)>>4)&0x02)
	{
		if(*(lpdata+4) == 0)
			return -1;
		if((*(lpdata+5))&0x10)
		{
			return (((long long)(*(lpdata+6))<<25)|((long long)(*(lpdata+7))<<17)|((long long)(*(lpdata+8))<<9)|((long long)(*(lpdata+9))<<1)|((long long)(*(lpdata+10))>>7))*300+((((*(lpdata+10))&0x01)<<8)|(*(lpdata+11)));
		}
		return -1;
	}
	return -1;
}
inline bool TS_Set_Pcr(unsigned char *lpdata,long long llpcrvalue)
{
	if(llpcrvalue < 0)
		return false;
	if(TS_Get_Pcr_flag(lpdata) != true)
		return false;

	long long lltopbit = llpcrvalue/300;
	unsigned short lllowbit = llpcrvalue%300;

	*(lpdata+6) = (lltopbit>>25)&0xff;
	*(lpdata+7) = (lltopbit>>17)&0xff;
	*(lpdata+8) = (lltopbit>>9)&0xff;
	*(lpdata+9) = (lltopbit>>1)&0xff;
	*(lpdata+10) = ((lltopbit & 0x01)<<7) | 0x7e | (lllowbit>>8);
	*(lpdata+11) = lllowbit & 0xff;

	return true;
}
inline unsigned char TS_Get_continuity_counter(unsigned char *lpdata)
{
	return (*(lpdata+3) & 0x0f);
}
inline void TS_Set_Continuity_Counter(unsigned char *lpdata,unsigned char Counter)
{
	*(lpdata+3) = ((*(lpdata+3))&0xf0) | (Counter & 0x0f);
}
inline static unsigned char *TS_Get_PES_Addr(unsigned char *lpdata)
{
	unsigned char byteAfc = TS_Get_adaptation_field_control(lpdata); //获取调正字段
	if(byteAfc == 0x01)
		return lpdata+4;
	else if(byteAfc == 0x03)
	{
		unsigned char ilength = *(lpdata+4);
		if(ilength == 0)
			return NULL;
		if((ilength + 4 + 1) >= _TS_PACKET_LENGTH_)
			return NULL;
		return lpdata + 4 + ilength + 1;
	}
	return NULL;
}
inline bool TS_Get_Discontinuity(unsigned char *lpdata)
{
	if(((*(lpdata +3) & 0x30)>>4)&0x02)
	{
		if(*(lpdata+4) == 0)
			return false;
		if(((*(lpdata+5))>>4)&0x08)
			return true;
		return false;
	}
	return false;

}
inline unsigned char TS_Get_PTSDTS_flag(unsigned char *lpdata)
{
	if(!TS_Get_payload_unit_start_indicator(lpdata))
		return 0;
	unsigned char *lp_pes_addr = TS_Get_PES_Addr(lpdata);
	if(lp_pes_addr == NULL)
		return 0;
	if((lp_pes_addr - lpdata) > (_TS_PACKET_LENGTH_-10))
		return 0;
	if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01)
		return ((*(lp_pes_addr+7))>>6);
	return 0;

}
inline long long TS_Get_PTS(unsigned char *lpdata)
{
	if(!TS_Get_payload_unit_start_indicator(lpdata))
		return -1;
	unsigned char *lp_pes_addr = TS_Get_PES_Addr(lpdata);
	if(lp_pes_addr == NULL)
		return -1;
	if((lp_pes_addr - lpdata) > (_TS_PACKET_LENGTH_-10))
		return -1;
	if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01)
	{
		if(((*(lp_pes_addr+7))>>6)&0x02)
		{
			return (((long long)((*(lp_pes_addr+9))&0x0e)<<29)|(((long long)(*(lp_pes_addr+10)))<<22)|((long long)((*(lp_pes_addr+11))&0xfe)<<14)|((*(lp_pes_addr+12))<<7)|((*(lp_pes_addr+13))>>1));
		}
		else
			return -1;
	}
	return -1;
}
inline long long TS_Get_DTS(unsigned char *lpdata)
{
	if(!TS_Get_payload_unit_start_indicator(lpdata))
		return -1;
	unsigned char *lp_pes_addr = TS_Get_PES_Addr(lpdata);
	if(lp_pes_addr == NULL)
		return -1;
	if((lp_pes_addr - lpdata) > (_TS_PACKET_LENGTH_-10))
		return -1;
	if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01)
	{
		if(((*(lp_pes_addr+7))>>6)&0x01)
		{
			lp_pes_addr += 5;
			return (((long long)((*(lp_pes_addr+9))&0x0e)<<29)|(((long long)(*(lp_pes_addr+10)))<<22)|((long long)((*(lp_pes_addr+11))&0xfe)<<14)|((*(lp_pes_addr+12))<<7)|((*(lp_pes_addr+13))>>1));
		}
		else
			return -1;
	}
	return -1;

}
inline bool TS_Set_PTS(unsigned char *lpdata,long long llptsvalue)
{
	if(!TS_Get_payload_unit_start_indicator(lpdata))
		return false;
	unsigned char *lp_pes_addr = TS_Get_PES_Addr(lpdata);
	if(lp_pes_addr == NULL)
		return false;
	if((lp_pes_addr - lpdata) > (_TS_PACKET_LENGTH_-10))
		return false;
	if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01)
	{
		if(((*(lp_pes_addr+7))>>6)&0x02)
		{
			*(lp_pes_addr+9) = (((*(lp_pes_addr+9))&0xf1)) | ((llptsvalue>>29)&0x0e);
			*(lp_pes_addr+10) = ((llptsvalue>>22)&0xff);
			*(lp_pes_addr+11) = (((llptsvalue>>14)&0xfe)|0x01);
			*(lp_pes_addr+12) = ((llptsvalue>>7)&0xff);
			*(lp_pes_addr+13) = (((llptsvalue<<1)&0xff)|0x01);
			//return (((long long)((*(lp_pes_addr+9))&0x0e)<<29)|(((long long)(*(lp_pes_addr+10)))<<22)|((long long)((*(lp_pes_addr+11))&0xfe)<<14)|((*(lp_pes_addr+12))<<7)|((*(lp_pes_addr+13))>>1));
			return true;
		}
		else
			return false;
	}
	return false;

}

inline bool TS_Set_DTS(unsigned char *lpdata,long long lldtsvalue)
{
	if(!TS_Get_payload_unit_start_indicator(lpdata))
		return false;
	unsigned char *lp_pes_addr = TS_Get_PES_Addr(lpdata);
	if(lp_pes_addr == NULL)
		return false;
	if((lp_pes_addr - lpdata) > (_TS_PACKET_LENGTH_-10))
		return false;
	if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01)
	{
		if((((*(lp_pes_addr+7))>>6)&0x03) == 0x03)
		{
			lp_pes_addr += 5;
			*(lp_pes_addr+9) = (((*(lp_pes_addr+9))&0xf1)) | ((lldtsvalue>>29)&0x0e);
			*(lp_pes_addr+10) = ((lldtsvalue>>22)&0xff);
			*(lp_pes_addr+11) = (((lldtsvalue>>14)&0xfe)|0x01);
			*(lp_pes_addr+12) = ((lldtsvalue>>7)&0xff);
			*(lp_pes_addr+13) = (((lldtsvalue<<1)&0xff)|0x01);
			//return (((long long)((*(lp_pes_addr+9))&0x0e)<<29)|(((long long)(*(lp_pes_addr+10)))<<22)|((long long)((*(lp_pes_addr+11))&0xfe)<<14)|((*(lp_pes_addr+12))<<7)|((*(lp_pes_addr+13))>>1));
			return true;
		}
		else
			return false;
	}
	return false;

}
inline int Get_Mpeg_H264_FrameType(unsigned char *lpdata,unsigned char ucvideotype,int &log2_max_frame_num_minus4)
{
	if(ucvideotype != 0x02 && ucvideotype != 0x1B)
	{
		return -1;
	}

	if(!TS_Get_payload_unit_start_indicator(lpdata))
	{
		return 0;
	}

	unsigned char *lp_pes_addr = TS_Get_PES_Addr(lpdata); //获取pes头位置
	if (lp_pes_addr == NULL)
	{
		return 0;
	}
	
	//int a = 0;//debug
	lp_pes_addr += 0x0a;
	if(ucvideotype == 0x02)  //mpeg2
	{
		while((lp_pes_addr - lpdata) < 188-4)
		{
			if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01 && lp_pes_addr[3] == 0xB3)
				return 1;
			if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01 && lp_pes_addr[3] == 0x00)
			{
				if((lp_pes_addr-lpdata) > 188-6)
					return 0;
				return ((lp_pes_addr[5]>>3)&0x07); //get frame type
			}
			++lp_pes_addr;
		}
		return 0;
	}

	if(ucvideotype == 0x1B) //h264
	{
		while((lp_pes_addr - lpdata) < 188-5)
		{
			if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01)
			{
				unsigned char nal_unit_type = lp_pes_addr[3]&0x1f;

				//printf(",nal_unit_type:%d,",nal_unit_type);
				switch(nal_unit_type)
				{
				case NALU_TYPE_SLICE:
					{
						Bitstream currStream;
						currStream.frame_bitoffset = 0;
						currStream.streamBuffer = lp_pes_addr+4;
												
						int start_mb_nr = ue_v("slice header first_mb_in_slice",&currStream);
						int  slice_type   = ue_v ("SH: slice_type", &currStream);//怀疑ue_v取值错误
						if (slice_type>4)
						{
							slice_type -=5;
						}
						if (slice_type == P_SLICE)
						{
							return 2;
						}
						if (slice_type == B_SLICE)
						{
							return 3;
						}
						if (slice_type == I_SLICE)
						{
							return 1;
						}
					}
				case NALU_TYPE_DPA:
					{
						Bitstream currStream;
						currStream.frame_bitoffset = 0;
						currStream.streamBuffer = lp_pes_addr+4;
						int start_mb_nr = ue_v("slice header first_mb_in_slice",&currStream);
						int  slice_type   = ue_v ("SH: slice_type", &currStream);
						if (slice_type>4)
						{
							slice_type -=5;
						}
						if (slice_type == P_SLICE)
						{
							return 2;
						}
						if (slice_type == B_SLICE)
						{
							return 3;
						}
						if (slice_type == I_SLICE)
						{
							return 1;
						}
					}
				case NALU_TYPE_DPB:

				case NALU_TYPE_DPC:

				case NALU_TYPE_IDR:
					{
						Bitstream currStream;
						currStream.frame_bitoffset = 0;
						currStream.streamBuffer = lp_pes_addr+4;
						
						int start_mb_nr = ue_v("slice header first_mb_in_slice",&currStream);
						int  slice_type   = ue_v ("SH: slice_type", &currStream);
						if (slice_type>4)
						{
							slice_type -=5;
						}
						if (slice_type == P_SLICE)
						{
							return 2;
						}
						if (slice_type == B_SLICE)
						{
							return 3;
						}
						if (slice_type == I_SLICE)
						{
							return 1;
						}
					}
				case NALU_TYPE_SEI:

				case NALU_TYPE_SPS:

				case NALU_TYPE_PPS:

				case NALU_TYPE_AUD:

				case NALU_TYPE_EOSEQ:

				case NALU_TYPE_EOSTREAM:

				case NALU_TYPE_FILL:

				default:
					;
				}
			}
			++lp_pes_addr;
			//a++;
		}
		return 0;
	}
}
/*
inline int RBSPtoSODB(byte *streamBuffer, int last_byte_pos)
{
	int ctr_bit, bitoffset;

	bitoffset = 0; 
	ctr_bit = (streamBuffer[last_byte_pos-1] & (0x01<<bitoffset));   // set up control bit

	while (ctr_bit==0)
	{
		bitoffset++;
		if(bitoffset == 8) 
		{
			if(last_byte_pos == 0)
				printf(" Panic: All zero data sequence in RBSP \n");
			assert(last_byte_pos != 0);
			last_byte_pos -= 1;
			bitoffset = 0;
		}
		ctr_bit= streamBuffer[last_byte_pos-1] & (0x01<<(bitoffset));
	}

	return(last_byte_pos);
}
*/
inline int TS_Get_IDRFrameFlag(unsigned char *lpdata)
{
	if(!TS_Get_payload_unit_start_indicator(lpdata))
		return 0;
	unsigned char *lp_pes_addr = TS_Get_PES_Addr(lpdata);
	if(lp_pes_addr == NULL)
		return 0;
	lp_pes_addr += 0x0a;

	while((lp_pes_addr - lpdata) < 188-5)
	{
		if(lp_pes_addr[0] == 0x00 && lp_pes_addr[1] == 0x00 && lp_pes_addr[2] == 0x01)
		{
			unsigned char nal_unit_type = lp_pes_addr[3]&0x1f;
			if(nal_unit_type == 7)
			{
				return 1;
			}
		}
		++lp_pes_addr;
	}
	return 0;

}
#endif
