/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: udpsource.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : 直播频道管理
       
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#ifndef WIN32
#include <sys/poll.h>
#endif

#include "libutil/stringtool.h"
#include "udpsource.h"
/*******************************************************************************
 Fuction name:CUDPSource
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CUDPSource::CUDPSource()
{
	m_UDPSocket = NULL;
	m_HLSRecord = NULL;
}
/*******************************************************************************
 Fuction name:CUDPSource
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CUDPSource::~CUDPSource()
{
	if (m_UDPSocket != NULL)
	{
		m_UDPSocket->LeaveMulticast(m_UrlAddr.GetIP(), m_LocaleIP.GetIP());
		delete m_UDPSocket;
		m_UDPSocket = NULL;
	}

	m_HLSRecord = NULL;
}
/*******************************************************************************
 Fuction name:Init
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CUDPSource::Init(const char *url, const char *localip, CHLSRecord *hlsrecord)
{
	if (url == NULL || hlsrecord == NULL || hlsrecord == NULL)
	{
		return -1;
	}

	//
	std::string tmpurl = STR::ToUpperStr(url);
	if (tmpurl.find("UDP://") == std::string::npos)
	{
		return -2;
	}

	//获取udp地址
	std::string udpaddr = STR::GetKeyValue(tmpurl.c_str(), "UDP://", "/");
	m_UrlAddr.SetAddr(udpaddr.c_str());

	m_LocaleIP.SetAddr(localip);
	m_UDPSocket = new UDPSocket();
	if (m_UDPSocket->Open() != OS_ok)
	{
		return -3;
	}

	m_UDPSocket->ReuseAddr();
	m_UDPSocket->SetSocketRcvBufSize(1316*4);
	OS_Error ret = m_UDPSocket->SetMulticastInterface(m_LocaleIP.GetIP());
	if (m_UrlAddr.IsMultiCastAddr())
	{
		m_UDPSocket->SetTtl(10);

		//const int loopback = 0; //禁止回馈
		//setsockopt(m_fd,IPPROTO_IP,IP_MULTICAST_LOOP,(char*)&loopback,sizeof(loopback));

		ret = m_UDPSocket->JoinMulticast(m_UrlAddr.GetIP(), m_LocaleIP.GetIP());
		if (ret != OS_ok)
		{
			return -4;
		}
	}

	//绑定网卡
	ret = m_UDPSocket->Bind(m_UrlAddr.GetIP(), m_UrlAddr.GetPort());
	if (ret != OS_ok)
	{
		return -5;
	}

	m_HLSRecord = hlsrecord;

	return 0;
}
/*******************************************************************************
 Fuction name:Receive
 Description :接收数据
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CUDPSource::Receive()
{
	Int32 result = 0;
#ifndef WIN32

	struct pollfd pfds;

	pfds.fd = m_UDPSocket->GetSocketFD();
	if (pfds.fd < 1)
	{
		printf("pfds.fd is error");
		return 0;
	}
	pfds.events	= POLLIN;
	pfds.revents	= 0;

	//result = ::poll(&(m_UDPSocket->GetSocketFD()), 1, 100);

	result = ::poll(&pfds, 1, 100);
	if (result == 0)
	{
		return 0;
	}
	else if (result == -1)
	{
		if (errno == EINTR)
		{
			return 0;
		}

		printf("poll() failed. errno: %d,strerror is %s\n",errno, strerror(errno));
	}
#endif
	
	UChar pszBuf[192512] = {0};
	UInt32 outRecvLen = 0;
	result = m_UDPSocket->RecvFrom(pszBuf, 192512, &outRecvLen);
	if (outRecvLen > 0)
	{
		m_HLSRecord->ProcessData(pszBuf, outRecvLen);
	}
	
	return 0;
}














