/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    scannerfilemgr.h
*  Description:  扫描文件管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libutil/stringtool.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/scannerfilemgr.h"
#include "mgr/cidbopreatermgr.h"
#include "mgr/rediscachefilemgr.h"
#include "libcistaticconf.h"

IMPLEMENT_SINGLETON(CSannerFileMgr)
/*******************************************************************************
*  Function   : CDiskFileScanner
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CSannerFileMgr::CSannerFileMgr()
{
}
/*******************************************************************************
*  Function   : CDiskFileScanner
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CSannerFileMgr::~CSannerFileMgr()
{
	this->StopTask();

	LOG_DEBUG("CSannerFileMgr exit");
}
/*******************************************************************************
*  Function   : CDiskFileScanner
*  Description: 初始化任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CSannerFileMgr::Init()
{
	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("CSannerFileMgr run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : AddFile
*  Description: 添加文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CSannerFileMgr::AddFile(const FileItem &file)
{
	if (FileFilter(file.m_FileName))
	{
		LOG_INFO("file is filter. "<<file.m_FileName)
		return -1;
	}

	m_AddMutex.Lock();
	FileItemMap::iterator it = m_AddFileMap.find(file.m_FileName);
	if (it == m_AddFileMap.end())
	{
		m_AddFileMap[file.m_FileName] = file;
	}
	m_AddMutex.Unlock();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DeleteFile
*  Description: 删除文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CSannerFileMgr::DeleteFile(const FileItem &file)
{
	m_DelMutex.Lock();
	FileItemMap::iterator it = m_DelFileMap.find(file.m_FileName);
	if (it == m_DelFileMap.end())
	{
		m_DelFileMap[file.m_FileName] = file;
	}
	m_DelMutex.Unlock();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CSannerFileMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//刷新数据到db
		RefreshFileToDB();
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : CDiskFileScanner
*  Description: 刷新数据到db
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CSannerFileMgr::RefreshFileToDB()
{
	//添加文件到数据
	AddFileToDB();

	//从数据库删除文件
	DeleteFileFromDB();

	return;
}
/*******************************************************************************
*  Function   : AddFileToDB
*  Description: 添加文件到数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CSannerFileMgr::AddFileToDB()
{
	FileItemMap  tmpaddfilemap;
	m_AddMutex.Lock();
	tmpaddfilemap.insert(m_AddFileMap.begin(), m_AddFileMap.end());
	m_AddFileMap.clear();
	m_AddMutex.Unlock();

	//添加redis缓存
	RedisFileInfoList  fileinfolist;

	FileItemMap::iterator it = tmpaddfilemap.begin();
	for (; it != tmpaddfilemap.end(); it++)
	{
		IngestTaskItem taskitem;

		SplitPidAndAid(it->second.m_FileName, taskitem.m_ContentKey.m_ProviderID, 
			           taskitem.m_ContentKey.m_AssetID);
		if (taskitem.m_ContentKey.m_ProviderID.empty())
		{
			continue;
		}

		taskitem.m_ContentKey.m_ServiceType = GetFileType(it->second.m_FileName);
		taskitem.m_FileName = it->second.m_FileName;
		taskitem.m_Path     = it->second.m_FilePath;
		taskitem.m_FileSize = it->second.m_FileSize;

		SInt32 ret = CCIDBOperatermgr::Intstance()->ScanFileInsertDB(taskitem);
		if (ret != CDN_ERR_SUCCESS)
		{
			LOG_ERROR("scan file to db fail.ret:"<<ret)
		}

		RedisFileInfo fileinfo;
		FillRedisFile(taskitem, fileinfo);
		fileinfolist.push_back(fileinfo);
	}

	//添加文件到redis
	CRedisCacheFileMgr::Intstance()->AddFile(fileinfolist);

	return;
}
/*******************************************************************************
*  Function   : DeleteFileFromDB
*  Description: 删除数据库文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CSannerFileMgr::DeleteFileFromDB()
{
	FileItemMap  tmpfilemap;
	m_DelMutex.Lock();
	tmpfilemap.insert(m_DelFileMap.begin(), m_DelFileMap.end());
	m_DelFileMap.clear();
	m_DelMutex.Unlock();

	//添加redis缓存
	RedisFileInfoList  fileinfolist;

	FileItemMap::iterator it = tmpfilemap.begin();
	for (; it != tmpfilemap.end(); it++)
	{
		IngestTaskItem taskitem;

		SplitPidAndAid(it->second.m_FileName, taskitem.m_ContentKey.m_ProviderID, 
			taskitem.m_ContentKey.m_AssetID);
		if (taskitem.m_ContentKey.m_ProviderID.empty())
		{
			continue;
		}

		taskitem.m_ContentKey.m_ServiceType = GetFileType(it->second.m_FileName);
		taskitem.m_FileName = it->second.m_FileName;
		taskitem.m_Path     = it->second.m_FilePath;
		taskitem.m_FileSize = it->second.m_FileSize;

		SInt32 ret = CCIDBOperatermgr::Intstance()->DeleteFileFromDB(taskitem.m_ContentKey.m_ProviderID, 
			                                                         taskitem.m_ContentKey.m_AssetID,
													                 CLibCIStaticConf::m_NodeID);
		if (ret != CDN_ERR_SUCCESS)
		{
			LOG_ERROR("scan file to db fail.ret:"<<ret)
		}

		RedisFileInfo fileinfo;
		FillRedisFile(taskitem, fileinfo);
		fileinfolist.push_back(fileinfo);
	}

	//添加文件到redis
	CRedisCacheFileMgr::Intstance()->DeleteFile(fileinfolist);

	return;
}
/*******************************************************************************
*  Function   : GetFileType
*  Description: 获取文件类型
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
ServiceType CSannerFileMgr::GetFileType(const std::string &filename)
{
	std::string fileextname = STR::GetExtName(filename);
	STR::ToUpper(fileextname);

	if (fileextname == "TXT")
	{
		return SERVICE_TYPE_FILE;
	}
	else if (fileextname == "PNG" || fileextname == "JPG")
	{
		return SERVICE_TYPE_FILE;
	}
	else if (fileextname == "M3U8")
	{
		return SERVICE_TYPE_HLS;
	}
	else if (fileextname == "MP4")
	{
		return SERVICE_TYPE_FILE;
	}
	else if (fileextname == "FLV")
	{
		return SERVICE_TYPE_FILE;
	}
	else if (fileextname == ".idx")
	{
		return SERVICE_TYPE_NGOD;
	}

	return SERVICE_TYPE_FILE;
}
/*******************************************************************************
*  Function   : FileFilter
*  Description: 文件过滤
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CSannerFileMgr::FileFilter(const std::string &filename)
{
	std::string extname = STR::GetExtName(filename);
	SInt32 pos = CLibCIStaticConf::m_ScanFilterFileExtName.find(extname);
	if (pos == std::string::npos)
	{
		return TRUE;
	}

	return FALSE;
}
/*******************************************************************************
*  Function   : SplitPidAndAid
*  Description: 分离pid aid   pid_aid or pid_aid.m3u8
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CSannerFileMgr::SplitPidAndAid(const std::string &filename, std::string &pid, 
	                                std::string &aid)
{
	if (filename.empty())
	{
		return;
	}

	pid = STR::SubStr(filename.c_str(), "", "_");
	aid = STR::SubStr(filename.c_str(), "_", ".");

	if (pid.empty() || aid.empty())
	{
		pid = CLibCIStaticConf::m_DefaultProviderID;
		aid = STR::MoveExtName(filename);
	}

	return;
}
/*******************************************************************************
*  Function   : FillRedisFile
*  Description: 填充redis缓存结构
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CSannerFileMgr::FillRedisFile(const IngestTaskItem &taskitem, RedisFileInfo &tmpfile)
{
	memcpy(tmpfile.m_FileName, taskitem.m_FileName.c_str(), taskitem.m_FileName.length());
	memcpy(tmpfile.m_ProviderID, taskitem.m_ContentKey.m_ProviderID.c_str(), taskitem.m_ContentKey.m_ProviderID.length());
	memcpy(tmpfile.m_AssetID, taskitem.m_ContentKey.m_AssetID.c_str(), taskitem.m_ContentKey.m_AssetID.length());
	memcpy(tmpfile.m_Path, taskitem.m_Path.c_str(), taskitem.m_Path.length());
	tmpfile.m_ServiceType = taskitem.m_ContentKey.m_ServiceType;
	tmpfile.m_FileSize = taskitem.m_FileSize;
	tmpfile.m_Duration = taskitem.m_Duration;
	tmpfile.m_Bitrate  = taskitem.m_BiteRate;

	return;
}





