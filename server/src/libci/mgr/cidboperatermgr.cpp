/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cidbopreater.h
*  Description:  数据库处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libdb/dbconnectmgr.h"
#include "libdb/dbexception.h"
#include "libdb/dbresultset.h"
#include "libdb/sqlstatementmgr.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/cidbopreatermgr.h"
#include "libcistaticconf.h"

IMPLEMENT_SINGLETON(CCIDBOperatermgr)
/*******************************************************************************
 Fuction name:
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCIDBOperatermgr::CCIDBOperatermgr()
{
}
/*******************************************************************************
 Fuction name:
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCIDBOperatermgr::~CCIDBOperatermgr()
{
}
/*******************************************************************************
 Fuction name:RecoveryTaskFromDB
 Description :初始化
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::RecoveryTaskFromDB(const std::string &nodeid, IngestTaskItemList &tasklist)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_RESUME_TASK", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_RESUME_TASK")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, nodeid.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			IngestTaskItem  taskinfo;
			taskinfo.m_TaskID = pRecord->GetColSInt64(0);
			taskinfo.m_Cmd = pRecord->GetColSInt32(1);
            taskinfo.m_ContentKey.m_ProviderID = pRecord->GetColString(2).c_str();
			taskinfo.m_ContentKey.m_AssetID = pRecord->GetColString(3).c_str();
			taskinfo.m_ContentKey.m_ServiceType = (ServiceType)(pRecord->GetColSInt32(4));
			taskinfo.m_FileName = taskinfo.m_ContentKey.m_ProviderID + "_" + taskinfo.m_ContentKey.m_AssetID;
			taskinfo.m_SourceURL = pRecord->GetColString(5).c_str();
			taskinfo.m_NodeID = pRecord->GetColString(6).c_str();
			taskinfo.m_Status = TRANF_STATUS_TRANSFERRING;
			taskinfo.m_Progress = pRecord->GetColSInt32(7);
            taskinfo.m_CreateTime = pRecord->GetColSInt64(8);
			taskinfo.m_StartTime = pRecord->GetColSInt64(9);
            taskinfo.m_Path = pRecord->GetColString(10).c_str();
			taskinfo.m_ReportURL = pRecord->GetColString(11).c_str();

			tasklist.push_back(taskinfo);
			pRecord = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : LoadTaskFromDB
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCIDBOperatermgr::LoadTaskFromDB(const std::string &nodeid, IngestTaskItemList &tasklist)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_TASK", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_TASK")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, nodeid.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			IngestTaskItem  taskinfo;
			taskinfo.m_TaskID = pRecord->GetColSInt64(0);
			taskinfo.m_Cmd = pRecord->GetColSInt32(1);
			taskinfo.m_ContentKey.m_ProviderID = pRecord->GetColString(2).c_str();
			taskinfo.m_ContentKey.m_AssetID = pRecord->GetColString(3).c_str();
			taskinfo.m_ContentKey.m_ServiceType = (ServiceType)(pRecord->GetColSInt32(4));
			taskinfo.m_FileName = taskinfo.m_ContentKey.m_ProviderID + "_" + taskinfo.m_ContentKey.m_AssetID;
			taskinfo.m_SourceURL = pRecord->GetColString(5).c_str();
			taskinfo.m_NodeID = pRecord->GetColString(6).c_str();
			taskinfo.m_Status = TRANF_STATUS_WAITING;
			taskinfo.m_CreateTime = pRecord->GetColSInt64(7);
			taskinfo.m_ReportURL = pRecord->GetColString(8).c_str();

			tasklist.push_back(taskinfo);
			pRecord = resultset.NextRow();
		}

	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
 Fuction name:UpdateTask
 Description :更新下载状态
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::UpdateTask(const IngestTaskItem &taskitem)
{
	const char *sqlstatement = GET_SQL_STRING("UP_DATE_TASK", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_TASK")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, taskitem.m_Status, taskitem.m_StartTime, taskitem.m_Path.c_str(), 
		     taskitem.m_NodeID.c_str(), taskitem.m_TaskID);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
Fuction name:UpdatePercent
Description :更新下载状态
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::UpdatePercent(const IngestTaskItem &taskitem)
{
	const char *sqlstatement = GET_SQL_STRING("UPDATE_PROGRESS", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UPDATE_PROGRESS")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, taskitem.m_Progress, taskitem.m_NodeID.c_str(), taskitem.m_TaskID);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
Fuction name:DoFinishFile
Description :
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::DoFinish(const IngestTaskItem &taskitem)
{
	//更新任务状态
	UpDateTaskResult(taskitem);
	if (taskitem.m_ErrCode != CDN_ERR_SUCCESS || taskitem.m_Cmd != CMD_TRANSFER)
	{
		return 0;
	}

	//更新文件表
	SInt32 ret = UpdateFileInfo(taskitem);
	if (ret != 0)
	{
		LOG_ERROR("update file info fail. ret:"<<ret)
	}

	//更新节点表
	ret = UpdateFileNode(taskitem);
	if (ret != 0)
	{
		LOG_ERROR("update file info fail. ret:"<<ret)
	}

	return ret;
}
/*******************************************************************************
Fuction name:UpDateTaskResult
Description :更新任务结果
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::UpDateTaskResult(const IngestTaskItem &taskitem)
{
	const char *sqlstatement = GET_SQL_STRING("UP_DATE_TASK_RESULT", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UP_DATE_TASK_RESULT")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, taskitem.m_Status, 100, taskitem.m_StartTime, taskitem.m_EndTime,
		    taskitem.m_ErrCode, taskitem.m_ErrDes.c_str(), taskitem.m_FileSize, taskitem.m_Duration,
			taskitem.m_BiteRate,taskitem.m_FileMD5.c_str(),taskitem.m_NodeID.c_str(), taskitem.m_TaskID);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
 Fuction name:DeleteFileFromDB
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::DeleteFileFromDB(const std::string &pid, const std::string &aid, 
	                                     const std::string &nodeid)
{
	//删除文件信息表
	const char *sqlstatement = GET_SQL_STRING("DELETE_FILE_INFO", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_FILE_INFO")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, pid.c_str(), aid.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		return SYS_ERR_OPER_DB_FAIL;
	}

	//删除文件分布表
	sqlstatement = GET_SQL_STRING("DELETE_FILE_NODE", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_FILE_NODE")
		return SYS_ERR_SQL_NO_EXIST;
	}

	memset(sql, 0, sizeof(sql));
	sprintf(sql, sqlstatement, pid.c_str(), aid.c_str(), nodeid.c_str());
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		return SYS_ERR_OPER_DB_FAIL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:UpdateDiskInfo
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::UpdateNodeInfo(const CINode &cinode)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_NODE_INFO", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_NODE_INFO")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, cinode.m_NodeID.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			return SYS_ERR_OPER_DB_FAIL;
		}
		else
		{
			countnum = 0;
		}
	}

	memset(sql, 0, sizeof(sql));
	if (countnum > 0)
	{
		sqlstatement = GET_SQL_STRING("UPDATE_NODE_INFO", CLibCIStaticConf::m_SqlFileName);
		if (sqlstatement == NULL)
		{
			LOG_ERROR("sql not find. UPDATE_NODE_INFO")
			return SYS_ERR_SQL_NO_EXIST;
		}

		sprintf(sql, sqlstatement, cinode.m_Path.c_str(), cinode.m_AllDiskSize, 
			    cinode.m_UseDiskSize, cinode.m_ReserDiskSize, cinode.m_PrivateStreamAddr.c_str(),
				cinode.m_PublicStreamAddr.c_str(),cinode.m_TotalBandWidth,cinode.m_FreeBandWidth,
				OS::Milliseconds()/1000ll, cinode.m_NodeID.c_str());
	}
	else
	{
		sqlstatement = GET_SQL_STRING("INSERT_NODE_INFO", CLibCIStaticConf::m_SqlFileName);
		if (sqlstatement == NULL)
		{
			LOG_ERROR("sql not find. INSERT_NODE_INFO")
			return SYS_ERR_SQL_NO_EXIST;
		}
		sprintf(sql, sqlstatement, cinode.m_NodeID.c_str(), cinode.m_Path.c_str(), 
			    cinode.m_AllDiskSize, cinode.m_UseDiskSize, cinode.m_ReserDiskSize, 
			    cinode.m_PrivateStreamAddr.c_str(), cinode.m_PublicStreamAddr.c_str(),
				cinode.m_TotalBandWidth,cinode.m_FreeBandWidth, OS::Milliseconds()/1000ll, 1);
	}

	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		return SYS_ERR_OPER_DB_FAIL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:UpdateDiskInfo
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::DeleteNode(const CINode &cinode)
{
	const char *sqlstatement = GET_SQL_STRING("DELETE_NODE_INFO", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_NODE_INFO")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, cinode.m_NodeID.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
Fuction name:DiskDataInsertDB
Description :
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::ScanFileInsertDB(const IngestTaskItem &taskitem)
{
	if (taskitem.m_ContentKey.m_ProviderID.empty() || taskitem.m_ContentKey.m_AssetID.empty() || 
		taskitem.m_Path.empty())
	{
		return -1;
	}

	//更新文件表
	SInt32 ret = UpdateFileInfo(taskitem);
	if (ret != 0)
	{
		LOG_ERROR("update file info fail. ret:"<<ret)
	}

	//更新节点表
	ret = UpdateFileNode(taskitem);
	if (ret != 0)
	{
		LOG_ERROR("update file info fail. ret:"<<ret)
		ret = -2;
	}

	return ret;
}
/*******************************************************************************
Fuction name:UpdateFileInfo
Description :更新文件信息表
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::UpdateFileInfo(const IngestTaskItem &taskitem)
{
	const char *sqlstatement = GET_SQL_STRING("FILE_INFO_COUNT", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. FILE_INFO_COUNT")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, taskitem.m_ContentKey.m_ProviderID.c_str(), taskitem.m_ContentKey.m_AssetID.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

			//检查连接
			CheckDBConnect();
			return SYS_ERR_OPER_DB_FAIL;
		}
		else
		{
			countnum = 0;
		}
	}

	if (countnum > 0)
	{
		return CDN_ERR_SUCCESS;
	}

	sqlstatement = GET_SQL_STRING("INSERT_FILE_INFO", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. INSERT_FILE_INFO")
		return SYS_ERR_SQL_NO_EXIST;
	}

	memset(sql, 0, sizeof(sql));
	sprintf(sql, sqlstatement, taskitem.m_ContentKey.m_ProviderID.c_str(), 
			 taskitem.m_ContentKey.m_AssetID.c_str(), taskitem.m_ContentKey.m_ServiceType,
			 taskitem.m_FileName.c_str(), taskitem.m_FileSize, taskitem.m_Duration,
			 taskitem.m_BiteRate, OS::Milliseconds()/1000ll, taskitem.m_FileMD5.c_str());
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		return SYS_ERR_OPER_DB_FAIL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
Fuction name:UpdateFileNode
Description :更新文件节点表
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::UpdateFileNode(const IngestTaskItem &taskitem)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_FILE_NODE_COUNT", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_FILE_NODE_COUNT")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, taskitem.m_ContentKey.m_ProviderID.c_str(), 
		    taskitem.m_ContentKey.m_AssetID.c_str(), taskitem.m_NodeID.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

			//检查连接
			CheckDBConnect();
			return SYS_ERR_OPER_DB_FAIL;
		}
		else
		{
			countnum = 0;
		}
	}

	//节点已经存在
	if (countnum > 0)
	{
		return CDN_ERR_SUCCESS;
	}

	sqlstatement = GET_SQL_STRING("INSERT_FILE_NODE", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. INSERT_FILE_NODE")
		return SYS_ERR_SQL_NO_EXIST;
	}

	memset(sql, 0, sizeof(sql));
	sprintf(sql, sqlstatement, taskitem.m_ContentKey.m_ProviderID.c_str(), 
		    taskitem.m_ContentKey.m_AssetID.c_str(), taskitem.m_FileName.c_str(),
			taskitem.m_Path.c_str(), taskitem.m_NodeID.c_str(), OS::Milliseconds()/1000ll);
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		return SYS_ERR_OPER_DB_FAIL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
Fuction name:GetFileDistribute
Description :获取节点上所有的文件分布信息
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::GetFileDistribute(std::list<FileDistribute> &filedistributeList)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_FILE_DISTRIBUTE_SQL", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_FILE_DISTRIBUTE_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, CLibCIStaticConf::m_NodeID.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			FileDistribute filedistribute;
			filedistribute.m_ProviderID = pRecord->GetColString(0).c_str();
			filedistribute.m_AssetID = pRecord->GetColString(1).c_str();
			filedistribute.m_FileName = pRecord->GetColString(2).c_str();
			filedistribute.m_Path = pRecord->GetColString(3).c_str();
			filedistribute.m_NodeID = pRecord->GetColString(4).c_str();
			filedistributeList.push_back(filedistribute);
			pRecord = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
Fuction name:DeleteFileFromDistribute
Description :从文件分布表中删除文件信息
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::DeleteFileFromDistribute(const FileDistribute &filedistibute)
{
	const char *sqlstatement = GET_SQL_STRING("DELETE_FILE_DISTIRBUTE_SQL", 
		                                      CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_FILE_DISTIRBUTE_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, filedistibute.m_FileName.c_str(), filedistibute.m_Path.c_str(), 
		    filedistibute.m_NodeID.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
Fuction name:GetCountFileDistribute
Description :文件分布表中文件总数
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::GetCountFileDistribute(const FileDistribute &filedistibute)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_COUNT_FILE_DISTIRBUTE_SQL", 
		                                      CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_COUNT_FILE_DISTIRBUTE_SQL")
		return -1;
	}

	SInt32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, filedistibute.m_FileName.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//加锁
	m_Mutex.Lock();
	if (m_DBConnect == NULL)
	{
		m_Mutex.Unlock();
		return -2;
	}

	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = -3;
		}
		else
		{
			countnum = 0;
		}
	}
	m_Mutex.Unlock();

	return (ret == CDN_ERR_SUCCESS)?countnum:ret;
}
/*******************************************************************************
Fuction name:DeleteFileFromFileInfo
Description :从文件信息表中删除文件信息
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCIDBOperatermgr::DeleteFileFromFileInfo(const FileDistribute &filedistibute)
{
	//删除文件信息表
	const char *sqlstatement = GET_SQL_STRING("DELETE_FILE_INFO", CLibCIStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_FILE_INFO")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, filedistibute.m_ProviderID.c_str(), filedistibute.m_AssetID.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<". "<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
