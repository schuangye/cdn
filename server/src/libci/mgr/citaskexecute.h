#ifndef CI_TASK_EXECUTE_H
#define CI_TASK_EXECUTE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    citaskexecute.h
*  Description:  注入任务执行管理器，负责消息具体处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "libcore/threadmanager.h"
#include "libci/cicommon.h"
#include "libcdnutil/citaskinterface.h"
#include "libcdnutil/libcdnutilmacros.h"


class CCITaskExecute : public CTask
{
	DECLARE_TASK(CCITaskExecute)
	DECLARATION_SINGLETON(CCITaskExecute)
public:

	Int32 Init();

	//线程调用函数
	virtual Int32 Run();

	//删除任务
	void CancelTask();

	//获取任务ID
	SInt64  GetTaskID();

	//是否取消
	Int32 GetCancel();

protected:

	//任务开始
	SInt32 PreTask(IngestTaskItem &taskitem);

	//开始任务
	SInt32 StartTask(IngestTaskItem &taskitem);

	//任务完成处理
	SInt32 FinishTask(IngestTaskItem &taskitem);

private:

	OSMutex		                  m_MutexCancle;
	SInt32                        m_IsCancel;    //是否取消

	CThreadManager                *m_ThreadManager;
};

#endif

