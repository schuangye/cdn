#ifndef HEART_BEAT_MGR_H
#define HEART_BEAT_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    heartbeatmgr.h
*  Description:  心跳管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"

class CHeartBeatMgr : public OSTask
{
	DECLARATION_SINGLETON(CHeartBeatMgr)
public:

	//初始化任务
	SInt32 Init();

	//线程调用函数
	virtual Bool Run();

protected:

	//更新心跳
	void RefreshHeartBeat();

	//刷新节点信息
	void RefreshNodeInfo();

	//检查磁盘是否需要上报告警
	void CheckDiskAlarm();

private:

	CINode                  m_Node;

	SInt64                  m_LastHeartBeatTime;

	SInt64                  m_LastNodeTime;

	Bool                    m_IsReportDiskAlarm;
};
#endif



