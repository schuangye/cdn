/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    diskmgr.h
*  Description:  磁盘管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libcore/ParamManager.h"
#include "libcore/netbandwidthmgr.h"
#include "libcore/notifyinterface.h"
#include "libutil/dodiskapi.h"
#include "libutil/stringtool.h"
#include "libutil/minixml.h"
#include "libutil/OS.h"
#include "libalarm/alarmcommon.h"
#include "libalarm/alarmdefine.h"
#include "libcdnutil/cdnalarmdefine.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/cidbopreatermgr.h"
#include "mgr/heartbeatmgr.h"
#include "mgr/diskmanage.h"
#include "libcistaticconf.h"

IMPLEMENT_SINGLETON(CHeartBeatMgr)
/*******************************************************************************
 Fuction name:CHeartBeatMgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CHeartBeatMgr::CHeartBeatMgr()
{
	m_LastHeartBeatTime = 0;
	m_LastNodeTime      = 0;
	m_IsReportDiskAlarm = FALSE;
}
/*******************************************************************************
 Fuction name:~CHeartBeatMgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CHeartBeatMgr::~CHeartBeatMgr()
{
	this->StopTask();

	LOG_DEBUG("CHeartBeatMgr exit");
}
/*******************************************************************************
 Fuction name:Init
 Description :初始化任务
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
SInt32 CHeartBeatMgr::Init()
{
	//获取节点信息
	m_Node.m_NodeID     = CLibCIStaticConf::m_NodeID;
	m_Node.m_PrivateStreamAddr = CLibCIStaticConf::m_PrivateStreamAddr;
	m_Node.m_PublicStreamAddr = CLibCIStaticConf::m_PublicStreamAddr;

	//刷新磁盘信息
	CDiskManage::Intstance()->GetNodeDiskInfo(m_Node);

	//网口初始化
	CNetBandWidthMgr::Intstance()->AddNetWorkCardConf(CLibCIStaticConf::m_NetworkCardFile.c_str());

	if (!this->RunTask())
	{
		ERROR_LOG("CHeartBeatMgr run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:Run
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
Bool CHeartBeatMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//更新心跳
		RefreshHeartBeat();

		//更新带宽
		RefreshNodeInfo();
	}

	return TRUE;
}
/*******************************************************************************
 Fuction name:UpdataDiskToDB
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
void CHeartBeatMgr::RefreshHeartBeat()
{
	if (OS::Milliseconds()- m_LastHeartBeatTime < 10000)
	{
		return;
	}

	//更新节点信息
	Int32 ret = CCIDBOperatermgr::Intstance()->UpdateNodeInfo(m_Node);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_WARN("Update disk info fail.ret:"<<ret)
	}

	m_LastHeartBeatTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
 Fuction name:RefreshNodeInfo
 Description :刷新节点信息
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
void CHeartBeatMgr::RefreshNodeInfo()
{
	if (OS::Milliseconds()- m_LastNodeTime < 30000)
	{
		return;
	}

	//获取空闲带宽
	m_Node.m_FreeBandWidth = CNetBandWidthMgr::Intstance()->GetFreeBandWidth(CLibCIStaticConf::m_NetworkCardFile.c_str());

	//获取总带宽
	m_Node.m_TotalBandWidth = CNetBandWidthMgr::Intstance()->GetTotalBandWidth(CLibCIStaticConf::m_NetworkCardFile.c_str());

	//刷新磁盘信息
	CDiskManage::Intstance()->GetNodeDiskInfo(m_Node);

	m_LastNodeTime = OS::Milliseconds();

	//检查磁盘告警
	CheckDiskAlarm();

	return;
}
/*******************************************************************************
 Fuction name:CheckDiskAlarm
 Description :检查磁盘是否需要上报告警
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
void CHeartBeatMgr::CheckDiskAlarm()
{
	//磁盘最低预警
	if (m_Node.m_AllDiskSize - m_Node.m_UseDiskSize > m_Node.m_ReserDiskSize)
	{
		if (!m_IsReportDiskAlarm)
		{
			//发送告警开始
			AlarmNotifyParam parm;
			parm.m_AlarmID = SYS_ALARM_DISK_NO_ENOUGH;
			parm.m_AlarmStatus = ALARM_START_FLAG;
			memcpy(parm.m_ModuleID, LIB_CI_ID, strlen(LIB_CI_ID));

			const char *serviceid = SERVICE_ID;
			memcpy(parm.m_ServiceID, serviceid, strlen(serviceid));
			parm.m_Time = OS::Milliseconds()/1000;
			memcpy(parm.m_Describe, m_Node.m_Path.c_str(), m_Node.m_Path.length());

			ASYN_BROADCAST_NOTIFY(ALARM_NOTIFY_ID, &parm, sizeof(AlarmNotifyParam));

			m_IsReportDiskAlarm = TRUE;
		}
	}
	else//告警结束
	{
		if (m_IsReportDiskAlarm)
		{
			//发送告警开始
			AlarmNotifyParam parm;
			parm.m_AlarmID = SYS_ALARM_DISK_NO_ENOUGH;
			parm.m_AlarmStatus = ALARM_STOP_FLAG;
			memcpy(parm.m_ModuleID, LIB_CI_ID, strlen(LIB_CI_ID));

			const char *serviceid = SERVICE_ID;
			memcpy(parm.m_ServiceID, serviceid, strlen(serviceid));
			parm.m_Time = OS::Milliseconds()/1000;

			ASYN_BROADCAST_NOTIFY(ALARM_NOTIFY_ID, &parm, sizeof(AlarmNotifyParam));

			m_IsReportDiskAlarm = TRUE;
		}
	}

    return;
}



