/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cgcachefilemgr.cpp
*  Description:  cg 缓存文件管理器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libredis/redisstringcommand.h"
#include "libredis/redissetdataobject.h"
#include "libredis/redissetcommand.h"
#include "libredis/redisservermgr.h"
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/libcdnutilmacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/rediscachefilemgr.h"
#include "mgr/cidbopreatermgr.h"
#include "libcistaticconf.h"

IMPLEMENT_SINGLETON(CRedisCacheFileMgr)
/*******************************************************************************
*  Function   : CRedisCacheFileMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CRedisCacheFileMgr::CRedisCacheFileMgr()
{
	m_LastTime   = 0;
}
/*******************************************************************************
*  Function   : ~CRedisCacheFileMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CRedisCacheFileMgr::~CRedisCacheFileMgr()
{
	this->StopTask();

	LOG_DEBUG("CRedisCacheFileMgr exit.");
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化文件缓存管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CRedisCacheFileMgr::Init()
{
	if (!this->RunTask())
	{
		ERROR_LOG("CRedisCacheFileMgr run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: OSTask的执行函数, 执行具体的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CRedisCacheFileMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//添加文件到数据
		SynFileListToRedis();

		//同步数据库数据到redis
		SynFromDBFileToRedis();

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynFileListToRedis
*  Description: 添加文件到redis
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CRedisCacheFileMgr::SynFileListToRedis()
{
	if (CLibCIStaticConf::m_CIIsEnableRedis == DISENABLE_REDIS_CACHE)
	{
		return;
	}

	//添加文件到redis
	DoAddFile();

	//删除文件
	DoDelFile();

	//更新文件
	DoUpdateFile();

	return;
}
/*******************************************************************************
*  Function   : SynFromDBFileToRedis
*  Description: 同步数据库数据到redis
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CRedisCacheFileMgr::SynFromDBFileToRedis()
{
	if (CLibCIStaticConf::m_CIIsEnableRedis == DISENABLE_REDIS_CACHE 
		|| OS::Milliseconds() - m_LastTime < CLibCIStaticConf::m_AsyRedisInterval)
	{
		return;
	}

	FileDistributeList filedistributeList;
	Int32 ret = CCIDBOperatermgr::Intstance()->GetFileDistribute(filedistributeList);
	if (ret != CDN_ERR_SUCCESS)
	{
		ERROR_LOG("get file distribute fail. ret:"<<ret)
		return;
	}

	FileDistributeList::iterator it = filedistributeList.begin();
	for (; it != filedistributeList.end(); it++)
	{
		RedisFileInfo file;
		memcpy(file.m_FileName, it->m_FileName.c_str(), it->m_FileName.length());
		memcpy(file.m_Path, it->m_Path.c_str(), it->m_Path.length());

		ret = DelFileDistributeFromRedis(file);
		if (ret != CDN_ERR_SUCCESS)
		{
			LOG_ERROR("DelFileDistributeFromRedis fail. ret:"<<ret)
		}

		ret = AddFileDistributeToRedis(file);
		if (ret != CDN_ERR_SUCCESS)
		{
			LOG_ERROR("AddFileDistributeToRedis fail. ret:"<<ret)
		}
	}

	m_LastTime = OS::Milliseconds();
	return;
}
/*******************************************************************************
*  Function   : AddFileCGNode
*  Description: 添加文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisCacheFileMgr::AddFile(const RedisFileInfoList &filelist)
{
	if (CLibCIStaticConf::m_CIIsEnableRedis == DISENABLE_REDIS_CACHE || filelist.empty())
	{
		return CDN_ERR_SUCCESS;
	}

	m_AddMutex.Lock();
	m_TmpAddFileList.insert(m_TmpAddFileList.end(), filelist.begin(), filelist.end());
	m_AddMutex.Unlock();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DelFileCGNode
*  Description: 删除文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisCacheFileMgr::DeleteFile(const RedisFileInfoList &filelist)
{
	if (CLibCIStaticConf::m_CIIsEnableRedis == DISENABLE_REDIS_CACHE)
	{
		return CDN_ERR_SUCCESS;
	}

	m_DelMutex.Lock();
	m_TmpDelFileList.insert(m_TmpDelFileList.end(), filelist.begin(), filelist.end());
	m_DelMutex.Unlock();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : UpdateFileCGNode
*  Description: 更新文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisCacheFileMgr::UpdateFile(const RedisFileInfoList &filelist)
{
	if (CLibCIStaticConf::m_CIIsEnableRedis == DISENABLE_REDIS_CACHE)
	{
		return CDN_ERR_SUCCESS;
	}

	m_UpdateMutex.Lock();
	m_TmpUpdateFileList.insert(m_TmpUpdateFileList.end(), filelist.begin(), filelist.end());
	m_UpdateMutex.Unlock();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DoAddFileCGNode
*  Description: 添加文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CRedisCacheFileMgr::DoAddFile()
{
	RedisFileInfoList   addfilelist;

	m_AddMutex.Lock();
	addfilelist.insert(addfilelist.end(), m_TmpAddFileList.begin(), m_TmpAddFileList.end());
	m_TmpAddFileList.clear();
	m_AddMutex.Unlock();

	RedisFileInfoList::iterator it = addfilelist.begin();
	for (; it != addfilelist.end(); it++)
	{
		//优先添加到redis
		Int32 ret = AddFileToRedis(*it);
		if (ret != 0)
		{
			ERROR_LOG("add file to redis fail. ret:"<<ret);
		}

		//添加文件分布
		ret = AddFileDistributeToRedis(*it);
		if (ret != 0)
		{
			ERROR_LOG("add file distribute to redis fail. ret:"<<ret);
		}
	}

	return;
}
/*******************************************************************************
*  Function   : DoDelFile
*  Description: 删除文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CRedisCacheFileMgr::DoDelFile()
{
	RedisFileInfoList   delfilelist;

	m_DelMutex.Lock();
	delfilelist.insert(delfilelist.end(), m_TmpDelFileList.begin(), m_TmpDelFileList.end());
	m_TmpDelFileList.clear();
	m_DelMutex.Unlock();


	RedisFileInfoList::iterator it = delfilelist.begin();
	for (; it != delfilelist.end(); it++)
	{
		//优先删除到redis
		Int32 ret = DelFileFromRedis(it->m_FileName);
		if (ret != 0)
		{
			ERROR_LOG("delete file from redis fail. ret:"<<ret);
		}

		//删除文件分布
		ret = DelFileDistributeFromRedis(*it);
		if (ret != 0)
		{
			ERROR_LOG("delete file distribute from redis fail. ret:"<<ret);
		}
	}

	return;
}
/*******************************************************************************
*  Function   : DoUpdateFile
*  Description: 更新文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CRedisCacheFileMgr::DoUpdateFile()
{
	RedisFileInfoList   updatefilelist;

	m_AddMutex.Lock();
	updatefilelist.insert(updatefilelist.end(), m_TmpUpdateFileList.begin(), m_TmpUpdateFileList.end());
	m_TmpUpdateFileList.clear();
	m_AddMutex.Unlock();

	RedisFileInfoList::iterator it = updatefilelist.begin();
	for (; it != updatefilelist.end(); it++)
	{
		//优先添加到redis
		Int32 ret = AddFileToRedis(*it);
		if (ret != 0)
		{
			ERROR_LOG("add file to redis fail. ret:"<<ret);
		}
	}

	return;
}
/*******************************************************************************
*  Function   : AddFileToRedis
*  Description: 添加文件到redis
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisCacheFileMgr::AddFileToRedis(const RedisFileInfo &file)
{
	CRedisFileInfDataObject *fileinfobject = new CRedisFileInfDataObject();
	fileinfobject->AddFileInfo(file);

	CRedisStringCommand  cmd;
	cmd.SetStringCmdType(REDIS_STRING_SET);
	cmd.SetKey(file.m_FileName);

	//获取文件分布
	Int32 ret = CRedisServerMgr::Intstance()->ExecuteRedisCmd(&cmd, fileinfobject);
	if (ret != 0)
	{
		ERROR_LOG("add file fail. ret:"<<ret)
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DelFileFromRedis
*  Description: 删除文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisCacheFileMgr::DelFileFromRedis(const char *filename)
{
	if (filename == NULL)
	{
		return -1;
	}

	CRedisStringCommand  cmd;
	cmd.SetStringCmdType(REDIS_STRING_DEL);
	cmd.SetKey(filename);

	//更新文件
	Int32 ret = CRedisServerMgr::Intstance()->ExecuteRedisCmd(&cmd, NULL);
	if (ret != 0)
	{
		ERROR_LOG("delete file fail. ret:"<<ret)
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : AddDistributeNodeToRedis
*  Description: 添加节点到文件分布set
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisCacheFileMgr::AddFileDistributeToRedis(const RedisFileInfo &file)
{
	CRedisSetCommand cmd;
	cmd.SetSetCmdType(REDIS_SET_ADD);

	char key[128] = {0};
	sprintf(key, "%s%s", FILE_DISTRIBUTE_SET, file.m_FileName);
	cmd.SetKey(key);

	std::string tmpvalue = CLibCIStaticConf::m_NodeID + "$"+ std::string(file.m_Path);
	CRedisSetDataObject object;
	Int32 ret = object.Decode(tmpvalue.c_str(), tmpvalue.length());
	if (ret != 0)
	{
		ERROR_LOG("Decode  fail. ret:"<<ret)
		return -1;
	}

	//执行redis命令
	ret = CRedisServerMgr::Intstance()->ExecuteRedisCmd(&cmd, &object);
	if (ret != 0)
	{
		ERROR_LOG("add dist node fail. ret:"<<ret)
		return -2;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DelDistributeNodeFromRedis
*  Description: 删除节点到文件分布set
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisCacheFileMgr::DelFileDistributeFromRedis(const RedisFileInfo &file)
{
	CRedisSetCommand cmd;
	cmd.SetSetCmdType(REDIS_SET_REM);

	char key[128] = {0};
	sprintf(key, "%s%s", FILE_DISTRIBUTE_SET, file.m_FileName);
	cmd.SetKey(key);

	std::string tmpvalue = CLibCIStaticConf::m_NodeID + "$"+ std::string(file.m_Path);
	CRedisSetDataObject object;
	Int32 ret = object.Decode(tmpvalue.c_str(), tmpvalue.length());
	if (ret != 0)
	{
		ERROR_LOG("Decode  fail. ret:"<<ret)
		return -1;
	}

	//执行redis命令
	ret = CRedisServerMgr::Intstance()->ExecuteRedisCmd(&cmd, &object);
	if (ret != 0)
	{
		ERROR_LOG("delete dist node fail. ret:"<<ret)
		return -2;
	}

	return CDN_ERR_SUCCESS;
}



