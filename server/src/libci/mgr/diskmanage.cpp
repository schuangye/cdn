/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    diskmgr.h
*  Description:  磁盘管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libcore/netbandwidthmgr.h"
#include "libutil/dodiskapi.h"
#include "libutil/stringtool.h"
#include "libutil/minixml.h"
#include "libutil/OS.h"
#include <sys/stat.h>
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/rediscachefilemgr.h"
#include "mgr/diskmanage.h"
#include "libcistaticconf.h"

IMPLEMENT_SINGLETON(CDiskManage)
/*******************************************************************************
 Fuction name:CDiskManage
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CDiskManage::CDiskManage()
{
	m_MaxFree             = 0;
	m_MaxPath             = "";
	m_DiskPath            = "";
	m_AllDiskSize         = 0;
	m_UseDiskSize         = 0;
	m_ReserDiskSize       = 0;
	m_LastTime            = 0;
}
/*******************************************************************************
 Fuction name:~CDiskManage
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CDiskManage::~CDiskManage()
{
	this->StopTask();

	LOG_INFO("CDiskManage exit");
}
/*******************************************************************************
 Fuction name:Init
 Description :初始化任务
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
SInt32 CDiskManage::Init()
{
	//加载磁盘信息
	LoadDiskInfo();

	//更新磁盘信息
	RefereshDisk();

	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("CDiskManage run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:GetMaxPath
 Description :获取剩余空间最大路径
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
std::string CDiskManage::GetMaxPath() 
{
	std::string tmp = "";
	m_MaxMutex.Lock();
	if(m_MaxFree > 0)
	{
  		tmp = m_MaxPath;
	}
	m_MaxMutex.Unlock();

	return tmp;
}
/*******************************************************************************
 Fuction name:GetMaxFree
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
SInt64 CDiskManage::GetMaxFree()   
{
	m_MaxMutex.Lock();
	SInt64 tmp = m_MaxFree;
	m_MaxMutex.Unlock();

	return tmp;
}
/*******************************************************************************
 Fuction name:GetDiskPath
 Description :获取磁盘磁盘路径
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
std::string CDiskManage::GetDiskPath()
{
	m_MaxMutex.Lock();
	std::string path = m_DiskPath;
	m_MaxMutex.Unlock();

	return path;
}
/*******************************************************************************
 Fuction name:GetDiskList
 Description :获取磁盘列表
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
std::string CDiskManage::GetDiskList(std::vector<std::string > &disklist)
{
	m_MaxMutex.Lock();
	std::string path = m_DiskPath;
	m_MaxMutex.Unlock();

	STR::ReadLinesExt(path.c_str(), disklist, "|");

	return path;
}
/*******************************************************************************
 Fuction name:GetAllDiskSize
 Description :获取总磁盘空间
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
SInt64 CDiskManage::GetAllDiskSize()
{
	m_MaxMutex.Lock();
	SInt64 disksize = m_AllDiskSize;
	m_MaxMutex.Unlock();

	return disksize;
}
/*******************************************************************************
 Fuction name:GetUseDiskSize
 Description :获取使用磁盘空间
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
SInt64 CDiskManage::GetUseDiskSize()
{
	m_MaxMutex.Lock();
	SInt64 disksize = m_UseDiskSize;
	m_MaxMutex.Unlock();

	return disksize;
}
/*******************************************************************************
 Fuction name:GetReserDiskSize
 Description :获取预留磁盘空间
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
SInt64 CDiskManage::GetReserDiskSize()
{
	m_MaxMutex.Lock();
	SInt64 disksize = m_ReserDiskSize;
	m_MaxMutex.Unlock();

	return disksize;
}
/*******************************************************************************
 Fuction name:GetNodeDiskInfo
 Description :获取磁盘信息
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
void CDiskManage::GetNodeDiskInfo(CINode &node)
{
	m_MaxMutex.Lock();
	node.m_Path          = m_DiskPath;
	node.m_AllDiskSize   = m_AllDiskSize;
	node.m_UseDiskSize   = m_UseDiskSize;
	node.m_ReserDiskSize = m_ReserDiskSize;
	m_MaxMutex.Unlock();

	return;
}
/*******************************************************************************
 Fuction name:DeleteFile
 Description :删除文件
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
void CDiskManage::DeleteFile(const std::string &filename)
{
	RedisFileInfoList filelist;

	std::list<CIDiskPath >::iterator it = m_DiskList.begin();
	for (; it != m_DiskList.end(); it++)
	{
		std::string tmpfile = it->m_PathName+filename;
		if (OS::DelFile(tmpfile.c_str()))
		{
			RedisFileInfo tmpfile;
			memcpy(tmpfile.m_FileName, filename.c_str(), filename.length());
			memcpy(tmpfile.m_Path, it->m_PathName.c_str(), it->m_PathName.length());

			filelist.push_back(tmpfile);
		}

		//删除子目录同名文件，只删除一级
		std::list<std::string>  subpathlist;
		OS::GetPathSubPath(it->m_PathName, subpathlist);
		std::list<std::string>::iterator it1 = subpathlist.begin();
		for (; it1 != subpathlist.end(); it1++)
		{
			std::string tmpsubfile = *it1 + filename;
			if (OS::DelFile(tmpsubfile.c_str()))
			{
				RedisFileInfo tmpfile;
				memcpy(tmpfile.m_FileName, filename.c_str(), filename.length());
				memcpy(tmpfile.m_Path, it1->c_str(), it1->length());

				filelist.push_back(tmpfile);
			}
		}
	}

	//从redis删除文件
	CRedisCacheFileMgr::Intstance()->DeleteFile(filelist);

	return;
}
/*******************************************************************************
 Fuction name:DeleteDir
 Description :删除目录
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
void CDiskManage::DeleteDir(const std::string &dir)
{
	std::list<CIDiskPath >::iterator it = m_DiskList.begin();
	for (; it != m_DiskList.end(); it++)
	{
		std::string tmpfile = it->m_PathName+dir;
		OS::DelDir(tmpfile.c_str());
	}

	return;
}
/*******************************************************************************
 Fuction name:Run
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
Bool CDiskManage::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//更新磁盘
		RefereshDisk();
	}

	return TRUE;
}
/*******************************************************************************
 Fuction name:RefereshDisk
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
void CDiskManage::RefereshDisk()
{
	if (OS::Milliseconds() - m_LastTime < 10000)
	{
		return;
	}

	std::string tmppath = "";
	std::string maxpath = "";
	SInt64 tmpDiskAllSize = 0;
	SInt64 tmpUserSize = 0;
	SInt64 tmpReserveSize = 0;
	SInt64 maxfree = 0;
	
	std::list<CIDiskPath>::iterator it = m_DiskList.begin();
	for (; it != m_DiskList.end(); it++)
	{
		SpaceInfo devspaceinfo;
		CDoDiskAPI::GetDiskSpaceInfo(it->m_PathName.c_str(), devspaceinfo);

		tmppath = tmppath+ it->m_PathName + "|";
		tmpDiskAllSize = tmpDiskAllSize +  devspaceinfo.m_TotalSize;
		tmpUserSize = tmpUserSize +  (devspaceinfo.m_TotalSize - devspaceinfo.m_FreeSize);
		tmpReserveSize = tmpReserveSize + it->m_ReserveSize;

		if (maxfree == 0)
		{
			maxpath = it->m_PathName;
			maxfree = devspaceinfo.m_FreeSize - it->m_ReserveSize;
		}
		else
		{
			if ((devspaceinfo.m_FreeSize - it->m_ReserveSize) > maxfree)
			{
				maxpath = it->m_PathName;
				maxfree = devspaceinfo.m_FreeSize - it->m_ReserveSize;
			}
		}
	}

	if (tmppath.empty())
	{
		return;
	}

	m_MaxMutex.Lock();
	m_DiskPath = tmppath;
	m_AllDiskSize = tmpDiskAllSize;
	m_UseDiskSize = tmpUserSize;
	m_ReserDiskSize = tmpReserveSize;

	m_MaxPath = maxpath;
	m_MaxFree = maxfree;
	m_MaxMutex.Unlock();

	m_LastTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
 Fuction name:LoadDiskInfo
 Description :加载磁盘信息
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.5.5
*******************************************************************************/
SInt32 CDiskManage::LoadDiskInfo()
{
	//加载模块配置文件
	CXMLDoc *pDoc = new CXMLDoc(CLibCIStaticConf::m_DiskFile.c_str());
	if (pDoc == NULL || !pDoc->LoadXMLDoc())
	{
		return -1;
	}

	Int32 num = pDoc->GetChildrenNum("disklist");
	for(Int32 i = 0; i < num; i++)
	{
		ostringstream path;
		path<<"disklist/disk&"<<i<<"/path";

		ostringstream reservesize;
		reservesize<<"disklist/disk&"<<i<<"/reservesize";

		std::string tmppath;
		std::string tmpreservesize;
		pDoc->GetContent(path.str(), tmppath);
		pDoc->GetContent(reservesize.str(), tmpreservesize);
		if (tmppath.empty() || tmpreservesize.empty())
		{
			continue;
		}

		CIDiskPath diskpath;
		diskpath.m_PathName = tmppath;
		if (diskpath.m_PathName.at(diskpath.m_PathName.length()-1) != '/')
		{
			diskpath.m_PathName += '/';
		}
		diskpath.m_ReserveSize = STR::Str2int64(tmpreservesize)*1024ll*1024ll*1024ll;
		m_DiskList.push_back(diskpath);
	}

	return CDN_ERR_SUCCESS;
}



