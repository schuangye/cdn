/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingestaskmgr.h
*  Description:  注入内容管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libcore/errcodemacros.h"
#include "libutil/OS.h"
#include "libcdnutil/citaskinterfacemgr.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/reporttaskstatusmgr.h"
#include "mgr/rediscachefilemgr.h"
#include "mgr/cidbopreatermgr.h"
#include "mgr/diskmanage.h"
#include "mgr/citaskmgr.h"
#include "libcistaticconf.h"

IMPLEMENT_SINGLETON(CCITasktmgr)
/*******************************************************************************
*  Function   : CCITasktmgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCITasktmgr::CCITasktmgr()
{
}
/*******************************************************************************
*  Function   : ~CCITasktmgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCITasktmgr::~CCITasktmgr()
{
	//停止线程
	this->StopTask();
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化注入内容管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITasktmgr::Init()
{
	Int32 ret = CCIDBOperatermgr::Intstance()->RecoveryTaskFromDB(CLibCIStaticConf::m_NodeID, m_TaskList);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("RecoveryTaskFromDB fail.ret:"<<ret);
	}

	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("CCITasktmgr run task fail")
		ret = -1;
	}

	return ret;
}
/*******************************************************************************
*  Function   : FinishTask
*  Description: 添加完成任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITasktmgr::FinishTask(const IngestTaskItem &taskitem)
{
	m_FinishTaskMutex.Lock();
	m_FinishTaskList.push_back(taskitem);
	m_FinishTaskMutex.Unlock();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetTaskItem
*  Description: 获取task任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITasktmgr::GetTaskItem(IngestTaskItem &taskitem)
{
	std::string path = CDiskManage::Intstance()->GetMaxPath();
	if (path.empty())
	{
		LOG_INFO("can not download,not enough space!");
		return -1;
	}

	Int32 ret = -2;
	m_TaskMutex.Lock();
	std::map<SInt64, IngestTaskItem >::iterator it = m_TaskIDItemMap.begin();
	for (; it != m_TaskIDItemMap.end(); it++)
	{
		if (it->second.m_Cmd == CMD_TRANSFER)
		{
			taskitem = it->second;
			m_TaskIDItemMap.erase(it);
			ret = CDN_ERR_SUCCESS;
			break;
		}
	}
	m_TaskMutex.Unlock();

	//有任务填充结构
	if (ret == CDN_ERR_SUCCESS)
	{
		taskitem.m_NodeID = CLibCIStaticConf::m_NodeID;
		taskitem.m_FileName = taskitem.m_ContentKey.m_ProviderID + "_" + taskitem.m_ContentKey.m_AssetID;
		taskitem.m_SpliceDur = CLibCIStaticConf::m_SliceDuration;
		taskitem.m_Path = path;
		taskitem.m_Status = TRANF_STATUS_TRANSFERRING;
		taskitem.m_StartTime = OS::Seconds();

		ret = CCIDBOperatermgr::Intstance()->UpdateTask(taskitem);
		if (ret != CDN_ERR_SUCCESS)
		{
			LOG_INFO("Update task fail."<<taskitem.m_FileName<<".ret:"<<ret);
			ret = -3;
		}
	}

	return ret;
}
/*******************************************************************************
*  Function   : AddTaskItem
*  Description: 获取task任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITasktmgr::AddTaskItem(IngestTaskItem &taskitem)
{
	m_TaskMutex.Lock();
	std::map<SInt64, IngestTaskItem >::iterator it = m_TaskIDItemMap.find(taskitem.m_TaskID);
	if(it == m_TaskIDItemMap.end())
	{
		m_TaskIDItemMap[taskitem.m_TaskID] = taskitem;
	}
	m_TaskMutex.Unlock();

	return 0;
}
/*******************************************************************************
*  Function   : RefreshePercent
*  Description: 更新任务进度
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITasktmgr::RefreshePercent(const IngestTaskItem &taskitem)
{
	SInt32 ret = CCIDBOperatermgr::Intstance()->UpdatePercent(taskitem);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_INFO("Update percent fail."<<taskitem.m_FileName<<".ret:"<<ret);
		ret = -1;
	}

	return ret;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCITasktmgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//处理完成任务
		DoFinishTask();

		//加载任务
		DoLoadTaskFromDB();

		//处理非下载任务
		DoNoDLoadTask();
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : DoLoadTaskFromDB
*  Description: 加载数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCITasktmgr::DoLoadTaskFromDB()
{
	std::list<IngestTaskItem> tmpTaskList;

	SInt32 ret = CCIDBOperatermgr::Intstance()->LoadTaskFromDB(CLibCIStaticConf::m_NodeID, 
																tmpTaskList);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_INFO("DoLoadTaskFromDB fail. ret:"<<ret);
		ret = -1;
	}

	if (!tmpTaskList.empty())
	{
		m_TaskList.insert(m_TaskList.end(), tmpTaskList.begin(), tmpTaskList.end());
	}

	return;
}
/*******************************************************************************
*  Function   : DoFinishTask
*  Description: 处理完成任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCITasktmgr::DoFinishTask()
{
	std::list<IngestTaskItem> tmptasklist;

	m_FinishTaskMutex.Lock();
	tmptasklist.insert(tmptasklist.end(), m_FinishTaskList.begin(), m_FinishTaskList.end());
	m_FinishTaskList.clear();
	m_FinishTaskMutex.Unlock();

	RedisFileInfoList filelist;
	std::list<IngestTaskItem>::iterator it = tmptasklist.begin();
	for (; it != tmptasklist.end();)
	{
		//判断是否是注入成功的任务
		if (it->m_ErrCode == CDN_ERR_SUCCESS && it->m_Cmd == CMD_TRANSFER)
		{
			RedisFileInfo tmpfile;
			FillRedisFile(*it, tmpfile);
			filelist.push_back(tmpfile);
		}

		SInt32 ret = CCIDBOperatermgr::Intstance()->DoFinish(*it);
		if (ret != CDN_ERR_SUCCESS)
		{
			LOG_WARN("Task update failed,task filename is:"<<it->m_FileName<<".ret:"<<ret);
			it++;
		}
		else
		{
			//添加上报任务队列
			CReportTaskStatusMgr::Intstance()->AddReportTask(*it);

			LOG_DEBUG("Task is complete,task filename is:"<<it->m_FileName);
			it = tmptasklist.erase(it);
		}
	}

	if (!tmptasklist.empty())
	{
		m_FinishTaskMutex.Lock();
		m_FinishTaskList.insert(m_FinishTaskList.end(), tmptasklist.begin(), tmptasklist.end());
		m_FinishTaskMutex.Unlock();
	}

	//添加文件到redis
	CRedisCacheFileMgr::Intstance()->AddFile(filelist);

	return;
}
/*******************************************************************************
*  Function   : DoNoDLoadTask
*  Description: 处理非下载任务（取消，删除）
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCITasktmgr::DoNoDLoadTask()
{
	if (m_TaskList.size() <= 0)
	{
		return;
	}

	std::list<IngestTaskItem>::iterator itt = m_TaskList.begin();
	for (;itt!=m_TaskList.end();)
	{
		if (itt->m_Cmd != CMD_CANCEL && itt->m_Cmd != CMD_DELETE)
		{
			//添加注入任务
			AddTaskItem(*itt);

			itt++;
			continue;
		}

		LOG_INFO("["<<itt->m_FileName<<"]need is "<<itt->m_Cmd)

		itt->m_Status = TRANF_STATUS_COMPLETE;
		itt->m_ErrCode = CDN_ERR_SUCCESS;
		SET_ERROR_DES(itt->m_ErrDes, CDN_ERR_SUCCESS);
		itt->m_StartTime = OS::Milliseconds()/1000;
		itt->m_EndTime = OS::Milliseconds()/1000;
		if (itt->m_Cmd == CMD_CANCEL)
		{
			CancelTask(*itt);
		}

		//删除文件
		DeleteFile(*itt);

		//处理数据库
		SInt32 ret = CCIDBOperatermgr::Intstance()->DoFinish(*itt);
		if (ret != CDN_ERR_SUCCESS)
		{
			LOG_ERROR("db do finish fail. ret:"<<ret)
		}
		itt = m_TaskList.erase(itt);
	}
	m_TaskList.clear();

	return;
}
/*******************************************************************************
*  Function   : FinishTask
*  Description: 找到指定的文件删除
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITasktmgr::DeleteFile(const IngestTaskItem &taskitem)
{
	IngestTaskItem  tmptaskitem = taskitem;

	//从数据库删除
	SInt32 ret = CCIDBOperatermgr::Intstance()->DeleteFileFromDB(tmptaskitem.m_ContentKey.m_ProviderID, 
		                                                         tmptaskitem.m_ContentKey.m_AssetID, 
		                                                         CLibCIStaticConf::m_NodeID);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("db do finish fail. ret:"<<ret)
	}

	//从磁盘删除
	CCITaskInterface *ciTask = CCITaskInterfaceMgr::Intstance()->CreateCITask(tmptaskitem.m_ContentKey.m_ServiceType);
	if (ciTask == NULL)
	{
		LOG_ERROR("create ci task fail. "<<tmptaskitem.m_ContentKey.m_ServiceType);
		CDiskManage::Intstance()->DeleteFile(tmptaskitem.m_FileName);
		return -1;
	}
	else
	{
		ciTask->DeleteContent(&tmptaskitem);
		CCITaskInterfaceMgr::Intstance()->ReleaseCITask(tmptaskitem.m_ContentKey.m_ServiceType, ciTask);
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CancelTask
*  Description: 取下载的任务，目的取消
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCITasktmgr::CancelTask(const IngestTaskItem &taskitem)
{
	return;
}
/*******************************************************************************
*  Function   : ReportTaskStatus
*  Description: 上报任务完成
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCITasktmgr::ReportTaskStatus(const IngestTaskItem &taskitem)
{

}
/*******************************************************************************
*  Function   : FillRedisFile
*  Description: 填充redis缓存结构
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCITasktmgr::FillRedisFile(const IngestTaskItem &taskitem, RedisFileInfo &tmpfile)
{
	memcpy(tmpfile.m_FileName, taskitem.m_FileName.c_str(), taskitem.m_FileName.length());
	memcpy(tmpfile.m_ProviderID, taskitem.m_ContentKey.m_ProviderID.c_str(), taskitem.m_ContentKey.m_ProviderID.length());
	memcpy(tmpfile.m_AssetID, taskitem.m_ContentKey.m_AssetID.c_str(), taskitem.m_ContentKey.m_AssetID.length());
	memcpy(tmpfile.m_Path, taskitem.m_Path.c_str(), taskitem.m_Path.length());
	tmpfile.m_ServiceType = taskitem.m_ContentKey.m_ServiceType;
	tmpfile.m_FileSize = taskitem.m_FileSize;
	tmpfile.m_Duration = taskitem.m_Duration;
	tmpfile.m_Bitrate  = taskitem.m_BiteRate;

	return;
}








