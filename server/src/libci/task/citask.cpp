/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingestcommontask.h
*  Description:  通用注入任务处理，只注入文件不做任务文件处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/httpdownload.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libutil/md5.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libci/cicommon.h"
#include "libci/citask.h"
#include "mgr/diskmanage.h"

CONF_IMPLEMENT_DYNCREATE(CCITask, CCITaskInterface)
/*******************************************************************************
*  Function   : CCITask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCITask::CCITask()
{
}
/*******************************************************************************
*  Function   : CCITask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
 CCITask::~CCITask()
 {
 }
 /*******************************************************************************
 *  Function   : DoTransferContent
 *  Description: 处理注入消息,不同的注入类型对应不同的注入接口
 *  Calls      : 见函数实现
 *  Called By  : 
 *  Input      : 无
 *  Output     : 无
 *  Return     : 
 *******************************************************************************/
Int32 CCITask::StartTask(void *taskitem)
{
	IngestTaskItem *tmptask = (IngestTaskItem*)taskitem;

	//下载文件
	Int32 ret = DownLoadFile(tmptask);
	if (ret != 0)
	{
		return CDN_ERR_DOWN_LOAD_FAIL;
	}

	return 0;
}
/*******************************************************************************
*  Function   : DoDeleteContent
*  Description: 删除内容接口
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITask::DeleteContent(void *taskitem)
{
	IngestTaskItem *tmptask = (IngestTaskItem*)taskitem;

	std::string filename = tmptask->m_ContentKey.m_ProviderID + "_" + tmptask->m_ContentKey.m_AssetID;
	CDiskManage::Intstance()->DeleteFile(filename);

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DownLoadFile
*  Description: 下载源文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCITask::DownLoadFile(IngestTaskItem *task)
{
	//下载文件
	CHttpDownLoad  httpdownload;
	std::string fullfilename = GetFileName(task);
	Int32 ret = httpdownload.Init(task->m_SourceURL.c_str(), fullfilename.c_str());
	if (ret != 0)
	{
		ERROR_LOG("down load file fail.url:"<<task->m_SourceURL<<".ret:"<<ret);
		return -1;
	}

	//开始下载,返回下载码率
	Int32 biterate;
	ret  = httpdownload.StartDownLoad(biterate);
	if (ret != 0)
	{
		ERROR_LOG("down load file fail.url:"<<task->m_SourceURL<<".ret:"<<ret);
		return -2;
	}

	//获取文件大小
	task->m_FileSize = OS::GetFileSize(fullfilename.c_str());
	task->m_FileMD5 = OS::GetFileMD5String(fullfilename);

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetFileName
*  Description: 获取文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
std::string  CCITask::GetFileName(const IngestTaskItem *task)
{
	if (task == NULL)
	{
		return "";
	}

	std::string fullfilename = "";
	if (task->m_FileName.empty())
	{
		fullfilename = task->m_Path + task->m_ContentKey.m_ProviderID + "_" + task->m_ContentKey.m_AssetID;
	}
	else
	{
		fullfilename = task->m_Path + task->m_FileName;
	}

	return fullfilename;
}


