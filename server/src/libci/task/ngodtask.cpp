/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsvideoingestimp.cpp
*  Description:  hls协议视频注入，只生成m3u8
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/OS.h"
#include "libutil/stringtool.h"
#include "libvideo/libvideoapi.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/ngodtask.h"
#include "mgr/diskmanage.h"
#include "libcistaticconf.h"

INGEST_TASK_REGISTER_DYNCREATE(SERVICE_TYPE_NGOD, CNGODTask, CCITask)
/*******************************************************************************
*  Function   : CNGODTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CNGODTask::CNGODTask()
{
}
/*******************************************************************************
*  Function   : ~CNGODTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CNGODTask::~CNGODTask()
{
}
/*******************************************************************************
*  Function   : StartTask
*  Description: 处理注入消息,不同的注入类型对应不同的注入接口
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNGODTask::StartTask(void *taskitem)
{
	IngestTaskItem *tmptask = (IngestTaskItem*)taskitem;

	//下载文件
	Int32 ret = DownLoadFile(tmptask);
	if (ret != 0)
	{
		return CDN_ERR_DOWN_LOAD_FAIL;
	}

	//创建m3u8
	std::string filename = GetFileName(tmptask);
	Int64 duration = 0;
	Int32 biteRate = CreateNGODFile(filename.c_str(), tmptask->m_ContentKey.m_ProviderID.c_str(), 
		                            tmptask->m_ContentKey.m_AssetID.c_str(), duration);
	if (biteRate <= 0)
	{
		return CDN_ERR_NO_SUPPORT_CONTENT;
	}

	//获取时长
	tmptask->m_Duration = duration;
	tmptask->m_BiteRate = biteRate/1024;;

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DeleteContent
*  Description: 删除内容接口
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNGODTask::DeleteContent(void *taskitem)
{
	IngestTaskItem *tmptask = (IngestTaskItem*)taskitem;

	std::string filename = tmptask->m_ContentKey.m_ProviderID + "_" + tmptask->m_ContentKey.m_AssetID;
	CDiskManage::Intstance()->DeleteFile(filename);
	CDiskManage::Intstance()->DeleteFile(filename+".idx");

	//删除倍速文件
	std::vector<std::string> lines;
	STR::ReadLinesExt(CLibCIStaticConf::m_NgodIndexScale.c_str(), lines, ",", 
					  CLibCIStaticConf::m_NgodIndexScale.length());

	std::vector<std::string>::iterator it = lines.begin();
	for (; it != lines.end(); it++)
	{
		std::string tmpfilename = filename + "_" + *it + "_1.ts";
		CDiskManage::Intstance()->DeleteFile(tmpfilename);

		tmpfilename = filename + "_" + *it + "_2.ts";
		CDiskManage::Intstance()->DeleteFile(tmpfilename);
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CreateM3U8File
*  Description: 生成m3u8文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNGODTask::CreateNGODFile(const char *filename, const char *pid, const char *aid, 
								Int64 &dur)
{
	return LibVideo::CreateNGODAndBitRateDur(filename, pid, aid, CLibCIStaticConf::m_NgodIndexScale.c_str(),
		                                       CLibCIStaticConf::m_ObjVersion, CLibCIStaticConf::m_FrFilesizeMinus1,
											   CLibCIStaticConf::m_OnlyWriteIDR, dur);
}
















