/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcimodule.cpp
*  Description:  libci模块初始化类
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "mgr/reporttaskstatusmgr.h"
#include "libmodulemgr/modulemac.h"
#include "libdb/sqlstatementmgr.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/rediscachefilemgr.h"
#include "mgr/diskfilescanner.h"
#include "mgr/cidbopreatermgr.h"
#include "mgr/citaskexecute.h"
#include "mgr/heartbeatmgr.h"
#include "mgr/diskmanage.h"
#include "mgr/citaskmgr.h"
#include "libcistaticconf.h"
#include "libci/libci_dll.h"

REGISTER_MODULE(libci, LIBCI_EXPORT)
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化libci模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libci::Initialize(const char *argv)
{	
	//初始化配置参数
	if (CLibCIStaticConf::Intstance()->Initialize() != TRUE)
	{
		printf("Init CLibCIStaticConf fail.\n");
		return -1;
	}

	//初始化sql 语句管理器
	SInt32 ret = ADD_SQL_FILE_STRING(CLibCIStaticConf::m_SqlFileName);
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CSqlStatementmgr fail. ret:%d\n", ret);
		return -2;
	}

	//初始化数据库
	ret = CCIDBOperatermgr::Intstance()->Initialize();
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CSqlStatementmgr fail. ret:%d\n", ret);
		return -3;
	}

	//初始化磁盘管理
	ret = CDiskManage::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CDiskManage fail. ret:%d\n", ret);
		return -4;
	}

	//启动心跳管理
	ret = CHeartBeatMgr::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CHeartBeatMgr fail. ret:%d\n", ret);
		return -5;
	}

	//启动redis缓存管理器
	ret = CRedisCacheFileMgr::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CHeartBeatMgr fail. ret:%d\n", ret);
		return -6;
	}

	//初始化下载任务线程
	ret = CCITaskExecute::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CCITaskExecute fail. ret:%d\n", ret);
		return -7;
	}

	//启动上报任务
	CReportTaskStatusMgr::Intstance()->Init();

	//初始化任务管理
	CCITasktmgr::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CCITasktmgr fail. ret:%d\n", ret);
		return -8;
	}

	return ret;
}
/*******************************************************************************
*  Function   : Release
*  Description: 退出libci模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libci::Release()
{
	CCITasktmgr::Intstance()->Destroy();

	CCITaskExecute::Intstance()->Destroy();

	CRedisCacheFileMgr::Intstance()->Destroy();

	CHeartBeatMgr::Intstance()->Destroy();

	CDiskManage::Intstance()->Destroy();

	CCIDBOperatermgr::Intstance()->Destroy();

	//退出sql语句
	REMOVE_SQL_FILE_STRING(CLibCIStaticConf::m_SqlFileName);

	CLibCIStaticConf::Intstance()->Destroy();

	return 0;
}





