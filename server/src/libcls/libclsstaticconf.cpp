/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsstaicparammgr.h
*  Description:  cls静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libcls/libclsmacros.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libclsstaticconf.h"

//cls缓存类型
Int32 CLibCLSStaticConf::m_ClsCacheType = CLS_CACHE_TYPE_MEN;

//时移分片时长
Int32 CLibCLSStaticConf::m_ShiftTimeDuration = 10;

//时移延时文件片数
Int32 CLibCLSStaticConf::m_ShiftTimeDelayNum = 3;

//时移m3u8返回的片数
Int32 CLibCLSStaticConf::m_ShiftTimeReserveNum = 3;

//数据库文件
std::string CLibCLSStaticConf::m_SqlFileName = "../conf/sql/sql.xml";

//全量同步时间间隔，单位为s
Int32 CLibCLSStaticConf::m_ClsIncSynInterval = 120000;

//增量同步时间间隔
Int32 CLibCLSStaticConf::m_ClsAllSynInterval = 86400000;

//增量同步时间间隔
Int32 CLibCLSStaticConf::m_ClsSynNodeInterval = 10000;

//默认使用流地址类型
Int32 CLibCLSStaticConf::m_ClsUseStreamAddrType = CLS_USE_PRIVATEADDR_TYPE;

//校验token key值
std::string CLibCLSStaticConf::m_ClsTokenKey = "songchuangye";

//是否开启token校验 0:标示不开启 1：标示开启
Int32 CLibCLSStaticConf::m_ClsIsCheckToken = 0;

//使用token算法   0：只校验url合法性 1：带有试看功能算法
Int32 CLibCLSStaticConf::m_ClsTokenAlgorithmic = 0;

IMPLEMENT_SINGLETON(CLibCLSStaticConf)
/*******************************************************************************
*  Function   : CLibCLSStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibCLSStaticConf::CLibCLSStaticConf()
{
}
/*******************************************************************************
*  Function   : CLibCLSStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibCLSStaticConf::~CLibCLSStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibCLSStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}/*******************************************************************************
 *  Function   : Run
 *  Description: 线程调用函数
 *  Calls      : 见函数实现
 *  Called By  : 
 *  Input      : 无
 *  Output     : 无
 *  Return     : 
 *******************************************************************************/
Bool CLibCLSStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibCLSStaticConf::SynStaticParam()
{
	//cls缓存类型
	CLibCLSStaticConf::m_ClsCacheType = GET_PARAM_INT(CLS_CACHE_TYPE, CLS_CACHE_TYPE_MEN);

	//时移分片时长
	CLibCLSStaticConf::m_ShiftTimeDuration = GET_PARAM_INT(CLS_SHIFT_TIME_DURATION, 10);

	//时移延时文件片数
	CLibCLSStaticConf::m_ShiftTimeDelayNum = GET_PARAM_INT(CLS_SHIFT_TIME_DELAYNUM, 3);

	//时移m3u8返回的片数
	CLibCLSStaticConf::m_ShiftTimeReserveNum = GET_PARAM_INT(CLS_SHIFT_TIME_RESERVE_NUM, 3);

	//时移当前时间延时，为分级部署使用
	CLibCLSStaticConf::m_SqlFileName = std::string(GET_PARAM_CHAR(CLS_SQL_STATEMENT_FILE, "../conf/sql/sql.xml"));

	//全量同步时间间隔，单位为s
	CLibCLSStaticConf::m_ClsIncSynInterval = GET_PARAM_INT(CLS_INCSYN_INTERVAL, 120)*1000;

	//增量同步时间间隔
	CLibCLSStaticConf::m_ClsAllSynInterval = GET_PARAM_INT(CLS_ALLSYN_INTERVAL, 86400)*1000;

	//节点同步时间间隔，单位为s
	CLibCLSStaticConf::m_ClsSynNodeInterval = GET_PARAM_INT(CLS_SYN_NODE_INTERVAL, 10)*1000;

	//默认使用流地址类型
	CLibCLSStaticConf::m_ClsUseStreamAddrType = GET_PARAM_INT(CLS_USE_STREAMADDR_TYPE, CLS_USE_PRIVATEADDR_TYPE);

	//校验token key值
	CLibCLSStaticConf::m_ClsTokenKey = std::string(GET_PARAM_CHAR(CLS_TOKENKEY, "songchuangye"));

	//是否开启token校验 0:标示不开启 1：标示开启
	CLibCLSStaticConf::m_ClsIsCheckToken = GET_PARAM_INT(CLS_IS_CHECK_TOKEN, 0);

	//使用token算法   0：只校验url合法性 1：带有试看功能算法
	CLibCLSStaticConf::m_ClsTokenAlgorithmic = GET_PARAM_INT(CLS_TOKEN_ALGORITHMIC, 0);

	return;
}








