#ifndef HTTP_NGOD_TASK_H
#define HTTP_NGOD_TASK_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:httpngodtask.h
version  :
created  : 2013-2-12   11:52
file path: 
modify   :
author   : songchuangye
purpose  : ott点播处理
*******************************************************************************/
#include "libhttpserver/httptaskbase.h"
#include "libcls/clscommon.h"
#include <string>


class CHTTPNGODTask : public CHttpTaskBase
{
	CONF_DECLARE_DYNCREATE(CHTTPNGODTask)
public:

	CHTTPNGODTask();

	virtual ~CHTTPNGODTask();

protected:

	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);

protected:

	//处理Locate消息
	SInt32 ParseLocateMessage(const char* pszBuf, LocateMessge &locate);

	//获取需要的文件名
	std::string GetFileName(LocateMessge &locate);

	//填充http头
	void FillHttpHead(CHTTPResponse *rsp);

};



#endif


