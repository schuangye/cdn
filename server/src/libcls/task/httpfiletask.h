#ifndef HTTP_FILE_TASK_H
#define HTTP_FILE_TASK_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:httpfiletask.h
version  :
created  : 2013-2-12   11:52
file path: 
modify   :
author   : songchuangye
purpose  : ott点播处理
*******************************************************************************/
#include "task/httpvodtask.h"
#include <string>

class CHTTPFileTask : public CHTTPVodTask
{
	CONF_DECLARE_DYNCREATE(CHTTPFileTask)
public:

	CHTTPFileTask();

	virtual ~CHTTPFileTask();

protected:

	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);
};



#endif


