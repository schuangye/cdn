/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clshttpdefaulttask.cpp
*  Description:  cls默认http处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpserver/httpsessionmgr.h"
#include "libcore/RuntimeObj.h"
#include "libcore/log.h"
#include "task/clshttpdefaulttask.h"
#include "libclsstaticconf.h"

CONF_IMPLEMENT_DYNCREATE(CCLSHttpDefaultTask, CHttpTaskBase)
/*******************************************************************************
 Fuction name:CCLSHttpDefaultTask
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
********************************************************************************/
CCLSHttpDefaultTask::CCLSHttpDefaultTask()
{
}
/*******************************************************************************
 Fuction name:~CCLSHttpDefaultTask
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
********************************************************************************/
CCLSHttpDefaultTask::~CCLSHttpDefaultTask()
{
}
/*******************************************************************************
 Fuction name:DealWith
 Description :处理任务,优先调用应用自己处理返回函数，如果失败调用响应统一返回函数
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
********************************************************************************/
void CCLSHttpDefaultTask::OnDealWith(CHTTPRequest *req)
{
	if (req == NULL)
	{
		return;
	}

	//创建实际处理task
	CHttpTaskBase *task = CreateRealHttpTask(req);
	if (task == NULL)
	{
		LOG_ERROR("CreateRealHttpTask fail. url:"<<req->GetUrl())

		//移除回话
		CHTTPSessionMgr::Intstance()->RemoveHTTPSession(req->GetIndex());
		return;
	}

	//处理消息
	task->OnDealWith(req);

	delete task;
	task = NULL;

	return;
}
/*******************************************************************************
 Fuction name:CreateRealHttpTask
 Description :创建实际http对象
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
********************************************************************************/
CHttpTaskBase* CCLSHttpDefaultTask::CreateRealHttpTask(CHTTPRequest*req)
{
	if (req == NULL)
	{
		return NULL;
	}

	LibCore::CString url = req->GetUrl();
	if (url.find("/vmhls?") != LibCore::CString::npos)
	{
		return (CHttpTaskBase*)CreateRuntimeObjectFromClassName("CHTTPVmhlsTask");
	}
	else if (url.find("/live/") != LibCore::CString::npos)
	{
		return (CHttpTaskBase*)CreateRuntimeObjectFromClassName("CHTTPLiveTask");
	}
	else if (url.find("/vod/") != LibCore::CString::npos)
	{
		return (CHttpTaskBase*)CreateRuntimeObjectFromClassName("CHTTPVodTask");
	}
	else if (url.find("/vodadi") != LibCore::CString::npos)
	{
		return (CHttpTaskBase*)CreateRuntimeObjectFromClassName("CHTTPNGODTask");
	}
	else if (url.find("/file/") != LibCore::CString::npos)
	{
		return (CHttpTaskBase*)CreateRuntimeObjectFromClassName("CHTTPFileTask");
	}
	else
	{
		LOG_WARN("param error."<<url)
		return (CHttpTaskBase*)CreateRuntimeObjectFromClassName("CHttpDefaultTask");
	}

	return NULL;
}






