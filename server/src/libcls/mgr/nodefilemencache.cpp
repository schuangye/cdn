/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 内存文件分布管理
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/nodefilemencache.h"
#include "mgr/clsdbopreatermgr.h"
#include "mgr/nodemgr.h"
#include "libclsstaticconf.h"
/*******************************************************************************
*  Function   :CNodeFileMenCache
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CNodeFileMenCache::CNodeFileMenCache()
{
	m_LastFileCreateTime = 0;
    m_LastIncTime        = 0;
	m_LastAllTime        = 0;
}
/*******************************************************************************
*  Function   :~CNodeFileMenCache
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CNodeFileMenCache::~CNodeFileMenCache()
{
	//退出线程
	this->StopTask();

	m_Mutex.Lock();
	m_FileNodeMap.clear();
	m_Mutex.Unlock();

	LOG_DEBUG("CNodeFileMenCache exit.");
}
/*******************************************************************************
*  Function   : Init
*  Description:初始化
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CNodeFileMenCache::Init()
{
	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("CNodeFileMenCache run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetNodeStreamAddress
*  Description: 获取流地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CNodeFileMenCache::GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
	                                                Node &node, std::string &path, SInt32 useaddrtype)
{
	//组装key
	std::string skey = pid + "_" + aid;

	FileDistributeList  filedistrilist;

	//内存查找
	m_Mutex.Lock();
	FileNodeMap::iterator it = m_FileNodeMap.find(skey);
	if (it != m_FileNodeMap.end())
	{
		filedistrilist.insert(filedistrilist.end(), it->second.begin(), it->second.end());
	}
	m_Mutex.Unlock();

	//为空数据查找
	if (filedistrilist.empty())
	{
		//数据库查询
		QueryFileFromDB(pid, aid, filedistrilist);
	}

	std::string url = "";

	//取出最优节点
	node = CNodeMgr::Intstance()->GetNodeFromNodeList(filedistrilist, path);

	//获取需要的流地址
	std::string streamaddr = GetNeedStreamAddress(node, useaddrtype);
	if (path.empty() || streamaddr.empty())
	{
		LOG_ERROR("GetNodeFromNodeList failed."<<skey);
	}
	else
	{
		url = "http://" + streamaddr + path;
	}

	return url;
}
/*******************************************************************************
*  Function   : Run
*  Description:线程调用函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CNodeFileMenCache::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//全量同步文件
		RefreshAllAddFileDistribute();

		//增量同步文件
		RefreshIncAddFileDistribute();

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : RefreshAllFileDistribute
*  Description: 全量同步文件
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CNodeFileMenCache::RefreshAllAddFileDistribute()
{
	if (OS::Milliseconds() - m_LastAllTime < CLibCLSStaticConf::m_ClsAllSynInterval)
	{
		return;
	}

	FileDistributeList tempfiledistributelist;
	SInt32 ret = CLSCDbOperatermgr::Intstance()->QueryAllFileDistribute(tempfiledistributelist);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("Query all file distribute fail.ret:"<<ret)
		return;
	}

	//添加全量文件
	AllAddFileToMem(tempfiledistributelist);

	m_LastAllTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : RefreshIncAddFileDistribute
*  Description: 增量同步文件
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CNodeFileMenCache::RefreshIncAddFileDistribute()
{
	if (OS::Milliseconds() - m_LastIncTime < CLibCLSStaticConf::m_ClsIncSynInterval)
	{
		return;
	}

	FileDistributeList tempfiledistributelist;
	SInt32 ret = CLSCDbOperatermgr::Intstance()->QueryAllFileDistribute(tempfiledistributelist, m_LastFileCreateTime);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("Query all file distribute fail.ret:"<<ret)
		return;
	}

	//添加增量文件
	IncAddFileToMem(tempfiledistributelist);

	m_LastIncTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : QueryFileFromDB
*  Description: 从数据库查询
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CNodeFileMenCache::QueryFileFromDB(const std::string &pid, const std::string &aid, 
	                                    FileDistributeList &filedistrilist)
{
	FileDistributeList tempfiledistributelist;
	SInt32 ret = CLSCDbOperatermgr::Intstance()->QueryFileDistribute(pid, aid, tempfiledistributelist);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("Query all file distribute fail.ret:"<<ret)
		return;
	}

	//添加增量文件
	IncAddFileToMem(tempfiledistributelist);

	return;
}
/*******************************************************************************
*  Function   : IncAddFileToMem
*  Description: 增量添加数据到内存中
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CNodeFileMenCache::IncAddFileToMem(FileDistributeList &filedistributelist)
{
	FileNodeMap tempfilenodemap;
	FileDistributeList::iterator it = filedistributelist.begin();
	for ( ;it != filedistributelist.end(); it++)
	{
		FileDistribute tempFileDistribute = *it;

		//组装key
		std::string stempkey = tempFileDistribute.m_ProviderID + "_" + tempFileDistribute.m_AssetID;
		tempfilenodemap[stempkey].push_back(tempFileDistribute);
	}

	//获取最大创建时间
	if (!filedistributelist.empty())
	{
		m_LastFileCreateTime = filedistributelist.back().m_CreateTime;
	}

	m_Mutex.Lock();
	FileNodeMap::iterator itmap = tempfilenodemap.begin();
	for (; itmap != tempfilenodemap.end(); itmap++)
	{
		m_FileNodeMap[itmap->first].insert(m_FileNodeMap[itmap->first].end(), 
			                               itmap->second.begin(), itmap->second.end());
	}
	m_Mutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : AddDataToMem
*  Description: 全量添加数据到内存中
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CNodeFileMenCache::AllAddFileToMem(FileDistributeList &filedistributelist)
{
	FileNodeMap tempfilenodemap;

	FileDistributeList::iterator it = filedistributelist.begin();
	for ( ;it != filedistributelist.end(); it++)
	{
		FileDistribute tempFileDistribute = *it;
		//组装key
		std::string stempkey = tempFileDistribute.m_ProviderID + "_" + tempFileDistribute.m_AssetID;
		tempfilenodemap[stempkey].push_back(tempFileDistribute);
	}

	//获取最大创建时间
	if (!filedistributelist.empty())
	{
		m_LastFileCreateTime = filedistributelist.back().m_CreateTime;
	}

	m_Mutex.Lock();
	m_FileNodeMap.clear();
	m_FileNodeMap.insert(tempfilenodemap.begin(),tempfilenodemap.end());
	m_Mutex.Unlock();

	return;
}






