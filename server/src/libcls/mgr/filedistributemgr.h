#ifndef FILE_DISTRIBUTE_MGR_H
#define FILE_DISTRIBUTE_MGR_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 文件分布管理
*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include "mgr/nodefilecache.h"
#include "libcls/clscommon.h"

class CFileDistributeMgr
{
	DECLARATION_SINGLETON(CFileDistributeMgr)
public:

	//初始化
	SInt32 Init();

	//获取流地址
	std::string GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
		                             Node &node, std::string &path, SInt32 useaddrtype = CLS_USE_PRIVATEADDR_TYPE);

	//获取最优节点,获取负载最小CG,返回CG工作地址
	virtual std::string GetBestNodeStreamAddress(Node &node, SInt32 useaddrtype = CLS_USE_PRIVATEADDR_TYPE);

private:
	
	CNodeFileCache               *m_NodeFileCache;  //文件缓存实现
};



#endif



