#ifndef NODE_FILE_CACHE_H
#define NODE_FILE_CACHE_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: nodefilemencache.h
modify   :
author   : songchuangye
purpose  : 文件分布基类
*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcls/libclsmacros.h"
#include "libcls/clscommon.h"

class CNodeFileCache
{
public:

	CNodeFileCache();

	virtual ~CNodeFileCache();

	//初始化
	virtual SInt32 Init(); 

	//获取流地址
	virtual std::string  GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
		                                       Node &node, std::string &path, SInt32 useaddrtype = CLS_USE_PRIVATEADDR_TYPE);

protected:

	//根据类型获取需要的流地址
	std::string  GetNeedStreamAddress(const Node &node, SInt32 useaddrtype);
};




#endif



