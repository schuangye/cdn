/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cgcachefilemgr.cpp
*  Description:  cg 缓存文件管理器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libredis/redisstringcommand.h"
#include "libredis/redissetdataobject.h"
#include "libredis/redissetcommand.h"
#include "libredis/redisservermgr.h"
#include "libcdnutil/libcdnutilmacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "mgr/nodefilerediscache.h"
#include "mgr/nodemgr.h"
#include "libclsstaticconf.h"
/*******************************************************************************
*  Function   : CNodeFileRedisCache
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CNodeFileRedisCache::CNodeFileRedisCache()
{
	m_LastTime = 0;
}
/*******************************************************************************
*  Function   : ~CNodeFileRedisCache
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CNodeFileRedisCache::~CNodeFileRedisCache()
{
	this->StopTask();
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化文件缓存管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CNodeFileRedisCache::Init()
{
	if (!this->RunTask())
	{
		ERROR_LOG("CNodeFileMenCache run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetNodeStreamAddress
*  Description: 获取流地址
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
std::string  CNodeFileRedisCache::GetNodeStreamAddress(const std::string &pid, 
													   const std::string &aid, 
	                                                   Node &node, std::string &path,
													   SInt32 useaddrtype)
{
	//组装key
	std::string skey = pid + "_" + aid;

	RedisFileDistributeVec nodepathvec;
    Int32 ret = GetFileNodeDistribute(skey.c_str(), nodepathvec);
	if (ret != CDN_ERR_SUCCESS)
	{
		return "";
	}

	std::string url = "";

	//取出最优节点
	node = CNodeMgr::Intstance()->GetNodeFromNodeVector(nodepathvec, path);

	//获取需要的流地址
	std::string streamaddr = GetNeedStreamAddress(node, useaddrtype);
	if (path.empty() || streamaddr.empty())
	{
		LOG_ERROR("GetNodeFromNodeVector failed");
	}
	else
	{
		url = "http://" + streamaddr + path;
	}

	return url;
}
/*******************************************************************************
*  Function   : Run
*  Description: OSTask的执行函数, 执行具体的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CNodeFileRedisCache::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//清除缓存
		CleanDataFromMem();

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : CleanCache
*  Description: 清除缓存数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CNodeFileRedisCache::CleanDataFromMem()
{
	if (OS::Milliseconds() - m_LastTime < CLibCLSStaticConf::m_ClsAllSynInterval)
	{
		return;
	}

	m_FileInfoMutex.Lock();
	m_FileInfoMap.clear();
	m_FileInfoMutex.Unlock();

	m_DistributeMutex.Lock();
	m_FileDistributeMap.clear();
	m_DistributeMutex.Unlock();

	m_LastTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : GetFileCGNodeDistribute
*  Description: 获取文件分布
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::GetFileNodeDistribute(const char *filename, RedisFileDistributeVec &nodepathvec)
{
	if (filename == NULL)
	{
		return -1;
	}

	//缓存获取分布信息
	m_DistributeMutex.Lock();
	FileDistributeMap::iterator it = m_FileDistributeMap.find(std::string(filename));
	if (it != m_FileDistributeMap.end())
	{
		nodepathvec.insert(nodepathvec.end(), it->second.begin(), it->second.end());
	}
	m_DistributeMutex.Unlock();

	//从redis获取分布信息
	if (nodepathvec.empty())
	{
		Int32 ret = GetFileDistributeFromRedis(filename, nodepathvec);
		if (ret == CDN_ERR_SUCCESS)
		{
			AddFileNodePathToMem(filename, nodepathvec);
		}
		else
		{
			ERROR_LOG("get file from redis fail."<<filename<<";ret:"<<ret)
			return -2;
		}
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CCGNodeServerMgr
*  Description: 获取文件信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::GetFileInfo(const char *filename, RedisFileInfo &fileinf)
{
	if (filename == NULL)
	{
		return -1;
	}

	//内存中查询文件信息
	m_FileInfoMutex.Lock(); 
	RedisFileInfoMap::iterator it = m_FileInfoMap.find(std::string(filename));
	if (it != m_FileInfoMap.end())
	{
		fileinf = it->second;
	}
	m_FileInfoMutex.Unlock();

	if (strlen(fileinf.m_FileName) == 0)
	{
		//从redis获取文件信息
		Int32 ret = GetFileInfoFromRedis(filename, fileinf);
		if (ret == CDN_ERR_SUCCESS)
		{
			AddFileInfoToMem(fileinf);
		}
		else
		{
			ERROR_LOG("get file from redis fail."<<filename<<";ret:"<<ret)
			return -2;
		}
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetFileInfoAndNodeDistribute
*  Description: 获取文件信息和文件分布
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::GetFileInfoAndNodeDistribute(const char *filename, RedisFileInfo &fileinf, 
								                         RedisFileDistributeVec &nodepathvec)
{
	if (filename == NULL)
	{
		return -1;
	}

	//优先从缓存找
	Int32 ret = GetFileInfoAndNodeFromMem(filename, fileinf, nodepathvec);
	if (ret != 0)
	{
		//从数据redis读取数据
		ret = GetFileInfoAndNodeFromRedis(filename, fileinf, nodepathvec);
		if (ret == 0)
		{
			//添加数据到缓存
			ret = AddFileInfoAndNodeToMem(fileinf, nodepathvec);
		}
	}

	return ret;
}
/*******************************************************************************
*  Function   : GetFileInfoAndNodeFromMem
*  Description: 从缓存获取文件分布，以及文件信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::GetFileInfoAndNodeFromMem(const char *filename, RedisFileInfo &file, 
	                                                 RedisFileDistributeVec &distnodelist)
{
	Int32 ret = CDN_ERR_SUCCESS;

	//获取文件信息
	m_FileInfoMutex.Lock();
	RedisFileInfoMap::iterator it = m_FileInfoMap.find(std::string(filename));
	if (it != m_FileInfoMap.end())
	{
		file = it->second;
	}
	else
	{
		ret = -1;
	}
	m_FileInfoMutex.Unlock();

	if (ret == CDN_ERR_SUCCESS)
	{
		//获取分布节点
		m_DistributeMutex.Lock();
		FileDistributeMap::iterator it1 = m_FileDistributeMap.find(std::string(filename));
		if (it1 != m_FileDistributeMap.end())
		{
			distnodelist.insert(distnodelist.end(), it1->second.begin(), it1->second.end());
		}
		else
		{
			ret = -2;
		}
		m_DistributeMutex.Unlock();
	}

	return ret;
}
/*******************************************************************************
*  Function   : GetFileInfoAndNodeFromRedis
*  Description: 从redis获取文件分布，以及文件信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::GetFileInfoAndNodeFromRedis(const char *filename, RedisFileInfo  &file, 
	                                                    RedisFileDistributeVec &distnodelist)
{
	//获取文件信息
	Int32 ret = GetFileInfoFromRedis(filename, file);
	if (ret != 0)
	{
		return -1;
	}

	//获取节点分布信息
	ret = GetFileDistributeFromRedis(filename, distnodelist);
	if (ret != 0)
	{
		return -2;
	}

	return 0;
}
/*******************************************************************************
*  Function   : GetFileInfoFromRedis
*  Description: 获取文件信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::GetFileInfoFromRedis(const char *filename, RedisFileInfo  &file)
{
	if (filename == NULL)
	{
		return -1;
	}

	CRedisFileInfDataObject fileinfobject;

	CRedisStringCommand  cmd;
	cmd.SetStringCmdType(REDIS_STRING_GET);
	cmd.SetKey(filename);

	//获取文件分布
	Int32 ret = CRedisServerMgr::Intstance()->ExecuteRedisCmd(&cmd, &fileinfobject);
	if (ret != 0)
	{
		ERROR_LOG("get file info fail. ret:"<<ret)
		return -2;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetFileDistributeFromRedis
*  Description: 查询文件分布
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::GetFileDistributeFromRedis(const char *filename, RedisFileDistributeVec &distnodelist)
{
	if (filename == NULL)
	{
		return -1;
	}

	CRedisSetCommand cmd;
	cmd.SetSetCmdType(REDIS_SET_MEMBERS);

	char key[128] = {0};
	sprintf(key, "%s%s", FILE_DISTRIBUTE_SET, filename);
	cmd.SetKey(key);

	CRedisSetDataObject object;

	//执行redis命令
	Int32 ret = CRedisServerMgr::Intstance()->ExecuteRedisCmd(&cmd, &object);
	if (ret != 0)
	{
		ERROR_LOG("get dist node fail. ret:"<<ret)
		return -2;
	}

	const char *tmp = object.Get();
	while(tmp != NULL)
	{
		std::vector<std::string > line;
		STR::ReadLinesExt(tmp, line, "$");
		if (line.size() == 2)
		{
			RedisFileDistribute  nodepath;
			memcpy(nodepath.m_NodeID, line[0].c_str(), line[0].length());
			memcpy(nodepath.m_Path, line[1].c_str(), line[1].length());

			distnodelist.push_back(nodepath);
		}

		tmp = object.Get();
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : AddFileInfoAndNodeToMem
*  Description: 添加文件到缓存
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::AddFileInfoAndNodeToMem(const RedisFileInfo  &file, 
												   const RedisFileDistributeVec &distnodelist)
{
	//添加文件到缓存
	m_FileInfoMutex.Lock();
	m_FileInfoMap[std::string(file.m_FileName)] = file;
	m_FileInfoMutex.Unlock();


	//添加文件分布
	m_DistributeMutex.Lock();
	FileDistributeMap::iterator it = m_FileDistributeMap.find(std::string(file.m_FileName));
	if (it != m_FileDistributeMap.end())
	{
		it->second.clear();
		it->second.insert(it->second.end(), distnodelist.begin(), distnodelist.end());
	}
	else
	{
		m_FileDistributeMap[std::string(file.m_FileName)].insert(m_FileDistributeMap[std::string(file.m_FileName)].end(),
			                                                     distnodelist.begin(), distnodelist.end());
	}
	m_DistributeMutex.Unlock();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : AddFileInfoToMem
*  Description: 添加文件到缓存
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::AddFileInfoToMem(const RedisFileInfo  &file)
{
	if (strlen(file.m_FileName) == 0)
	{
		return -1;
	}

	//添加文件到缓存
	m_FileInfoMutex.Lock();
	m_FileInfoMap[std::string(file.m_FileName)] = file;
	m_FileInfoMutex.Unlock();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : AddFileNodePathToMem
*  Description: 添加节点路径到缓存
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CNodeFileRedisCache::AddFileNodePathToMem(const char *filename, const RedisFileDistributeVec &distnodelist)
{
	if (filename == NULL || strlen(filename) == 0)
	{
		return -1;
	}

	std::string tmpfilename = std::string(filename);

	//添加文件分布
	m_DistributeMutex.Lock();
	FileDistributeMap::iterator it = m_FileDistributeMap.find(tmpfilename);
	if (it != m_FileDistributeMap.end())
	{
		it->second.clear();
		it->second.insert(it->second.end(), distnodelist.begin(), distnodelist.end());
	}
	else
	{
		m_FileDistributeMap[tmpfilename].insert(m_FileDistributeMap[tmpfilename].end(),
			                                    distnodelist.begin(), distnodelist.end());
	}
	m_DistributeMutex.Unlock();

	return CDN_ERR_SUCCESS;
}


