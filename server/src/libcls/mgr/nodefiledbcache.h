#ifndef NODE_FILE_DB_CACHE_H
#define NODE_FILE_DB_CACHE_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: nodefiledbcache.h
modify   :
author   : songchuangye
purpose  : 文件分布管理
*******************************************************************************/
#include "libcore/OSThread.h"
#include "mgr/nodefilecache.h"
#include "libcls/clscommon.h"

class CNodeFileDBCache :public CNodeFileCache
{
public:

	CNodeFileDBCache();

	virtual ~CNodeFileDBCache();

	//初始化
	virtual SInt32 Init();

	//获取流地址
	virtual std::string  GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
		                                      Node &node, std::string &path, SInt32 useaddrtype = CLS_USE_PRIVATEADDR_TYPE);
};



#endif



