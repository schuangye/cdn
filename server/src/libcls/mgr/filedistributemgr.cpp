/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 文件分布管理
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcls/libclsmacros.h"
#include "mgr/filedistributemgr.h"
#include "mgr/nodefilemencache.h"
#include "mgr/nodefiledbcache.h"
#include "mgr/nodefilerediscache.h"
#include "mgr/nodemgr.h"
#include "libclsstaticconf.h"

IMPLEMENT_SINGLETON(CFileDistributeMgr)
/*******************************************************************************
*  Function   :CFileDistributeMgr
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CFileDistributeMgr::CFileDistributeMgr()
{
	m_NodeFileCache = NULL;
}
/*******************************************************************************
*  Function   :~CFileDistributeMgr
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CFileDistributeMgr::~CFileDistributeMgr()
{

	if (m_NodeFileCache != NULL)
	{
		delete m_NodeFileCache;
		m_NodeFileCache = NULL;
	}

	LOG_DEBUG("CFileDistributeMgr exit.");
}
/*******************************************************************************
*  Function   : Init
*  Description:初始化
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CFileDistributeMgr::Init()
{
	if (m_NodeFileCache != NULL)
	{
		delete m_NodeFileCache;
		m_NodeFileCache = NULL;
	}

	switch(CLibCLSStaticConf::m_ClsCacheType)
	{
	case CLS_CACHE_TYPE_DB:
		{
			m_NodeFileCache = new CNodeFileDBCache();
			break;
		}
	case CLS_CACHE_TYPE_MEN:
		{
			m_NodeFileCache = new CNodeFileMenCache();
			break;
		}
	case CLS_CACHE_TYPE_REDIS:
		{
			m_NodeFileCache = new CNodeFileRedisCache();
			break;
		}
	default:
		{
			ERROR_LOG("no support cache type. "<<CLibCLSStaticConf::m_ClsCacheType)
		}
	}

	SInt32 ret = -1;
	if (m_NodeFileCache != NULL)
	{
		ret = m_NodeFileCache->Init();
	}

	return ret;
}
/*******************************************************************************
*  Function   : GetNodeStreamAddress
*  Description:获取流地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CFileDistributeMgr::GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
	                                                 Node &node, std::string &path, SInt32 useaddrtype)
{
	if (m_NodeFileCache == NULL)
	{
		return "";
	}

	return m_NodeFileCache->GetNodeStreamAddress(pid, aid, node, path, useaddrtype);
}
/*******************************************************************************
*  Function   : GetBestNodeStreamAddress
*  Description:获取最优节点,获取负载最小CG,返回CG工作地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CFileDistributeMgr::GetBestNodeStreamAddress(Node &node, SInt32 useaddrtype)
{
	return CNodeMgr::Intstance()->GetBestNodeStreamAddress(node, useaddrtype);
}






