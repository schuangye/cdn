/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsstaicparammgr.h
*  Description:  cls静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libhttprti/libhttprtimacros.h"
#include "libhttprtistaticconf.h"

//http下载失败重新连接时间间隔
SInt32 CLibHTTPRTIStaticConf::m_HttpRtiReconnectTime = DEFAULT_RECONNECT_TIME;

IMPLEMENT_SINGLETON(CLibHTTPRTIStaticConf)
/*******************************************************************************
*  Function   : CHLSRTIStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibHTTPRTIStaticConf::CLibHTTPRTIStaticConf()
{
}
/*******************************************************************************
*  Function   : CHLSRTIStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibHTTPRTIStaticConf::~CLibHTTPRTIStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibHTTPRTIStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibHTTPRTIStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibHTTPRTIStaticConf::SynStaticParam()
{
	//http下载失败重新连接时间间隔
	CLibHTTPRTIStaticConf::m_HttpRtiReconnectTime = GET_PARAM_INT(HTTP_RTI_RECONNECT_TIME, DEFAULT_RECONNECT_TIME);
	return;
}









