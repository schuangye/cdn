/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    livehlsdownload.h
*  Description:  hls客户端下载
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OS.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libcore/errcodemacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/livehlsdownload.h"
#include "libhlsrtistaticconf.h"
/*******************************************************************************
*  Function   : CLiveHLSDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CLiveHLSDownload::CLiveHLSDownload()
{
	m_LastTime = 0;
	m_CurSeq   = 0;
}
/*******************************************************************************
*  Function   : CLiveHLSDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CLiveHLSDownload::~CLiveHLSDownload()
{
	//停止线程
	this->StopTask();
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化url
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CLiveHLSDownload::Init(const char *url, const char *storepath, SInt32 splitdur)
{
	if (url == NULL || storepath == NULL)
	{
		return -1;
	}

	//初始化m3u8文件
	Int32 ret = CHLSDownload::Init(url, CLibHLSRTIStaticConf::m_HlsDataBuffSize, 
		                           CLibHLSRTIStaticConf::m_Hlsm3u8BuffSize,
								   CLibHLSRTIStaticConf::m_HlsOneceDownLoadNum,
								   CLibHLSRTIStaticConf::m_HlsDownLoadTimeOut);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("init hls down load fail.ret:"<<ret)
		return -2;
	}

	m_SplitDur  = splitdur;
	if (m_SplitDur <= 0)
	{
		m_SplitDur = 10;
	}
	
	m_StorePath = std::string(storepath);

	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("run task fail")
		return -3;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:Run
 Description :线程调用函数
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Bool CLiveHLSDownload::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//刷新直播数据
		RefreshLive();
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SaveMediaData
*  Description: 保存下载数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CLiveHLSDownload::SaveMediaData(const char *buff, SInt32 len)
{
	if (buff == NULL || len < 0)
	{
		return -1;
	}

	SInt64 tmpseq = OS::Seconds()/m_SplitDur;
	if (m_CurSeq == 0)
	{
		m_CurSeq = tmpseq;
	}
	else
	{
		m_CurSeq++;
		if ((m_CurSeq > tmpseq && m_CurSeq - tmpseq > 3) || 
			(m_CurSeq < tmpseq && tmpseq - m_CurSeq > 3))
		{
			m_CurSeq = tmpseq;
		}
	}

	//打开文件保存数据
	std::string filename = OS::AddEndSymbolToPath(m_StorePath) + STR::SInt64ToStr(m_CurSeq) + ".ts";
	FILE *file = fopen(filename.c_str(), "wb+");
	if (file != NULL)
	{
		SInt32 ret = fwrite(buff, 1, len, file);
		if (ret <= 0)
		{
			LOG_ERROR("write down load m3u8 data fail. ret:"<<ret<<";"<<filename)
			m_CurSeq--;
			fclose(file);

			//删除写失败文件
			OS::DelFile(filename.c_str());

			return -2;
		}
	}

	//关闭文件
	fflush(file);
	fclose(file);
	
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : RefreshLiveM3U8
*  Description: 刷新m3u8数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CLiveHLSDownload::RefreshLive()
{
	if (OS::Seconds() - m_LastTime < CLibHLSRTIStaticConf::m_HlsRefreshTime)
	{
		return;
	}

	//开始下载
	Int32 ret = StartDownLoad(CLibHLSRTIStaticConf::m_HlsSubM3U8Index);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("start down load fail. ret:"<<ret)
	}

	m_LastTime = OS::Seconds();

	return;
}











