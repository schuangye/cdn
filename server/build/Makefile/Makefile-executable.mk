##################################################################################
# 
# 
# 通用静态编译输出
#
# 
##################################################################################
# patsubst把$(SRC)中的符合后缀是.cpp的全部替换成.o
OBJS = $(patsubst %.cpp,%.o, $(patsubst %.c,%.o,$(SRCS)))

#连接
$(TARGET): $(OBJS)
	$(C++PLUS) $(DEBUG) -fPIC $(LINKS) -o $(TARGET) $(OBJS) $(LFLAGS)
	mv $(TARGET) $(LIBPATH)

#编译
%.o : %.cpp
	$(C++PLUS) $(DEBUG) -fPIC -c $< $(CFLAGS) $(INC) -o $@
	
%.o : %.c
	$(CC) $(DEBUG) -fPIC -c $< $(CFLAGS) $(INC) -o $@
	



