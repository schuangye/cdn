#!/bin/sh
#*************************************************************************
# Copyright (c) 2011-2030, songchuangye
#
# Author	: 
# 修改    : scy
# Date		: 2014-07-08 #
#*************************************************************************
##############定义打印#####################
ECHO_RED()
{
    echo -e "\033[41;37m$1 \033[0m"
}
ECHO_YELLOW()
{
    echo -e "\033[43;30m$1 \033[0m"
}
ECHO_BLUE()
{
    echo -e "\033[44;37m$1 \033[0m"
}
#######################执行配置参数脚本##################################
if [ -f envsetup_linux.sh ]; then
   chmod +x envsetup_linux.sh
	 . envsetup_linux.sh
	 if [ "$CXX" = "" ];then
		ECHO_RED "setup compile fail."
		exit 2
	 fi
fi
#########################定义全局参数##################################
build_path=$(cd "$(dirname "$0")"; pwd)
ECHO_BLUE $build_path

project_path=$build_path/../
ECHO_BLUE $project_path

AppName=cdn

usage="Usage: $0 [-re]"
#########################获取版本号##################################
version=$(sed -n '1p' ../version.txt)
echo "##############################"
echo "#########"$version"###########"
echo "##############################"
##############获取系统时间##################################
myDate=$(date +%Y%m%d%H%M%S)
tar_pkg_name=${AppName}_$version\_release_$myDate.tar.gz
install_sdk_sh=install_linux_release.sh
##################################解压三方库############################
extract_sdk()
{
    if [ $need_clean -eq 1 ] ; then
		cd $project_path/sdk
		rm -rf ../bin/*
		rm -rf ../lib/*
		sh $install_sdk_sh
    fi
}
##################################编译模块###############################
build_module()
{
    cd $project_path/src/$1
    
    #清理工程
    if [ $need_clean -eq 1 ] ; then
        make -f Makefile.linux clean --include-dir=$build_path/Makefile MNAME=$1 MEXTNAME=$2 $DEBUG CXX=$CXX CC=$CC AR=$AR
    fi
    
    #编译工程
    make -f Makefile.linux --include-dir=$build_path/Makefile MNAME=$1 MEXTNAME=$2 $DEBUG CXX=$CXX CC=$CC AR=$AR
    if [ $? -ne 0 ];then
        ECHO_RED "build $module_name failed, exit"
        exit 1
    fi
}
#############################初始化编译参数###############################
need_clean=0
DEBUG="DEBUG="
for i in $(echo $@ | tr ' ' '\n' | sort | uniq)
	do
	    case $i in
	        -re)
	            need_clean=1
	        ;;
	        -debug)
	            DEBUG="DEBUG=-g"
	            tar_pkg_name=${AppName}_$version\_debug_$myDate.tar.gz
	            install_sdk_sh=install_linux_debug.sh
	        ;;
	        *)
	            echo "para must be -re or -debug"
	    esac
done
echo "$need_clean $DEBUG"
#############################初始化编译参数结束#############################
#解压三方库
extract_sdk

######################循环模块#######################################  
cat $build_path/module/module_linux.txt | while read modulename
do
  libname=`echo $modulename | cut -d \  -f 1`
	extname=`echo $modulename | cut -d \  -f 2`
	ECHO_BLUE "########### compile $libname#############"
	if [ "$libname" == "$extname" ];then
		extname=
	fi
  build_module $libname $extname 
done

#判断是否编译失败
if [ $? -ne 0 ];then
   ECHO_RED "build  failed, exit"
   exit 1
fi

#打包目录
cd $build_path
sh packge.sh $tar_pkg_name $version ${AppName}
if [ $? -ne 0 ];then
    ECHO_RED "packege failed, exit"
    exit 1
fi
ECHO_BLUE "All modules build sucess !!!"
