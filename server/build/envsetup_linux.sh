#*************************************************************************************
# Copyright (c) 2011-2030, Shenzhen Coship Electronics Co., Ltd, All rights reserved.
#
# Author	: 
# 修改    : scy 907818
# Date		: 2014-07-08 #
#*************************************************************************************
#定义编译器
CXX=g++
CC=gcc
AR=ar

#打印变量
echo "*******************************************************************************"
echo CXX=$CXX
echo CC=$CC
echo AR=$AR
echo "*******************************************************************************"