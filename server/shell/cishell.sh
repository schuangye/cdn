#/bin/sh
#保存工作路径
srcpath=`pwd`

#进入脚本路径
cd `dirname $0`

#进入直播工具目录
cd vm_hls
chmod +x ./*
#$1:输出收流地址;$2:切片时长;$3:存储地址;$4:切片保留个数
./create_vmhls_vod.sh $1 $2

#返回工作目录
cd $srcpath

