@rem *************************************************
@rem
@rem     安装win sdk库
@rem
@rem *************************************************

@rem 解压库
mkdir tmp
.\\7-Zip\\7za.exe x sdk_win*.zip  -y -aos -o".\\tmp"

@rem 复制bin目录文件
xcopy /S/E/Y .\\tmp\\bin  ..\\bin

@rem 复制lib目录文件
xcopy /S/E/Y .\\tmp\\lib  ..\\lib 

@rem 复制pdb目录文件
xcopy /S/E/Y .\\tmp\\pdb  ..\\pdb

@rem 复制conf目录文件
xcopy /S/E/Y .\\tmp\\conf  ..\\conf

@rem 复制include目录文件
rmdir /S/Q ..\\include\\inc3
xcopy /S/E/Y/I .\\tmp\\include  ..\\include\\inc3

@rem 删除临时目录
rmdir /S/Q .\\tmp
